<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs;

use Beibob\Blibs\Interfaces\Viewable;

/**
 * A JsonView class
 *
 * @package blibs
 * @author Tobias Lode <tobias@beibob.de>
 * @author Fabian Möller <fab@beibob.de>
 *
 */
class JsonView extends View
{
    /**
     * Views to be encoded
     */
    private $views = [];

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the content as string
     *
     * @param  -
     * @return string
     */
    public function __toString()
    {
        return json_encode($this->getData());
    }
    // End __toString

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Assigns a variable to the template for substitution
     *
     * @param  string $key
     * @param  mixed $value
     * @return -
     */
    public function assignView(Viewable $View, $viewName = null)
    {
        return $this->views[$viewName? $viewName : get_class($View)] = $View;
    }
    // End assign

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns an assigned View
     *
     * @param  string $viewname
     * @return Viewable
     */
    public function getView($name = null)
    {
        if(is_null($name))
            return reset($this->views);

        return isset($this->views[$name])? $this->views[$name] : null;
    }
    // End getView

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns all assigned Views
     *
     * @param  -
     * @return array
     */
    public function getViews()
    {
        return $this->views;
    }
    // End getViews

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns true if a view was assigned
     *
     * @param  string $viewname
     * @return Viewable
     */
    public function hasView($name)
    {
        return isset($this->views[$name]);
    }
    // End hasView

    //////////////////////////////////////////////////////////////////////////////////////
    // protected
    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Renders the view
     *
     * @param  -
     * @return -
     */
    protected function render()
    {
        /**
         * If other views were assigned, render them too
         */
        foreach($this->views as $name => $View)
        {
            $View->process($this->getData(), $this->getModule());

            if($content = trim((string)$View))
                $this->assign($name, $content);
        }
    }
    // End render

    //////////////////////////////////////////////////////////////////////////////////////
}
// End JsonView
