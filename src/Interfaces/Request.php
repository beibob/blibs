<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs\Interfaces;

/**
 * Request interface
 *
 * @package blibs
 * @interface  Request
 * @author Tobias Lode <tobias@beibob.de>
 * @author     Fabian M�ller <fab@beibob.de>
 *
 */
interface Request extends \Iterator
{
    /**
     * Returns the request URI
     *
     * @return  string
     */
    public function getUri();

    /**
     * Returns a value from the request by key
     *
     * @param  string  $key   - request key
     * @param  boolean $asIs  - return raw as it is
     * @return mixed
     */
    public function get($key);

    /**
     * Returns a numeric value from the request by key
     * or false if the value isn't numeric
     *
     * @param  string $key
     * @return mixed
     */
    public function getNumeric($key);

    /**
     * Returns an array from the request by key
     * or an empty array if the value does not exist
     *
     * @param  string $key
     * @return array
     */
    public function getArray($key);

    /**
     * Returns the request as array
     *
     * @param  -
     * @return array
     */
    public function getAsArray();

    /**
     * Magic method __isset
     *
     * @param  string $key
     * @return boolean
     */
    public function __isset($key);

    /**
     * Magic method __get
     *
     * @param  string $key
     * @return mixed
     */
    public function __get($key);

    /**
     * Magic method __sleep
     *
     * @param  -
     * @return array
     */
    public function __sleep();
}
// End Request
