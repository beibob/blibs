<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs\Interfaces;

use \Exception;
/**
 * Repsonse interface
 *
 * @package blibs
 * @interface  Response
 * @author Tobias Lode <tobias@beibob.de>
 * @author     Fabian M�ller <fab@beibob.de>
 *
 */
interface Response
{
    /**
     * Sets the status of the response
     *
     * @param  mixed $status
     * @return -
     */
    public function setStatus($status);

    /**
     * Returns the content
     *
     * @param  -
     * @return string
     */
    public function getContent();

    /**
     * Sets content at once
     *
     * @param  string $content
     * @return -
     */
    public function setContent($content);

    /**
     * Appends content
     *
     * @param  string $content
     * @return -
     */
    public function appendContent($content);

    /**
     * Adds an exception to the response
     *
     * @param  Exception $Exception
     * @return -
     */
    public function addException(\Exception $Exception);

    /**
     * Returns true if an exception was catched
     *
     * @param  -
     * @return boolean
     */
    public function hasExceptions();

    /**
     * Returns the exception array
     *
     * @return Exception[]|array
     */
    public function getExceptions();

    /**
     * Sends the data to the client
     *
     * @param  -
     * @return -
     */
    public function send();
}
// End Response
