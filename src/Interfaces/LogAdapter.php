<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs\Interfaces;

use Beibob\Blibs\Config;

/**
 * Log adapter interface
 *
 * @package blibs
 * @author Tobias Lode <tobias@beibob.de>
 *
 */
interface LogAdapter
{
    /**
     * Constructor
     *
     * @param  Config $Config
     * @return LogAdapter
     */
    public function __construct(Config $Config);

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Logs a variable
     *
     * @param  int $severity - one of LOG::XXX
     * @param  mixed $issue  - what to log
     * @param  string $label - additional label
     * @return -
     */
    public function log($severity, $issue, $label = null);

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Registers the Log instance
     *
     * @param string $name
     * @param Log $Log
     * @return Log
     */
    //public function registerLogger($name, Log $Log);
}
// End interface LogAdapter
