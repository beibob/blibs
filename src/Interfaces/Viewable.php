<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs\Interfaces;

/**
 * A Viewable interface
 *
 * @package blibs
 * @interface  Viewable
 * @author Tobias Lode <tobias@beibob.de>
 * @author     Fabian M�ller <fab@beibob.de>
 *
 */
interface Viewable
{
    /**
     * Assigns a variable to the template for substitution
     *
     * @param  string $key
     * @param  mixed $value
     * @return -
     */
    public function assign($key, $value = true);

    /**
     * Returns an assigned substitution variable
     *
     * @param  string $key
     * @return mixed
     */
    public function get($key);

    /**
     * Returns true if an assignment for key exists
     *
     * @param  string $key
     * @return boolean
     */
    public function has($key);

    /**
     * Returns the content as string
     *
     * @param  -
     * @return string
     */
    public function __toString();

    /**
     * Processes the view
     *
     * Calls all necessary methods and hooks in the proper order to load,
     * initialize and render this view.
     *
     * @param  array $args
     * @return -
     */
    public function process(array $args = [], $module = null);

    /**
     * Returns the input encoding
     *
     * @param  -
     * @return string
     */
    public function getInputEncoding();

    /**
     * Returns the output encoding
     *
     * @param  -
     * @return string
     */
    public function getOutputEncoding();
}
// End Viewable
