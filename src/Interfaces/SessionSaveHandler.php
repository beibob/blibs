<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs\Interfaces;

use Beibob\Blibs\Session;

/**
 * Handles saving session data
 *
 * @package blibs
 * @interface  SessionSaveHandler
 * @author Tobias Lode <tobias@beibob.de>
 * @author     Fabian M�ller <fab@beibob.de>
 *
 */
interface SessionSaveHandler
{
    /**
     * Registers the Session
     *
     * @param  Session $Session
     * @return -
     */
    public function __construct(Session $Session);

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Opens a session
     *
     * @param  string  $savePath
     * @param  integer $sessionName
     * @return boolean
     */
    public function open($savePath, $sessionName);

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Closes a session
     *
     * @param  -
     * @return boolean
     */
    public function close();

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Reads session data
     *
     * @param  string $sid
     * @param  int    $expires
     * @return array
     */
    public function read($sid);

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Writes session data
     *
     * @param  string $sid
     * @param  array $data
     * @return boolean
     */
    public function write($sid, $data);

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Destroys a session
     *
     * @param  0
     * @return boolean
     */
    public function destroy($sid);

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Garbage collector method
     *
     * @param  int $maxLifeTime
     * @return -
     */
    public function gc($maxLifeTime);
}
// End interface SessionHandler
