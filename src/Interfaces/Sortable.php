<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs\Interfaces;

/**
 * INTERFACE Sortable
 *
 * Sortable ermoeglicht die Sortierung eines Elements innerhalb einer Liste
 * Das Interface stellt die beiden Methoden zum Setzen und Lesen der Ordnungszahl
 * des zu sortierenden Elements. Die Methoden muessen positiven Ganzzahl-Wert
 * setzen bzw. zurueckgeben.
 *
 * @package blibs
 *
 */
interface Sortable
{
    /**
     * Gibt eine positive, ganzzahlige Ordnungszahl zurueck
     *
     * @return integer $ordinalNumber
     */
    public function getOrdinalNumber();

    ///////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Setzt eine positive, ganzzahlige Ordnungszahl
     *
     * @param integer $ordinalNumber
     */
    public function setOrdinalNumber($ordinalNumber);

    ///////////////////////////////////////////////////////////////////////////////////////////
}
// End Sortable
