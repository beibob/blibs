<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian MÃ¶ller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs\Interfaces;

/**
 * Router interface
 *
 * @package blibs
 * @author Tobias Lode <tobias@beibob.de>
 * @author Fabian Moeller <fab@beibob.de>
 * @copyright 2013 BEIBOB GbR
 */
interface Router
{
    /**
     * Constructor
     *
     * @param  FrontController $FrontController
     * @return Router
     */
    //public function __construct(FrontController $FrontController);

    /**
     * Resolves and returns the name of the invoked ActionController,
     * the action and the query string by the given Request
     *
     * @param Request $Request
     * @return array(string, string, array)
     */
    public function resolve(Request $Request);

    /**
     * Build the url to the given name of an ActionController,
     * an optional action and optional a query string
     *
     * @param string  $actionControllerName
     * @param string  $action
     * @param array   $query
     * @return string
     */
    public function getUrlTo($actionControllerName, $action = null, array $query = null);

    /**
     * Returns the last resolved name of an action controller
     *
     * @return string
     */
    public function getActionController();

    /**
     * Returns the last resolved action
     *
     * @return string
     */
    public function getAction();

    /**
     * Returns the last resolved request query
     *
     * @return array
     */
    public function getQuery();
}
// End Router