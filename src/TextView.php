<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs;

/**
 * A simple TextView
 *
 * @package blibs
 * @author Tobias Lode <tobias@beibob.de>
 * @author Fabian Möller <fab@beibob.de>
 *
 */
class TextView extends View
{
    /**
     * Template name
     */
    private $tplName;

    /**
     * Content
     */
    private $content = '';

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Constructor
     *
     * @param  string $tplName
     * @param  mixed  $module
     * @return -
     */
    public function __construct($tplName = false, $module = false)
    {
        if($tplName)
            $this->setTplName($tplName, $module);
    }
    // End __construct

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the template name and module
     */
    public function setTplName($tplName, $module = null)
    {
        $this->tplName = $tplName;

        if(!is_null($module))
            $this->setModule($module);
    }
    // End setTplName

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the content
     *
     * @param  string
     * @return -
     */
    public function setContent($content)
    {
        $this->content = $content;
    }
    // End setContent

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Appends content
     *
     * @param  string
     * @return -
     */
    public function appendContent($content)
    {
        $this->content .= $content;
    }
    // End appendContent

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the content as string
     *
     * @param  -
     * @return string
     */
    public function __toString()
    {
        return iconv($this->getInputEncoding(), $this->getOutputEncoding(), (string)$this->content);
    }
    // End __toString

    //////////////////////////////////////////////////////////////////////////////////////
    // protected
    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Will be called on initialization. Assigns the given argument array to this view
     *
     * @param  array $args
     * @return -
     */
    protected function init(array $args = [])
    {
        if(is_null($args))
            return;

        foreach($args as $name => $value)
        {
            if(!is_string($name))
                continue;

            switch ($name)
            {
                case 'tplName':
                    $this->tplName = $value;
                    break;
                default:
                    $this->assign($name, $value);
                    break;
            }
        }
    }
    // End init

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Renders the view
     *
     * @return -
     */
    final protected function render()
    {
        $this->beforeLoading();
        $this->loadTemplate();
        $this->afterLoading();

        $this->beforeRender();
        $TemplateEngine = new TemplateEngine($this);
        $this->content = $TemplateEngine->process();
        $this->afterRender();
    }
    // End render

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Callback triggered before loading the template
     *
     * Note: Nothing is sure to be initialized here!
     *
     * @param  -
     * @return -
     */
    protected function beforeLoading() {}
    // End beforeLoading

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Callback triggered after loading the template
     *
     * Note: Basic functionality is initialized here. Template is loaded
     *
     * @param  -
     * @return -
     */
    protected function afterLoading() {}
    // End afterLoading

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Callback triggered before rendering the template
     *
     * @param  -
     * @return -
     */
    protected function beforeRender() {}
    // End beforeRender

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Callback triggered after rendering the template
     *
     * @param  -
     * @return -
     */
    protected function afterRender() {}
    // End afterRender

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Prepares the given template.
     *
     * Each template call which has an appropiate handler installed
     * will be replaced by the replacement content they provide.
     *
     * @param  Template $Template
     * @return boolean
     */
    protected function loadTemplate()
    {
        if(!$this->tplName)
            return;

        $this->content = TemplateEngine::loadTemplate($this->tplName, $this->getModule());
    }
    // End loadTemplate

    //////////////////////////////////////////////////////////////////////////////////////
}
// End TextView
