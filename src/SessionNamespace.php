<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs;

/**
 * A SessionNamespace class
 *
 * @package blibs
 * @author Tobias Lode <tobias@beibob.de>
 * @author     Fabian M�ller <fab@beibob.de>
 *
 */
class SessionNamespace
{
    /**
     * Name
     */
    private $_name;

    /**
     * data
     */
    private $_data;

    /**
     * Expire time
     */
    private $_expires;

    /**
     * Scope
     */
    private $_scope;

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the name
     *
     * @param  -
     * @return string
     */
    public function getName()
    {
        return $this->_name;
    }
    // End getName

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the scope
     *
     * @param  int $scope
     * @return int
     */
    public function setScope($scope)
    {
        $this->_scope = (int)$scope;
    }
    // End setScope

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the scope
     *
     * @param  -
     * @return int
     */
    public function getScope()
    {
        return $this->_scope;
    }
    // End getScope

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns true if the expire time has reached
     *
     * @param  -
     * @return boolean
     */
    public function isExpired()
    {
        if(is_null($this->_expires))
            return false;

        return time() > $this->_expires;
    }
    // End isExpired

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the expire time
     *
     * @param  int $expires
     * @return int
     */
    public function setExpireTime($expires)
    {
        $this->_expires = $expires;
    }
    // End setExpireTime

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the expire time
     *
     * @param  -
     * @return int
     */
    public function getExpireTime()
    {
        return $this->_expires;
    }
    // End getExpireTime

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns true if the data pool is empty
     *
     * @param  -
     * @return bool
     */
    public function isEmpty()
    {
        return empty($this->_data);
    }
    // End isEmpty

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Magic method __isset
     *
     * @param  string $key
     * @return boolean
     */
    public function __isset($key)
    {
        return isset($this->_data[$key]);
    }
    // End __isset

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Magic method __get
     *
     * @param  string $key
     * @return mixed
     */
    public function __get($key)
    {
        return isset($this->_data[$key])? $this->_data[$key] : null;
    }
    // End __get

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Magic method __set
     *
     * @param  string $key
     * @param  mixed  $value
     * @return -
     */
    public function __set($key, $value)
    {
        $this->_data[$key] = $value;
    }
    // End __set

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Unsetzt
     */
    public function __unset($key)
    {
        unset($this->_data[$key]);
    }
    // End __unset

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the data array
     *
     * @param  -
     * @return array
     */
    public function getData()
    {
        return $this->_data;
    }
    // End getData

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Frees all data in the namespace
     *
     * @param  -
     * @return -
     */
    public function freeData()
    {
        $this->_data = [];
    }
    // End free

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * magic __toString method
     *
     * @param  -
     * @return string
     */
    public function __toString()
    {
        return print_r($this->_data, true);
    }
    // End __toString

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Constructs the namespace. Should be called by Session
     *
     * @param  array $data
     * @return SessionNamespace
     */
    public function __construct($name, $scope = Session::SCOPE_REQUEST, $expires = null, array $data = [])
    {
        $this->_name = $name;
        $this->_data = $data;
        $this->setScope($scope);
        $this->setExpireTime($expires);
    }
    // End __construct

    //////////////////////////////////////////////////////////////////////////////////////
}
// End SessionNamespace
