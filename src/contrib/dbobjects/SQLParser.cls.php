<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */

class SQLParser
{
    /**
     * KlassenPrefix
     */
    public $classPrefix = "";

    /**
     * Zielverzeichnis
     */
    public $directory;

    //////////////////////////////////////////////////////////////////////////////////////

    public function __construct()
    {
    }
    // End __construct

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Setzt das Zielverzeichnis in das generierte Klassen
     * abgelegt werden.
     *
     * @param  string $dir - Verzeichnispfad
     * @return -
     */
    public function setDirectory($dir)
    {
        if($dir == "" || !file_exists($dir))
            throw new Exception("Verzeichnis `$dir' ungültig");

        if($dir[-1] != '/')
            $dir .= '/';

        $this->directory = $dir;
    }
    // End setDirectory

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Legt einen Prefix fuer erzeugte Klassen fest
     *
     * @param string $classPrefix
     * @return -
     */
    public function setClassPrefix($prefix = "")
    {
        $this->classPrefix = $prefix;
    }
    // End setClassPrefix

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Generiert anhand der uebergebenen SQL Tabellen Struktur
     * Klassen fuer jede gefundene Tabelle
     *
     * @param   string $sqlfile - Dateipfad auf die SQL Datei
     * @returns -
     */
    public function generate($sqlfile)
    {
        if(!file_exists($sqlfile))
            throw new Exception("Datei `$sqlfile' existiert nicht");

        $lines   = file($sqlfile);
        $content = join('', $this->removeComments($lines));
        $content = strtr($content, array("\n" => " ",
                                         "\r" => " ",
                                         "\t" => " ",
                                         "\"" => "",
                                         //                                         "'"  => "",
                                         "`"  => ""));

        $pcre = "/\s*?CREATE\s+?TABLE\s+?(.+?)\s+?\((.+?)\);/umi";

        if(preg_match_all($pcre, $content, $matches, PREG_SET_ORDER))
        {
            foreach($matches as $no => $match)
            {
                $tablename  = utf8_trim($match[1]);
                $classname  = $this->getClassName($match[1]);
                list($properties, $primaryKeys, $uniqueKeys, $references) = $this->getClassProperties($match[2]);

                $this->createClass($tablename, $classname, $properties, $primaryKeys, $uniqueKeys, $references);
            }
        }
    }
    // End generate

    //////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////
    // protected

    /**
     * Bildet aus einem Tabellennamen (MySQL oder PgSQL Format)
     * einen Klassennamen fuer die Tabelle
     *
     * @param string $tablename - Tabellenname
     * @return string - Classname
     */
    protected function getClassName($tablename)
    {
        $parts = preg_split('/[\._]|([0-9])/u', $tablename, -1, PREG_SPLIT_DELIM_CAPTURE);

        $nParts = count($parts);

        if($nParts > 0)
        {
            if($nParts > 1 && $parts[0] == 'public')
                array_shift($parts);

            foreach($parts as $no => $part)
            {
                $parts[$no] = utf8_ucfirst($part);
            }
        }

        $classname = $this->classPrefix . join('', $parts);

        // case like processes
        if(mb_substr($classname, -4) == 'sses')
            $classname = mb_substr($classname, 0, mb_strlen($classname) - 2);

        // case like entries, industries
        if(mb_substr($classname, -3) == 'ies')
            $classname = mb_substr($classname, 0, mb_strlen($classname) - 3) . 'y';

        // remove trailing plural s
        elseif(mb_substr($classname, -1) == 's' && mb_substr($classname, -2) != 'ss')
            $classname = mb_substr($classname, 0, mb_strlen($classname) - 1);

        return $classname;
    }
    // End getClassName

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Bildet aus einem Tabellennamen (MySQL oder PgSQL Format)
     * den Tabellennamen
     *
     * @param string $tablename - Tabellenname
     * @return string - Classname
     */
    protected function getTableName($tablename)
    {
        $parts = preg_split('/[\._]/u', $tablename, -1, PREG_SPLIT_NO_EMPTY);

        if(count($parts) > 1)
        {
            foreach($parts as $no => $part)
            {
                $parts[$no] = $part;
            }
        }
        return join('_', $parts);
    }
    // End getClassName

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Bildet aus einer Tabellenbeschreibung Klassen-Attribute
     *
     * @param  string $tableProperties
     * @return array  - Variabelnamen
     */
    protected function getClassProperties($tableProperties)
    {
        $properties = $primaryKeys = $uniqueKeys = $references = array();

        //print $tableProperties."\n";

        //if(preg_match_all("/\s*?(\S+)( \s+? (\S+)? \(.+?\)  )? .+? (--.*? --)?(,|$)/mxi", $tableProperties, $matches, PREG_SET_ORDER))

        if(preg_match_all('/\s*? (\S+) \s+ (\S+) \s*? (.+?) \s*?(--(.+?)--)? \s+? (,|$)/umxi', $tableProperties, $matches, PREG_SET_ORDER))
        {
            foreach($matches as $no => $match)
            {
                switch(mb_strtoupper($match[1]))
                {
                    case 'PRIMARY':
                        if(preg_match('/\( (.+?) \)/umxi', utf8_trim($match[3]), $pks))
                        {
                            $pks = preg_split('/[,\s]+/u', $pks[1], -1, PREG_SPLIT_NO_EMPTY);

                            foreach($pks as $pk)
                                $primaryKeys[$pk] = $this->getPropertyName($pk);
                        }

                        break;
                    case 'UNIQUE':
                        if(preg_match('/\( (.+?) \)/umxi', utf8_trim($match[0]), $uks))
                        {
                            $uks = preg_split('/[,\s]+/u', $uks[1], -1, PREG_SPLIT_NO_EMPTY);

                            foreach($uks as $uk)
                                $uniqueKey[$uk] = $this->getPropertyName($uk);

                            $uniqueKeys[] = $uniqueKey;
                        }
                        break;

                    case 'FOREIGN':
                        if(preg_match('/\((\w+)\)\s+REFERENCES\s+([\w\._]+)\s*\(\"?(\w+)\"?\)/umxi', $matches[$no][3], $fkMatches))
                            $references[$fkMatches[1]] = array($this->getClassName($fkMatches[2]) => $this->getPropertyName($fkMatches[3]));
                        break;

                    case 'CHECK':
                        break;
                    default:
                        $name = $this->getPropertyName($match[1]);
                        $realName = utf8_trim($match[1]);

                        $properties[$realName]->name = $name;
                        $properties[$realName]->type = $this->getPropertyType($match[2]);
                        $properties[$realName]->typeLength = $this->getPropertyTypeLength($match[2]);
                        $properties[$realName]->comment = utf8_trim($match[5]);
                        $properties[$realName]->realname = $realName;

                        if(preg_match('/NOT\s+NULL/ui', $match[3]))
                            $properties[$realName]->notNull = true;
                        else
                            $properties[$realName]->notNull = false;

                        if(preg_match('/default\s+(\S+)/ui', $match[3], $defaults))
                        {
                            $default = utf8_trim($defaults[1]);
                            $default = strtr($default, array("'" => ''));

                            if(mb_strpos($default, '(') === false)
                                $properties[$realName]->default = (string)$default;
                            else
                                $properties[$realName]->default = '';
                        }
                }
            }
        }

        return array($properties, $primaryKeys, $uniqueKeys, $references);
    }
    // End getClassProperties

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Bildet einen Variablennamen der Tabellenklasse aus einem Feldnamen
     * Unterstriche, falls vorhanden, werden entfernt und Wortglieder je mit
     * grossen Anfangsbuchstaben aneinandergekettet
     *
     * @param  string $tablefield - Tabellenfeldname
     * @return string - propertyName
     */
    protected function getPropertyName($tablefield)
    {
        $parts = preg_split("/[_]/u", $tablefield, -1, PREG_SPLIT_NO_EMPTY);

        foreach($parts as $no => $part)
        {
            $property .= $no > 0? ucfirst($part) : $part;
        }

        return $property;
    }
    // End normalizePropertyName

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Uebersetzt den Datentyp eines Feldes in einen PHP-Datentyp
     *
     * @param string $dbype - dbtype
     * @return string - phptype
     */
    protected function getPropertyType($dbtype)
    {
        $dbtype = preg_replace('/^(\w+)(\(\d+\))?$/u', '$1', $dbtype);

        switch(mb_strtoupper($dbtype))
        {
            case 'INT':
            case 'INT2':
            case 'INT4':
            case 'INT8':
            case 'INTEGER':
            case 'SMALLINT':
            case 'BIGINT':
            case 'SERIAL':
            case 'BIGSERIAL':
            case 'TINYINT':
            case 'MEDIUMINT':
                return "int";

            case 'NUMERIC':
            case 'DECIMAL':
            case 'DEC':
            case 'REAL':
            case 'DOUBLE':
            case 'DOUBLE PRECISION':
            case 'FLOAT':
                return "float";

            case 'BOOL':
            case 'BOOLEAN':
                return "bool";
                break;

            default:
                break;
        }

        return "string";
    }
    // End getPropertyType

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Uebersetzt den Datentyp eines Feldes in einen PHP-Datentyp
     *
     * @param string $dbype - dbtype
     * @return string - phptype
     */
    protected function getPropertyTypeLength($dbtype)
    {
        if(!preg_match('/^(\w+)\((\d+)\)$/u', $dbtype, $matches))
            return false;

        switch(mb_strtoupper($matches[1]))
        {
            case 'VARCHAR':
                return $matches[2];
        }

        return false;
    }
    // End getPropertyTypeLength

    //////////////////////////////////////////////////////////////////////////////////////


    protected function createClass($tablename, $classname, $properties, $primaryKeys, $uniqueKeys, $references)
    {
        $DbObject = new DbObjectClass($tablename, $classname, array(), $properties, $primaryKeys, $uniqueKeys, $references);
        $DbObject->writeTo('./classes/'.$classname.'.cls.php');

        $setClassname = $classname.'Set';

        $DbObjectSet = new DbObjectSetClass($tablename, $setClassname, $properties, array('Handles a set of '.$classname));
        $DbObjectSet->writeTo('./classes/'.$setClassname.'.cls.php');
    }
    // End createClass

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Entfernt alle Kommentare aus der Datei
     *
     * @param array $lines - Array von Dateizeilen
     * @returns array
     */
    protected function removeComments($lines)
    {
        $newlines = array();

        if(is_array($lines))
        {
            foreach($lines as $line)
            {
                $newlines[] = preg_replace('/^(.*?)--(.*)$/u', '${1} --${2}--', $line);
            }
        }

        return $newlines;
    }
    // End removeComments

    //////////////////////////////////////////////////////////////////////////////////////
}
// End class SQLParser
?>
