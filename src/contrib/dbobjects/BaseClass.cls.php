<?php
/**
 * This file is part of blibs - mvc development framework
 * 
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 * 
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */

class BaseClass
{
    /**
     * Klassenname
     */
    public $classname = 'stdClass';

    /**
     * Klassenbeschreibung
     */
    public $classDescription;

    /**
     * primary keys
     */
    protected $primaryKeys;

    /**
     * foreign keys
     */
    protected $foreignKeys;

    /**
     * unique keys
     */
    protected $uniqueKeys;

    /**
     * Anzahl Whitespace einer Einrueckungsebene
     */
    public $indentation = 4;

    /**
     * Klassenmethoden
     */
    protected $methods = array();

    /**
     * KlassenAttribute
     */
    protected $attributes = array();

    /**
     * Klassenkonstanten
     */
    protected $constants = array();

    /**
     * Interfaces
     */
    protected $interfaces = array();

    /**
     * ParentClass
     */
    protected $parentClass;

    /*
     * max StrLength of Properties
     */
    protected $maxPropertyLength = 0;

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Erzeugt eine neue Klassendatei
     *
     * @param string $classname   - Klassenname
     * @param array  $description - Liste von Beschreibungszeilen
     */
    public function __construct($classname, $description = array(), $primaryKeys = array(), $uniqueKeys = array(), $references = array())
    {
        $this->classname = $classname;
        $this->classDescription = $description;
        $this->primaryKeys = $primaryKeys;
        $this->uniqueKeys  = $uniqueKeys;
        $this->foreignKeys = $references;
    }
    // End __construct

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Setzt die Elternklasse
     *
     * @param string $parentClass - Elternklassenname
     * @return -
     */
    public function setParentClass($parentClass)
    {
        $this->parentClass = $parentClass;
    }
    // End setParentClass

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Fuegt ein Interface hinzu
     *
     * @param string $interface - Name des Interfaces
     * @return -
     */
    public function addInterface($interfacename)
    {
        $this->interfaces[] = $interfacename;
    }
    // End setParentClass

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Erzeugt ein neues Klassen-Attribut
     *
     * @param array  $comments - Liste von Kommentarzeilen
     * @param string $property - Attributname
     * @param string $access   - Zugriff-Schluesselwort [optional]
     * @return -
     */
    public function addProperty($comments, $property, $type = 'string', $access = 'public', $default = null, $realname = "")
    {
        $Property->name     = $property;
        $Property->access   = $access;
        $Property->comments = $comments;
        $Property->value    = $default;
        $Property->type     = $type;
        $Property->realname = $realname;

        $this->properties[] = $Property;

        $this->maxPropertyLength  = $this->getMaxLength($this->properties, 'name');
        $this->maxFieldnameLength = $this->getMaxLength($this->properties, 'realname');
    }
    // End addProperty

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Erzeugt eine neue Klassen-Konstante
     *
     * @param string $constantname - Attributname
     * @param array  $comments     - Liste von Kommentarzeilen [optional]
     * @return -
     */
    public function addConstant($constantname, $value, $comments = array())
    {
        $Constant->name     = $constantname;
        $Constant->value    = $value;
        $Constant->comments = $comments;

        $this->constants[] = $Constant;
    }
    // End addConstant

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Erzeugt eine neue Klassen-Methode
     *
     * @param array  $comments - Liste von Kommentarzeilen
     * @param string $method   - Methodenname
     * @param array  $params   - Parameter(key), defaultwert(value) [optional]
     *                           Wenn der Parameter 'return' ist ein Sonderfall
     * @param string $access   - Zugriff-Schluesselwort [optional]
     * @return -
     */
    public function addMethod($comments, $method, $params = array(), $access = 'public', $source = array())
    {
        $Method->name     = $method;
        $Method->comments = $comments;
        $Method->params   = $params;
        $Method->access   = $access;
        $Method->source   = $source;

        $this->methods[] = $Method;
    }
    // End addMethod

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Schreibt den Quellcode in das angegebene Verzeichnis
     *
     * @param  string $classpath - Verzeichnispfad
     * @return -
     */
    public function writeTo($classpath)
    {
        $code = $this->getHeader();
        $code .= $this->getClassDeclaration();

        if($constants = $this->getConstants())
        {
            $code .= $constants;
            $code .= $this->getSeparator();
        }

        if($properties = $this->getProperties())
            $code .= $properties;

        if($pubMethods = $this->getMethods('public'))
        {
            $code .= $this->getSeparator(true, 'public');
            $code .= $pubMethods;
        }

        if($protMethods = $this->getMethods('protected'))
        {
            $code .= $this->getSeparator(true, 'protected');
            $code .= $protMethods;
        }

        if($privMethods = $this->getMethods('private'))
        {
            $code .= $this->getSeparator(true, 'private');
            $code .= $privMethods;
        }

        $code .= $this->getFooter();

        file_put_contents($classpath, $code);
    }
    // End writeTo

    //////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////

    protected function getHeader()
    {
        $header[] = "<?php";
        $header[] = "/**";
        if(count($this->classDescription) > 0)
        {
            foreach($this->classDescription as $desc)
            {
                $header[] = " * ".$desc;
            }
        }
        else
        {
            $header[] = " * ";
        }

        $header[] = " *";
        $header[] = " * @package    -";
        $header[] = " * @class      ".$this->classname;
        $header[] = " * @author Fabian Möller <fab@beibob.de>";
        $header[] = " * @author Tobias Lode <tobias@beibob.de>";
        $header[] = " * @copyright  ".date('Y')." BEIBOB Medienfreunde";
        $header[] = " */";
        $header[] = "";

        return join("\n", $header);
    }
    // End getHeader

    //////////////////////////////////////////////////////////////////////////////////////

    protected function getClassDeclaration()
    {
        $declaration[0] = "class ".$this->classname;

        if(isset($this->parentClass))
            $declaration[0] .= " extends ".$this->parentClass;

        if(count($this->interfaces) > 0)
            $declaration[0] .= " implements ". join(", ", $this->interfaces);

        $declaration[1] = "{\n";

        return join("\n", $declaration);
    }
    // End getClassDeclaration

    //////////////////////////////////////////////////////////////////////////////////////

    protected function getProperties()
    {
        $properties = array();

        if(!is_array($this->properties))
            return false;

        foreach($this->properties as $no => $Property)
        {
            if(count($Property->comments) > 0)
            {
                $properties[] = $this->indent()."/**";

                foreach($Property->comments as $comment)
                {
                    $properties[] =  $this->indent()." * ".$comment;
                }

                $properties[] =  $this->indent()." */";
            }

            $temp = $Property->access ." $". $Property->name;

            if(is_array($Property->value))
            {
                $val    = $Property->value;
                $maxLen = $this->maxPropertyLength;

                if(count($val) == 0)
                {
                    $properties[] = $this->indent(). $temp ." = array();";
                }
                elseif(count($val) == 1)
                {
                    $properties[] = $this->indent(). $temp ." = array(" . str_pad("'".key($val)."'", $maxLen+2) . " => " . current($val).");";
                }
                else
                {
                    $properties[] = $this->indent(). $temp ." = array(" . str_pad("'".key($val)."'", $maxLen + 2) . " => " . current($val).",";

                    $index = 1;
                    foreach($val as $key => $value)
                    {
                        if($index > 1 && $index < count($val))
                        {
                            $properties[] = $this->indent(). str_repeat(' ', mb_strlen($temp)) .'         ' . str_pad('\''.$key.'\'', $maxLen + 2) . ' => ' . $value.',';
                        }
                        elseif($index == count($val))
                        {
                            $properties[] = $this->indent(). str_repeat(' ', mb_strlen($temp)) ."         " . str_pad('\''.$key.'\'', $maxLen + 2) . ' => ' .$value.');';
                        }
                        $index++;
                    }
                }
            }
            elseif(is_null($Property->value))
            {
                $properties[] = $this->indent() . $temp .";";
            }
            else
            {
                $temp .= " = ". $this->quote($Property->value, $Property->type);
                $properties[] = $this->indent() . $temp .";";
            }

            if($no < count($this->properties))
                $properties[] = "";
        }

        return join("\n", $properties);
    }
    // End getProperties

    //////////////////////////////////////////////////////////////////////////////////////

    protected function getConstants()
    {
        $constants = array();

        foreach($this->constants as $no => $Constant)
        {
            if(count($Constant->comments) > 0)
            {
                $constants[] = $this->indent()."/**";

                foreach($Constant->comments as $comment)
                {
                    $constants[] =  $this->indent()." * ".$comment;
                }
                $constants[] =  $this->indent()." */";
            }

            $constants[] = $this->indent(). 'const '. $Constant->name .' = \''. $Constant->value.'\';';
            $constants[] = "";
        }

        return join("\n", $constants);
    }
    // End getConstants

    //////////////////////////////////////////////////////////////////////////////////////

    protected function getMethods($access = false)
    {
        $methods = array();

        foreach($this->methods as $Method)
        {
            if($access && mb_substr($Method->access, 0, mb_strlen($access)) != $access)
                continue;

            if(is_array($Method->comments) &&
               count($Method->comments) > 0)
            {
                $methods[] = $this->indent()."/**";
                foreach($Method->comments as $comment)
                {
                    $methods[] =  $this->indent()." * ".$comment;
                }

                $methods[] =  $this->indent()." *";

                if(count($Method->params) > 0)
                {
                    $maxParamLength = $this->getMaxLength($Method->params, 'name');

                    foreach($Method->params as $Param)
                    {
                        switch(mb_strtolower($Param->name))
                        {
                            case 'return':
                                $return =  "@return ". $Param->type;
                                if($Param->description)
                                    $return .= " - " .$Param->description;
                                break;

                            default:
                                $comment = "@param  ".str_pad($Param->type, 8).' '.str_pad('$'.$Param->name, $maxParamLength);

                                if(isset($Param->description))
                                    $comment .= " - " .$Param->description;
                                $methods[] = $this->indent(). " * " . $comment;
                        }
                    }

                    if(isset($return))
                        $methods[] = $this->indent(). " * " . $return;
                }
                else
                {
                    $methods[] = $this->indent(). " * @return void";
                }

                $methods[] =  $this->indent()." */";
            }

            $function = $Method->access ." function ". $Method->name ."(";

            $args = array();
            if(is_array($Method->params))
            {
                foreach($Method->params as $no => $Param)
                {
                    if(mb_strtolower($Param->name) == 'return')
                        continue;

                    switch($Param->type)
                    {
                        case 'boolean':
                        case 'bool':
                        case 'int':
                        case 'integer':
                        case 'float':
                        case 'mixed':
                        case 'numeric':
                        case 'string':
                            break;

                        default:
                            $args[$no] = $Param->type.' ';
                    }

                    $args[$no] .= "$".$Param->name;

                    if(isset($Param->value))
                        $args[$no] .= " = ".$this->quote($Param->value, $Param->type);
                }
            }

            $methods[] = $this->indent() . $function . join(", ", $args) .")";
            $methods[] = $this->indent() . "{";

            foreach($Method->source as $sourceline)
            {
                $methods[] = $this->indent(2) . $sourceline;
            }

            $methods[] = $this->indent() . "}";
            $methods[] = $this->indent() . "// End " .$Method->name;
            $methods[] = $this->indent() . $this->getSeparator(false);
        }

        /**
         * Remove last separator
         */
        if(count($methods))
            $methods[count($methods) - 1] = '';

        return join("\n", $methods);
    }
    // End getMethods

    //////////////////////////////////////////////////////////////////////////////////////

    protected function getFooter()
    {
        $footer[] = "}";
        $footer[] = "// End class ".$this->classname;

        return join("\n", $footer);
    }
    // End getFooter

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Quotet in Abhaengigkeit des Types den gegebenen Wert
     * Type ist entweder string oder nicht
     *
     * @returns mixed - quoted or unquoted
     */
    protected function quote($value, $type)
    {
        if(mb_strpos($value, '::') !== false ||
           $type != 'string'  ||
           $value == 'null'   ||
           $value == 'false'  ||
           $value == 'true')
            return $value;

        return  "'".$value."'";
    }
    // End quote

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Liefert in Abhaengigkeit des Typen den printf Formatierungscode
     * Type ist entweder string oder nicht
     *
     * @returns mixed - quoted or unquoted
     */
    protected function typeToFormat($Property)
    {
        switch($Property->type)
        {
            case 'string':
            case 'boolean':
            case 'bool':
            case 'numeric':
                $format = '%s';
                break;

            case 'float':
                if($Property->notNull)
                    $format = '%f';
                else
                    $format = '%s';
                break;

            default:
                if($Property->notNull)
                    $format = '%d';
                else
                    $format = '%s';
                break;
        }

        return $format;
    }
    // End typeToFormat

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Gibt in Abhaengigkeit des Levels Whitespaces fuer die Einrueckung
     * zurueck
     *
     * @param  int $level - Level
     * @return string - whitespace
     */
    protected function indent($level = 1)
    {
        return str_repeat(" ", $this->indentation * $level);
    }
    // End indent

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Fuegt eine Trennline ein
     *
     * @param  -
     * @return string - Trennlinie
     */
    protected function getSeparator($newline = true, $section = false)
    {
        $sep[] = "";
        $sep[] = "";

        return join("\n", $sep);
    }
    // End getSeparator

    //////////////////////////////////////////////////////////////////////////////////////

    protected function getMaxLength($objList, $field)
    {
        foreach($objList as $Obj)
        {
            $length = mb_strlen($Obj->$field);

            if($length > $maxLength)
                $maxLength = $length;
        }

        return $maxLength;
    }
    // End getMaxLength
}
// End class BaseClass
