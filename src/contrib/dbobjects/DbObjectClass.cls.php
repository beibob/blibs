<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */

class DbObjectClass extends BaseClass
{
    /**
     * TableProperties
     */
    public $tableProperties;

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Erzeugt eine neue DBSync-Klassendatei
     *
     * @param string $classname   - Klassenname
     * @param array  $description - Liste von Beschreibungszeilen
     */
    public function __construct($tablename, $classname, $classDescription = array(), $properties, $primaryKeys = array(), $uniqueKeys = array(), $references = array())
    {
        parent::__construct($classname, $classDescription, $primaryKeys, $uniqueKeys, $references);

        $this->tableConstant = 'TABLE_NAME';
        $this->setParentClass('DbObject');

        $this->addConstant($this->tableConstant, $tablename, array("Tablename"));
        $this->addProperties($properties);
        $this->addMethods($properties);
    }
    // End __construct

    //////////////////////////////////////////////////////////////////////////////////////
    // protected:
    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Setzt die Klassen-Attribute
     *
     * @param  array $properties
     * @return -
     */
    protected function addProperties($properties)
    {
        $this->addTableProperties($properties);
    }
    // End addProperties

    //////////////////////////////////////////////////////////////////////////////////////

    protected function addMethods($properties)
    {
        $this->addCreateMethod();
        $this->addFindByIdMethod();
        $this->addFindByUniqueKeyMethods();
        $this->addTablePropertyMethods($properties);
        $this->addExistsMethod();
        $this->addUpdateMethod();
        $this->addDeleteMethod();
        $this->addGetPrimaryKeyMethod();
        $this->addGetTableNameMethod();
        $this->addGetColumnTypesMethod();
        $this->addInsertMethod();
        $this->addInitByDOMethod();
    }
    // End addMethods

    //////////////////////////////////////////////////////////////////////////////////////

    protected function addCreateMethod()
    {
        /**
         * Split properties in required and optional properties
         */
        $required = array();
        $optional = array();

        foreach($this->tableProperties as $no => $Property)
        {
            if($Property->notNull && !isset($Property->default))
            {
                $required[] = $Property;
            }
            else
            {
                $optional[] = $Property;
            }
        }

        $source[] = '$'.$this->classname.' = new '.$this->classname.'();';

        /**
         * And merge them again
         */
        $properties = array_merge($required, $optional);

        foreach($properties as $no => $Property)
        {
            if($Property->name != 'id' &&
               $Property->name != 'modified' &&
               $Property->name != 'created')
            {
                $params[$no]->name     = $Property->name;
                $params[$no]->type     = $Property->type;
                $params[$no]->description = $Property->comment;

                if(!$Property->notNull)
                {
                    if(isset($Property->default))
                        $params[$no]->value = $Property->default;
                    else
                        $params[$no]->value = 'null';
                }
                else
                {
                    if(isset($Property->default))
                        $params[$no]->value = $Property->default;
                }

                $source[] = '$'.$this->classname.'->set'.ucfirst($Property->name).'($'.$Property->name.');';
            }
        }

        $params[$no + 1]->name     = 'return';
        $params[$no + 1]->type     = $this->classname;


        $source[] = '';
        $source[] = 'if($'.$this->classname.'->getValidator()->isValid())';
        $source[] = $this->indent().'$'.$this->classname.'->insert();';
        $source[] = '';
        $source[] = 'return $'.$this->classname.';';

        $this->addMethod(array("Creates the object"), "create", $params, 'public static', $source);
    }
    // End addCreateMethod

    //////////////////////////////////////////////////////////////////////////////////////

    protected function addFindByIdMethod()
    {
        $index = 0;

        foreach($this->primaryKeys as $pkRealName => $pkProperty)
        {
            $PKProp = $this->tableProperties[$pkRealName];

            $args[$index]->name        =  $PKProp->name;
            $args[$index]->type        =  $PKProp->type;
            $args[$index]->description =  $PKProp->comment;

            $index++;
        }

        $args[$index]->name        = "force";
        $args[$index]->value       = "false";
        $args[$index]->type        = "bool";
        $args[$index]->description = "Bypass caching";

        $index++;
        $args[$index]->name        = "return";
        $args[$index]->type        = $this->classname;
        $args[$index]->description = "";

        $checks = array();
        foreach($this->primaryKeys as $pkProperty)
            $checks[] = '!$'.$pkProperty;

        $source[] = 'if('.join(' || ', $checks).')';
        $source[] = $this->indent().'return new '.$this->classname.'();';
        $source[] = '';

        $no = 0;
        foreach($this->tableProperties as $realName => $Property)
        {
            if($no == 0)
                $source[] = '$sql = sprintf("SELECT '.$realName;
            else
                $source[] = '                     , '.$realName;

            $no++;
        }

        $source[] = '                  FROM %s';

        $no = 0;
        foreach($this->primaryKeys as $realName => $name)
        {
            $Property = $this->tableProperties[$realName];

            if($no == 0)
                $source[] = '                 WHERE '.$Property->realname.' = :'.$Property->name;
            else
                $source[] = '                   AND '.$Property->realname.' = :'.$Property->name;

            $no++;
        }

        end($source);
        $source[key($source)] .= '"';


        $source[] = '               , self::'. $this->tableConstant;
        $source[] = '               );';
        $source[] = '';

        $inits = array();
        foreach($this->primaryKeys as $name)
            $inits[] = '\''.$name.'\' => $'.$name;

        $source[] = 'return self::findBySql(get_class(), $sql, array('. join(', ', $inits) .'), $force);';

        reset($this->primaryKeys);

        $this->addMethod(array("Inits a `".$this->classname."' by its primary key"),
                         count($this->primaryKeys) == 1? "findBy". ucfirst($this->tableProperties[key($this->primaryKeys)]->name) : 'findByPk',
                         $args,
                         "public static",
                         $source);
    }
    // End addInitByIdMethod

    //////////////////////////////////////////////////////////////////////////////////////

    protected function addFindByUniqueKeyMethods()
    {
        foreach($this->uniqueKeys as $uniqueKeyIndex => $uniqueKey)
        {
            $index = 0;
            $args = array();

            foreach($uniqueKey as $ukRealName => $ukProperty)
            {
                $UKProp = $this->tableProperties[$ukRealName];

                $args[$index]->name        =  $UKProp->name;
                $args[$index]->type        =  $UKProp->type;
                $args[$index]->description =  $UKProp->comment;

                $index++;
            }

            $args[$index]->name        = "force";
            $args[$index]->value       = "false";
            $args[$index]->type        = "bool";
            $args[$index]->description = "Bypass caching";

            $index++;
            $args[$index]->name        = "return";
            $args[$index]->type        = $this->classname;
            $args[$index]->description = "";

            $checks = array();
            foreach($uniqueKey as $ukProperty)
                $checks[] = '!$'.$ukProperty;

            $source = array();
            $source[] = 'if('.join(' || ', $checks).')';
            $source[] = $this->indent().'return new '.$this->classname.'();';
            $source[] = '';

            $no = 0;
            foreach($this->tableProperties as $realName => $Property)
            {
                if($no == 0)
                    $source[] = '$sql = sprintf("SELECT '.$realName;
                else
                    $source[] = '                     , '.$realName;

                $no++;
            }

            $source[] = '                  FROM %s';

            $no = 0;
            foreach($uniqueKey as $realName => $name)
            {
                $Property = $this->tableProperties[$realName];

                if($no == 0)
                    $source[] = '                 WHERE '.$Property->realname.' = :'.$Property->name;
                else
                    $source[] = '                   AND '.$Property->realname.' = :'.$Property->name;

                $no++;
            }

            end($source);
            $source[key($source)] .= '"';


            $source[] = '               , self::'. $this->tableConstant;
            $source[] = '               );';
            $source[] = '';

            $inits = array();
            foreach($uniqueKey as $name)
                $inits[] = '\''.$name.'\' => $'.$name;

            $source[] = 'return self::findBySql(get_class(), $sql, array('. join(', ', $inits) .'), $force);';

            $props = array();

            foreach($uniqueKey as $property)
                $props[] = ucfirst($property);

            $methodName = "findBy". join('And', $props);

            $this->addMethod(array("Inits a `".$this->classname."' by its unique key (". join(', ', $uniqueKey) .")"),
                             $methodName,
                             $args,
                             "public static",
                             $source);
        }
    }
    // End addInitByUniqueMethod

    //////////////////////////////////////////////////////////////////////////////////////

    protected function addInitByDOMethod()
    {
        $args[0]->name        = "DO";
        $args[0]->value       = 'null';
        $args[0]->type        = "\stdClass";
        $args[0]->description = "Data object";

        foreach($this->tableProperties as $no => $Property)
        {
            $forceCase = false;

            switch($Property->type)
            {
                case 'boolean':
                case 'bool':
                    $forceCase = 'bool';
                    break;

                case 'integer':
                case 'int':
                    $forceCase = 'int';
                    break;
            }

            if($forceCase && $Property->notNull)
                $source[] = '$this->'.str_pad($Property->name, $this->maxPropertyLength ). ' = ('.$forceCase.')$DO->'.$Property->realname.';';
            else
                $source[] = '$this->'.str_pad($Property->name, $this->maxPropertyLength ). ' = $DO->'.$Property->realname.';';
        }

        $source[] = '';
        $source[] = '/**';
        $source[] = ' * Set extensions';
        $source[] = ' */';

        $this->addMethod(array("Inits the object with row values"),
                         "initByDataObject",
                         $args,
                         "protected",
                         $source);
    }
    // End addInitByDOMethod

    //////////////////////////////////////////////////////////////////////////////////////

    protected function addExistsMethod()
    {
        $index = 0;
        $pkArgs = array();

        foreach($this->primaryKeys as $pkRealName => $pkProperty)
        {
            $PKProp = $this->tableProperties[$pkRealName];

            $args[$index]->name        =  $PKProp->name;
            $args[$index]->type        =  $PKProp->type;
            $args[$index]->description =  $PKProp->comment;

            $pkArgs[] = '$'.$PKProp->name;

            $index++;
        }

        $args[$index]->name        = "force";
        $args[$index]->value       = "false";
        $args[$index]->type        = "bool";
        $args[$index]->description = "Bypass caching";

        $index++;
        $args[$index]->name        = "return";
        $args[$index]->type        = "bool";
        $args[$index]->description = "";

        reset($this->primaryKeys);
        $findByPkMethod = count($this->primaryKeys) == 1? "findBy".ucfirst($this->tableProperties[key($this->primaryKeys)]->name) : 'findByPk';
        $source[] = 'return self::'.$findByPkMethod.'('.join(', ', $pkArgs).', $force)->isInitialized();';

        $this->addMethod(array("Checks, if the object exists"),
                         "exists",
                         $args,
                         "public static",
                         $source);
    }
    // End addExistsMethod

    //////////////////////////////////////////////////////////////////////////////////////

    protected function addRefreshMethod()
    {
        $PKProp = $this->tableProperties[0];

        $source[] = 'if(!$this->isValid())';
        $source[] = $this->indent().'return false;';
        $source[] = '';
        $source[] = 'return self::findBy'.ucfirst($PKProp->name).'($this->'.$PKProp->name.', true);';

        $this->addMethod(array("Reloads the data object from database"),
                         "refresh",
                         array(),
                         "public",
                         $source);
    }
    // End addIsValidMethod

    //////////////////////////////////////////////////////////////////////////////////////

    protected function addUpdateMethod()
    {
        $args[0]->name        = "return";
        $args[0]->type        = "bool";
        $args[0]->description = "";

        foreach($this->tableProperties as $Property)
        {
            if($Property->name == 'modified')
            {
                if($Property->type == 'string')
                    $source[] = '$this->modified = self::getCurrentTime();';
                else
                    $source[] = '$this->modified = time();';

                $source[] = '';
                break;
            }
        }

        $updateCols = array();
        foreach($this->tableProperties as $realName => $Property)
            if(!isset($this->primaryKeys[$realName]))
                $updateCols[] = $Property;

        $source[] = '$sql = sprintf("UPDATE %s';
        $source[] = '                   SET '.str_pad($updateCols[0]->realname, $this->maxPropertyLength).' = :'. $updateCols[0]->name;

        foreach($updateCols as $no => $Property)
            if($no > 0)
                $source[] = '                     , '.str_pad($Property->realname, $this->maxPropertyLength) .' = :'. $Property->name;

        $no = 0;
        foreach($this->primaryKeys as $realName => $name)
        {
            $Property = $this->tableProperties[$realName];

            if($no == 0)
                $source[] = '                 WHERE '.$Property->realname.' = :'.$Property->name;
            else
                $source[] = '                   AND '.$Property->realname.' = :'.$Property->name;

            $no++;
        }
        end($source);
        $source[key($source)] .= '"';

        $source[] = '               , self::'.$this->tableConstant;
        $source[] = '               );';
        $source[] = '';
        $source[] = 'return $this->updateBySql($sql,';

        $firstPropertyName = reset($this->primaryKeys);
        $source[] = '                          array('.str_pad("'".$firstPropertyName."'", $this->maxPropertyLength + 1).' => $this->'.$firstPropertyName.',';

        $no = 0;
        foreach($this->tableProperties as $realName => $Property)
        {
            if($no > 0)
            {
                $line = '                                '. str_pad("'".$Property->name."'", $this->maxPropertyLength + 1).' => $this->'.$Property->name;

                if($no < count($this->tableProperties) - 1)
                    $line .= ',';
                else
                    $line .= ')';

                $source[] = $line;
            }

            $no++;
        }

        $source[] = '                          );';

        $this->addMethod(array("Updates the object in the table"),
                         "update",
                         $args,
                         "public",
                         $source);
    }
    // End addUpdateMethod

    //////////////////////////////////////////////////////////////////////////////////////

    protected function addInsertMethod()
    {
        $args[0]->name        = "return";
        $args[0]->type        = "bool";
        $args[0]->description = "";


        if(count($this->primaryKeys) == 1 && reset($this->primaryKeys) == 'id')
        {
            if($PKProp->type == 'string')
                $idGetter = 'IdFactory::getSequelId()';
            else
                $idGetter = '$this->getNextSequenceValue()';

            $source[] = '$this->'.str_pad('id', $this->maxPropertyLength).' = '.$idGetter.';';
        }

        $no = 0;
        foreach($this->tableProperties as $realName => $Property)
        {
            $propertylist[] = $Property->realname;
            $formatlist[]   = ':'.$Property->name;

            if($Property->name == 'created' ||
               $Property->name == 'modified')
            {
                if($Property->name == 'modified')
                    $source[] = '$this->'.str_pad($Property->name, $this->maxPropertyLength).' = null;';

                elseif($Property->name == 'created')
                {
                    if($Property->type == 'string')
                        $timeGetter = 'self::getCurrentTime()';
                    else
                        $timeGetter = 'time()';

                    $source[] = '$this->'.str_pad($Property->name, $this->maxPropertyLength).' = '.$timeGetter.';';
                }
            }
            $no++;
        }

        $source[] = '';
        $source[] = '$sql = sprintf("INSERT INTO %s ('.join(', ', $propertylist).')';
        $source[] = '                       VALUES  ('.join(', ', $formatlist).')"';
        $source[] = '               , self::'.$this->tableConstant;
        $source[] = '               );';
        $source[] = '';
        $source[] = 'return $this->insertBySql($sql,';

        $firstPropertyName = reset($this->primaryKeys);
        $source[] = '                          array('.str_pad("'".$firstPropertyName."'", $this->maxPropertyLength + 1).' => $this->'.$firstPropertyName.',';

        $no = 0;
        foreach($this->tableProperties as $realName => $Property)
        {
            if($no > 0)
            {
                $line = '                                '. str_pad("'".$Property->name."'", $this->maxPropertyLength + 1).' => $this->'.$Property->name;

                if($no < count($this->tableProperties) - 1)
                    $line .= ',';
                else
                    $line .= ')';

                $source[] = $line;
            }

            $no++;
        }

        $source[] = '                          );';

        $this->addMethod(array("Inserts a new object in the table"),
                         "insert",
                         $args,
                         "protected",
                         $source);
    }
    // End addInsertMethod

    //////////////////////////////////////////////////////////////////////////////////////

    protected function addDeleteMethod()
    {
        $args[0]->name        = "return";
        $args[0]->type        = "bool";
        $args[0]->description = "";

        $PKProp = $this->tableProperties[0];

        $source[] = '$sql = sprintf("DELETE FROM %s';

        $no = 0;
        foreach($this->primaryKeys as $realName => $name)
        {
            $Property = $this->tableProperties[$realName];

            if($no == 0)
                $source[] = '                      WHERE '.$Property->realname.' = :'.$Property->name;
            else
                $source[] = '                        AND '.$Property->realname.' = :'.$Property->name;

            $no++;
        }
        end($source);
        $source[key($source)] .= '"';

        $source[] = '               , self::'.$this->tableConstant;
        $source[] = '              );';
        $source[] = '';
        $source[] = 'return $this->deleteBySql($sql,';

        $inits = array();
        foreach($this->primaryKeys as $name)
            $inits[] = '\''.$name.'\' => $this->'.$name;

        $source[] = '                          array('. join(', ', $inits)  .'));';

        $this->addMethod(array("Deletes the object from the table"),
                         "delete",
                         $args,
                         "public",
                         $source);
    }
    // End addDeleteMethod

    //////////////////////////////////////////////////////////////////////////////////////

    protected function addGetTablenameMethod()
    {
        $args[0]->name        = "return";
        $args[0]->type        = "string";
        $args[0]->description = "";

        $source[] = 'return self::'.$this->tableConstant.';';

        $this->addMethod(array("Returns the tablename constant. This is used",
                               "as interface for other objects."),
                         "getTablename",
                         $args,
                         "public static",
                         $source);
    }
    // End addGetTablenameMethod

    //////////////////////////////////////////////////////////////////////////////////////

    protected function addGetPrimaryKeyMethod()
    {
        $args[0]->name        = "propertiesOnly";
        $args[0]->value       = "false";
        $args[0]->type        = "bool";

        $args[1]->name        = "return";
        $args[1]->type        = "array";
        $args[1]->description = "";

        $source[] = 'if($propertiesOnly)';
        $source[] = $this->indent().'return self::$primaryKey;';
        $source[] = '';
        $source[] = '$primaryKey = array();';
        $source[] = '';
        $source[] = 'foreach(self::$primaryKey as $key)';
        $source[] = $this->indent().'$primaryKey[$key] = $this->$key;';
        $source[] = '';
        $source[] = 'return $primaryKey;';

        $this->addMethod(array("Returns an array with the primary key properties and",
                               "associates its values, if it's a valid object"),
                         "getPrimaryKey",
                         $args,
                         "public",
                         $source);
    }
    // End addGetPrimaryKeyMethod

    //////////////////////////////////////////////////////////////////////////////////////

    protected function addValidateMethod()
    {
        $args[0]->name        = "return";
        $args[0]->type        = "Validator";
        $args[0]->description = "";


        $source[] = '$Validator = new Validator($this)';
        $source[] = '';

        foreach($this->tableProperties as $Property)
        {
            if(!$Property->notNull || $Property->default)
                continue;

            $source[] = '$Validator->assertNotEmpty($'.$Property->name. ');';
        }

        $source[] = '';
        $source[] = 'return $Validator;';

        $this->addMethod(array("Validates the object and returns a Validator"),
                         "validate",
                         $args,
                         "public",
                         $source);
    }
    // End addValidateMethod

    //////////////////////////////////////////////////////////////////////////////////////

    protected function addGetColumnTypesMethod()
    {
        $args[0]->name        = "extColumns";
        $args[0]->value       = "false";
        $args[0]->type        = "bool";

        $args[1]->name        = "column";
        $args[1]->value       = "false";
        $args[1]->type        = "mixed";

        $args[2]->name        = "return";
        $args[2]->type        = "mixed";
        $args[2]->description = "";

        $source[] = '$columnTypes = $extColumns? array_merge(self::$columnTypes, self::$extColumnTypes) : self::$columnTypes;';
        $source[] = '';
        $source[] = 'if($column)';
        $source[] = $this->indent().'return $columnTypes[$column];';
        $source[] = '';
        $source[] = 'return $columnTypes;';

        $this->addMethod(array('Returns the columns with their types. The columns may also return extended columns',
                               'if the first argument is set to true. To access the type of a single column, specify',
                               'the column name in the second argument'),
                         "getColumnTypes",
                         $args,
                         "public static",
                         $source);
    }
    // End addGetFieldMapMethod

    //////////////////////////////////////////////////////////////////////////////////////

    protected function addTableProperties($tableProperties)
    {
        $this->tableProperties = $tableProperties;

        foreach($tableProperties as $Property)
        {
            switch($Property->type)
            {
                case 'string':
                case 'numeric':
                case 'float': // ???
                    $paramType = 'PDO::PARAM_STR';
                    break;

                case 'boolean':
                case 'bool':
                $paramType = 'PDO::PARAM_BOOL';
                    break;

                default:
                    $paramType = 'PDO::PARAM_INT';
                    break;
            }

            $fieldMap[$Property->name] = $paramType;

            $this->addProperty(array($Property->comment),
                               $Property->name,
                               $Property->type,
                               'private',
                               null,
                               $Property->realname);
        }

        $this->addProperty(array("Primary key"),
                           "primaryKey",
                           "array",
                           "private static",
                           'array(\''.join('\', \'', $this->primaryKeys).'\')'
                           );

        $this->addProperty(array("Column types"),
                           "columnTypes",
                           "array",
                           "private static",
                           $fieldMap
                           );

        $this->addProperty(array("Extended column types"),
                           "extColumnTypes",
                           "array",
                           "private static",
                           'array()'
                           );
    }
    // End addTableProperties

    //////////////////////////////////////////////////////////////////////////////////////

    protected function addTablePropertyMethods($tableProperties)
    {
        /**
         * Setters
         */
        foreach($tableProperties as $Property)
        {
            if($Property->name == 'id' ||
               $Property->name == 'created' ||
               $Property->name == 'modified')
                continue;

            $args = array();
            $args[0]->name        = $Property->name;
            $args[0]->type        = $Property->type;
            $args[0]->description = $Property->comment;
            $args[1]->name        = 'return';
            $args[1]->type        = 'void';

            $forceCase = false;

            if($Property->notNull)
            {
                if(isset($Property->default))
                    $args[0]->value = $Property->default;

                switch($Property->type)
                {
                    case 'boolean':
                    case 'bool':
                        $forceCase = 'bool'; break;
                    case 'integer':
                    case 'int':
                        $forceCase = 'int';  break;
                    case 'string':  $forceCase = 'string';  break;

                    default:
                        $forceCase = false;
                        break;
                }
            }
            else
            {
                if(isset($Property->default))
                    $args[0]->value = $Property->default;
                else
                    $args[0]->value = 'null';
            }

            $source = array();

            if($Property->notNull && !isset($Property->default))
            {
                $source[] = 'if(!$this->getValidator()->assertNotEmpty(\''.$Property->name.'\', $'.$Property->name.'))';
                $source[] = $this->indent().'return;';
                $source[] = '';
            }

            if($Property->type == 'string' && $Property->typeLength)
            {
                $source[] = 'if(!$this->getValidator()->assertMaxLength(\''.$Property->name.'\', '.$Property->typeLength.', $'.$Property->name.'))';
                $source[] = $this->indent().'return;';
                $source[] = '';
            }


            $assignment = '$this->'.$Property->name.' = ';
            if($forceCase)
                $assignment .= '('.$forceCase.')';

            $assignment .= '$'.$Property->name.';';
            $source[] = $assignment;

            $this->addMethod(array("Sets the property ".$Property->name),
                             "set" . ucfirst($Property->name),
                             $args,
                             "public",
                             $source);
        }

        /**
         * Getters
         */
        foreach($tableProperties as $realName => $Property)
        {
            $args = array();
            $args[0]->name        = 'return';
            $args[0]->type        = $Property->type;

            if($Property->notNull)
            {
                if(isset($Property->default))
                    $args[0]->value = $Property->default;
            }
            else
            {
                if(isset($Property->default))
                    $args[0]->value = $Property->default;
                else
                    $args[0]->value = 'null';
            }

            $source = array();
            $source[] = 'return $this->'.$Property->name.';';

            if(mb_substr($Property->name, 0, 2) == 'is' ||
               mb_substr($Property->name, 0, 3) == 'has')
                $methodName = $Property->name;
            else
                $methodName = "get" . ucfirst($Property->name);

            $this->addMethod(array("Returns the property ".$Property->name),
                             $methodName,
                             $args,
                             "public",
                             $source);

            /**
             * Add getter for relation object
             */
            if(isset($this->foreignKeys[$realName]))
            {
                $className  = key($this->foreignKeys[$realName]);
                $fkProperty = current($this->foreignKeys[$realName]);

                $Property = $this->tableProperties[$realName];

                $args = array();
                $args[0]->name        = 'force';
                $args[0]->type        = 'bool';
                $args[0]->value       = 'false';
                $args[1]->name        = 'return';
                $args[1]->type        = $className;

                $source   = array();
                $source[] = 'return '. $className. '::findBy'. ucfirst($fkProperty) .'($this->'. $Property->name  .', $force);';

                $this->addMethod(array('Returns the associated '. $className.' by property '. $Property->name),
                                 'get' . mb_substr(ucfirst($Property->name), 0, -2),
                                 $args,
                                 'public',
                                 $source);
            }
        }
    }
    // End addTableProperties

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Bildet aus einem Tabellennamen (MySQL oder PgSQL Format)
     * den Konstantennamen
     *
     * @param string $tablename - Tabellenname
     * @return string - Konstantenname
     */
    protected function getTableConstantName($tablename)
    {
        $parts = preg_split('/[\._]/u', $tablename, -1, PREG_SPLIT_NO_EMPTY);

        if(count($parts) > 1)
        {
            foreach($parts as $no => $part)
            {
                $parts[$no] = $part;
            }
        }
        return 'TABLE_'.mb_strtoupper(join('_', $parts));
    }
    // End getTableName

    //////////////////////////////////////////////////////////////////////////////////////
}
// End class DbObjectClass
?>
