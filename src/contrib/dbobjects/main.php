#!/usr/bin/php
<?php
/**
 * This file is part of the blibs project
 *
 * blibs - mvc development framework
 *
 * Copyright (c) 2008-2011 Tobias Lode <tobias@beibob.de>
 *                         Fabian Möller <fab@beibob.de>
 *               BEIBOB Medienfreunde GbR - http://beibob.de/
 * Licensed under Creative Commons license CC BY-NC 3.0
 * http://creativecommons.org/licenses/by-nc/3.0/de/
 */

date_default_timezone_set('Europe/Berlin');

/**
 * UTF8
 */
mb_internal_encoding("UTF-8");
mb_regex_encoding("UTF-8");
iconv_set_encoding("input_encoding", "UTF-8");
iconv_set_encoding("internal_encoding", "UTF-8");
iconv_set_encoding("output_encoding", "UTF-8");

ini_set('display_errors', 'On');
ini_set('error_reporting', E_ALL & ~E_NOTICE);

include("../../StringFactory.cls.php");
include("../utf8/utf8.php");
include("./BaseClass.cls.php");
include("./DbObjectClass.cls.php");
include("./DbObjectSetClass.cls.php");
include("./SQLParser.cls.php");

$sqlfile = './create.sql';
$SQLParser = new SQLParser();
$SQLParser->setDirectory('./classes');
$SQLParser->generate($sqlfile);

exit(0);

function show($mixed)
{
    echo "<pre>";
    print_r($mixed);
    echo "</pre>";
}
// End show
?>

