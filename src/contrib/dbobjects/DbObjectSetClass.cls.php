<?php
/**
 * This file is part of blibs - mvc development framework
 * 
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 * 
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */

class DbObjectSetClass extends BaseClass
{
    /**
     * TableProperties
     */
    public $tableProperties;

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Erzeugt eine neue DBSync-Klassendatei
     *
     * @param string $classname   - Klassenname
     * @param array  $description - Liste von Beschreibungszeilen
     */
    public function __construct($tablename, $classname, $properties, $classDescription = array())
    {
        parent::__construct($classname, $classDescription);

        $this->tableConstant = 'TABLE_NAME';

        $this->dbObjectName = mb_substr($classname, 0, -3);

        $this->setParentClass('DbObjectSet');
        $this->addMethods($properties);
    }
    // End __construct

    //////////////////////////////////////////////////////////////////////////////////////
    // protected:
    //////////////////////////////////////////////////////////////////////////////////////

    protected function addMethods($properties)
    {
        $this->addFindMethod();
        $this->addCountMethod();
    }
    // End addMethods

    //////////////////////////////////////////////////////////////////////////////////////

    protected function addFindMethod()
    {
        $args[0]->name        = "initValues";
        $args[0]->value       = "null";
        $args[0]->type        = "array";
        $args[0]->description = "key value array";

        $args[1]->name        = "orderBy";
        $args[1]->value       = "null";
        $args[1]->type        = "array";
        $args[1]->description = "array map of columns on to directions array('id' => 'DESC')";

        $args[2]->name        = "limit";
        $args[2]->value       = "null";
        $args[2]->type        = "int";
        $args[2]->description = "limit on resultset";

        $args[3]->name        = "offset";
        $args[3]->value       = "null";
        $args[3]->type        = "int";
        $args[3]->description = "offset on resultset";

        $args[4]->name        = "force";
        $args[4]->value       = "false";
        $args[4]->type        = "bool";
        $args[4]->description = "Bypass caching";

        $args[5]->name        = "return";
        $args[5]->type        = $this->classname;
        $args[5]->description = "";

        $source[] = 'return self::_find(get_class(), '.$this->dbObjectName.'::getTablename(), $initValues, $orderBy, $limit, $offset, $force);';

        $this->addMethod(array('Lazy find'),
                         'find',
                         $args,
                         "public static",
                         $source);
    }
    // End addInitByIdMethod

    //////////////////////////////////////////////////////////////////////////////////////

    protected function addCountMethod()
    {
        $args[0]->name        = "initValues";
        $args[0]->value       = "null";
        $args[0]->type        = "array";
        $args[0]->description = "key value array";

        $args[4]->name        = "force";
        $args[4]->value       = "false";
        $args[4]->type        = "bool";
        $args[4]->description = "Bypass caching";

        $args[5]->name        = "return";
        $args[5]->type        = 'int';
        $args[5]->description = "";

        $source[] = 'return self::_count(get_class(), '.$this->dbObjectName.'::getTablename(), $initValues, $force);';

        $this->addMethod(array('Lazy count'),
                         'dbCount',
                         $args,
                         "public static",
                         $source);
    }
    // End addInitByIdMethod

    //////////////////////////////////////////////////////////////////////////////////////
}
// End class DbObjectSetClass
