<?php
/**
 * This file is part of the elca project
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *               BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * elca is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * elca is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with elca. If not, see <http://www.gnu.org/licenses/>.
 */

namespace Beibob\Blibs;
use Symfony\Component\Translation\Loader\PoFileLoader;
use Symfony\Component\Translation\Translator;


/**
 * BlibsGettext ${CARET}
 *
 * @package
 * @author Tobias Lode <tobias@beibob.de>
 * @author Fabian Möller <fab@beibob.de>
 */
class BlibsGettext
{
    private $file;
    private $fileType;
    private $log = array();

    const FILETYPE_PHP = 'php';
    const FILETYPE_TPL = 'tpl';

    private static $fileTypes = array('php' => 'PHP source file', 'tpl' => 'BLibs template');

    public function __construct($resource = null)
    {
        if ($resource)
            $this->setFile($resource);
    }

    /**
     * @return array
     */
    public function getLog()
    {
        return $this->log;
    }

    protected function log($msg)
    {
        $this->log[] = $msg;
        return $msg;
    }

    /**
     * @param File $file
     */
    public function setFile($resource = null)
    {
        if (is_null($resource))
            throw new \Exception('No input file given');

        if ($resource instanceof File)
            $this->file = $resource;
        else
            $this->file = File::factory($resource);

        if (!$this->file->exists())
            throw new \Exception('Input file `' . $resource . '\' not found');

        $this->setFileType();
    }

    protected function setFileType()
    {
        if (!isset(self::$fileTypes[$this->file->getExtension()]))
            throw new \Exception('Unknown inputfile type `' . $this->file->getExtension() . "'");

        $this->fileType = $this->file->getExtension();
    }

    public function parse()
    {
        if (!$this->fileType)
            throw new \Exception('No input file specified');


        $this->log('parse ' . self::$fileTypes[$this->fileType] . ' `' . $this->file->getFilepath() . "' ");
        $messages = [];
        switch ($this->fileType)
        {
            case self::FILETYPE_PHP:
                foreach ($this->findByXGettext() as $msgId => $msgStr)
                    $messages[$msgId] = $msgId;

                $this->log('    ' . count($messages) . ' messages found by t()-function calls.');

                foreach ($this->findInDocComments() as $msgId => $msgStr)
                    $messages[$msgId] = $msgId;

                $this->log('    ' . count($messages) . ' messages found by @translate comments calls.');

                break;
            case self::FILETYPE_TPL:
                $messages = $this->findUnderscoreCalls();
                $this->log('    ' . count($messages) . ' messages found by _()-function calls.');
                break;
        }


        return $messages;
    }

    /**
     *
     */
    protected function findInDocComments()
    {
        $src = file_get_contents($this->file->getFilepath());
        $matches = [];
        preg_match_all('/@translate\s+(value|const|array|db)\s+(.+)$/m', $src, $matches);

        $messages = [];
        foreach ($matches[0] as $index => $match)
        {
            switch ($matches[1][$index])
            {
                case 'const':
                    foreach ($this->resolveConstantTranslation($matches[2][$index]) as $msgId => $msgStr)
                        $messages[$msgId] = $msgId;
                    break;
                case 'value':
                    foreach ($this->resolveValueTranslation($matches[2][$index]) as $msgId => $msgStr)
                        $messages[$msgId] = $msgId;
                    break;
                case 'array':
                    foreach ($this->resolveArrayTranslation($matches[2][$index]) as $msgId => $msgStr)
                        $messages[$msgId] = $msgId;
                    break;
                case 'db':
                    foreach ($this->resolveDatabaseTranslation($matches[2][$index]) as $msgId => $msgStr)
                        $messages[$msgId] = $msgId;
                    break;
                default:
                    $this->log('        ERROR: Skipping @translate comment `' . $matches[0] . "'");
                    break;
            }
        }

        return $messages;
    }
    // End

    /**
     *
     */
    protected function resolveValueTranslation($value)
    {
        $msgId = trim(trim($value), '\'"');
        return [$msgId => ''];
    }
    // End resolveValueTranslation

    /**
     *
     */
    protected function resolveConstantTranslation($definition)
    {
        $code = sprintf('return %s;', $definition);
        $value = eval($code);
        $msgId = trim(trim($value), '\'"');
        return [$msgId => ''];
    }

    /**
     *
     */
    protected function resolveArrayTranslation($definition)
    {
        $code = sprintf('return %s;', $definition);
        $array = eval($code);
        if (!is_array($array))
        {
            $this->log('        ERROR: Could not resolve array definition `' . $definition . "'");
            return [];
        }

        $messages = [];
        foreach ($array as $key => $value)
        {
            $messages[$value] = '';
        }

        return $messages;
    }
    // End resolveArrayTranslation

    /**
     *
     */
    protected function resolveDatabaseTranslation($resource)
    {
        $parts = explode(' ', $resource);
        $properties = [];
        foreach ($parts as $part)
        {
            if (trim($part))
                $properties[] = $part;
        }

        if (!count($properties) || count($properties) < 2)
        {
            $this->log('        ERROR: resolving database resource `' . $resource . "'");
            return [];
        }

        $method = array_shift($properties);

        $code = sprintf('return %s;', $method);
        $ObjectSet = eval($code);

        if (!($ObjectSet instanceof \Iterator))
        {
            $this->log('        ERROR: Could not iterate database resource `' . $resource . "'");
            return [];
        }

        $messages = [];
        foreach ($ObjectSet as $index => $Obj)
        {
            foreach ($properties as $property) {
                if (isset($Obj->$property) && $Obj->$property != '') {
                    $messages[$Obj->$property] = '';
                } else {
                    if (!isset($Obj->$property))
                        $this->log('        WARNING: Property ' . $property . ' not set at database resource `' . $resource . "'");
                }
            }
        }

        return $messages;
    }
    // End resolveDatabaseTranslation


    /**
     *
     */
    public function findByXGettext()
    {
        $config = Environment::getInstance()->getConfig();

        $tmpFile = new File();
        $tmpFile->createTemporaryFile();

        $cmd = sprintf("%s --from-code=UTF-8 -L PHP --keyword=t -o %s %s"
            , isset($config->translate->xgettext) ? $config->translate->xgettext : '/usr/bin/xgettext'
            , $tmpFile->getFilepath()
            , $this->file->getFilepath()
        );

        exec($cmd, $out);

        $translator = new Translator('en');
        $translator->addLoader('po', new PoFileLoader());
        $translator->addResource('po', $tmpFile->getFilepath(), 'en');

        $messages = [];
        $all = $translator->getCatalogue('en')->all();
        foreach ($all['messages'] as $msgId => $msgStr)
            $messages[$msgId] = $msgId;

        return $messages;
    }

    // End findByXGettext


    public function findUnderscoreCalls()
    {
        $fileContent = file_get_contents($this->file->getFilepath());

        $matches = [];
        preg_match_all('/\_\((.+?)\)\_/ums', $fileContent, $matches);

        if (!count($matches) || !isset($matches[1]))
            return [];

        $messages = [];
        foreach ($matches[1] as $i => $msgId)
        {
            $messages[$msgId] = $msgId;
        }
        return $messages;
    }
}
// End BlibsGettext