<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs;

/**
 * A simple TextView
 *
 * @package blibs
 * @author Tobias Lode <tobias@beibob.de>
 * @author     Fabian M�ller <fab@beibob.de>
 *
 */
class MediaView extends View
{
    /**
     * Media
     */
    private $Media;

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Constructs the view
     *
     * @param  Media $Media
     * @return -
     */
    public function __construct(Media $Media = null)
    {
        if(!is_null($Media))
            $this->setMedia($Media);
    }
    // End __construct

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the media
     *
     * @param  Media $Media
     * @return -
     */
    public function setMedia(Media $Media)
    {
        $this->Media = $Media;
    }
    // End setMedia

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the content as string
     *
     * @param  -
     * @return string
     */
    public function __toString()
    {
        if(!$this->Media->isInitialized())
            return '';

        return file_get_contents($this->Media->getFullPath());
    }
    // End __toString

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Will be called on initialization. Assigns the given argument array to this view
     *
     * @param  array $args
     * @return -
     */
    public function process(array $args = [], $module = null) {}

    //////////////////////////////////////////////////////////////////////////////////////
}
// End Viewable
