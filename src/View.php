<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs;

use Beibob\Blibs\Interfaces\Viewable;

/**
 * A base View
 *
 * @package blibs
 * @author Tobias Lode <tobias@beibob.de>
 * @author Fabian Möller <fab@beibob.de>
 *
 */
abstract class View implements Viewable
{
    /**
     * Keeps the module for this view
     */
    private $module;

    /**
     * keeps the view data
     */
    private $data = [];

    /*
     * Marks the view as processed
     */
    private $isProcessed = false;

    /**
     * Input and output encoding
     */
    private $inputEncoding = 'UTF-8';
    private $outputEncoding = 'UTF-8';

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the module
     */
    public function setModule($module)
    {
        $this->module = $module;
    }
    // End setModule

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the module
     *
     * @param  -
     * @return string
     */
    public function getModule()
    {
        return $this->module;
    }
    // End getModule

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Assigns a variable to the view for substitution
     *
     * @param  string $key
     * @param  mixed $value
     * @return -
     */
    public function assign($key, $value = true)
    {
        $this->data[$key] = $value;
    }
    // End assign

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns an assigned substitution variable
     *
     * @param  string $key
     * @return mixed
     */
    public function get($key, $defaultValue = null)
    {
        return isset($this->data[$key])? $this->data[$key] : $defaultValue;
    }
    // End get

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns true if a assigned substitution variable exists
     *
     * @param  string $key
     * @return boolean
     */
    public function has($key)
    {
        return isset($this->data[$key]);
    }
    // End has

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Processes the view
     *
     * Calls all necessary methods and hooks in the proper order to load,
     * initialize and render this view.
     *
     * @param  array $initArgs - init arguments
     * @param  string $module - module of the view if still unset
     * @return XmlDocument
     */
    public function process(array $initArgs = [], $module = null)
    {
        if($module && !$this->module)
            $this->setModule($module);

        $this->init($initArgs);
        $this->render();
        $this->isProcessed = true;

        return $this;
    }
    // End process

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns true if the view is processed
     */
    public function isProcessed()
    {
        return $this->isProcessed;
    }
    // End isProcessed

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the input encoding
     *
     * @param  -
     * @return string
     */
    public function getInputEncoding() { return $this->inputEncoding; }

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the output encoding
     *
     * @param  -
     * @return string
     */
    public function getOutputEncoding() { return $this->outputEncoding; }

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the input encoding
     *
     * @param  string
     * @return -
     */
    public function setInputEncoding($inputEncoding) { $this->inputEncoding = $inputEncoding; }

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the output encoding
     *
     * @param  string
     * @return -
     */
    public function setOutputEncoding($outputEncoding) { $this->outputEncoding = $outputEncoding; }

    //////////////////////////////////////////////////////////////////////////////////////
    // protected
    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Will be called on initialization. Assigns the given argument array to this view
     *
     * @param  array $args
     * @return -
     */
    protected function init(array $args = [])
    {
        if(is_null($args))
            return;

        foreach($args as $name => $value)
            $this->assign($name, $value);
    }
    // End init

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Renders the view
     *
     * @param  -
     * @return -
     */
    protected function render() {}
    // End _render

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns all assigned data
     *
     * @param  -
     * @return array
     */
    protected function getData()
    {
        return $this->data;
    }
    // End getData

    //////////////////////////////////////////////////////////////////////////////////////
}
// End View
