<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs;

/**
 * A class representing a timestamp
 *
 * @package blibs
 * @author Fabian Möller  <fab@beibob.de>
 * @author Tobias Lode <tobias@beibob.de>
 *
 */
class BlibsTime
{
    /**
     * Hour
     */
    protected $hour = null;

    /**
     * Minutes
     */
    protected $minutes = null;

    /**
     * Seconds
     */
    protected $seconds = null;

    /**
     * Timezone
     */
    protected $timezone = null;

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * 24-hour format of an hour with leading zeros
     */
    const FORMAT_HOUR_24 = 'H';

    /**
     * Alias fuer FORMAT_HOUR_24
     */
    const FORMAT_HOUR = 'H';

    /**
     * 12-hour format of an hour with leading zeros
     */
    const FORMAT_HOUR_12 = 'h';

    /**
     * 24-hour format of an hour without leading zeros
     */
    const FORMAT_HOUR_24_INT = 'G';

    /**
     * 12-hour format of an hour without leading zeros
     */
    const FORMAT_HOUR_12_INT = 'g';

    /**
     * Uppercase Ante meridiem and Post meridiem AM and PM
     */
    const FORMAT_MERIDIEM = 'A';

    /**
     * Lowercase Ante meridiem and Post meridiem am and pm
     */
    const FORMAT_MERIDIEM_LC = 'a';

    /**
     * Minutes with leading zeros
     */
    const FORMAT_MINUTES = 'i';

    /**
     * Seconds, with leading zeros
     */
    const FORMAT_SECONDS = 's';

    /**
     * Difference to Greenwich time (GMT) with colon between
     * hours and minutes (added in PHP 5.1.3), +02:00
     */
    const FORMAT_TIMEZONE = 'P';

    /**
     * Difference to Greenwich time (GMT) in hours, +0200
     */
    const FORMAT_TIMEZONE_SMALL = 'O';

    /**
     * ISO time string format
     */
    const FORMAT_ISO_TIME = 'H:i:sP';

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Time Factory
     *
     * @param  mixed   $time   - may either a Time object, a time string
     * @param  string  $format - [only if $time is a date string]
     * @return BlibsTime
     */
    public static function factory($time = false, $format = false)
    {
        return new BlibsTime($time, $format);
    }
    // End factory

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Constructs the object
     *
     * @param  mixed   $time   - may either a Time object, a time string
     * @param  string  $format - [only if $time is a date string]
     * @return object
     */
    public function __construct($time = false, $format = false)
    {
        if(is_string($time))
            $this->setTimeString($time, $format);

        elseif(is_array($time))
            $this->setTimeArray($time);

        elseif(is_object($time))
            $this->setTime($time);

        elseif(is_numeric($time))
            $this->setTimestamp($time);

        elseif($time === false)
            $this->setTimestamp();

        else
            $this->setInvalid();
    }
    // End __construct

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the current iso (or format specific) date time
     *
     * @param  string $format
     * @return string
     */
    public static function now($format = false)
    {
        $Now = new BlibsTime(false, $format);
        return $Now->getTimeString($format);
    }
    // End now

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Inits the object by tosecondss time
     *
     * @param  -
     * @return boolean
     */
    public function setNow()
    {
        return $this->setTimestamp();
    }
    // End setToday

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Inits the object by another Time object
     *
     * @param  BlibsTime $Time
     * @return boolean
     */
    public function setTime(BlibsTime $Time)
    {
        if(!$Time->isValid())
            return false;

        return $this->setTimeParts($Time->getHour(), $Time->getMinutes(), $Time->getSeconds(), $Time->getTimezone());
    }
    // End initByTime

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Inits the object by a time string. the format string expects
     * the format conventions (see FORMAT_* constants above)
     *
     * Order of placeholders does not matter
     *
     * @param  string $timeStr
     * @param  string $format
     * @return boolean
     */
    public function setTimeString($timeStr, $format = false)
    {
        if(!$format)
            return $this->setISOTimeString($timeStr);

        if(empty($timeStr))
            return false;

        list($hour, $minutes, $seconds, $timezone) = $this->parseTimeString($timeStr, $format);

        return $this->setTimeParts($hour, $minutes, $seconds, $timezone);
    }
    // End initByTimeString

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets time by an array. The array must contain the keys hour, minutes and seconds
     *
     * @param  array $dataArray
     * @return boolean
     */
    public function setTimeArray(array $timeArray)
    {
        /**
         * TimeArray must contain at least an hour or hour and minutes or hour, minutes and seconds!
         * or the ones that are specified in $format
         */
        if(!$this->checkValidTimeArray($timeArray))
            return false;

        return $this->setTimeParts($timeArray['hour'], $timeArray['minutes'], $timeArray['seconds']);
    }
    // End setTimeArray

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Inits the object by a iso time string format HH24-MM-SS[TZ]
     *
     * @param  string $timeStr
     * @return boolean
     */
    public function setISOTimeString($timeStr)
    {
        if(empty($timeStr))
            return false;

        list($hour, $minutes, $seconds, $timezone) = $this->parseISOTimeString($timeStr);

        return $this->setTimeParts($hour, $minutes, $seconds, $timezone);
    }
    // End initByISOTimeString

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Inits the object by an unix timestamp
     *
     * @param  integer $timestamp - optional, if false time() is used
     * @return boolean
     */
    public function setTimestamp($timestamp = false)
    {
        if($timestamp === false)
        {
            $timeArr = getdate();
            $timezone = false;
        }
        else
        {
            $timeArr = getdate($timestamp);
            $timezone = date("O", $timestamp);
        }

        return $this->setTimeParts($timeArr['hours'], $timeArr['minutes'], $timeArr['seconds'], $timezone);
    }
    // End initByTimestamp

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the time parts
     * Standard behaviour is to check the time parts. if any of them is invalid, the method
     * returns false without setting the time.
     *
     * @param  mixed  $hour
     * @param  mixed  $minutes
     * @param  mixed  $seconds
     * @param  mixed  $timezone  - +1:00 or Z or GMT or UTC
     * @return boolean
     */
    public function setTimeParts($hour = false, $minutes = false, $seconds = false, $timezone = false)
    {
        if(!$this->checkTimeParts($hour, $minutes, $seconds))
        {
            $this->setInvalid();
            return false;
        }

        $this->hour     = $hour     !== false? (int)$hour     : 0;
        $this->minutes  = $minutes  !== false? (int)$minutes  : 0;
        $this->seconds  = $seconds  !== false? (int)$seconds : 0;
        $this->timezone = is_int($timezone)? $timezone : $this->parseTimezone($timezone);

        return true;
    }
    // End setTimeParts

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Unsets the time object. This makes it invalid
     *
     * @param  -
     * @return -
     */
    public function setInvalid()
    {
        $this->hour     = null;
        $this->minutes  = null;
        $this->seconds  = null;
        $this->timezone = null;
    }
    // End setInvalid

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns a formated time string
     *
     * @param  string $format
     * @return string
     */
    public function getTimeString($format = false)
    {
        if(!$this->isValid())
            return '';

        if(!$format)
            return $this->getISOTimeString();

        return strtr( $format, [self::FORMAT_HOUR_24        => $this->getHour('H'),
                                     self::FORMAT_HOUR_24_INT    => $this->getHour('G'),
                                     self::FORMAT_HOUR_12        => $this->getHour('h'),
                                     self::FORMAT_HOUR_12_INT    => $this->getHour('g'),
                                     self::FORMAT_MINUTES        => $this->getMinutes('i'),
                                     self::FORMAT_SECONDS        => $this->getSeconds('s'),
                                     self::FORMAT_MERIDIEM       => $this->getMeridiem('A'),
                                     self::FORMAT_MERIDIEM_LC    => $this->getMeridiem('a'),
                                     self::FORMAT_TIMEZONE       => $this->getTimezone('P'),
                                     self::FORMAT_TIMEZONE_SMALL => $this->getTimezone('O')
                                     ]
                      );
    }
    // End getTimeString

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns a iso formated time string
     *
     * @param  -
     * @return string
     */
    public function getISOTimeString()
    {
        if(!$this->isValid())
            return false;

        return $this->getHour().":".$this->getMinutes().":".$this->getSeconds(). $this->getTimezone();
    }
    // End getISOTimeString

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns a time array
     *
     * @param  -
     * @return array
     */
    public function getTimeArray()
    {
        if(!$this->isValid())
            return false;

        return ['hour'     => $this->getHour(),
                     'minutes'  => $this->getMinutes(),
                     'seconds'  => $this->getSeconds(),
                     'meridiem' => $this->getMeridiem(),
                     'timezone' => $this->getTimezone()
                     ];
    }
    // End getTimeArray

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the todays timestamp
     *
     * @param  -
     * @return integer
     */
    public function getUnixtime()
    {
        $Today = new BlibsDate();
        $Today->setToday();

        $BlibsDateTime = new BlibsDateTime();
        $BlibsDateTime->setDateTimeObjects($Today, $this);

        return $BlibsDateTime->getUnixtime();
    }
    // End getTimestamp

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the hour part of the time, if either format is empty or
     * contains a hour format specifier
     *
     * @param  string $format
     * @return string
     */
    public function getHour($format = false)
    {
        if($format === false || strpos($format, 'H') !== false)
        {
            $hour    = $this->hour;
            $padding = true;
        }
        elseif(strpos($format, 'G') !== false)
        {
            $hour = $this->hour;
            $padding = false;
        }
        elseif(strpos($format, 'h') !== false)
        {
            $hour    = $this->hour % 12;
            $padding = true;
        }
        elseif(strpos($format, 'g') !== false)
        {
            $hour    = $this->hour % 12;
            $padding = false;
        }
        else
        {
            return false;
        }

        if($padding)
            $hour = str_pad($hour, 2, '0', STR_PAD_LEFT);

        return $hour;
    }
    // End getHour

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the meridiem part
     *
     * @param  string $format
     * @return string
     */
    public function getMeridiem($format = false)
    {
        if($format === false || strpos($format, 'A') !== false)
            $meridiem = $this->hour > 11? 'PM' : 'AM';

        elseif(strpos($format, 'a') !== false)
            $meridiem = $this->hour > 11? 'pm' : 'am';

        else
            return false;

        return $meridiem;
    }
    // End getMeridiem

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the minutes part of the time, if either format is empty or contains a minutes format specifier
     *
     * @param  string $format - output format
     * @return mixed
     */
    public function getMinutes($format = false)
    {
        if($format === false || strpos($format, 'i') !== false)
        {
            $minutes = str_pad($this->minutes, 2, '0', STR_PAD_LEFT);
        }
        else
            return false;

        return $minutes;
    }
    // End getMinutes

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the seconds part of the time, if either format is empty or
     * contains a seconds format specifier
     *
     * @param  string $format
     * @return mixed
     */
    public function getSeconds($format = false)
    {
        if($format === false || strpos($format, 's') !== false)
        {
            $seconds = str_pad($this->seconds, 2, '0', STR_PAD_LEFT);
        }
        else
            return false;

        return $seconds;
    }
    // End getSeconds

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the timezone
     *
     * @param  string $format
     * @return string
     */
    public function getTimezone($format = false)
    {
        if($format === false || strpos($format, 'P') !== false)
        {
            if(is_null($this->timezone))
            {
                $timezone = date('P');
            }
            elseif($this->timezone > 0)
            {
                $Time = new BlibsTime((string)$this->timezone, 'Gi');
                $timezone = '+'. $Time->getTimeString('H:i');
            }
            elseif($this->timezone < 0)
            {
                $Time = new BlibsTime((string)$this->timezone, 'Gi');
                $timezone = '-'. $Time->getTimeString('H:i');
            }
            else
            {
                $timezone = 'Z';
            }
        }
        elseif(strpos($format, 'O') !== false)
        {
            if(is_null($this->timezone))
                $timezone = date('O');

            elseif($this->timezone > 0)
            {
                $Time = new BlibsTime((string)$this->timezone, 'Gi');
                $timezone = '+'. $Time->getTimeString('Hi');
            }
            elseif($this->timezone < 0)
            {
                $Time = new BlibsTime((string)$this->timezone, 'Gi');
                $timezone = '-'. $Time->getTimeString('Hi');
            }
            else
            {
                $timezone = 'Z';
            }
        }
        else
            return false;

        return $timezone;
    }
    // End getTimezone

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Checks if the Time object represents a valid time
     *
     * @param  -
     * @return boolean
     */
    public function isValid()
    {
        return !(is_null($this->seconds) && is_null($this->minutes) && is_null($this->hour));
    }
    // End isValid

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Static method to verify time parts
     *
     * @param  int $hour
     * @param  int $minutes
     * @param  int $seconds
     * @return boolean
     */
    public function checkTimeParts($hour = false, $minutes = false, $seconds = false)
    {
        if($hour < 0 || $hour > 23)
            return false;

        if($minutes < 0 || $minutes > 59)
            return false;

        if($seconds < 0 || $seconds > 59)
            return false;

        return true;
    }
    // End checkTimeParts

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Overwrites the php-method __toString for converting an object to string
     *
     * @param  --
     * @return string
     */
    public function __toString()
    {
        return (string)$this->getISOTimeString();
    }
    // End __toString

    //////////////////////////////////////////////////////////////////////////////////////
    // protected
    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Parses a time string by a given format
     *
     * not really performant
     *
     * @param  string $timestr
     * @param  string $format
     * @return array
     */
    protected function parseTimeString($timeStr, $format)
    {
        $found   = [];
        $pattern = '';

        for($position = 0; $position < utf8_strlen($format); $position++)
        {
            switch($format{$position})
            {
                case self::FORMAT_HOUR_24:
                    $found[]  = self::FORMAT_HOUR_24;
                    $pattern .= '(\d{2})';
                    break;

                case self::FORMAT_HOUR_12:
                    $found[]  = self::FORMAT_HOUR_12;
                    $pattern .= '(\d{2})';
                    break;

                case self::FORMAT_HOUR_24_INT:
                    $found[]  = self::FORMAT_HOUR_24_INT;
                    $pattern .= '(\d{1,2})';
                    break;

                case self::FORMAT_HOUR_12_INT:
                    $found[]  = self::FORMAT_HOUR_12_INT;
                    $pattern .= '(\d{1,2})';
                    break;

                case self::FORMAT_MERIDIEM:
                    $found[]  = self::FORMAT_MERIDIEM;
                    $pattern .= '(AM|PM)';
                    break;

                case self::FORMAT_MERIDIEM_LC:
                    $found[]  = self::FORMAT_MERIDIEM_LC;
                    $pattern .= '(am|pm)';
                    break;

                case self::FORMAT_MINUTES:
                    $found[]  = self::FORMAT_MINUTES;
                    $pattern .= '(\d{2})';
                    break;

                case self::FORMAT_SECONDS:
                    $found[]  = self::FORMAT_SECONDS;
                    $pattern .= '(\d{2})\.?\d{0,6}';
                    break;

                case self::FORMAT_TIMEZONE:
                    $found[]  = self::FORMAT_TIMEZONE;
                    $pattern .= '([+\-]\d{2}:?\d{0,2}|\s?Z|\s?GMT|\s?UTC|\s?z|\s?gmt|\s?utc)';
                    break;

                case self::FORMAT_TIMEZONE_SMALL:
                    $found[]  = self::FORMAT_TIMEZONE_SMALL;
                    $pattern .= '([+\-]\d{4}|\s?Z|\s?GMT|\s?UTC|\s?z|\s?gmt|\s?utc)';
                    break;

                case '.':
                case '/':
                case '(':
                case ')':
                case '-':
                    $pattern .= '\\'.$format{$position};
                    break;

                default:
                    $pattern .= $format{$position};
                    break;
            }
        }

        $meridiem = $hour = $minutes = $seconds = $timezone = null;

        if(preg_match("/".$pattern."/u", $timeStr, $matches))
        {
            foreach($found as $index => $format)
            {
                switch($format)
                {
                    case self::FORMAT_HOUR_24:
                    case self::FORMAT_HOUR_24_INT:
                        $hour = $matches[$index + 1];
                        break;

                    case self::FORMAT_HOUR_12:
                    case self::FORMAT_HOUR_12_INT:
                        $hour = $matches[$index + 1];
                        $meridiem = 'AM';
                        break;

                    case self::FORMAT_MINUTES:
                        $minutes = $matches[$index + 1];
                        break;

                    case self::FORMAT_SECONDS:
                        $seconds = $matches[$index + 1];
                        break;

                    case self::FORMAT_MERIDIEM:
                    case self::FORMAT_MERIDIEM_LC:
                        $meridiem = $matches[$index + 1];
                        break;

                    case self::FORMAT_TIMEZONE:
                    case self::FORMAT_TIMEZONE_SMALL:
                        $timezone = $matches[$index + 1];
                        break;
                }
            }
        }

        /**
         * Normalize hour
         */
        if($meridiem &&
           utf8_strtolower($meridiem) == 'pm')
            $hour = 12 + $hour % 12;

        $timeArr[] = is_numeric($hour)?    (int)$hour    : null;
        $timeArr[] = is_numeric($minutes)? (int)$minutes : null;
        $timeArr[] = is_numeric($seconds)? (int)$seconds : null;
        $timeArr[] = $timezone?            $timezone     : null;

        return $timeArr;
    }
    // End parseTimeString

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Parses a iso time string format
     *
     * @param  string $timestr
     * @return array
     */
    protected function parseISOTimeString($timeStr)
    {
        $hour = $minutes = $seconds = $timezone = null;

        if(preg_match('/^(\d{2}):(\d{2}):(\d{2})([+\-]\d{2}:?\d{0,2}|\s?Z|\s?GMT|\s?UTC)?$/ui', $timeStr, $timeArr))
        {
            $hour     = $timeArr[1];
            $minutes  = $timeArr[2];
            $seconds  = $timeArr[3];
            $timezone = $this->parseTimezone($timeArr[4]);
        }

        return [$hour, $minutes, $seconds, $timezone];
    }
    // End parseISOTimeString

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Checks if the time array is valid. If strict is set to true, all time keys has to be set.
     * Otherwise at least a hour must be specified
     *
     * @param  array  $timeArray
     * @return boolean
     */
    protected function checkValidTimeArray($timeArray)
    {
        if (!is_array($timeArray) || !isset($timeArray['hour']) || !isset($timeArray['minutes']) || !isset($timeArray['seconds']))
            return false;

        return true;
    }
    // End checkValidTimeArray

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Parses a timezone string and returns its +/-
     *
     * @param  string $timezone
     * @return -
     */
    protected function parseTimezone($timezoneStr)
    {
        $timezoneStr = trim($timezoneStr);

        if(!preg_match('/(([+\-])(\d{2}):?(\d{2})?|\s?Z|\s?GMT|\s?UTC)/ui', $timezoneStr, $matches))
            return null;

        switch($matches[2])
        {
            case '+':
                $min = isset($matches[4]) && $matches[4] >= 0 && $matches[4] < 60? $matches[4] : '00';
                $timezone = (int)($matches[3]. $min);

                break;

            case '-':
                $min = isset($matches[4]) && $matches[4] >= 0 && $matches[4] < 60? $matches[4] : '00';
                $timezone = (int)($matches[3]. $min) * (-1);
                break;

            default:
                $timezone = 0;
        }

        return (int)$timezone;
    }
    // End parseTimezone

    //////////////////////////////////////////////////////////////////////////////////////
}
// End class BlibsTime
