<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs;

/**
 * String Manipulation Helperclass
 *
 * @package blibs
 * @author Fabian Möller  <fab@beibob.de>
 * @author Tobias Lode <tobias@beibob.de>
 *
 */
class StringFactory
{
    /**
     * Baut aus einem _-enthaltenden String einen Camelcase-String
     * aus ich_bin_geil wird IchBinGeil
     *
     * @param  string $input -
     * @return string $seperator - Das Zeichen nach dem seperiert werden soll
     */
    public static function camelCase($input, $seperator = '_')
    {
        $parts = preg_split("/[" . $seperator . "]/u", $input, -1, PREG_SPLIT_NO_EMPTY);

        $output = '';
        foreach($parts as $part)
        {
            $output .= utf8_ucfirst($part);
        }

        return $output;
    }
    // End normalizePropertyName

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Ersetzt Großbuchstaben durch _-Kleinbuchstabe
     * aus IchBinGeil wird ich_bin_geil
     *
     * @param  string $input -
     * @return string $seperator - Das Zeichen nach dem seperiert werden soll
     */
    public static function unCamelCase($input, $separator = '_')
    {
        $bits = self::splitCamelCase($input);

        $a = [];
        for($i = 0; $i < count($bits); ++$i)
        {
            if (preg_match('/^[A-Z]$/u', $bits[$i])) {
                $a[$i] = ($i > 0 ? $separator : '') . utf8_strtolower($bits[$i]);
            } elseif (preg_match('/^\d+$/u', $bits[$i])) {
                $a[$i] = ($i > 0 ? $separator : '') . $bits[$i];
            } else {
                $a[$i] = $bits[$i];
            }
        }

        return join('', $a);
    }
    // End unCamelCase


    /**
     * @param      $input
     * @param bool $limit
     * @return array
     */
    public static function splitCamelCase($input, $limit = false)
    {
        return preg_split('/([A-Z])|(\d+)/u', $input, $limit, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY);
    }

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Entfernt doppelte leerzeichen
     */
    public static function removeHorizontalWhitespace($string)
    {
        if (!is_string($string))
            return false;

        return trim(preg_replace('|\h+|u', ' ', $string));
    }
    // End removeHorizontalWhitespace

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Trimmt alle Werte der übergeben Liste
     */
    public static function arrayValueTrim($array)
    {
        return array_map('trim', $array);
    }
    // End arrayValueTrim

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Checks if the given string is encoded in utf8
     *
     * @param  string $input
     * @return boolean
     */
    public static function isUTF8($input)
    {
        return utf8_encode(utf8_decode($input)) == $input;
    }
    // End isUTF8

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Kürzt einen String bei Bedar auf die angegebene Anzahl von Zeichen und
     * hängt optional pünktchen an.
     */
    public static function stringCut($value, $maxLength, $replacer = false)
    {
        if ($maxLength && utf8_strlen($value) > $maxLength)
        {
            $cut = utf8_substr($value, 0, $maxLength);

            if ($replacer)
                $cut .= is_string($replacer)? $replacer : '...';

            return $cut;
        }

        return $value;
    }
    // End stringCut

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Cuts a string to given length without cut words
     */
    public static function stringCutWordSafe($string, $length, $replacer = '...')
    {
        if(utf8_strlen($string) > $length)
              return (preg_match('/^(.*)\W.*$/u', utf8_substr($string, 0, $length+1), $matches) ? $matches[1] : utf8_substr($string, 0, $length)) . $replacer;

          return $string;
    }
    // End stringCutWordSafe

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Cuts a string to given length without cut words
     */
    public static function stringMidCut($string, $length, $replacer = '...', $replacerPosition = null)
    {
        if(utf8_strlen($string) > $length)
        {
            if(!is_null($replacerPosition))
                $len = $replacerPosition;
            else
                $len = intval($length / 2);
            return utf8_substr($string, 0, $len). $replacer . utf8_substr($string, -1 * ($length - $len), $length - $len);
        }

        return $string;
    }
    // End stringMidCut

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     *
     */
    public static function getRandomString($lenght = 8)
    {
        $chars = "abcdefghijklmnopqrstuvwABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";

        $string = '';
        for ($i = 0; $i < $lenght; $i++)
        {
            $string .= $chars{rand(0, utf8_strlen($chars) - 1)};
        }
        return $string;
    }
    // End getRandomString

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Converts an PHP-Array to javascript object in literal notation
     *
     * @param  XmlDocument $Context
     * @return XmlDocument
     */
    public static function buildJsLiteralNotation(array $args = [], $escapeString = "\"")
    {
        $pairs = [];
        foreach ($args as $key => $value)
            $pairs[] = $key . ':' . $escapeString . $value . $escapeString;

        return '{' . join(',', $pairs) . '}';
    }
    // End buildJsLiteralNotation

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Formats goven number in english ordinal
     */
    public static function formatOrdinalNumber($number)
    {
        $number = (int) $number;

        $ends = ['th','st','nd','rd','th','th','th','th','th','th'];
        if (($number %100) >= 11 && ($number%100) <= 13)
           $abbreviation = $number. 'th';
        else
           $abbreviation = $number. $ends[$number % 10];

        return $abbreviation;
    }
    // End formatOrdinalNumber

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * @param        $value
     * @param string $unit
     *
     * @return string
     */
    public static function formatCurrency($value, $unit = 'EUR')
    {
        return number_format ($value,2 , ',', '.') . ' ' . $unit;
    }
    // End formatCurrency

    /**
     * Cleans up a string, e.g. to safely represent a url or filename
     */
    public static function toAscii($str, $delimiterReplacement = '-', $customDelimiters = [])
    {
        if (!empty($customDelimiters)) {
            $str = \str_replace((array)$customDelimiters, ' ', $str);
        }

        $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
        $clean = \preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
        $clean = \preg_replace("/[\/_|+ -]+/", $delimiterReplacement, $clean);
        $clean = \trim($clean, $delimiterReplacement);

        return $clean;
    }

}
// End StringFactory
