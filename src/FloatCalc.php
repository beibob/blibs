<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs;

/**
 * Static helper methods for calculations
 */
class FloatCalc
{
    /**
     * Compares to floats
     *
     * @param  float $a
     * @param  float $b
     * @param  float $epsilon - precision
     * @return boolean
     */
    public static function cmp($a, $b, $epsilon = 0.0000000001)
    {
        $absA = abs($a);
        $absB = abs($b);
        $diff = abs($a - $b);

        if($a == $b)
            // shortcut, handles infinities
            return true;

        else if($a * $b == 0) // a or b or both are zero
            // relative error is not meaningful here
            return $diff < ($epsilon * $epsilon);

        // use relative error
        return $diff / ($absA + $absB) < $epsilon;
    }
    // End floatCmp

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns true if $a is greater then $b with precision $epsilon,
     * false otherwise
     *
     * @param  float $a
     * @param  float $b
     * @param  float $epsilon - precision
     * @return boolean
     */
    public static function gt($a, $b, $epsilon = 0.00000000001)
    {
        return $a - $b > $epsilon;
    }
    // End floatGt

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns true if $a is greater then or equal to$b with precision $epsilon,
     * false otherwise
     *
     * @param  float $a
     * @param  float $b
     * @param  float $epsilon - precision
     * @return boolean
     */
    public static function ge($a, $b, $epsilon = 0.00000000001)
    {
        return $a - $b > $epsilon || self::cmp($a, $b, $epsilon);
    }
    // End floatGe

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns true if $a is lesser then $b with precision $epsilon,
     * false otherwise
     *
     * @param  float $a
     * @param  float $b
     * @param  float $epsilon - precision
     * @return boolean
     */
    public static function lt($a, $b, $epsilon = 0.00000000001)
    {
        return $a - $b < $epsilon;
    }
    // End floatLt

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns true if $a is lesser then or equal to $b with precision $epsilon,
     * false otherwise
     *
     * @param  float $a
     * @param  float $b
     * @param  float $epsilon - precision
     * @return boolean
     */
    public static function le($a, $b, $epsilon = 0.00000000001)
    {
        return $a - $b < $epsilon || self::cmp($a, $b, $epsilon);
    }
    // End floatLe

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Linear interpolation
     *
     * y = y1 + ((y2 - y1) / (x2 - x1)) ( x - x1)
     *
     * @param  float $x
     * @param  float $y1
     * @param  float $y2
     * @param  float $x1
     * @param  float $x2
     * @return float
     */
    public static function linInterpol($x, $y1, $y2, $x1, $x2)
    {
        return $y1 + ( ($y2 - $y1) / ($x2 - $x1) ) * ($x - $x1);
    }
    // End floatLinInterpol

    //////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Calculates bnb benchmark index from the input value
	 *
	 * @param array $benchmarkValues
	 * @param       float input
	 *
	 * @return float
	 */
    public static function computeBenchmark(array $benchmarkValues, $input)
    {
		if (!count($benchmarkValues))
			return null;

	    $scores = array_keys($benchmarkValues);
	    $firstScore = $scores[0];
	    $firstScoreVal = $benchmarkValues[$firstScore];
	    $lastScore = $scores[count($scores) - 1];
	    $lastScoreVal = $benchmarkValues[$lastScore];

        if (self::lt($input, $firstScoreVal)) return $firstScore;
	    if (self::gt($input, $lastScoreVal))  return $lastScore;

	    $lastIndex = $result = null;
	    $lastValue = 0;
	    foreach($benchmarkValues as $index => $value)
        {
            if(self::gt($input, $lastValue) && self::le($input, $value))
            {
                $result = $lastValue? self::linInterpol($input, $lastIndex, $index, $lastValue, $value) : $index;
                break;
            }

            $lastValue = $value;
            $lastIndex = $index;
        }

        return is_null($result)? $lastIndex : $result;
    }
    // End computeBenchmark

    //////////////////////////////////////////////////////////////////////////////////////
}
// End FloatCalc
