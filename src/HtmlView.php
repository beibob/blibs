<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs;

use DOMElement;
use DOMNode;
use DOMText;
use ErrorException;

/**
 * A HtmlView class
 *
 * @package blibs
 * @author Tobias Lode <tobias@beibob.de>
 * @author Fabian Möller <fab@beibob.de>
 *
 * @method DOMText    getText($text = '')
 * @method DOMElement getP($content, array $attributes = [])
 * @method DOMElement getPre($content, array $attributes = [])
 * @method DOMElement getH1($content, array $attributes = [])
 * @method DOMElement getH2($content, array $attributes = [])
 * @method DOMElement getH3($content, array $attributes = [])
 * @method DOMElement getH4($content, array $attributes = [])
 * @method DOMElement getH5($content, array $attributes = [])
 * @method DOMElement getSpan($content = null, array $attributes = [])
 * @method DOMElement getSub($content = null, array $attributes = [])
 * @method DOMElement getSup($content = null, array $attributes = [])
 * @method DOMElement getStrong($content = null, array $attributes = [])
 * @method DOMElement getEm($content = null, array $attributes = [])
 * @method DOMElement getLabel($content = null, array $attributes = [])
 * @method DOMElement getNobr($content = null, array $attributes = [])
 * @method DOMElement getScript($content, array $attributes = [])
 * @method DOMElement getStyle($content, array $attributes = [])
 * @method DOMElement getForm(array $attributes = [])
 * @method DOMElement getBr()
 * @method DOMElement getSelect(array $attributes = [])
 * @method DOMElement getOption(array $attributes = [], $content = null)
 * @method DOMElement getOptGroup(array $attributes = [])
 * @method DOMElement getA(array $attributes = [], $content = null)
 * @method DOMElement getDiv(array $attributes = [], $Content = null)
 * @method DOMElement getUl(array $attributes = [], DOMElement $Content = null)
 * @method DOMElement getOl(array $attributes = [], DOMElement $Content = null)
 * @method DOMElement getLi(array $attributes = [], $content = null)
 * @method DOMElement getTable(array $attributes = [])
 * @method DOMElement getTHead(array $attributes = [])
 * @method DOMElement getTFoot(array $attributes = [])
 * @method DOMElement getTBody(array $attributes = [])
 * @method DOMElement getTr(array $attributes = [], DOMElement $Content = null)
 * @method DOMElement getTd(array $attributes = [], DOMNode $Content = null)
 * @method DOMElement getTh(array $attributes = [], DOMNode $Content = null)
 * @method DOMElement getButton(array $attributes = [], DOMElement $Content = null)
 * @method DOMElement getDl(array $attributes = [], DOMElement $Content = null)
 * @method DOMElement getDt(array $attributes = [], $content = null)
 * @method DOMElement getDd(array $attributes = [], $content = null)
 * @method DOMElement getImg(array $attributes = [])
 * @method DOMElement getInput(array $attributes = [])
 * @method DOMElement getTextarea(array $attributes = [], $content = '')
 * @method void       addClass(DOMElement $Elt, $cssClass)
 * @method boolean    hasClass(DOMElement $Elt, $cssClass)
 * @method void       removeClass(DOMElement $Elt, $cssClass)
 * @method DOMElement getImage($src, array $attributes = [])
 * @method DOMElement getContainer($cssClass = null, DOMElement $Content = null)
 * @method void       appendMultilineAsPTags($Container, $text, $ignoreEmptyParagraphs = true, $cssClass = null)
 * @method DOMElement getHiddenInput($name, $value)
 */
class HtmlView extends XmlDocument
{
    /**
     * HtmlDOMFactory
     *
     * @var HtmlDOMFactory $HtmlDOMFactory
     */
    private $HtmlDOMFactory;

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Constructs the Document
     *
     * @param bool   $tplName
     * @param bool   $module
     * @param string $version
     * @param string $charSet
     */
    public function __construct($tplName = false, $module = false, $version = '1.0', $charSet = 'UTF-8')
    {
        parent::__construct($tplName, $module, $version, $charSet);

        // init DOM Factory
        $this->HtmlDOMFactory = new HtmlDOMFactory($this);
    }
    // End __construct

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Magic method __call
     *
     * @param  string $function
     * @param  array $args
     * @throws ErrorException
     * @return mixed
     */
    public function __call($function, $args)
    {
        if(method_exists($this->HtmlDOMFactory, $function))
            return call_user_func_array([$this->HtmlDOMFactory, $function], $args);

        throw new ErrorException('Method '.get_class($this). '::'.$function.' does not exist');
    }
    // End __call

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the output of the document
     *
     * @param  -
     * @return string
     */
    public function __toString()
    {
        return $this->saveHtml();
    }
    // End getOutput

    //////////////////////////////////////////////////////////////////////////////////////
}
// End HtmlView
