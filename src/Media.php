<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs;

use PDO;

/**
 *
 * @package blibs
 * @author     Fabian Möller  <fab@beibob.de>
 * @author Tobias Lode <tobias@beibob.de>
 *
 */
class Media extends DbObject
{
    /**
     * Tablename
     */
    const TABLE_NAME = 'public.media';

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * mediaId
     */
    private $id;

    /**
     * mediaIdent
     */
    private $ident;

    /**
     * name
     */
    private $name;

    /**
     * base-relative path
     */
    private $relPath;

    /**
     * mimeType
     */
    private $mimeType;

    /**
     * image width
     */
    private $width;

    /**
     * image width
     */
    private $height;

    /**
     * creation time
     */
    private $created;

    /**
     * file name
     */
    private $fileName;

    /**
     * file name
     */
    private $extension;

    /**
     * Media directory
     */
    private $mediaDir;

    /**
     * temp path
     */
    private $tempPath;

    /**
     * temp path
     */
    private $sourceMediaId;

    /**
     * Primary key
     */
    private static $primaryKey = ['id'];

    /**
     * Column types
     */
    private static $columnTypes = ['id'             => PDO::PARAM_INT,
                                        'name'           => PDO::PARAM_STR,
                                        'fileName'       => PDO::PARAM_STR,
                                        'extension'      => PDO::PARAM_STR,
                                        'relPath'        => PDO::PARAM_STR,
                                        'mimeType'       => PDO::PARAM_STR,
                                        'width'          => PDO::PARAM_INT,
                                        'height'         => PDO::PARAM_INT,
                                        'sourceMediaId'  => PDO::PARAM_INT,
                                        'ident'          => PDO::PARAM_STR,
                                        'created'        => PDO::PARAM_STR];

    /**
     * Extended column types
     */
    private static $extColumnTypes = [];

    //////////////////////////////////////////////////////////////////////////////////////
    // public
    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Creates the object
     *
     * @param  string   $filePath - path to file
     * @param  string   $mediaDir - path to mediaDir
     * @param  string   $fileName - alternative filename
     * @return Media
     */
    public static function create($filePath, $mediaDir = false, $fileName = null, $sourceMediaId = null, $ident = null)
    {
        if(!is_file($filePath))
            throw new Exception('File `'.$filePath.'\' does not exist');

        $Media = new Media();
        $Media->setMediaDir($mediaDir);
        $Media->setName(basename($filePath));
        $Media->setExtension(File::factory($filePath)->getExtension());
        $Media->setFileName($fileName);
        $Media->setSourceMediaId($sourceMediaId);
        $Media->setIdent($ident);

        if($Media->getValidator()->isValid())
        {
            try
            {
                if(!is_null($fileName) && file_exists($Media->getMediaDir() . $fileName))
                    throw new Exception('A file with name `'.$fileName.'\' already exists');

                $Media->tempPath = tempnam($Media->getMediaDir(), 'tmp_');
                chmod($Media->tempPath, 0775);
                File::copy($filePath, $Media->tempPath);
                $Media->insert();
            }
            catch(Exception $Exception)
            {
                /**
                 * Delete copied file in case of a error occurs
                 */
                if(file_exists($Media->tempPath))
                    unlink($Media->tempPath);

                /**
                 * And let everyone know what's gone wrong
                 */
                throw $Exception;
            }
        }
        else
        {
            Log::getInstance('fb')->debug($Media->getValidator()->getErrors());
        }

        return $Media;
    }
    // End create

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Creates the object
     *
     * @param  string   $filePath - path to file
     * @param  string   $mediaDir - path to mediaDir
     * @param  string   $fileName - alternative filename
     * @return Media
     */
    public static function createByUpload($fileArrayKey, $mediaDir = false, $fileName = null, $sourceMediaId = null, $ident = null)
    {
        $destFilePath = File::moveUploadedFile($fileArrayKey, sys_get_temp_dir());

        $Media = self::create($destFilePath, $mediaDir, $fileName, $sourceMediaId = null, $ident = null);

        if (file_exists($destFilePath))
            unlink($destFilePath);

        return $Media;
    }
    // End createByUpload

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Create from File object
     *
     * @param  File $File
     * @param  string   $mediaDir - path to mediaDir
     * @param  string   $fileName - alternative filename
     * @return Media
     */
    public static function createFromFile(File $File, $mediaDir = false, $fileName = null, $sourceMediaId = null, $ident = null)
    {
        return self::create($File->getFilepath(), $mediaDir, $fileName, $sourceMediaId = null, $ident = null);
    }
    // End createFromFile

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Inits a User by id
     *
     * @param  integer  $id    - mediaId
     * @param  boolean  $force - Bypass caching
     * @return Media
     */
    public static function findById($id, $force = false)
    {
        if(!$id)
            return new Media();

        $sql = sprintf("SELECT *
                          FROM %s
                         WHERE id = :id"
                       , self::TABLE_NAME
                       );

        return self::findBySql(get_class(), $sql, ['id' => $id], $force);
    }
    // End findById

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Inits a User by id
     *
     * @param  integer  $id    - mediaId
     * @param  boolean  $force - Bypass caching
     * @return Media
     */
    public static function findBySourceMediaIdAndIdent($sourceMediaId, $ident, $force = false)
    {
        if(!$sourceMediaId)
            return new Media();

        $sql = sprintf("SELECT *
                          FROM %s
                         WHERE source_media_id = :sourceMediaId
                         AND   ident = :ident"
                       , self::TABLE_NAME
                       );

        return self::findBySql(get_class(), $sql, [  'sourceMediaId' => $sourceMediaId
                                                        , 'ident' => $ident
                                                       ], $force);
    }
    // End findBySourceMediaIdAndIdent

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the property name
     *
     * @param  string   $name  - name
     * @return
     */
    public function setName($name)
    {
        if(!$this->getValidator()->assertNotEmpty('name', $name))
            return;

        if(!$this->getValidator()->assertMaxLength('name', 100, $name))
            return;

        if(!$this->getValidator()->assertTrue('name', File::validateFilename($name)))
            return;

        $this->setMimeType(MimeType::getByFilename($name));
        $this->setExtension((string) pathinfo('/' . $name,  PATHINFO_EXTENSION));

        $this->name = (string)$name;
    }
    // End setName

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets property ident
     *
     * @param type $ident
     */
    public function setIdent($ident)
    {
        $this->ident = $ident;
    }
    // End setIdent

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets property extension
     *
     * @param type $extension
     */
    public function setExtension($extension)
    {
        $this->extension = $extension;
    }
    // End setExtension

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the property relPath
     *
     * @param  string   $relPath - base-relative path
     * @return
     */
    public function setRelPath($relPath)
    {
        if(!$this->getValidator()->assertNotEmpty('relPath', $relPath))
            return;

        if(!$this->getValidator()->assertMaxLength('relPath', 150, $relPath))
            return;

        $this->relPath = (string)File::normalizePath($relPath);
    }
    // End setRelPath

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the property mimeType
     *
     * @param  string   $mimeType - mimeType
     * @return
     */
    public function setMimeType($mimeType = '')
    {
        if(!$this->getValidator()->assertMaxLength('mimeType', 100, $mimeType))
            return;

        $this->mimeType = (string)$mimeType;
    }
    // End setMimeType

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the property width
     *
     * @param  integer  $width - image width
     * @return
     */
    public function setWidth($width)
    {
        $this->width = $width;
    }
    // End setWidth

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the property height
     *
     * @param  integer  $height - image width
     * @return
     */
    public function setHeight($height)
    {
        $this->height = $height;
    }
    // End setHeight

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the property sourceMediaId
     *
     * @param  integer  $mediaId - id to source media
     * @return
     */
    public function setSourceMediaId($sourceMediaId)
    {
        if (is_null($sourceMediaId))
            return;

        if(!$this->getValidator()->assertTrue('sourceMediaId', Media::exists($sourceMediaId)))
            return;

        $this->sourceMediaId = $sourceMediaId;
    }
    // End setSourceId

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the property id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    // End getId

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the property name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    // End getName

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the property ident
     *
     * @return string
     */
    public function getIdent()
    {
        return $this->ident;
    }
    // End getIdent

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the fileName
     *
     * @return string
     */
    public function getFileName()
    {
        if(!is_null($this->fileName))
            return $this->fileName;

        /**
         * Fallback to id + extension if mime type exists
         */
        if(!$this->extension)
            return $this->id;

        return $this->id .'.'. $this->extension;
    }
    // End getFileName

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns Fileobject of media
     */
    public function getFile()
    {
        return File::factory($this->getFullPath());
    }
    // End getFile

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the property extension
     *
     * @return string
     */
    public function getExtension()
    {
        return $this->extension;
    }
    // End getRelPath

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the property relPath
     *
     * @return string
     */
    public function getRelPath()
    {
        return $this->relPath;
    }
    // End getRelPath

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the full path
     *
     * @return string
     */
    public function getFullPath()
    {
        return $this->mediaDir . $this->getFileName();
    }
    // End getFullPath

    ///////////////////////////////////////////////////////////////////////////

    /**
     *
     */
    public function getURI($fullURI = false)
    {
        $Config = Environment::getInstance()->getConfig();

        if (!File::getBaseRelativePath($Config->docRoot, $this->getFullPath()))
            return false;

        $uri = str_replace($Config->docRoot, '', $this->getFullPath());
        if (!$fullURI)
            return $uri;

        $host = Environment::sslActive() ? 'https://' : 'http://';
        $host .= Environment::getServerHost();
        return $host . $uri;
    }
    // End getURI

    ///////////////////////////////////////////////////////////////////////////

    /**
     * Returns the property mimeType
     *
     * @return string
     */
    public function getMimeType()
    {
        return $this->mimeType;
    }
    // End getMimeType

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the property width
     *
     * @return integer
     */
    public function getWidth()
    {
        return $this->width;
    }
    // End getWidth

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the property height
     *
     * @return integer
     */
    public function getHeight()
    {
        return $this->height;
    }
    // End getHeight

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns property sourceId
     *
     * @return type
     */
    public function getSourceMediaId()
    {
        return $this->sourceMediaId;
    }
    // End getSourceId

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     *
     * @return Media - Source Media
     */
    public function getSourceMedia()
    {
        return Media::findById($this->sourceMediaId);
    }
    // End getSourceMedia

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     *
     * @param type $force
     */
    public function getRelatedMediaSet($force = false)
    {
        if (!$this->isInitialized())
            return;

        return MediaSet::find(['source_media_id' => $this->getId()], null, null, null, $force);
    }
    // End getRelatedMediaSet

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the property created
     *
     * @return string
     */
    public function getCreated()
    {
        return $this->created;
    }
    // End getCreated

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the property created
     *
     * @return BlibsDateTime
     */
    public function getCreatedDateTime()
    {
        return BlibsDateTime::factory($this->created);
    }
    // End getCreatedDateTime

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the media directory the media is stored in
     *
     * @return string
     */
    public function getMediaDir()
    {
        return $this->mediaDir;
    }
    // End getMediaDir

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Liefert die Größe des Medias
     */
    public function getFilesize()
    {
        return File::getFilesize($this->getFullPath());
    }
    // End getFilesize

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Checks if the media file is an image
     *
     * @param  -
     * @return boolean
     */
    public function isImage()
    {
        if(isset($this->mimeType) && $this->mimeType)
        {
            switch($this->mimeType)
            {
                case "image/gif":
                case "image/jpeg":
                case "image/png":
                    return true;
            }

            return false;
        }

        $imageInfo = getimagesize($this->getFullPath());

        if($imageInfo == false || !isset($imageInfo['mime']))
            return false;

        return true;
    }
    // End isImage

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Checks if the media file is an image
     *
     * @param  -
     * @return boolean
     */
    public function isVideo()
    {
        if(isset($this->mimeType) 
           && $this->mimeType 
           && strpos($this->mimeType, 'video/') === 0)
            return true;
        
        return false;
    }
    // End isImage

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Checks, if the object exists
     *
     * @param  integer  $id    - mediaId
     * @param  boolean  $force - Bypass caching
     * @return boolean
     */
    public static function exists($id, $force = false)
    {
        return self::findById($id, $force)->isInitialized();
    }
    // End exists

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Updates the object in the table
     *
     * @return boolean
     */
    public function update($setAttributes = true)
    {
        $sql = sprintf("UPDATE %s
                           SET name            = :name
                             , rel_path        = :relPath
                             , mime_type       = :mimeType
                             , width           = :width
                             , height          = :height
                             , created         = :created
                             , file_name       = :fileName
                             , extension       = :extension
                             , source_media_id = :sourceMediaId
                             , ident           = :ident
                         WHERE id              = :id"
                       , self::TABLE_NAME
                       );

        if ($setAttributes)
            $this->setMediaAttributes();

        return $this->updateBySql($sql,
                                  ['id'            => $this->id,
                                        'name'          => $this->name,
                                        'relPath'       => $this->relPath,
                                        'mimeType'      => $this->mimeType,
                                        'width'         => $this->width,
                                        'height'        => $this->height,
                                        'created'       => $this->created,
                                        'fileName'      => $this->fileName,
                                        'extension'     => $this->extension,
                                        'sourceMediaId' => $this->sourceMediaId,
                                        'ident'         => $this->ident
                                        ]
                                  );
    }
    // End update

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Deletes the object from the table
     *
     * @return boolean
     */
    public function delete()
    {
        if (!$this->isInitialized())
            return;

        foreach ($this->getRelatedMediaSet() as $RelatedMedia)
            $RelatedMedia->delete();

        if (is_dir($this->getMediaDir() . $this->getId()))
            rmdir($this->getMediaDir() . $this->getId());

        $sql = sprintf("DELETE FROM %s
                              WHERE id = :id"
                       , self::TABLE_NAME
                       );

        if(!$this->deleteBySql($sql, ['id' => $this->id]))
            return false;

        $filePath = $this->getFullPath();

        if(file_exists($filePath))
            unlink($filePath);

        return true;
    }
    // End delete

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns an array with the primary key properties and
     * associates its values, if it's a valid object
     *
     * @param  boolean  $propertiesOnly
     * @return array
     */
    public function getPrimaryKey($propertiesOnly = false)
    {
        if($propertiesOnly)
            return self::$primaryKey;

        $primaryKey = [];

        foreach(self::$primaryKey as $key)
            $primaryKey[$key] = $this->$key;

        return $primaryKey;
    }
    // End getPrimaryKey

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the tablename constant. This is used
     * as interface for other objects.
     *
     * @return string
     */
    public static function getTablename()
    {
        return self::TABLE_NAME;
    }
    // End getTablename

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the columns with their types. The columns may also return extended columns
     * if the first argument is set to true. To access the type of a single column, specify
     * the column name in the second argument
     *
     * @param  boolean  $extColumns
     * @param  mixed    $column
     * @return mixed
     */
    public static function getColumnTypes($extColumns = false, $column = false)
    {
        $columnTypes = $extColumns? array_merge(self::$columnTypes, self::$extColumnTypes) : self::$columnTypes;

        if($column)
            return $columnTypes[$column];

        return $columnTypes;
    }
    // End getColumnTypes

    //////////////////////////////////////////////////////////////////////////////////////
    // protected
    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the path to rhe base-relative media directory
     *
     * @param  string $path
     * @return -
     */
    protected function setMediaDir($mediaDir = false)
    {
        $Config = Environment::getInstance()->getConfig();

        if(!$mediaDir)
            $mediaDir = isset($Config->mediaDir)? $Config->toDir('mediaDir') : $Config->toDir('baseDir');

        /**
         * relativer Pfad... normalisieren und weiter
         */
        if($mediaDir{0} != '/')
            $relativePath = File::normalizePath($mediaDir);

        /**
         * absoluter Pfad... relativen Teil rausholen, wird automatisch normalisiert
         */
        else
        {
            if(($relativePath = File::getBaseRelativePath($Config->toDir('baseDir'), $mediaDir)) === false)
                throw new Exception('Media directory `'.$mediaDir.'\' is not within base directory `'.$Config->toDir('baseDir').'\'');

            if(!file_exists($mediaDir))
                mkdir ($mediaDir, 0777, true);

            if(!is_dir($mediaDir))
                throw new Exception('Media directory `'.$mediaDir.'\' is no directory');

        }

        $absolutePath = $Config->toDir('baseDir') . $relativePath;

        $this->mediaDir = File::normalizePath($absolutePath, true);
        $this->relPath  = File::normalizePath($relativePath);
    }
    // End setMediaDir

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the file size and in case it is an image, its attributes
     *
     * @param  -
     * @return -
     */
    protected function setMediaAttributes()
    {
        if($this->isImage())
        {
            $imagesize = getimagesize($this->getFullPath());

            $this->width  = $imagesize[0];
            $this->height = $imagesize[1];

            if(isset($imagesize['mime']) && $imagesize['mime'] != $this->mimeType)
            {
                $oldPath = $this->getFullpath();
                $this->mimeType = $imagesize['mime'];
                $newPath = $this->getFullpath();

                File::move($oldPath, $newPath);

                if($this->isInitialized())
                    $this->update();
            }
        }
        else
        {
            $this->width  = null;
            $this->height = null;
            $this->setMimeType(MimeType::getByFilename($this->name));
        }
    }
    // End setMediaAttributes

    ///////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Inserts a new object in the table
     *
     * @return boolean
     */
    protected function insert()
    {
        $this->id             = $this->getNextSequenceValue();
        $this->created        = self::getCurrentTime();

        /**
         * Rename the temporary named file into its id
         */
        $tmpFilePath = $this->tempPath;

        if(file_exists($tmpFilePath))
        {
            File::move($tmpFilePath, $this->getFullPath());
            $this->setMediaAttributes();
        }

        $sql = sprintf("INSERT INTO %s (id, name, rel_path, mime_type, width, height, file_name, extension, source_media_id, ident, created)
                               VALUES  (:id, :name, :relPath, :mimeType, :width, :height, :fileName, :extension, :sourceMediaId, :ident, :created)"
                       , self::TABLE_NAME
                       );

        return $this->insertBySql($sql, ['id'             => $this->id,
                                              'name'           => $this->name,
                                              'relPath'        => $this->relPath,
                                              'mimeType'       => $this->mimeType,
                                              'width'          => $this->width,
                                              'height'         => $this->height,
                                              'fileName'       => $this->fileName,
                                              'extension'      => $this->extension,
                                              'sourceMediaId'  => $this->sourceMediaId,
                                              'ident'          => $this->ident,
                                              'created'        => $this->created
                                              ]);
    }
    // End insert

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Inits the object with row values
     *
     * @param  \stdClass $DO - Data object
     * @return boolean
     */
    protected function initByDataObject(\stdClass $DO = null)
    {
        $this->id             = (int)$DO->id;
        $this->name           = $DO->name;
        $this->relPath        = $DO->rel_path;
        $this->mimeType       = $DO->mime_type;
        $this->width          = $DO->width;
        $this->height         = $DO->height;
        $this->fileName       = $DO->file_name;
        $this->extension      = $DO->extension;
        $this->sourceMediaId  = $DO->source_media_id;
        $this->ident          = $DO->ident;
        $this->created        = $DO->created;

        $this->setMediaDir($this->relPath);

        /**
         * Set extensions
         */
    }
    // End initByDataObject

    //////////////////////////////////////////////////////////////////////////////////////
    // private
    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the property fileName
     *
     * @param  string   $name  - name
     * @return
     */
    private function setFileName($name = null)
    {
        if(!$this->getValidator()->assertMaxLength('fileName', 100, $name))
            return;

        if(!$this->getValidator()->assertTrue('fileName', File::validateFilename($name)))
            return;

        $this->fileName = $name;
    }
    // End setFileName

    //////////////////////////////////////////////////////////////////////////////////////
}
// End class Media
