<?php
/**
 * Helps dealing with processes
 *
 * @package blibs
 * @author Tobias Lode <tobias@beibob.de>
 * @author Fabian Möller <fab@beibob.de>
 * @copyright 2013 BEIBOB GbR
 */
namespace Beibob\Blibs;

class Process
{
    /**
     * handle
     */
    private $resource;

    /**
     * Cmd
     */
    private $cmd;

    /**
     * Pipes
     */
    private $pipes;

    /**
     * Directory context
     */
    private $cwd;

    /**
     * Environment
     */
    private $env = [];

    /**
     * default descriptor specifications
     */
    private static $defaultDescriptorSpecs = [0 => ['pipe', 'r'],  // stdin is a pipe that the child will read from
        1 => ['pipe', 'w']   // stdout is a pipe that the child will write to
    ];

    /**
     * Start time
     */
    private $startTime;

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Constructor
     *
     * @see http://de1.php.net/manual/en/function.proc-open.php
     *
     * @param  string $cmd
     * @param  array $descriptorSpec
     * @param  string $cwd
     * @param  array $env
     * @return Process
     */
    public function __construct($cmd = null, array $descriptorSpec = null, $cwd = './', array $env = null)
    {
        if($cmd)
            $this->open($cmd, $descriptorSpec, $cwd, $env);
    }
    // End __construct

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Open
     *
     * @see http://de1.php.net/manual/en/function.proc-open.php
     *
     * @param  string $cmd
     * @param  array $descriptorSpec
     * @param  string $cwd
     * @param  array $env
     */
    public function open($cmd, array $descriptorSpec = null, $cwd = null, array $env = null)
    {
        if($this->isRunning())
            $this->close();

        $this->cmd = $cmd;
        $this->descriptorSpec = is_null($descriptorSpec)? self::$defaultDescriptorSpecs : $descriptorSpec;
        $this->cwd = $cwd;
        $this->env = $env;

        $this->resource = proc_open($this->cmd, $this->descriptorSpec, $this->pipes, $this->cwd, $this->env);
        $this->startTime = $this->getMicrotime();
    }
    // End __construct

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns a pipe or fileDescriptor handle by index
     *
     * @param  int $index
     * @return resource
     */
    public function getPipe($index)
    {
        return $this->pipes[intval($index)];
    }
    // End getPipe

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Closes the process
     *
     * @param  -
     * @return float
     */
    public function close()
    {
        //if(!$this->isRunning())
        //return null;

        $this->closePipes();

        $exitCode = proc_close($this->resource);
        $this->executionTime = $this->getMicrotime() - $this->startTime;

        return $exitCode;
    }
    // End close

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Terminates the process
     *
     * @param  int $signalNo
     * @return boolean
     */
    public function terminate($signal = 15)
    {
        if(!$this->isRunning())
            return false;

        $this->closePipes();

        return proc_terminate($this->resource, $signal);
    }
    // End close

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the command
     *
     * @return string
     */
    public function getCmd()
    {
        return $this->cmd;
    }
    // End getCmd

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the process status
     *
     * @return boolean
     */
    public function isRunning()
    {
        if(!is_resource($this->resource))
            return false;

        $status = proc_get_status($this->resource);
        return $status['running'];
    }
    // End isRunning

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the pid
     *
     * @return int
     */
    public function getPid()
    {
        if(!is_resource($this->resource))
            return false;

        $status = proc_get_status($this->resource);
        return $status['pid'];
    }
    // End getPid

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the pid
     *
     * @return int
     */
    public function getExitCode()
    {
        if(!is_resource($this->resource))
            return false;

        $status = proc_get_status($this->resource);
        return $status['exitcode'];
    }
    // End getExitCode

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the status array
     *
     * @return array
     */
    public function getStatus()
    {
        if(!is_resource($this->resource))
            return null;

        return proc_get_status($this->resource);
    }
    // End getStatus

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the execution time
     *
     * @return float
     */
    public function getExecutionTime()
    {
        if(!$this->startTime)
            return null;

        return $this->getMicrotime() - $this->startTime;
    }
    // End getExecutionTime

    //////////////////////////////////////////////////////////////////////////////////////
    // private
    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Closes all pipes
     *
     */
    private function closePipes()
    {
        foreach($this->pipes as $index => $resource)
            fclose($resource);
    }
    // End closePipes

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the micro time
     */
    private function getMicrotime()
    {
        list($usec, $sec) = explode(" ",microtime());
        return ((float)$usec + (float)$sec);
    }
    // End getMicrotime

    //////////////////////////////////////////////////////////////////////////////////////
}
// End Process