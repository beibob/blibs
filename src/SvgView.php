<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs;

use DOMElement;
use ErrorException;

/**
 * A SvgView class
 *
 * @package blibs
 * @author Tobias Lode <tobias@beibob.de>
 * @author Fabian Möller <fab@beibob.de>
 *
 *
 * @method DOMElement getDefs()
 * @method DOMElement getImage($x, $y, $width ,$heigth, $href, array $attributes = [])
 * @method DOMElement getPattern($id, $x, $y, $width ,$heigth, array $attributes = [])
 * @method DOMElement getRect($x, $y, $width ,$heigth, array $attributes = [])
 * @method DOMElement getSvg(array $attributes = [])
 * @method DOMElement getLine($x1, $y1, $x2, $y2, $attributes = [])
 * @method DOMElement getCircle($cx, $cy, $r, $attributes = [])
 * @method DOMElement getText($text, $x, $y, $attributes = [])
 * @method DOMElement getTSpan($text, $x, $y, $attributes = [])
 * @method DOMElement getGroup($attributes = [])
 * @method string rotate($angle, $x = 0, $y = null)
 * @method string translate($x = 0, $y = null)
 * @method string scale($x = 0, $y = null)
 */
class SvgView extends XmlDocument
{
    /**
     * SvgDOMFactory
     */
    private $SvgDOMFactory;

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Constructs the Document
     *
     * @param bool   $tplName
     * @param bool   $module
     * @param string $version
     * @param string $charSet
     * @return SvgView -
     */
    public function __construct($tplName = false, $module = false, $version = '1.0', $charSet = 'UTF-8')
    {
        parent::__construct($tplName, $module, $version, $charSet);

        // init DOM Factory
        $this->SvgDOMFactory = new SvgDOMFactory($this);
    }
    // End __construct

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Magic method __call
     *
     * @param  string $function
     * @param  array  $args
     * @throws ErrorException
     * @return mixed
     */
    public function __call($function, $args)
    {
        if(method_exists($this->SvgDOMFactory, $function))
            return call_user_func_array([$this->SvgDOMFactory, $function], $args);

        throw new ErrorException('Method ' . get_class($this). '::' .$function . ' does not exist');
    }
    // End __call

    //////////////////////////////////////////////////////////////////////////////////////
}
// End SvgView
