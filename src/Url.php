<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs;

/**
 * Url parsing and manipulation
 *
 * @package blibs
 * @author Tobias Lode <tobias@beibob.de>
 *
 */
class Url
{
    /**
     * Protocol
     */
    private $protocol;

    /**
     * Hostname
     */
    private $hostname;

    /**
     * Port
     */
    private $port;

    /**
     * scriptName
     */
    private $scriptName;

    /**
     * Parameter
     */
    private $parameter = [];

    /**
     * Anchor
     */
    private $anchor;

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Builds an URL
     *
     * @param bool    $scriptName
     * @param  array  $params
     * @param  string $anchor
     * @param  string $hostname
     * @param null    $port
     * @param  string $protocol
     * @return Url
     */
    public static function factory($scriptName = false, array $params = null, $anchor = null, $hostname = null, $port = null, $protocol = null)
    {
        if($scriptName === false)
            $scriptName = Environment::getScriptName();

        return new Url($scriptName, $params, $anchor, $hostname, $port, $protocol);
    }
    // End getUrl

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Builds an URL
     *
     * @param $url
     * @return Url
     */
    public static function parse($url)
    {
        $components = parse_url($url);

        $parameter  = [];

        if(isset($components['query']))
           parse_str($components['query'], $parameter);

        return new Url(isset($components['path'])?     $components['path']     : false,
                       $parameter,
                       isset($components['fragment'])? $components['fragment'] : null,
                       isset($components['host'])?     $components['host']     : null,
                       isset($components['port'])?     $components['port']     : null,
                       isset($components['scheme'])?   $components['scheme']   : null);
    }
    // End getUrl

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Constructor
     *
     * @param bool    $scriptName
     * @param  array  $params
     * @param  string $anchor
     * @param  string $hostname
     * @param null    $port
     * @param  string $protocol
     * @return Url
     */
    public function __construct($scriptName = false, array $params = null, $anchor = null, $hostname = null, $port = null, $protocol = null)
    {
        if(!is_null($params))
            $this->addParameter($params);

        if($anchor)
            $this->setAnchor($anchor);

        if($scriptName)
            $this->setScriptName($scriptName);

        if($hostname)
            $this->setHostname($hostname);

        if($port)
            $this->setPort($port);

        if($protocol)
            $this->setProtocol($protocol);
    }
    // End constructor

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the Anchor
     *
     * @param  string $anchor
     * @return void -
     */
    public function setAnchor($anchor = null)
    {
        $this->anchor = $anchor;
    }
    // End setAnchor

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the script name
     *
     * @param bool|string $scriptName
     * @return void -
     */
    public function setScriptName($scriptName = false)
    {
        $this->scriptName = $scriptName !== false? $scriptName : Environment::getScriptName();
    }
    // End setScriptName

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets parameter (overwrites old parameter)
     *
     * @param  array $parameter
     * @return void -
     */
    public function setParameter(array $parameter = [])
    {
        $this->parameter = $parameter;
    }
    // End setParameter

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Adds parameter
     *
     * @param  array $parameter
     * @return void -
     */
    public function addParameter(array $parameter)
    {
        /**
         * Left-hand keys have precedence!
         */
        $this->parameter = $parameter + $this->parameter;
    }
    // End addParameter

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the hostname
     *
     * @param  string $hostname
     * @return void -
     */
    public function setHostname($hostname = null)
    {
        $this->hostname = $hostname;
    }
    // End setHostname

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the port
     *
     * @param  string $port
     * @return void -
     */
    public function setPort($port = null)
    {
        $this->port = $port;
    }
    // End setPort

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the protocol
     *
     * @param  string $protocol
     * @return void -
     */
    public function setProtocol($protocol = null)
    {
        $this->protocol = $protocol;
    }
    // End setProtocol

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Gets the Anchor
     *
     * @param -
     * @return string
     */
    public function getAnchor()
    {
        return $this->anchor;
    }
    // End getAnchor

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Gets the script name
     *
     * @param  -
     * @return string
     */
    public function getScriptName()
    {
        return $this->scriptName;
    }
    // End getScriptName

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Gets all or one parameter, indexed by key
     *
     * @param  string $key
     * @return mixed
     */
    public function getParameter($key = false)
    {
        if($key)
            return $this->parameter[$key];

        return $this->parameter;
    }
    // End getParameter

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns true if parameter key exists
     *
     * @param  string $key
     * @return bool
     */
    public function hasParameter($key)
    {
        return isset($this->parameter[$key]);
    }
    // End hasParameter

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Removes a parameter key
     *
     * @param  string $key
     * @return void
     */
    public function removeParameter($key)
    {
        unset($this->parameter[$key]);
    }
    // End removeParameter

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Gets the hostname
     *
     * @param  -
     * @return string
     */
    public function getHostname()
    {
        return $this->hostname;
    }
    // End getHostname

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Gets the port
     *
     * @param  -
     * @return integer
     */
    public function getPort()
    {
        return $this->port;
    }
    // End getPort

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Gets the protocol
     *
     * @param  -
     * @return string
     */
    public function getProtocol()
    {
        return $this->protocol;
    }
    // End getProtocol

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns an url string
     *
     * @param  -
     * @return string
     */
    public function __toString()
    {
        $url = '';

        if($this->protocol)
            $url .= $this->protocol.'://';

        if($this->hostname)
        {
            if($url === '')
                $url = 'http://';

            $url .= $this->hostname;
        }

        if($this->port)
            $url .= ':'.$this->port;

        if($this->scriptName)
            $url .= $this->scriptName;

        if(count($this->parameter))
        {
            $parts = [];
            foreach ($this->parameter as $key => $value)
            {
                if(!is_null($value))
                {
                    if(is_array($value))
                    {
                        foreach($value as $pKey => $pVal)
                            $parts[] = $key . '['.rawurlencode($pKey).']=' . rawurlencode($pVal);
                    }
                    else
                        $parts[] = $key . '=' . rawurlencode($value);
                }
                else
                    $parts[] = $key;
            }

            $url .= '?'.join('&', $parts);
        }

        if($this->anchor)
            $url .= '#'.$this->anchor;

        return $url;
    }
    // End __toString
}
// End class Url
