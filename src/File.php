<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs;

/**
 * @package blibs
 * @author  Fabian Möller  <fab@beibob.de>
 * @author  Tobias Lode <tobias@beibob.de>
 *
 */
class File
{
    /**
     * Filepath
     */
    private $filepath;

    /**
     * Filehandle
     */
    private $filehandle;

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Default file creation mask
     */
    const DEFAULT_FILE_CREATION_MASK = '0640';

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Factory
     *
     * @param  string $filepath
     * @param  string $filemode
     *
     * @return File
     */
    public static function factory($filepath = false, $filemode = false)
    {
        return new File($filepath, $filemode);
    }
    // End factory

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * From upload
     *
     * @param  string $filepath
     * @param  string $filemode
     *
     * @return File
     */
    public static function fromUpload($fileArrayKey, $tmpDir = false, $filemode = false)
    {
        $filepath = self::moveUploadedFile($fileArrayKey, is_dir($tmpDir) ? $tmpDir : sys_get_temp_dir());
        return new File($filepath, $filemode);
    }
    // End factory

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Constructs a file instance. opens a filehandle
     * only if mode is specified
     *
     * @param  string $filepath
     * @param  string $filemode
     *
     * @return -
     */
    public function __construct($filepath = false, $filemode = false)
    {
        if ($filepath && !$filemode)
            $this->setFilepath($filepath);

        if ($filepath && $filemode)
            $this->open($filemode, $filepath);
    }
    // End __construct

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the filepath, but do not open a filehandle nor
     * check if that filepath is valid
     *
     * @param  string $filepath
     *
     * @return -
     */
    public function setFilepath($filepath)
    {
        /**
         * close open filehandle
         */
        $this->close();

        $this->filepath = $filepath;
    }
    // End setFilepath

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns a filepath
     *
     * @param  -
     *
     * @return string
     */
    public function getFilepath()
    {
        return $this->filepath;
    }
    // End getFilepath

    ////////////////////////////////////////////////////////////////////////////

    /*
     * Get the file name
     */
    public function getFilename()
    {
        return basename($this->getFilepath());
    }
    //End getFilename

    ////////////////////////////////////////////////////////////////////////////

    /*
     * Get the file name
     */
    public function getExtension()
    {
        return (string)pathinfo('/' . $this->getFilename(), PATHINFO_EXTENSION);
    }
    //End getExtension

    ////////////////////////////////////////////////////////////////////////////

    /*
     * get directory file is stored in
     */
    public function getDirname()
    {
        return (string)pathinfo($this->getFilepath(), PATHINFO_DIRNAME);
    }
    // End getDirname

    ////////////////////////////////////////////////////////////////////////////

    /**
     *
     */
    public function getStat()
    {
        if ($this->isOpen())
            return fstat($this->filehandle);
        else
            return stat($this->getFilepath());
    }
    // End getStat

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     *
     */
    public function getMTime()
    {
        $stat = $this->getStat();
        return $stat['mtime'];
    }
    // End getMTime

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     *
     */
    public function getCTime()
    {
        $stat = $this->getStat();
        return $stat['ctime'];
    }
    // End getCTime

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     *
     */
    public function getAsArray()
    {
        return file($this->getFilepath());
    }
    // End getAsArray

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     *
     */
    public function getCsv($delimiter = ';', $enclosure = '"', $length = 0)
    {
        if (!$this->isOpen())
            $this->open('r');

        return fgetcsv($this->filehandle, $length, $delimiter, $enclosure);
    }
    // End getCsv

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Creates a temporary file
     *
     * @param  $string prefix
     *
     * @return -
     */
    public function createTemporaryFile($tmpDir = '/tmp', $prefix = '')
    {
        /**
         * just in case
         */
        $this->close();

        $temppath = tempnam($tmpDir, $prefix);
        $this->open('w+', $temppath);
    }
    // End createTemproaryFile

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Opens a filehandle in specified filemode
     *
     * @param  string $filemode [default is r]
     * @param  string $filepath [optional, if setFilepath was called before]
     *
     * @return -
     */
    public function open($filemode = 'r', $filepath = false)
    {
        if ($filepath)
            $this->setFilepath($filepath);

        if (($this->filehandle = fopen($this->filepath, $filemode)) === false)
            throw new Exception("Could not open file `" . $filepath . "' in mode " . $filemode);

        $this->filemode = $filemode;
        return true;
    }
    // End open

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     *
     * @param type $mode
     */
    public function chmod($mode)
    {
        if (!$this->exists())
            return;

        chmod($this->getFilepath(), $mode);
        return true;
    }
    // End chmod

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Replaces content of the file.
     *
     * Note: if the file is currently opened, it will be temporary closed
     * and after replacement operation reopened in same filemode like it was
     * opened before. The file position counter will be resetted to to the top
     * of the file.
     *
     * @param  string $search
     * @param  string $replacement
     *
     * @return occurrences
     */
    public function replaceContent($search, $replacement)
    {
        /**
         * Close file in case it is opened and remember the
         * current filemode
         */
        if ($this->isOpen()) {
            $filemode = $this->filemode;
            $this->close();
        }

        $occurrences = 0;

        /**
         * Read the file content in and start replaceing the content
         */
        $content = file_get_contents($this->filepath);
        $content = str_replace($search, $replacement, $content, $occurrences);

        /**
         * Write the new content into an tempfile first and then overwrite
         * the oldfile
         */
        $Tempfile = new File();
        $Tempfile->createTemporaryFile();
        $Tempfile->write($content);
        $Tempfile->moveTo($this->filepath);

        /**
         * Reopen file in case it was opened
         */
        if ($filemode)
            $this->open($filemode);

        return $occurrences;
    }
    // End replaceContent

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Writes something into the file. if the file is not opened yet
     * it will be opened and closed afterwards
     *
     * @param  string  $content
     * @param  boolean $append
     *
     * @return boolean
     */
    public function write($content)
    {
        if (!$this->isOpen(['w', 'w+', 'a', 'a+']))
            return;

        fwrite($this->filehandle, $content);
    }
    // End write

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Closes a filehandle
     *
     * @param  -
     *
     * @return -
     */
    public function close()
    {
        if (!$this->filehandle)
            return;

        if ($this->isOpen())
            fclose($this->filehandle);

        $this->filehandle = false;
        $this->filemode = false;
    }
    // End close

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns true if a filehandle is opened.
     *
     * If the argument filemodes is specified, the method checks
     * if the open mode is one of the specified one
     *
     * @param  mixed $filemodes [optional]
     *
     * @return boolean
     */
    public function isOpen($filemodes = false)
    {
        if (!$this->filehandle)
            return false;

        if (!is_resource($this->filehandle)) {
            $this->filehandle = false;
            return false;
        }

        if (is_string($filemodes) && $this->filemode != $filemodes)
            return false;

        if (is_array($filemodes) && !in_array($this->filemode, $filemodes))
            return false;

        return true;
    }
    // End isOpen

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Loescht die Datei
     */
    public function delete()
    {
        if (!$this->exists())
            return;

        if ($this->isOpen())
            $this->close();

        unlink($this->filepath);
        $this->filepath = false;
    }
    // End delete

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns true if the file exists
     *
     * @param  -
     *
     * @return boolean
     */
    public function exists()
    {
        return file_exists($this->filepath);
    }
    // End exists

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns true if the file is an file
     *
     * @param  -
     *
     * @return boolean
     */
    public function isFile()
    {
        return is_file($this->filepath);
    }
    // End isFile

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns true if the file is an directory
     *
     * @param  -
     *
     * @return boolean
     */
    public function isDir()
    {
        return is_dir($this->filepath);
    }
    // End isFile

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Copies the file. A new File instance will be returned on success
     * or false otherwise
     *
     * @param  string $destinationPath
     *
     * @return mixed
     */
    public function copyTo($destinationPath)
    {
        if (!$this->filepath)
            return false;

        $newpath = self::copy($this->filepath, $destinationPath);
        return new File($newpath);
    }
    // End copyTo

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Moves the file. A new File instance will be returned on success
     * or false otherwise
     *
     * @param  string $destinationPath
     *
     * @return mixed
     */
    public function moveTo($destinationPath)
    {
        if (!$this->filepath)
            return false;

        /**
         * Close open filehandle
         */
        $this->close();

        $newpath = self::move($this->filepath, $destinationPath);
        return new File($newpath);
    }
    // End moveTo

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     *
     * @param type $file
     */
    public function forceDownload($filename = null, $contentType = null)
    { // $file = include path
        if ($this->exists()) {
            $filename = $filename ? $filename : $this->getFilename();
            $contentType = $contentType ? $contentType : 'application/octet-stream';

            header('Content-Description: File Transfer');
            header('Content-Type: ' . $contentType);
            header('Content-Disposition: attachment; filename=' . $filename);
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
            header('Content-Length: ' . File::getFilesize($this->getFilepath()));
            ob_clean();
            flush();
            $this->readfileChunked();
            exit;
        }
    }
    // End forceDownload

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     *
     * @param type $retbytes
     *
     * @return boolean
     */
    public function readfileChunked($retbytes = true)
    {
        $chunksize = 1 * (1024 * 1024); // how many bytes per chunk
        $buffer = '';
        $cnt = 0;
        // $handle = fopen($filename, 'rb');
        $handle = fopen($this->getFilepath(), 'rb');
        if ($handle === false) {
            return false;
        }
        while (!feof($handle)) {
            $buffer = fread($handle, $chunksize);
            echo $buffer;
            ob_flush();
            flush();
            if ($retbytes) {
                $cnt += strlen($buffer);
            }
        }
        $status = fclose($handle);
        if ($retbytes && $status) {
            return $cnt; // return num. bytes delivered like readfile() does.
        }
        return $status;
    }
    // End readfileChunked

    //////////////////////////////////////////////////////////////////////////////////////
    // static helpers
    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Move like unix mv
     *
     * @param string $from
     * @param string $to
     *
     * @return string - new filepath
     */
    public static function move($from, $to)
    {
        if (file_exists($from) == false)
            throw new Exception("File `" . $from . "' not found");

        if (!is_file($from))
            throw new Exception("File `" . $from . "' ist not a file");

        if (is_dir($to))
            $to .= '/' . basename($from);

        if (!is_readable($from))
            throw new Exception("Error while moving file `" . $from . "' to `" . $to . "': file `" . basename($from) . "' is not readable");

        if (file_exists($to) && !is_writeable($to))
            throw new Exception("Error while moving file `" . $from . "' to `" . $to . "': target `" . $to . "' is not writeable");

        if (rename($from, $to)) {
            if (file_exists($to) == false)
                throw new Exception("Error while moving file `" . $from . "' to `" . $to . "'");
        } else
            throw new Exception("Error while moving file `" . $from . "' to `" . $to . "'");

        return $to;
    }
    // End Move

    ///////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Copy like unix cp
     *
     * @param string $from
     * @param string $to
     *
     * @return string - new filepath
     */
    public static function copy($from, $to)
    {
        if (file_exists($from) == false)
            throw new Exception("File `" . $from . "' not found");

        if (is_dir($to))
            $to .= '/' . basename($from);

        if (!is_readable($from))
            throw new Exception("Error while copying file `" . $from . "' to `" . $to . "': file `" . basename($from) . "' is not readable");

        if (file_exists($to) && !is_writeable($to))
            throw new Exception("Error while copying file `" . $from . "' to `" . $to . "': target `" . $to . "' is not writeable");

        if (copy($from, $to)) {
            if (file_exists($to) == false)
                throw new Exception("Error while copying file `" . $from . "' to `" . $to . "'");
        } else
            throw new Exception("Error while copying file `" . $from . "' to `" . $to . "'");

        return $to;
    }
    // End copy

    ///////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Removes a file. If jail path is given it will be checked
     * if the jail path is the base part of filePath and returned
     * if that is not the case.
     *
     * @param  string $filePath
     * @param  string $jailPath
     *
     * @return boolean
     */
    public static function remove($filePath, $jailPath = false)
    {
        $filePath = realpath($filePath);

        if (!file_exists($filePath) || !is_file($filePath))
            return false;

        /**
         * Check against jail path
         */
        if ($jailPath) {
            $jailPath = realpath($jailPath);

            if (!preg_match('/^' . preg_quote($jailPath, '/') . '/u', $filePath))
                return false;
        }

        if (!unlink($filePath))
            return false;

        return true;
    }
    // End removeFile

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Removes a directory which may contain files or subdirectories.
     * It also takes care that only directories below a given jail-directory-path
     * will be deleted.
     *
     * @param  string $dirPath
     * @param  string $jailPath
     *
     * @return boolean
     */
    public static function removeDir($dirPath, $jailPath = false, $keepLinks = true)
    {
        $dirPath = realpath($dirPath);

        if (is_link($dirPath) || !is_dir($dirPath))
            return false;

        /**
         * Check against jail path
         */
        if ($jailPath) {
            $jailPath = realPath($jailPath);

            if (!preg_match('/^' . preg_quote($jailPath, '/') . '/u', $dirPath))
                return false;
        }

        /**
         * Append slash if necessary
         */
        $dirPath = self::normalizePath($dirPath, true);
        $handle = opendir($dirPath);

        while (false !== ($file = readdir($handle))) {
            /**
             * Ignore . and ..
             */
            if ($file != '.' && $file != '..') {
                $path = $dirPath . $file;

                /**
                 * Recurse if subdir, Delete if file
                 */
                if (is_link($path) && $keepLinks)
                    return false;

                if (is_dir($path)) {
                    if (!self::removeDir($path, $jailPath, $keepLinks))
                        return false;
                } elseif (is_file($path) || (!$keepLinks && is_link($path))) {
                    if (!unlink($path))
                        return false;
                } else
                    return false;
            }
        }
        closedir($handle);

        /**
         * Remove the directory itself
         */
        rmdir($dirPath);
        return true;
    }
    // End removeDir

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * The method returns the difference between basePath and path
     * Therefor path must be in basePath or false will be returned.
     * Path will be returned, if path is not an absolute path!
     *
     * getPathDiff('/a/b', '/a/b/c/d') => 'c/d/'  (with Path normalization)
     *
     * @param  string $basePath
     * @param  string $path
     *
     * @return mixed
     */
    public static function getBaseRelativePath($basePath, $path)
    {
        if (!$basePath || !$path)
            return false;

        if ($path{0} != '/')
            return $path;

        $pathDiff = str_replace(self::normalizePath($basePath, true), '', $path);

        if ($pathDiff == $path)
            return false;

        return self::normalizePath($pathDiff);
    }
    // End getBaseRelativePath

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Normalizes a path. Normalized pathes start without and end with a slash
     * Optional, the leading slash can be kept touched
     *
     * @param  string  $path
     * @param  boolean $removeLeadingSlash
     *
     * @return string
     */
    public static function normalizePath($path, $keepLeadingSlash = false)
    {
        /**
         * remove leading /
         */
        if (!$keepLeadingSlash && $path{0} == '/')
            $path = utf8_substr($path, 1);

        /**
         * add trailing /
         */
        if (utf8_substr($path, -1, 1) != '/')
            $path .= '/';

        return $path;
    }
    // End normalizePath

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Moves an uploaded file from the $_FILES array to the destination directory
     * and returns the new filepath. On error it throws an exception with the error
     * code
     *
     * @throws FileException
     *
     * @param  mixed  $fileArrayKey
     * @param  string $destDir
     *
     * @return string
     */
    public static function moveUploadedFile($fileArrayKey, $destDir)
    {
        if (!self::uploadFileExists($fileArrayKey))
            return self::checkUploadError($fileArrayKey);

        $file = $_FILES[$fileArrayKey];
        $filename = basename($file['name']);
        $tmpname = $file['tmp_name'];

        if (!self::validateFilename($filename))
            throw new Exception("Invalid filename `" . $filename . "'");

        $destpath = self::normalizePath($destDir, true) . $filename;

        if (!is_writable($destDir))
            throw new Exception("Uploading file `" . $tmpname . "' to `" . $destpath . "' failed: directory `" . $destDir . "' not writeable!");

        if (move_uploaded_file($tmpname, $destpath)) {
            if (!file_exists($destpath))
                throw new Exception("Moving temporary file `" . $tmpname . "' to `" . $destpath . "' failed");
        } else
            throw new Exception("Moving temporary file `" . $tmpname . "' to `" . $destpath . "' failed");

        return $destpath;
    }
    // End upload

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Checks if an upload file exists for the given files array key
     *
     * @param  mixed $fileArrayKey
     *
     * @return boolean
     */

    public static function uploadFileExists($fileArrayKey)
    {
        return isset($_FILES[$fileArrayKey]) && is_array($_FILES[$fileArrayKey]) && $_FILES[$fileArrayKey]['error'] == UPLOAD_ERR_OK;
    }
    // End uploadFileExists

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Checks for errors during upload
     *
     * @param  mixed $fileArrayKey
     *
     * @return
     */
    public static function checkUploadError($fileArrayKey)
    {
        if (!isset($_FILES[$fileArrayKey]) || !is_array($_FILES[$fileArrayKey]))
            throw new Exception('No such key `' . $fileArrayKey . '\' in files array');

        switch ($_FILES[$fileArrayKey]["error"]) {
            case UPLOAD_ERR_OK:
                return true;

            case UPLOAD_ERR_INI_SIZE:
                throw new Exception("The uploaded file exceeds the upload_max_filesize directive (" . ini_get("upload_max_filesize") . ") in php.ini.", UPLOAD_ERR_INI_SIZE);
                break;
            case UPLOAD_ERR_FORM_SIZE:
                throw new Exception("The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form.", UPLOAD_ERR_FORM_SIZE);
                break;
            case UPLOAD_ERR_PARTIAL:
                throw new Exception("The uploaded file was only partially uploaded.", UPLOAD_ERR_PARTIAL);
                break;
            case UPLOAD_ERR_NO_FILE:
                throw new Exception("No file was uploaded.", UPLOAD_ERR_NO_FILE);
                break;
            case UPLOAD_ERR_NO_TMP_DIR:
                throw new Exception("Missing a temporary folder.", UPLOAD_ERR_NO_TMP_DIR);
                break;
            case UPLOAD_ERR_CANT_WRITE:
                throw new Exception("Failed to write file to disk", UPLOAD_ERR_CANT_WRITE);
                break;
            default:
                throw new Exception("Unknown File Error");
        }

        return false;
    }
    // End checkUploadError

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Liefert die Dateigroesse
     */
    public static function getFilesize($filepath)
    {
        return filesize($filepath);
    }
    // End getFilesize

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Prueft den Dateinamen auf unerlaubten Zeichen
     *
     * @param  string $filename
     *
     * @return boolean
     */
    public static function validateFilename($filename)
    {
        return !((bool)strspn($filename, '?/\\*'));
    }
    // End validateFilename

    ///////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Pretty print method for file sizes
     *
     * @param int $size   - Die zu formatierende Anzahl von Bytes
     * @param int $digits - Die Anzahl von Nachkommastellen die angezeigt werden soll
     *
     * @return string
     */
    public static function formatFileSize($size, $digits = 2)
    {
        if (!is_numeric($size))
            return '';

        $negative = false;

        if ($size < 0) {
            $negative = true;
            $size = ($size * (-1));
        }

        $unit = [" B", " KB", " MB", " GB"];
        $size = ($size) ? ($size) : ($size + 1);
        $n = (int)(log($size) / log(2)) / 10;
        $size = $size / pow(1024, floor($n));

        if ($unit[$n] == "B")
            $ret = $size . $unit[$n];
        else
            $ret = sprintf("%." . $digits . "f%s", $size, $unit[$n]);

        if ($negative)
            $ret = "-" . $ret;

        return $ret;
    }
    // End formatFileSize

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Replaces DateTime flags within the given filePath string
     *
     * Substitution symbols starts with % followed by date format character
     *
     * @param string $filePath
     *
     * @return string
     */
    public static function substituteDateFormat($filePath)
    {
        $matches = [];
        if (!preg_match_all("/%(.?)/", $filePath, $matches))
            return;

        if (!count($matches[1]))
            return $filePath;

        $currentTime = time();

        foreach ($matches[1] as $symbol)
            $filePath = str_replace('%' . $symbol, date($symbol, $currentTime), $filePath);

        return $filePath;
    }
    // substituteDateFormat

    ///////////////////////////////////////////////////////////////////////////

    /**
     * Liefert die maximale Dateigroesse die aufgrund der Einstellungen in der php.ini
     * möglich ist.
     *
     * Wenn gar keine file_uploads erlaubt sind wird false zurückgegeben, sonst die
     * maximale Dateigrössee in Bytes
     *
     * @param -
     *
     * @return mixed
     */
    public static function getMaxUploadFilesize()
    {
        if (utf8_strtolower(ini_get('file_uploads')) == 'off' || utf8_strtolower(ini_get('file_uploads')) === '0')
            return false;

        $postMaxSize = ini_get("post_max_size");
        if (!is_numeric($postMaxSize))
            $postMaxSize = self::formatIniValueToBytes($postMaxSize);

        $uploadMaxFilesize = ini_get("upload_max_filesize");
        if (!is_numeric($uploadMaxFilesize))
            $uploadMaxFilesize = self::formatIniValueToBytes($uploadMaxFilesize);

        $memoryLimit = ini_get("memory_limit");
        if (!is_numeric($memoryLimit))
            $memoryLimit = self::formatIniValueToBytes($memoryLimit);

        //if (min($postMaxSize, $uploadMaxFilesize, $memoryLimit) < $uploadMaxFilesize)
        //                BLibs::getLogger()->warning("Die PHP-Variable 'post_max_size' (" . ini_get("post_max_size") . ") oder die PHP-Variable 'memory_limit' (" . ini_get("memory_limit") . ") ist kleiner als die PHP Variable 'upload_max_filesize' (" . ini_get("upload_max_filesize") . "). Dadurch ist kein sauberes Fehlerhandling bei Dateiuploads möglich! Bitte passen Sie die Einstellungen in der php.ini an!", "Media", "getMaxUploadFilesize");

        return min($postMaxSize, $uploadMaxFilesize, $memoryLimit);
    }
    // End getMaxUploadFilesize

    //////////////////////////////////////////////////////////////////////////////////////
    // protected
    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Formatiert Werte der php.ini die wie
     * post_max_size oder upload_max_filesize
     * so das eine Anzahl von Bytes zurückkommt
     */
    protected static function formatIniValueToBytes($val)
    {
        $val = trim($val);
        $last = utf8_strtolower($val{utf8_strlen($val) - 1});
        switch ($last) {
            case 'g':
                $val *= pow(1024, 3);
                break;
            case 'm':
                $val *= pow(1024, 2);
                break;
            case 'k':
                $val *= 1024;
                break;
        }
        return $val;
    }
    // End formatIniValueToBytes

    //////////////////////////////////////////////////////////////////////////////////////


    /**
     * Checks if file with given name exists in given directory
     *
     * @param $directory
     * @param $filename
     *
     * @return bool
     */
    public static function checkFilename($directory, $filename)
    {
        if (!is_dir($directory))
            throw new Exception('Directory `' . $directory . "' not readable or not existing");

        if (substr($directory, -1, 1) != '/')
            $directory .= '/';

        return !file_exists($directory . $filename);
    }
// End checkFilename

//////////////////////////////////////////////////////////////////////////////////////

    /**
     *
     * Returns given filename, extended by numeration to make filename unique in given
     * directory.
     *
     * @param     $directory
     * @param     $filename
     * @param int $digits
     *
     * @return string
     * @throws Exception
     */
    public static function getNumberedFilename($directory, $filename, $digits = 3)
    {
        $count = -1;
        while (!self::checkFilename($directory, $filename)) {
            if ($count == -1)
                $count = self::getMaxNumberedFile($directory, $filename, $digits);

            if ($count > pow(10, $digits) - 1)
                throw new Exception('File numeration limit reached.');

            $File = File::factory($filename);
            $extension = $File->getExtension();
            $name = str_replace('.' . $extension, '', $File->getFilename());

            $matches = [];
            preg_match("/.*\-(\d{" . $digits . "})$/u", $name, $matches);

            $hasCount = false;
            if (count($matches))
                $hasCount = $matches[1];

            if ($hasCount) {
                $count = $matches[1] + 1;
                $lastMinus = utf8_strrpos($name, '-');
                $filename = utf8_substr($name, 0, $lastMinus) . '-' . str_pad($count, $digits, '0', STR_PAD_LEFT) . '.' . $extension;
            } else
                $filename = $name . '-' . str_pad($count, $digits, '0', STR_PAD_LEFT) . '.' . $extension;

            $count++;
        }

        return $filename;
    }
    // End getNumberedFilename

    /**
     * Returns the maximum used numeration for given filename in given directory
     *
     * @param     $directory
     * @param     $filename
     * @param int $digits
     *
     * @return mixed
     * @throws Exception
     */
    protected static function getMaxNumberedFile($directory, $filename, $digits = 3)
    {
        if (!is_dir($directory))
            throw new Exception('Directory `' . $directory . "' not readable or not existing");

        if (substr($directory, -1, 1) != '/')
            $directory .= '/';

        $File = File::factory($filename);
        $extension = $File->getExtension();
        $name = str_replace('.' . $extension, '', $File->getFilename());

        $matches = [];
        preg_match("/.*\-(\d{" . $digits . "})$/u", $name, $matches);

        if (count($matches))
            $name = str_replace('-' . $matches[1], '', $name);

        $existingFiles = [1];
        $questionmarks = str_pad('', $digits, '?');
        foreach (glob($directory . $name . '-' . $questionmarks . '.' . $extension) as $filename) {
            $matches = [];
            preg_match("/.*\-(\d{" . $digits . "})\." . preg_quote($extension, '/') . "$/u", basename($filename), $matches);
            $existingFiles[basename($filename)] = (int)$matches[1];
        }

        return max($existingFiles);
    }
    // End getMaxNumberedFile
}
// End File
