<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */

namespace Beibob\Blibs;

/**
 * Curl class
 *
 * @package    blibs
 * @author     Tobias Lode <tobias@beibob.de>
 * @author     Fabian Möller <fab@beibob.de>
 *
 */
class Curl
{
    const DEFAULT_TIMEOUT = -1;
    const METHOD_RAW = 'raw';
    const METHOD_GET = 'get';
    const METHOD_DELETE = 'delete';
    const METHOD_POST = 'post';
    const METHOD_POST_FILE = 'post_file';
    const METHOD_PUT = 'put';

    private $sslVerifyPeer = false; // certificat verification
    private $timeout = self::DEFAULT_TIMEOUT; // curl timeout
    private $lastError; // last curl error
    protected $info; // curl transfer info
    private $proxy = array(); // optional proxy definition
    protected $responseType = self::RESPONSE_TYPE_FORM; // Content-Type of Response
    protected $authentication; // optional authentication

    const AUTHTYPE_NONE = 0;
    const AUTHTYPE_BASIC = 1;
    const AUTHTYPE_NTLM = 1;


    const RESPONSE_TYPE_FORM = 'application/x-www-form-urlencoded';
    const RESPONSE_TYPE_JSON = 'application/json';

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     *
     */
    public function __construct(array $proxy = array(), $responseType = self::RESPONSE_TYPE_FORM, array $authentication = null)
    {
        $this->setProxy($proxy);
        $this->setResponseType($responseType);

        if (self::hasAuthentication($authentication))
            $this->setAuthentication($authentication['username'], $authentication['password'], $authentication['type']);
    }
    // End __construct

    /**
     * @param string $responseType
     */
    public function setResponseType($responseType = self::RESPONSE_TYPE_FORM)
    {
        $this->responseType = $responseType;
    }
    // End setResponseType

    /**
     *
     * Curl call method
     *
     * @param string $url    url of the call
     * @param array  $data   data of the call
     * @param string $method method of the call (get/post/post_file/delete/raw)
     */
    protected function call($url, array $data = array(), $method = self::METHOD_POST, array $headers = null, $responseContentType = null)
    {
        // Check if curl extension is loaded
        if (extension_loaded('curl')) {
            $parsedUrl = parse_url($url);

            // parse url for params
            $tmpUrl = '';
            $tmpUrl .= isset($parsedUrl['scheme']) ? $parsedUrl['scheme'] . '://' : '';
            $tmpUrl .= isset($parsedUrl['host']) ? $parsedUrl['host'] : '';
            $tmpUrl .= isset($parsedUrl['port']) ? ':' . $parsedUrl['port'] : '';
            $tmpUrl .= isset($parsedUrl['path']) ? $parsedUrl['path'] : '';

            $ch = curl_init();

            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            if (!is_array($headers))
                $headers = array('Expect:');
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

            // Check if proxy informations are defined
            if (self::isProxy($this->proxy)) {
                curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, true);
                curl_setopt($ch, CURLOPT_PROXY, $this->proxy['host']);
                curl_setopt($ch, CURLOPT_PROXYUSERPWD, $this->proxy['username'] . ':' . $this->proxy['password']);
                curl_setopt($ch, CURLOPT_PROXYPORT, $this->proxy['port']);
            }

            if (self::hasAuthentication($this->authentication))
            {
                curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
                curl_setopt($ch, CURLOPT_USERPWD, $this->authentication['username'] . ':' . $this->authentication['password']);

                if ($this->authentication['type'] == self::AUTHTYPE_NTLM)
                    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_NTLM);
            }

            // Check ssl peer's certicate
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $this->sslVerifyPeer);
            if (!$this->sslVerifyPeer)
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);

            // Check timeout
            if ($this->timeout != self::DEFAULT_TIMEOUT)
                curl_setopt($ch, CURLOPT_TIMEOUT, $this->timeout);

            // Getting GET Params
            $postData = array();
            if (isset($parsedUrl['query'])) {
                $queryArg = explode('&', $parsedUrl['query']);
                foreach ($queryArg as $arg) {
                    $detailArg = explode('=', $arg);
                    $postData[$detailArg[0]] = $detailArg[1];
                }
            }

            // Getting data params
            if (count($data)) {
                foreach ($data as $key => $value)
                    $postData[$key] = $value;
            }


            $responseType = is_null($responseContentType) ? $this->responseType : $responseContentType;
            switch ($method) {
                case self::METHOD_POST_FILE:
                    curl_setopt($ch, CURLOPT_POST, true);
                    switch ($responseType)
                    {
                        case self::RESPONSE_TYPE_FORM:
                            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postData));
                            break;
                        case self::RESPONSE_TYPE_JSON:
                            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode(array_shift($postData)));
                            break;
                    }
                    curl_setopt($ch, CURLOPT_URL, $url);
                    break;
                case self::METHOD_POST:
                    curl_setopt($ch, CURLOPT_POST, true);

                    switch ($responseType)
                    {
                        case self::RESPONSE_TYPE_FORM:
                            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postData));
                        break;
                        case self::RESPONSE_TYPE_JSON:
                            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode(array_shift($postData)));
                        break;
                    }
                    curl_setopt($ch, CURLOPT_URL, $url);

                    break;
                case self::METHOD_GET:
                    if (count($data) && !empty($data)) {
                        $url .= (strpos($url, '?') !== false) ? '&' : '?';
                        curl_setopt($ch, CURLOPT_URL, $url . http_build_query($data));
                    } else
                        curl_setopt($ch, CURLOPT_URL, $url);
                    break;
                case self::METHOD_DELETE:
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
                    switch ($responseType)
                    {
                        case self::RESPONSE_TYPE_FORM:
                            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postData));
                            break;
                        case self::RESPONSE_TYPE_JSON:
                            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode(array_shift($postData)));
                            break;
                    }
                    curl_setopt($ch, CURLOPT_URL, $url);
                    break;
                case self::METHOD_RAW:
                    curl_setopt($ch, CURLOPT_URL, $url);
                    break;
                case self::METHOD_PUT:
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
                    switch ($responseType)
                    {
                        case self::RESPONSE_TYPE_FORM:
                            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postData));
                            break;
                        case self::RESPONSE_TYPE_JSON:
                            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode(array_shift($postData)));
                            break;
                    }
                    curl_setopt($ch, CURLOPT_URL, $url);
                    break;
                default:
                    throw new Exception('Method `' . $method . "' not implemented");
            }

            // Execute
            $response = curl_exec($ch);
            $this->info = curl_getinfo($ch);
            $this->lastError = curl_error($ch);

            // Close connection
            curl_close($ch);
            return $response;
        } else
            throw new Exception('cURL module not implemented');
    }
    // End Call

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     *
     * Curl call with GET method
     *
     * @param string $url
     * @param array  $params
     * @param mixed  $info information about the last transfer
     */
    public function get($url, $params = array(), array $headers = null)
    {
        return $this->call($url, $params, self::METHOD_GET, $headers);
    }
    // End get

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     *
     * Curl call with POST method
     *
     * @param string $url
     * @param array  $params
     */
    public function post($url, $params = array(), array $headers = null, $responseContentType = null)
    {
        return $this->call($url, $params, self::METHOD_POST, $headers, $responseContentType);
    }
    // End post

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     *
     * Curl call with POST method for file
     *
     * @param string $url
     * @param array  $params Value content an '@' for file upload
     */
    public function postFile($url, $params = array(), array $headers = null, $responseContentType = null)
    {
        return $this->call($url, $params, self::METHOD_POST_FILE, $headers, $responseContentType);
    }
    // End postFile

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     *
     * Curl call with PUT method
     *
     * @param string $url
     * @param array  $params
     */
    public function put($url, $params = array(), array $headers = null, $responseContentType = null)
    {
        return $this->call($url, $params, self::METHOD_PUT, $headers, $responseContentType);
    }
    // End put

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     *
     * Curl call with DELETE method
     *
     * @param string $url
     * @param array  $params
     */
    public function delete($url, $params = array(), array $headers = null, $responseContentType = null)
    {
        return $this->call($url, $params, self::METHOD_DELETE, $headers, $responseContentType);
    }
    // End delete

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     *
     * Curl call with RAW method
     *
     * @param string $url
     * @param array  $params
     */
    public function raw($url, array $headers = null, $responseContentType = null)
    {
        return $this->call($url, array(), self::METHOD_RAW, $headers, $responseContentType);
    }
    // End raw

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     *
     * Setting SSL peer's certificate verification
     *
     * @param boolean $ssl_verify_peer
     */
    public function setSSLVerifyPeer($ssl_verify_peer)
    {
        $this->sslVerifyPeer = $ssl_verify_peer;
    }
    // End setSSLVerifyPeer

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     *
     * Setting timeout of call
     *
     * @param integer $timeout -1 to disable
     */
    public function setTimeOut($timeout)
    {
        $this->timeout = $timeout;
    }
    // End setTimeOut

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     *
     * Getting last error of curl call
     */
    public function getLastError()
    {
        return $this->lastError;
    }
    // End getLastError

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     *
     * Getting info of last curl call
     */
    public function getInfo()
    {
        return $this->info;
    }
    // End getInfo

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     *
     * @param array $proxy
     */
    public function setProxy(array $proxy = array())
    {
        if (count($proxy) && !self::isProxy($proxy))
            throw new Exception('Invalid proxy definition. host, username, password, port expected.');

        $this->proxy = $proxy;
    }
    // End setProxy

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     *
     */
    public function setAuthentication($username, $password = '', $authType = self::AUTHTYPE_NONE)
    {
        $this->authentication = array();
        $this->authentication['type'] = $authType;
        if ($authType == self::AUTHTYPE_NONE)
            return;

        $this->authentication['username'] = $username;
        $this->authentication['password'] = $password;
    }
    // End setAuthentication

    /**
     * @param $authentication
     *
     * @return bool
     */
    public static function hasAuthentication($authentication)
    {
        if (!is_array($authentication))
            return false;

        if (!isset($authentication['password']) || !isset($authentication['username']))
            return false;

        if (!isset($authentication['type']))
            $authentication['type'] = self::AUTHTYPE_BASIC;

        if (!in_array($authentication['type'], array(self::AUTHTYPE_BASIC, self::AUTHTYPE_NTLM)))
            return false;

        return true;
    }
    // End hasAuthentication

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Checks if given proxy configuration is valid
     *
     * @param array $proxy
     */
    public static function isProxy(array $proxy = array())
    {
        if (!isset($proxy['host'])
            || !isset($proxy['username'])
            || !isset($proxy['password'])
            || !isset($proxy['port'])
        )
            return false;

        return true;
    }
    // End isProxy

    //////////////////////////////////////////////////////////////////////////////////////
}
// End Curl