<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs;

use PDO;

/**
 *
 * @package blibs
 * @author Fabian Möller  <fab@beibob.de>
 * @author Tobias Lode <tobias@beibob.de>
 *
 */
class Role extends DbObject
{
    /**
     * Tablename
     */
    const TABLE_NAME = 'public.roles';

    /**
     * Viewname
     */
    const VIEW_NAME = 'public.roles_v';

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * nodeId
     */
    private $nodeId;

    /**
     * roleName
     */
    private $roleName;

    /**
     * description
     */
    private $description;

    /**
     * Primary key
     */
    private static $primaryKey = ['nodeId'];

    /**
     * Column types
     */
    private static $columnTypes = ['nodeId'         => PDO::PARAM_INT,
                                        'roleName'       => PDO::PARAM_STR,
                                        'description'    => PDO::PARAM_STR];

    /**
     * Extended column types
     */
    private static $extColumnTypes = [];

    //////////////////////////////////////////////////////////////////////////////////////
    // public
    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Creates a root role
     *
     * @param  string   $ident
     * @param  string   $roleName
     * @param  string   $description
     */
    public static function createRoot($ident, $roleName = '', $description = '')
    {
        $Role = new Role();

        try
        {
            $Role->beginTransaction();

            $Node = NestedNode::createRoot($ident);

            if(!$Node->isInitialized())
                throw new Exception('Can\'t create root node');

            $Role->setNodeId($Node->getId());
            $Role->setRoleName($roleName);
            $Role->setDescription($description);

            if(!$Role->getValidator()->isValid())
                throw new Exception('Can\'t create Role `'.$roleName.'\': '.(string)$Role->getValidator());

            $Role->insert();
            $Role->commit();
        }
        catch(Exception $Exception)
        {
            $Role->rollback();
            throw $Exception;
        }

        return $Role;
    }
    // End createRoot

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Creates the object
     *
     * @param  integer  $nodeId     - nodeId
     * @param  string   $roleName   - roleName
     * @param  string   $description - description
     */
    public static function create(NestedNode $ParentNode, $ident, $roleName, $description = '')
    {
        $Role = new Role();

        try
        {
            $Role->beginTransaction();

            if(!$ParentNode->isInitialized())
                throw new Exception('ParentNode not valid');

            $Node = NestedNode::createAsChildOf($ParentNode, $ident);

            if(!$Node->isInitialized())
                throw new Exception('Can\'t create node');

            $Role->setNodeId($Node->getId());
            $Role->setRoleName($roleName);
            $Role->setDescription($description);

            if(!$Role->getValidator()->isValid())
                throw new Exception('Can\'t create Role `'.$roleName.'\': '.(string)$Role->getValidator());

            $Role->insert();
            $Role->commit();
        }
        catch(Exception $Exception)
        {
            $Role->rollback();
            throw $Exception;
        }

        return $Role;
    }
    // End create

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Inits a `Role' by its primary key
     *
     * @param  integer  $nodeId - nodeId
     * @param  boolean  $force - Bypass caching
     * @return Role
     */
    public static function findByNodeId($nodeId, $force = false)
    {
        if(!$nodeId)
            return new Role();

        $sql = sprintf("SELECT node_id
                             , role_name
                             , description
                          FROM %s
                         WHERE node_id = :nodeId"
                       , self::TABLE_NAME
                       );

        return self::findBySql(get_class(), $sql, ['nodeId' => $nodeId], $force);
    }
    // End findByNodeId

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the Role by rootIdent and roleIdent
     *
     * @param  string  $rootIdent
     * @param  string  $ident
     * @return Role
     */
    public static function findByIdent($rootIdent, $ident, $force = false)
    {
        if(!$rootIdent || !$ident)
            return new Role();

        $sql = sprintf('SELECT r.node_id
                             , r.role_name
                             , r.description
                          FROM %s n
                          JOIN %s r ON n.id = r.root_id
                         WHERE n.ident = :rootIdent
                           AND r.ident = :ident'
                       , NestedNode::TABLE_NAME
                       , self::VIEW_NAME
                       );

        return self::findBySql(get_class(), $sql, ['rootIdent' => $rootIdent,
                                                        'ident'     => $ident], $force);
    }
    // End findByIdent

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the root role by rootIdent
     *
     * @param  string  $rootIdent
     * @return Role
     */
    public static function findRootByIdent($rootIdent, $force = false)
    {
        if(!$rootIdent)
            return new Role();

        $sql = sprintf('SELECT node_id
                             , role_name
                             , description
                          FROM %s
                         WHERE id = root_id
                           AND ident = :rootIdent'
                       , self::VIEW_NAME
                       );

        return self::findBySql(get_class(), $sql, ['rootIdent' => $rootIdent], $force);
    }
    // End findRootByIdent

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the property nodeId
     *
     * @param  integer  $nodeId - nodeId
     * @return
     */
    public function setNodeId($nodeId)
    {
        if(!$this->getValidator()->assertNotEmpty('nodeId', $nodeId))
            return;

        $this->nodeId = (int)$nodeId;
    }
    // End setNodeId

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the property roleName
     *
     * @param  string   $roleName - roleName
     * @return
     */
    public function setRoleName($roleName)
    {
        if(!$this->getValidator()->assertNotEmpty('roleName', $roleName))
            return;

        if(!$this->getValidator()->assertMaxLength('roleName', 100, $roleName))
            return;

        $this->roleName = $roleName;
    }
    // End setRoleName

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the property description
     *
     * @param  string   $description - description
     * @return
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }
    // End setDescription

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the property nodeId
     *
     * @return integer
     */
    public function getNodeId()
    {
        return $this->nodeId;
    }
    // End getNodeId

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the property nodeId
     *
     * @return integer
     */
    public function getId()
    {
        return $this->getNodeId();
    }
    // End getId

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the Node
     *
     * @return NestedNode
     */
    public function getNode()
    {
        return NestedNode::findById($this->nodeId);
    }
    // End getNode

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the property roleName
     *
     * @return string
     */
    public function getRoleName()
    {
        return $this->roleName;
    }
    // End getRoleName

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the property description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
    // End getDescription

    //////////////////////////////////////////////////////////////////////////////////////

       /**
     * Returns true if the node has child nodes
     *
     * @param  -
     * @return boolean
     */
    public function hasChildNodes()
    {
        return $this->getNode()->hasChildNodes();
    }
    // End hasChildNodes

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns a set of child nodes
     *
     * @param  boolean $force
     * @return NestedNodeSet
     */
    public function getChildNodes($force = false, $initValues = [])
    {
        return $this->getNode()->getChildNodes($force, $initValues);
    }
    // End getChildNodes

    //////////////////////////////////////////////////////////////////////////////////////

       /**
     * Returns the level
     *
     * @param  -
     * @return int
     */
    public function getLevel()
    {
        return $this->getNode()->getLevel();
    }
    // End getLevel

    //////////////////////////////////////////////////////////////////////////////////////
    /**
     * Checks, if the object exists
     *
     * @param  integer  $nodeId - nodeId
     * @param  boolean  $force - Bypass caching
     * @return boolean
     */
    public static function exists($nodeId, $force = false)
    {
        return self::findByNodeId($nodeId, $force)->isInitialized();
    }
    // End exists

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Checks, if the object exists
     *
     * @param  integer  $nodeId - nodeId
     * @param  boolean  $force - Bypass caching
     * @return boolean
     */
    public static function existsByIdent($rootIdent, $ident, $force = false)
    {
        return self::findByIdent($rootIdent, $ident, $force)->isInitialized();
    }
    // End existsByIdent

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Updates the object in the table
     *
     * @return boolean
     */
    public function update()
    {
        $sql = sprintf("UPDATE %s
                           SET role_name      = :roleName
                             , description    = :description
                         WHERE node_id = :nodeId"
                       , self::TABLE_NAME
                       );

        return $this->updateBySql($sql,
                                  ['nodeId'        => $this->nodeId,
                                        'roleName'      => $this->roleName,
                                        'description'   => $this->description]
                                  );
    }
    // End update

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Deletes the object from the table
     *
     * @return boolean
     */
    public function delete()
    {
        // just delete the nestedNode
        $result = $this->getNode()->delete();
        DbObjectCache::freeByObject($this);
        return $result;
    }
    // End delete

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns an array with the primary key properties and
     * associates its values, if it's a valid object
     *
     * @param  boolean  $propertiesOnly
     * @return array
     */
    public function getPrimaryKey($propertiesOnly = false)
    {
        if($propertiesOnly)
            return self::$primaryKey;

        $primaryKey = [];

        foreach(self::$primaryKey as $key)
            $primaryKey[$key] = $this->$key;

        return $primaryKey;
    }
    // End getPrimaryKey

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the tablename constant. This is used
     * as interface for other objects.
     *
     * @return string
     */
    public static function getTablename()
    {
        return self::TABLE_NAME;
    }
    // End getTablename

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the columns with their types. The columns may also return extended columns
     * if the first argument is set to true. To access the type of a single column, specify
     * the column name in the second argument
     *
     * @param  boolean  $extColumns
     * @param  mixed    $column
     * @return mixed
     */
    public static function getColumnTypes($extColumns = false, $column = false)
    {
        $columnTypes = $extColumns? array_merge(self::$columnTypes, self::$extColumnTypes) : self::$columnTypes;

        if($column)
            return $columnTypes[$column];

        return $columnTypes;
    }
    // End getColumnTypes

    //////////////////////////////////////////////////////////////////////////////////////
    // protected
    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Inserts a new object in the table
     *
     * @return boolean
     */
    protected function insert()
    {
        $sql = sprintf("INSERT INTO %s (node_id, role_name, description)
                               VALUES  (:nodeId, :roleName, :description)"
                       , self::TABLE_NAME
                       );

        return $this->insertBySql($sql,
                                  ['nodeId'        => $this->nodeId,
                                        'roleName'      => $this->roleName,
                                        'description'   => $this->description]
                                  );
    }
    // End insert

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Inits the object with row values
     *
     * @param  \stdClass $DO - Data object
     * @return boolean
     */
    protected function initByDataObject(\stdClass $DO = null)
    {
        $this->nodeId         = (int)$DO->node_id;
        $this->roleName       = $DO->role_name;
        $this->description    = $DO->description;

        /**
         * Set extensions
         */
    }
    // End initByDataObject
}
// End class Role
