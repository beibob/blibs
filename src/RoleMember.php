<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs;

use PDO;

/**
 *
 * @package blibs
 * @author Fabian Möller  <fab@beibob.de>
 * @author Tobias Lode <tobias@beibob.de>
 *
 */
class RoleMember extends DbObject
{
    /**
     * Tablename
     */
    const TABLE_NAME = 'public.role_members';

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * roleId
     */
    private $roleId;

    /**
     * groupId
     */
    private $groupId;

    /**
     * ceased membership
     */
    private $isCeased;

    /**
     * Primary key
     */
    private static $primaryKey = ['roleId', 'groupId'];

    /**
     * Column types
     */
    private static $columnTypes = ['roleId'         => PDO::PARAM_INT,
                                        'groupId'        => PDO::PARAM_INT,
                                        'isCeased'       => PDO::PARAM_BOOL];

    /**
     * Extended column types
     */
    private static $extColumnTypes = [];

    //////////////////////////////////////////////////////////////////////////////////////
    // public
    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Grants a role membership to a group.
     *
     * There has to be distinguished between an user's group and an ordinary group.
     * With ordinary groups inheritation of role memberships aren't possible, because
     * a group can't be a member of another group.
     *
     * With user-groups inheritation is possible. Therefore extra checks are
     * required and the special flag isCeased has to be involved to avoid
     * a role membership conflict.
     *
     * @param  numeric $roleId
     * @param  numeric $groupId
     * @return boolean
     */
    public static function grant($roleId, $groupId)
    {
        if(!$roleId || !$groupId || self::isGranted($roleId, $groupId))
            return false;

        /**
         * Try to get the membership
         */
        $RoleMember = RoleMember::findByPk($roleId, $groupId);

        $Group = Group::findById($groupId);

        if($Group->isUsergroup())
        {
            /**
             * Check if the membership already exists
             */
            if($RoleMember->isInitialized())
            {
                /**
                 * If the membership exists and is ceased, it is inherited by another group
                 * and was once overwritten for this usergroup
                 * Just delete it and return (by assuming that the other group still grants
                 * the membership - is that really alright?)
                 */
                if($RoleMember->isCeased())
                    return $RoleMember->delete();
            }

            /**
             * Check if one of the user's groups inherits the membership
             * onto the user. In that case there is nothing to do.
             *
             * If no group grants the membership to the user,
             * create a new permission on the user's usergroup.
             */
            $User = User::findByGroupId($Group->getId());

            $GroupMemberSet = GroupMemberSet::find(['user_id' => $User->getId()]);

            foreach($GroupMemberSet as $GroupMember)
                if(self::exists($roleId, $GroupMember->getGroupId()))
                    return false;
        }

        /**
         * If the group is not a user group, then can not be an
         * ceased membership, it has to be granted. And then is
         * nothing to do here
         */
        if($RoleMember->isInitialized())
            return false;

        /**
         * Create the membership
         */
        self::create($roleId, $groupId);
        return true;
    }
    // End grant

    ///////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Removes a role membership
     *
     * @param  numeric $roleId
     * @param  numeric $groupId
     * @return boolean -
     */
    public static function cease($roleId, $groupId)
    {
        if(!$roleId || !$groupId || !self::isGranted($roleId, $groupId))
            return false;

        /**
         * Get role membership
         */
        $RoleMember = RoleMember::findByPk($roleId, $groupId);

        /**
         * Check type of group
         */
        $Group = Group::findById($groupId);
        if(!$Group->isUsergroup())
        {
            /**
             * If group is not a usergroup and no memberships exists
             * there is nothing to do
             */
            if(!$RoleMember->isInitialized())
                return false;

            /**
             * Remove the membership
             */
            $RoleMember->delete();
            return true;
        }

        /**
         * Nothing to do, if the membership exists and is already ceased
         */
        if($RoleMember->isInitialized() &&
           $RoleMember->isCeased())
            return false;

        /**
         * check if one of the groups the user is member of
         * inherits and grants the permission to the user
         */
        $User = User::findByGroupId($Group->getId());
        $GroupMemberSet = GroupMemberSet::find(['user_id' => $User->getId()]);

        foreach($GroupMemberSet as $GroupMember)
        {
            if(self::exists($roleId, $GroupMember->getGroupId()))
            {
                if($RoleMember->isInitialized())
                {
                    $RoleMember->setIsCeased(true);
                    $RoleMember->update();
                }
                else
                    RoleMember::create($roleId, $groupId, true);

                return true;
            }
        }

        if(!$RoleMember->isInitialized())
            return false;

        $RoleMember->delete();
        return true;
    }
    // End cease

    ///////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Inits a `RoleMember' by its primary key
     *
     * @param  integer  $roleId - roleId
     * @param  integer  $groupId - groupId
     * @param  boolean  $force  - Bypass caching
     * @return RoleMember
     */
    public static function findByPk($roleId, $groupId, $force = false)
    {
        if(!$roleId || !$groupId)
            return new RoleMember();

        $sql = sprintf("SELECT role_id
                             , group_id
                             , is_ceased
                          FROM %s
                         WHERE role_id = :roleId
                           AND group_id = :groupId"
                       , self::TABLE_NAME
                       );

        return self::findBySql(get_class(), $sql, ['roleId' => $roleId, 'groupId' => $groupId], $force);
    }
    // End findByPk

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the property roleId
     *
     * @param  integer  $roleId - roleId
     * @return
     */
    public function setRoleId($roleId)
    {
        if(!$this->getValidator()->assertNotEmpty('roleId', $roleId))
            return;

        $this->roleId = (int)$roleId;
    }
    // End setRoleId

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the property groupId
     *
     * @param  integer  $groupId - groupId
     * @return
     */
    public function setGroupId($groupId)
    {
        if(!$this->getValidator()->assertNotEmpty('groupId', $groupId))
            return;

        $this->groupId = (int)$groupId;
    }
    // End setGroupId

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the property isCeased
     *
     * @param  boolean  $isCeased - ceased membership
     * @return
     */
    public function setIsCeased($isCeased = false)
    {
        $this->isCeased = (bool)$isCeased;
    }
    // End setIsCeased

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the property roleId
     *
     * @return integer
     */
    public function getRoleId()
    {
        return (int)$this->roleId;
    }
    // End getRoleId

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the associated Role by property roleId
     *
     * @param  boolean  $force
     * @return Role
     */
    public function getRole($force = false)
    {
        return Role::findByNodeId($this->roleId, $force);
    }
    // End getRole

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the property groupId
     *
     * @return integer
     */
    public function getGroupId()
    {
        return (int)$this->groupId;
    }
    // End getGroupId

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the associated Group by property groupId
     *
     * @param  boolean  $force
     * @return Group
     */
    public function getGroup($force = false)
    {
        return Group::findById($this->groupId, $force);
    }
    // End getGroup

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the property isCeased
     *
     * @return boolean
     */
    public function isCeased()
    {
        return (bool)$this->isCeased;
    }
    // End isCeased

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Checks if a role membership exists for the given groupId. This method
     * also respects group inheritations
     *
     * @param  integer  $roleId  - roleId
     * @param  integer  $groupId - groupId
     * @param  boolean  $force  - Bypass caching
     * @return boolean
     */
    public static function isGranted($roleId, $groupId)
    {
        if(!$roleId || !$groupId)
            return false;

        $sql = sprintf('SELECT bool_and(NOT is_ceased) AS result
                          FROM %s n
                          JOIN %s nn ON n.root_id = nn.root_id AND nn.lft BETWEEN n.lft AND n.rgt
                          JOIN %s m  ON nn.id = m.role_id
                          JOIN %s g  ON g.id  = m.group_id
                         WHERE n.id = :roleId
                           AND g.id IN (SELECT gm.group_id
                                          FROM %s gm
                                          JOIN %s u ON gm.user_id = u.id
                                         WHERE u.group_id = :groupId1 UNION SELECT :groupId2)'
                       , NestedNode::TABLE_NAME
                       , NestedNode::TABLE_NAME
                       , self::TABLE_NAME
                       , Group::TABLE_NAME
                       , GroupMember::TABLE_NAME
                       , User::TABLE_NAME
                       );

        $returnValues = [];
        self::executeSql(get_class(), $sql, ['roleId' => $roleId,
                                     'groupId1' => $groupId,
                                     'groupId2' => $groupId], $returnValues);

        if(!count($returnValues))
            return false;

        return (bool)$returnValues[0]->result;
    }
    // End isGranted

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Checks, if the object exists
     *
     * @param  integer  $roleId - roleId
     * @param  integer  $groupId - groupId
     * @param  boolean  $force  - Bypass caching
     * @return boolean
     */
    public static function exists($roleId, $groupId, $force = false)
    {
        return self::findByPk($roleId, $groupId, $force)->isInitialized();
    }
    // End exists

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Updates the object in the table
     *
     * @return boolean
     */
    public function update()
    {
        $sql = sprintf("UPDATE %s
                           SET is_ceased = :isCeased
                         WHERE role_id = :roleId
                           AND group_id = :groupId"
                       , self::TABLE_NAME
                       );

        return $this->updateBySql($sql,
                                  ['roleId'        => $this->roleId,
                                        'groupId'       => $this->groupId,
                                        'isCeased'      => $this->isCeased]
                                  );
    }
    // End update

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Deletes the object from the table
     *
     * @return boolean
     */
    public function delete()
    {
        try
        {
            $this->beginTransaction();

            /**
             * @todo: Check if the group is a userGroup.
             * If not, check all members of this group that
             * have an ceased permission on this role
             */
            $Group = Group::findById($this->groupId);

            if($Group->isInitialized() && !$Group->isUserGroup())
            {
                $UserSet = UserSet::findByGroupId($Group->getId());

                foreach($UserSet as $User)
                {
                    $Member = RoleMember::findByPk($this->roleId, $User->getGroupId());

                    if($Member->isCeased())
                        $Member->delete();
                }
            }

            /**
             * Delete membership
             */
            $sql = sprintf("DELETE FROM %s
                                  WHERE role_id = :roleId
                                    AND group_id = :groupId"
                           , self::TABLE_NAME
                           );

            $result = $this->deleteBySql($sql,
                                         ['roleId'  => $this->roleId,
                                               'groupId' => $this->groupId]
                                         );

            $this->commit();
        }
        catch(Exception $Exception)
        {
            $this->rollback();
            throw $Exception;
        }

        return $result;
    }
    // End delete

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns an array with the primary key properties and
     * associates its values, if it's a valid object
     *
     * @param  boolean  $propertiesOnly
     * @return array
     */
    public function getPrimaryKey($propertiesOnly = false)
    {
        if($propertiesOnly)
            return self::$primaryKey;

        $primaryKey = [];

        foreach(self::$primaryKey as $key)
            $primaryKey[$key] = $this->$key;

        return $primaryKey;
    }
    // End getPrimaryKey

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the tablename constant. This is used
     * as interface for other objects.
     *
     * @return string
     */
    public static function getTablename()
    {
        return self::TABLE_NAME;
    }
    // End getTablename

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the columns with their types. The columns may also return extended columns
     * if the first argument is set to true. To access the type of a single column, specify
     * the column name in the second argument
     *
     * @param  boolean  $extColumns
     * @param  mixed    $column
     * @return mixed
     */
    public static function getColumnTypes($extColumns = false, $column = false)
    {
        $columnTypes = $extColumns? array_merge(self::$columnTypes, self::$extColumnTypes) : self::$columnTypes;

        if($column)
            return $columnTypes[$column];

        return $columnTypes;
    }
    // End getColumnTypes

    //////////////////////////////////////////////////////////////////////////////////////
    // protected
    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Creates the object
     *
     * @param  integer  $roleId - roleId
     * @param  integer  $groupId - groupId
     * @param  boolean  $isCeased - isCeased membership
     */
    protected static function create($roleId, $groupId, $isCeased = false)
    {
        $RoleMember = new RoleMember();
        $RoleMember->setRoleId($roleId);
        $RoleMember->setGroupId($groupId);
        $RoleMember->setIsCeased($isCeased);

        if($RoleMember->getValidator()->isValid())
        {
            try
            {
                $RoleMember->beginTransaction();
                $RoleMember->insert();

                /**
                 * Remove explicit parent memberships
                 */
                $RoleNode = Role::findByNodeId($roleId)->getNode();
                $NodeSet  = NestedNodeSet::findParentsOf($RoleNode, true);

                foreach($NodeSet as $ParentNode)
                    RoleMember::findByPk($ParentNode->getId(), $groupId)->delete();

                $RoleMember->commit();
            }
            catch(Exception $Exception)
            {
                $RoleMember->rollback();
                throw $Exception;
            }
        }

        return $RoleMember;
    }
    // End create

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Inserts a new object in the table
     *
     * @return boolean
     */
    protected function insert()
    {
        $sql = sprintf("INSERT INTO %s (role_id, group_id, is_ceased)
                               VALUES  (:roleId, :groupId, :isCeased)"
                       , self::TABLE_NAME
                       );

        return $this->insertBySql($sql,
                                  ['roleId'        => $this->roleId,
                                        'groupId'       => $this->groupId,
                                        'isCeased'      => $this->isCeased]
                                  );
    }
    // End insert

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Inits the object with row values
     *
     * @param  \stdClass $DO - Data object
     * @return boolean
     */
    protected function initByDataObject(\stdClass $DO = null)
    {
        $this->roleId         = (int)$DO->role_id;
        $this->groupId        = (int)$DO->group_id;
        $this->isCeased       = (bool)$DO->is_ceased;

        /**
         * Set extensions
         */
    }
    // End initByDataObject
}
// End class RoleMember
