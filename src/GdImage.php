<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs;

use Imagick;

/**
 * @package blibs
 * @author Fabian Möller  <fab@beibob.de>
 * @author Tobias Lode <tobias@beibob.de>
 *
 */
class GdImage
{
    /**
     * Der Pfad zum Bild mit dem gearbeitet wird
     */
    private $imagePath = null;

    /**
     * Das GDImage-Object mit dem gearbeitet wird
     */
    private $Image = null;

    /**
     *
     */
    private $imageInfo = [];

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Factory
     *
     * @param  string $filepath
     * @param  string $filemode
     * @return File
     */
    public static function factory($filepath = false)
    {
        if (is_object($filepath) && $filepath instanceof File)
            $filepath = $filepath->getFilepath ();

        return new GdImage($filepath);
    }
    // End factory

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Setz den Pfad zum Bild mit dem gearbeitet werden soll,
     * Lädt das Bild in ein GDImage-Object falls ein valider Pfad gegeben
     * wurde
     *
     * @param string $imagePath
     */
    public function __construct($imagePath = null)
    {
        if ($imagePath)
            $this->setImagePath($imagePath);
    }
    // End __construct

    ///////////////////////////////////////////////////////////////////////////

    /**
     *
     */
    public function destroy()
    {
        if (is_resource($this->Image))
            imagedestroy($this->Image);
    }
    // End destroy

    ///////////////////////////////////////////////////////////////////////////

    /**
     * Setzt den Pfad auf das Bild mit dem gearbeitet werden soll, falls dieser
     * Existiert
     */
    public function setImagePath($imagePath)
    {
        if (!file_exists($imagePath))
            return false;

        $this->imagePath = $imagePath;
        $this->openImage();
    }
    // End setImagePath

    ///////////////////////////////////////////////////////////////////////////

    /**
     * Returns true if the image is a valid resource
     *
     * @param  -
     * @return boolean
     */
    public function isInitialized()
    {
        return is_resource($this->Image);
    }
    // End isInitialized

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Liefert die interne Image-Resource um irgendwas anderes mit dem Ding
     * zu machen, was die Lib zur Zeit nicht unterstützt
     *
     * @return object
     */
    public function getImage()
    {
        return $this->Image;
    }
    // End getImage

    ///////////////////////////////////////////////////////////////////////////

    /**
     * Speichert das Bild im vorgebenen Mimetype.
     *
     * @param string $path    - Der Pfad in dem das Bild landen soll
     * @param string $mime    - Derzeit image/jpeg, image/png oder image/gif
     * @param string $quality - Wenn mime=image/jpeg, dann bestimmt das die
     *                          Qualität des jpgs
     *
     * @return bool
     */
    public function save($path, $mime = 'image/jpeg', $quality = 100)
    {
        if(!$this->isInitialized() || !is_writable(dirname($path)))
            return false;

        switch ($mime)
        {
            case 'image/jpeg':
                return imagejpeg($this->Image, $path, $quality);
            break;
            case 'image/png':
                return imagepng($this->Image, $path);
            break;
            case 'image/gif':
                return imagegif($this->Image, $path);
            break;
            default:
                throw new Exception("Mimetype `" . $mime . "' unknown!");
        }
    }
    // End save

    ///////////////////////////////////////////////////////////////////////////

    /**
     * Dreht das Bild
     *
     * @param int   $degree  - Winkel
     * @param mixed $bgColor - Entweder der Zeiger (int) auf eine bereits
     *                         allokiert Farbe, oder eine Angabe der Form
     *                         #000000, #000000; oder 0,0,0
     *                         für den RGB-Wert der Farbe mit der durch die
     *                         Drehung entstehende neue Leinwandfläche
     *                         gefüllt wird
     * @param bool - $ignore - Keine Ahnung! Siehe PHP-Docu!
     *
     */
    public function rotate($degree, $bgColor = null, $ignoreTransparent = null)
    {
        if(!$this->isInitialized())
            return false;

        if (!is_numeric($bgColor))
            $bgColor = $this->allocateColor($bgColor);

        $this->Image = imagerotate($this->Image, $degree, $bgColor, $ignoreTransparent);
        return $this->isInitialized();
    }
    // End rotate

    ///////////////////////////////////////////////////////////////////////////

    /**
     * Schneidet das Bild so zurecht, dass es dem geforderten Seitenverhältnis entspricht
     */
    public function crop($ratio)
    {
        if(!$this->isInitialized())
            return false;

        $imageInfo = $this->getImageInfo();
        if (round($imageInfo['ratio'], 2) == round($ratio, 2))
            return true;

        // Umrechnung ist für landscape richtig, daher vertauschen wir vor- und nach der
        // Umrechnung breite und Höhe von portrait bildern
        if ($imageInfo['orientation'] == 'portrait')
        {
            $help = $imageInfo['width'];
            $imageInfo['width'] = $imageInfo['height'];
            $imageInfo['height'] = $help;
        }
        if ($imageInfo['ratio'] > $ratio)
        {
            //höhe ändern
            $cropHeight = ceil($imageInfo['width']*$ratio);

            $srcY = ceil(($imageInfo['height'] - $cropHeight)/2);
            $srcHeight = $cropHeight;
            $destHeight = $cropHeight;

            $srcX = 0;
            $srcWidth = $imageInfo['width'];
            $destWidth = $imageInfo['width'];
        }
        else
        {
            //breite ändern
            $cropWidth = ceil($imageInfo['height']/$ratio);

            $srcX = ceil(($imageInfo['width'] - $cropWidth)/2);
            $srcWidth = $cropWidth;
            $destWidth = $cropWidth;

            $srcY = 0;
            $srcHeight = $imageInfo['height'];
            $destHeight = $imageInfo['height'];
        }

        if ($imageInfo['orientation'] == 'portrait')
        {
            $h = $srcHeight;
            $srcHeight = $srcWidth;
            $srcWidth = $h;

            $h = $destHeight;
            $destHeight = $destWidth;
            $destWidth = $h;

            $h = $srcY;
            $srcY = $srcX;
            $srcX = $h;
        }

        $cropedImage = imagecreatetruecolor($destWidth, $destHeight);
        imagecopyresampled($cropedImage, $this->Image, 0, 0, $srcX, $srcY, $destWidth, $destHeight, $srcWidth, $srcHeight);
        $this->Image = $cropedImage;
        $this->getImageInfo();

        return $this->isInitialized();
    }
    // End cropImage

    ///////////////////////////////////////////////////////////////////////////

    /**
     * Skaliert das Bild auf die übergebenen Maximalabmessungen
     *
     * @param int $maxWidth - Maximale Breite
     * @param int $maxHeight - Maximale Höhe
     */
    public function scaleToMaxSize($maxWidth, $maxHeight)
    {
        if(!$this->isInitialized())
            return false;

        $imageInfo = $this->getImageInfo();

        if ($imageInfo['width'] <= $maxWidth && $imageInfo['height'] <= $maxHeight)
            return true;

        $destWidth = 0;
        $destHeight = 0;
        if($imageInfo['orientation'] == 'portrait')
        {
            //Portrait format
            $destHeight = $maxHeight;
            $destWidth  = ceil(($destHeight * $imageInfo['width']) / $imageInfo['height']);

            if ($destWidth > $maxWidth)
            {
                $destWidth  = $maxWidth;
                $destHeight = ceil(($destWidth * $imageInfo['height']) / $imageInfo['width']);
            }
        }
        elseif($imageInfo['orientation'] == 'landscape')
        {
            //Landscape format
            $destWidth  = $maxWidth;
            $destHeight = ceil(($destWidth * $imageInfo['height']) / $imageInfo['width']);

            if ($destHeight > $maxHeight)
            {
                $destHeight = $maxHeight;
                $destWidth = ceil(($destHeight * $imageInfo['width']) / $imageInfo['height']);
            }
        }
        else
        {
            //Quadrat
            if ($imageInfo['width'] > $maxWidth)
                $destWidth = $destHeight = $maxWidth;
            else
                $destWidth = $destHeight = $maxHeight;
        }

        $DestImage = imagecreatetruecolor($destWidth, $destHeight);
        imagecopyresampled($DestImage, $this->Image, 0, 0, 0, 0, $destWidth, $destHeight, $imageInfo['width'], $imageInfo['height']);
        $this->Image = $DestImage;
        $this->getImageInfo();

        return $this->isInitialized();
    }
    // End scaleToMaxSize

    //////////////////////////////////////////////////////////////////////////////////////
    
    /**
     * Scales image without aspect ratio
     * 
     * @param type $width
     * @param type $height
     */
    public function scale($width, $height)
    {
        if(!$this->isInitialized())
            return false;

        $imageInfo = $this->getImageInfo();
        
        $DestImage = imagecreatetruecolor($width, $height);
        imagecopyresampled($DestImage, $this->Image, 0, 0, 0, 0, $width, $height, $imageInfo['width'], $imageInfo['height']);
        
        $this->Image = $DestImage;

        return $this->isInitialized();
    }
    // End scale
    
    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Liefert ein Array mit interassanten Infos über das Bild
     *
     * @param string $key - optionaler key, falls nur ein Wert zurückgegeben werden soll   
     * @return mixed
     */
    public function getImageInfo($key = null)
    {
        if (!file_exists($this->imagePath))
            return [];

        $sizeInfo = getimagesize($this->imagePath);
        if ($this->isInitialized())
        {
            $sizeInfo[0] = imagesx($this->Image);
            $sizeInfo[1] = imagesy($this->Image);
        }

        $this->imageInfo = [];
        $this->imageInfo['srcPath'] = $this->imagePath;
        $this->imageInfo['width'] = $sizeInfo[0];
        $this->imageInfo['height'] = $sizeInfo[1];
        $this->imageInfo['type'] = $sizeInfo[2];
        $this->imageInfo['mime'] = $sizeInfo['mime'];
        $this->imageInfo['bits'] = $sizeInfo['bits'];
        if (isset($sizeInfo['channels']))
            $this->imageInfo['channels'] = $sizeInfo['channels'];

        if ($this->imageInfo['height'] == $this->imageInfo['width'])
            $this->imageInfo['orientation'] = 'quadrat';
        else
            $this->imageInfo['orientation'] = ($this->imageInfo['height'] > $this->imageInfo['width']) ? 'portrait' : 'landscape';

        if($this->imageInfo['width'] == 0 ||
           $this->imageInfo['height'] == 0)
            return $this->imageInfo;

        $this->imageInfo['ratio'] = ($this->imageInfo['height'] > $this->imageInfo['width']) ? round($this->imageInfo['width']/$this->imageInfo['height'], 2) : round($this->imageInfo['height']/$this->imageInfo['width'], 2);

        if (is_null($key) || !isset($this->imageInfo[$key]))
            return $this->imageInfo;
        else
            return $this->imageInfo[$key];
    }
    // End getImageInfo

    ///////////////////////////////////////////////////////////////////////////
    // protected
    ///////////////////////////////////////////////////////////////////////////

    /**
     * Öffnet das Bild als ImageGD-Object
     */
    protected function openImage()
    {
        $this->getImageInfo();

        switch ($this->imageInfo['mime'])
        {
            case "image/jpeg":
                if (!function_exists("imagecreatefromjpeg"))
                    throw new Exception("jpg-support is disabled! I need it!");

                $this->Image = @imagecreatefromjpeg($this->imagePath);
            break;
            case "image/png":
                $this->Image = @imagecreatefrompng($this->imagePath);
            break;
            case "image/gif":
                $this->Image = @imagecreatefromgif($this->imagePath);
            break;
            default:
                throw new Exception("Unknown Imagetype `" . $this->imageInfo['mime'] . "'");
            break;
        }
    }
    // End openImage

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Allokiert eine Farbe an Hande einer Angabe der Form:
     * #000000;
     * #000000
     * 0,0,0
     *
     * verkürzte Varianten der hexadezimalen Angaben (#ccc) werden derzeit
     * nicht unterstützt und münden in die allokierung eines tristen
     * schwarz
     *
     * @param string - Die Farbangabe
     *
     * @return int - Der interne Zeiger auf die allokierte Farbe
     */
    protected function allocateColor($colorString)
    {
        if (utf8_substr($colorString, -1) == ';')
            $colorString = utf8_substr($colorString, 0, utf8_strlen($colorString) - 1);

        $r = 0;
        $g = 0;
        $b = 0;

        if (utf8_substr($colorString, 0, 1) == '#' && utf8_strlen($colorString) == 7)
        {
            $r = hexdec(utf8_substr($colorString, 1, 2));
            $g = hexdec(utf8_substr($colorString, 3, 2));
            $b = hexdec(utf8_substr($colorString, 5, 2));
        }
        else
        {
            $parts = explode(',', $colorString);
            if (count($parts) == 3)
            {
                $r = $parts[0];
                $g = $parts[1];
                $b = $parts[2];
            }
        }

        if (is_numeric($r) && is_numeric($g) && is_numeric($b))
            return imagecolorallocate($this->Image, $r, $g, $b);
        else
            return imagecolorallocate($this->Image, 0, 0, 0);
    }
    // End allocateColor

    ///////////////////////////////////////////////////////////////////////////
}
///////////////////////////////////////////////////////////////////////////
// Replacement of imagerotate if php is not compiled against bundled version
///////////////////////////////////////////////////////////////////////////
if ( !function_exists( 'imagerotate' ) ) {
    function imagerotate( $source_image, $angle, $bgd_color ) {
        $angle = 360-$angle; // GD rotates CCW, imagick rotates CW
        $temp_src = '/tmp/temp_src.png';
        $temp_dst = '/tmp/temp_dst.png';
        if (!imagepng($source_image,$temp_src)){
            return false;
        }
        $imagick = new Imagick();
        $imagick->readImage($temp_src);
        $imagick->rotateImage(new ImagickPixel($bgd_color?$bgd_color:'black'), $angle);
        $imagick->writeImage($temp_dst);
        $result = imagecreatefrompng($temp_dst);
        unlink($temp_dst);
        unlink($temp_src);
        return $result;
    }
}
///////////////////////////////////////////////////////////////////////////
// Replacement of imagerotate if php is not compiled against bundled version
///////////////////////////////////////////////////////////////////////////
// End GdImage
