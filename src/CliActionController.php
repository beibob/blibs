<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs;

use Beibob\Blibs\Interfaces\ApplicationController;

/**
 * An ActionController class that handles a web page
 *
 * @package blibs
 * @author Tobias Lode <tobias@beibob.de>
 * @author     Fabian M�ller <fab@beibob.de>
 *
 */
class CliActionController extends ActionController implements ApplicationController
{
    /**
     * Session
     */
    protected $Session;

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Constructs the controller and handles an action
     *
     * @param  -
     * @return ActionController
     */
    public function __construct()
    {
        /**
         * Check if application type
         */
        /* if(Environment::getInstance()->getApplicationType() != APP_TYPE_CLI) */
        /*     throw new Exception('Instances of CliActionController ('.get_class($this).') can only be processed in cli mode'); */

        /**
         * Call parent constructor
         */
        parent::__construct();

        /**
         * Init a session
         */
        $this->Session = $this->FrontController->getSession();

        /**
         * Sets an empty default view
         */
        $View = $this->setView(new TextView());
    }
    // End __construct

    //////////////////////////////////////////////////////////////////////////////////////

}
// End PageActionController
