<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs;

/**
 *
 * @package blibs
 * @author     Fabian M�ller  <fab@beibob.de>
 * @author Tobias Lode <tobias@beibob.de>
 *
 */
class CaptchaImage
{
    /**
     * Image directory
     */
    private $imageDir = '/tmp/';

    /**
     * Image width
     */
    private $imageWidth = 200;

    /**
     * Image height
     */
    private $imageHeight = 50;

    /**
     * Font path
     */
    private $fontPath;

    /**
     * Font left
     */
    private $fontLeft = 15;

    /**
     * Font top
     */
    private $fontTop = 35;

    /**
     * Font size
     */
    private $fontSize = 24;

    /**
     * Font padding
     */
    private $fontPadding = 25;

    /**
     * Font color
     */
    private $fontColor = ['r' => 0, 'g' => 0, 'b' => 0];

    /**
     * Max angle
     */
    private $maxAngle = 25;

    /**
     * Opacity
     */
    private $opacity = 25;

    /**
     * Background color
     */
    private $bgColor = ['r' => 147, 'g' => 214, 'b' => 0];

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Constructor
     *
     * @param  string $fontPath
     * @return -
     */
    public function __construct($fontPath)
    {
        $this->setFontPath($fontPath);
    }
    // End __construct

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the image directory
     *
     * @param  string $path
     * @return -
     */
    public function setImageDir($path  = '/tmp/')
    {
        $this->imageDir = File::normalizePath($path, true);
    }
    // End setImageDir

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the image width
     *
     * @param  int $width
     * @return -
     */
    public function setImageWidth($width = 150)
    {
        $this->imageWidth = $width;
    }
    // End setImageWidth

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the image height
     *
     * @param  int $height
     * @return -
     */
    public function setImageHeight($height = 50)
    {
        $this->imageHeight = $height;
    }
    // End setImageHeight

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the path to the ttf font to use
     *
     * @param  string $path
     * @return -
     */
    public function setFontPath($fontPath)
    {
        $this->fontPath = $fontPath;
    }
    // End setFontPath

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the font position from left
     *
     * @param  int $left
     * @return -
     */
    public function setFontLeft($left = 15)
    {
        $this->fontLeft = $left;
    }
    // End setFontLeft

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the font position from top
     *
     * @param  int $top
     * @return -
     */
    public function setFontTop($top = 35)
    {
        $this->fontTop = $top;
    }
    // End setFontTop

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the font size in px
     *
     * @param  int $size
     * @return -
     */
    public function setFontSize($size = 24)
    {
        $this->fontSize = $size;
    }
    // End setFontSize

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the font padding
     *
     * @param  int $padding
     * @return -
     */
    public function setFontPadding($padding = 35)
    {
        $this->fontPadding = $padding;
    }
    // End setFontPadding

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the font color
     *
     * @param  int $r
     * @param  int $g
     * @param  int $b
     * @return -
     */
    public function setFontColor($r = 0, $g = 0, $b = 0)
    {
        $this->fontColor = [];
        $this->fontColor['r'] = $r;
        $this->fontColor['g'] = $g;
        $this->fontColor['b'] = $b;
    }
    // End setFontColor

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the max font angle as absolute value
     *
     * @param  int $maxAngle
     * @return -
     */
    public function setMaxAngle($angle = 25)
    {
        $this->maxAngle = $angle;
    }
    // End setMaxAngle

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the opacity of the font
     *
     * @param  int $opacity
     * @return -
     */
    public function setOpacity($opacity = 25)
    {
        $this->opacity = $opacity;
    }
    // End setOpacity

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the background color
     *
     * @param  int $r
     * @param  int $g
     * @param  int $b
     * @return -
     */
    public function setBgColor($r = 255, $g = 255, $b = 255)
    {
        $this->bgColor = [];
        $this->bgColor['r'] = $r;
        $this->bgColor['g'] = $g;
        $this->bgColor['b'] = $b;
    }
    // End setBgColor

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Renders the captcha image and returns the path of the image
     *
     * @param  string $code [optional]
     * @return string
     */
    public function render($captchaCode)
    {
        if(!$this->fontPath)
            throw new Exception('No true type font specified');

        $tmpName = 'captcha_'. IdFactory::getSequelId() . '.jpeg';

        $img_bg   = imagecreate($this->imageWidth, $this->imageHeight);
        $img_font = imagecreate($this->imageWidth, $this->imageHeight);

        $bgcolor1 = ImageColorAllocate($img_bg, 255, 255, 255);
        $bgcolor2 = ImageColorAllocate($img_font, $this->bgColor['r'], $this->bgColor['g'], $this->bgColor['b']);
        $fontcolor = ImageColorAllocate($img_font, $this->fontColor['r'], $this->fontColor['g'], $this->fontColor['b']);

        imagecolortransparent($img_bg,   $bgcolor1);
        imagecolortransparent($img_font, $bgcolor2);

        $colors = [];

        for($i = 0; $i <= 64; $i++)
        {
            $greyval = rand(160,255);
            $colors[$i] = ImageColorAllocate ($img_bg, $greyval, $greyval, $greyval);
        }

        for($x = 0; $x < $this->imageWidth; $x++)
        {
            for($y = 0; $y < $this->imageHeight; $y++)
            {
                $greyval = rand(0,64);
                imagesetpixel ( $img_bg, $x, $y, $colors[$greyval]);
            }
        }

        $len = utf8_strlen($captchaCode);
        $fontLeft = $this->fontLeft;

        for($i = 0; $i < $len; $i++)
        {
            $angle = rand(0, $this->maxAngle);
            $dir   = rand(1,2);
            $angle = $dir == 1? $angle : (-1) * $angle;

            imagettftext ($img_font, $this->fontSize, $angle, $fontLeft, $this->fontTop, $fontcolor, $this->fontPath, $captchaCode{$i});
            $fontLeft = $fontLeft + $this->fontPadding;
        }

        imagecopymerge ($img_bg, $img_font, 0, 0, 0, 0, $this->imageWidth, $this->imageHeight, $this->opacity);
        imagejpeg($img_bg, $this->imageDir . $tmpName);

        ImageDestroy ($img_font);
        ImageDestroy ($img_bg);

        return $this->imageDir . $tmpName;
    }
    // End render

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns a random captcha code
     *
     * @param  -
     * @return string
     */
    public static function getCaptchaCode($length = 6)
    {
        /**
         * list all possible characters, similar looking characters
         * and vowels have been removed
         */
        $characters = '23456789bcdfghjkmnprstvwxyzBCDFGHJKMNPRSTVWXYZ';

        $code = '';
        for($i = 0; $i < $length; $i++)
            $code .= utf8_substr($characters, mt_rand(0, utf8_strlen($characters) - 1), 1);

        return $code;
    }
    // End getCaptchaCode

    //////////////////////////////////////////////////////////////////////////////////////
}
// End CaptchaImage
