<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */

namespace Beibob\Blibs;

use Beibob\Blibs\Interfaces\Viewable;
use Beibob\Blibs\Interfaces\ViewPostProcessingPlugin;

/**
 * Bindet CSS-Dateien ein
 *
 * @package blibs
 * @author  Tobias Lode <tobias@beibob.de>
 * @author  Fabian Möller <fab@beibob.de>
 *
 */
class CssLoader implements ViewPostProcessingPlugin
{
    /**
     * Singleton instance
     */
    private static $Instance;

    /**
     * Liste registrierter CSS-Dateien.
     */
    protected static $registeredFiles = [];

    protected static $medias = [];

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Constructs the CssLoader singleton
     *
     * @return CssLoader
     */
    public static function getInstance()
    {
        if (!self::$Instance) {
            self::$Instance = new CssLoader();
        }

        return self::$Instance;
    }
    // End getInstance

    //////////////////////////////////////////////////////////////////////////////////////

    public function append($module, $cssUrl, $media = 'all', $baseCssUrl = null)
    {
        self::register($module, $cssUrl, $media, $baseCssUrl, false);
    }

    public function prepend($module, $cssUrl, $media = 'all', $baseCssUrl = null)
    {
        self::register($module, $cssUrl, $media, $baseCssUrl, true);
    }

    /**
     * Registriert eine CSS-Datei
     *
     * @param string $module    - Das Modul das die JS-Datei liefert
     * @param string $cssUrl    - Die Url zur einzubindenen CSS-Datei.
     *                          Relativ zur baseUrl aus der config
     */
    public function register($module, $cssUrl, $media = 'all', $baseCssUrl = null, bool $prepend = false)
    {
        if (null === $baseCssUrl) {
            $config     = Environment::getInstance()->getConfig();
            $baseCssUrl = $config->cssUrl;
        }

        $cssUrl                         = join('/', [$baseCssUrl, $module, $cssUrl]);
        self::$registeredFiles[$cssUrl] = $prepend;
        self::$medias[$cssUrl]          = $media;
    }
    // End register

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Unregistriert eine CSS-Datei
     *
     * @param string $module    - Das Modul das die JS-Datei liefert
     * @param string $cssUrl    - Die Url zur nicht mehr einzubindenen CSS-Datei.
     *                          Relativ zur baseUrl aus der config
     */
    public function unRegister($module, $cssUrl)
    {
        $Config = Environment::getInstance()->getConfig();
        $cssUrl = join('/', [$Config->cssUrl, $module, $cssUrl]);

        if (isset(self::$registeredFiles[$cssUrl])) {
            unset(self::$registeredFiles[$cssUrl]);
            unset(self::$medias[$cssUrl]);
        }
    }
    // End unRegister

    /**
     *
     */
    public function unregisterAll()
    {
        self::$registeredFiles = [];
        self::$medias          = [];
    }

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Hängt alle registierten JS-Dateien als script-Tags in den
     * Head der übergeben Seite. Wird von der Plugin-Schnittstelle
     * des FrontControllers aufgerufen
     *
     * @param  Viewable $View
     * @return -
     */
    public function doViewPostProcessing(Viewable $BaseView)
    {
        if ($BaseView instanceof XmlDocument) {
            $Head = $BaseView->getElementsByTagName('head')->item(0);
            if (!$Head) {
                return;
            }

            uasort(self::$registeredFiles, function ($a, $b) { return $b <=> $a; });

            foreach (self::$registeredFiles as $cssUrl => $prepend) {
                $Link = $BaseView->createElement('link');
                $Link->setAttribute('rel', 'stylesheet');
                $Link->setAttribute('type', 'text/css');
                $Link->setAttribute('media', self::$medias[$cssUrl]);
                $Link->setAttribute('href', $cssUrl);
                $Head->appendChild($Link);
            }
        }
    }
    // End doViewPostProcessing

    //////////////////////////////////////////////////////////////////////////////////////
    // protected
    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Constructor
     * Registriert sich selbst an der Plugin-Schnittstelle des FrontControllers
     */
    protected function __construct()
    {
        $FrontController = FrontController::getInstance();
        $FrontController->registerViewPostProcessingPlugin($this);
    }
    // End __construct

    //////////////////////////////////////////////////////////////////////////////////////
}
// End CssLoader
