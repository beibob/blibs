<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs;

/**
 * Deals with authenication
 *
 * @package blibs
 * @author     Fabian Möller  <fab@beibob.de>
 * @author Tobias Lode <tobias@beibob.de>
 *
 */
class Auth
{
    //////////////////////////////////////////////////////////////////////////////////////
    // THESE BOTH VALUES NEED TO BE SET - NO SETTERS AND GETTERS FOR SECURITY REASONS !!!
    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Value of authentication name -
     * AuthName is always treated case insensitive
     */
    public $authName;

    /**
     * Value of authentication key
     */
    public $authKey;

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * authentication method
     */
    protected $authMethod = self::METHOD_NONE;

    /**
     * Name of login name property
     */
    protected $nameProperty;

    /**
     * Name of key property
     */
    protected $keyProperty;

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Authentication methods
     */
    const METHOD_NONE     = 0;
    const METHOD_PLAIN    = 1;
    const METHOD_DES      = 2;
    const METHOD_MD5      = 3;
    const METHOD_BLOWFISH = 4;

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Constructs the auth object
     *
     * @param  -
     * @return object
     */
    public function __construct($authMethod = self::METHOD_MD5, $nameProperty = 'authName', $keyProperty = 'authKey')
    {
        $this->setAuthMethod($authMethod);
        $this->setNameProperty($nameProperty);
        $this->setKeyProperty($keyProperty);
    }
    // End __construct

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the authentication method
     *
     * @param  integer $method
     * @return -
     */
    public function setAuthMethod($authMethod = self::METHOD_MD5)
    {
        $this->authMethod = $authMethod;
    }
    // End setAuthMethod

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the name of the login property
     *
     * @param  string $property
     * @return -
     */
    public function setNameProperty($property)
    {
        $this->nameProperty = $property;
    }
    // End setLoginProperty

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the name of the authentication key property
     *
     * @param  string $property
     * @return -
     */
    public function setKeyProperty($property)
    {
        $this->keyProperty = $property;
    }
    // End setkeyProperty

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Gets the name of the login property
     *
     * @param  -
     * @return string
     */
    public function getNameProperty()
    {
        return $this->nameProperty;
    }
    // End getNameProperty

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Gets the name of the authentication key property
     *
     * @param  -
     * @return string
     */
    public function getKeyProperty()
    {
        return $this->keyProperty;
    }
    // End getkeyProperty

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns authMethod
     *
     * @param  -
     * @return integer
     */
    public function getAuthMethod()
    {
        return $this->authMethod;
    }
    // End getAuthName

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns authName if it's set
     *
     * @param  -
     * @return string
     */
    public function getAuthName()
    {
        return $this->authName;
    }
    // End getAuthName

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns authKey in respect of the method specified in authMethod
     *
     * @param  -
     * @return string
     */
    public function getAuthKey()
    {
        if(!$this->authKey)
            return false;

        switch($this->authMethod)
        {
            case self::METHOD_PLAIN:
                return $this->authKey;

            case self::METHOD_DES:
                return $this->getDESAuthKey();

            case self::METHOD_MD5:
                return $this->getMD5AuthKey();

            case self::METHOD_BLOWFISH:
                return $this->getBlowFishAuthKey();

            case self::METHOD_NONE:
                return false;

            default:
                throw new Exception("Unknown authentication method `".$this->authMethod."'");
        }

        return false;
    }
    // End getAuthKey

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Validates the authentication name and the key on the given Object.
     *
     * Important: this method requires authName and authKey to be set and public read
     * access to the properties on the Object itself
     *
     * @param  object
     * @return bool
     */
    public function validate($Object)
    {
        if(!$this->validateAuthName($Object))
            return false;

        switch($this->authMethod)
        {
            case self::METHOD_PLAIN:
                return $this->validatePlain($Object);

            case self::METHOD_MD5:
                return $this->validateMD5($Object);

            case self::METHOD_BLOWFISH:
                return $this->validateBlowFish($Object);

            case self::METHOD_DES:
                return $this->validateDES($Object);

            case self::METHOD_NONE:
                return true;

            default:
                throw new Exception("Unknown authentication method `".$this->authMethod."'");
        }

        return false;
    }
    // End validate

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns true if the auth name and authMethod is set
     *
     * @param  -
     * @return boolean
     */
    public function isValid()
    {
        return $this->authName && isset($this->authMethod);
    }
    // End isValid

    //////////////////////////////////////////////////////////////////////////////////////
    // protected
    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Validates the authName case insensitive
     *
     * @param  object $Object
     * @return boolean
     */
    protected function validateAuthName($Object)
    {
        $nameProperty = $this->nameProperty;
        return utf8_strtolower($Object->$nameProperty) === utf8_strtolower($this->authName);
    }
    // End validateAuthName

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Validates a plain authKey - very simple
     *
     * @param  object $Object - Object which needs the properties named in nameProperty and keyProperty to be set
     * @return boolean
     */
    protected function validatePlain($Object)
    {
        $keyProperty = $this->keyProperty;
        return $Object->$keyProperty === $this->authKey;
    }
    // End validatePlain

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Validates a crypted authKey
     *
     * @param  object $Object - Object which needs the properties named in nameProperty and keyProperty to be set
     * @return boolean
     */
    protected function validateDES($Object)
    {
        if(CRYPT_STD_DES == 0)
            throw new Exception('Encryption method DES not available on this system!');

        $keyProperty = $this->keyProperty;
        $storedPass  = $Object->$keyProperty;

        return crypt($this->authKey, $storedPass) == $storedPass;
    }
    // End validateDES

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Validates a crypted authKey in blowFish
     *
     * @param  object $Object - Object which needs the properties named in nameProperty and keyProperty to be set
     * @return boolean
     */
    protected function validateBlowFish($Object)
    {
        if(CRYPT_BLOWFISH == 0)
            throw new Exception('Encryption method BLOWFISH not available on this system!');

        $keyProperty = $this->keyProperty;
        $storedPass  = $Object->$keyProperty;

        return crypt($this->authKey, $storedPass) == $storedPass;
    }
    // End validateDES

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Validates a md5 authKey
     *
     * @param  object $Object - Object which needs the properties named in nameProperty and keyProperty to be set
     * @return boolean
     */
    protected function validateMD5($Object)
    {
        if(CRYPT_MD5 == 0)
            throw new Exception('Encryption method MD5 not available on this system!');

        $keyProperty = $this->keyProperty;
        $storedPass  = $Object->$keyProperty;

        return crypt($this->authKey, $storedPass) == $storedPass;
    }
    // End validateMD5

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Encrypts the value set in authKey via crypt
     *
     * @param  -
     * @return string
     */
    protected function getDESAuthKey()
    {
        if(CRYPT_STD_DES == 0)
            throw new Exception('Encryption method DES not available on this system!');

        $jumble = md5(time() . getmypid());
        $salt = utf8_substr($jumble, 0, CRYPT_SALT_LENGTH);

        return crypt($this->authKey, $salt);
    }
    // End encryptPassword

    ///////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Encrypts the value set in authKey
     *
     * @param  -
     * @return string
     */
    protected function getMD5AuthKey()
    {
        if(CRYPT_MD5 == 0)
            throw new Exception('Encryption method MD5 not available on this system!');

        $jumble = md5(time() . getmypid());
        $salt = '$1$'. utf8_substr($jumble, 0, 8);

        return crypt($this->authKey, $salt);
    }
    // End encryptPassword

    ///////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Encrypts the value set in authKey via blowfish
     *
     * @param  -
     * @return string
     */
    protected function getBlowFishAuthKey()
    {
        if(CRYPT_BLOWFISH == 0)
            throw new Exception('Encryption method BLOWFISH not available on this system!');

        $jumble = md5(time()) . md5(getmypid());
        $salt = '$2a$07$'. utf8_substr($jumble, 0, CRYPT_SALT_LENGTH);

        return crypt($this->authKey, $salt);
    }
    // End encryptPassword

    ///////////////////////////////////////////////////////////////////////////////////////////
}
// End class Auth
