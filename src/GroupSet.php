<?php
/**
 * This file is part of blibs - mvc development framework
 * 
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 * 
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs;


/**
 * Handles a set of Group
 *
 * @package blibs
 * @author Fabian Möller  <fab@beibob.de>
 * @author Tobias Lode <tobias@beibob.de>
 *
 */
class GroupSet extends DbObjectSet
{

    //////////////////////////////////////////////////////////////////////////////////////
    // public
    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Finds all true groups the user is member of
     *
     * @param  int $userId
     * @param  array    $orderBy   - array map of columns on to directions array('id' => 'DESC')
     * @param  integer  $limit     - limit on resultset
     * @param  integer  $offset    - offset on resultset
     * @param  boolean  $force     - Bypass caching
     * @return GroupSet
     */
    public static function findByUserId($userId, array $orderBy = null, $limit = null, $offset = null, $force = false)
    {
        $orderView  = self::buildOrderView($orderBy, $limit, $offset);

        $sql = sprintf('SELECT g.*
                          FROM %s g
                          JOIN %s m ON (g.id = m.group_id)
                         WHERE m.user_id = :userId',
                        Group::TABLE_NAME,
                        GroupMember::TABLE_NAME
                       );

        if($orderView)
            $sql .= ' ' . $orderView;

        return self::_findBySql(get_class(), $sql, ['userId' => $userId] , $force);
    }
    // End findByUserId

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Finds all groups including the usergroup the user is member of
     *
     * @param  int $userId
     * @param  array    $orderBy   - array map of columns on to directions array('id' => 'DESC')
     * @param  integer  $limit     - limit on resultset
     * @param  integer  $offset    - offset on resultset
     * @param  boolean  $force     - Bypass caching
     * @return GroupSet
     */
    public static function findAllByUserId($userId, array $orderBy = null, $limit = null, $offset = null, $force = false)
    {
        $orderView  = self::buildOrderView($orderBy, $limit, $offset);

        $sql = sprintf('SELECT g.*
                          FROM %s g
                          JOIN %s m ON (g.id = m.group_id)
                         WHERE m.user_id = :userId
                        UNION
                        SELECT g.*
                          FROM %s g
                          JOIN %s u ON (g.id = u.group_id)
                         WHERE u.id = :userId',
                       Group::TABLE_NAME,
                       GroupMember::TABLE_NAME,
                       Group::TABLE_NAME,
                       User::TABLE_NAME
                       );

        if($orderView)
            $sql .= ' ' . $orderView;

        return self::_findBySql(get_class(), $sql, ['userId' => $userId] , $force);
    }
    // End findByUserId

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Lazy find
     *
     * @param  array    $initValues - key value array
     * @param  array    $orderBy   - array map of columns on to directions array('id' => 'DESC')
     * @param  integer  $limit     - limit on resultset
     * @param  integer  $offset    - offset on resultset
     * @param  boolean  $force     - Bypass caching
     * @return GroupSet
     */
    public static function find(array $initValues = null, array $orderBy = null, $limit = null, $offset = null, $force = false)
    {
        return self::_find(get_class(), Group::getTablename(), $initValues, $orderBy, $limit, $offset, $force);
    }
    // End find

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Lazy count
     *
     * @param  array    $initValues - key value array
     * @param  boolean  $force     - Bypass caching
     * @return int
     */
    public static function dbCount(array $initValues = null, $force = false)
    {
        return self::_count(get_class(), Group::getTablename(), $initValues, $force);
    }
    // End count

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Lazy find
     *
     * @param  array    $initValues - key value array
     * @param  array    $orderBy   - array map of columns on to directions array('id' => 'DESC')
     * @param  integer  $limit     - limit on resultset
     * @param  integer  $offset    - offset on resultset
     * @param  boolean  $force     - Bypass caching
     * @return RzPressAnnounceSet
     */
    public static function findByAdminView(array $initValues = null, array $orderBy = null, $limit = null, $offset = null, $force = false)
    {
        $orderView  = self::buildOrderView($orderBy, $limit, $offset);

        $sql = 'SELECT * FROM ' . Group::getTablename();

        $conditions = self::buildAdminFilterConditions($initValues);

        if(count($conditions))
            $sql .= ' WHERE ' . join(' AND ', $conditions);

        if($orderView)
            $sql .= ' ' . $orderView;

        return self::_findBySql(get_class(), $sql, $initValues, $force);
    }
    // End findByAdminView

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Z�hlt wieviele Datens�tze die Admin-Tabelle enth�lt
     */
    public static function countByAdminView(array $initValues = null, $force = false)
    {
        $sql = 'SELECT count(*) AS counter FROM ' . Group::getTablename();

        $conditions = self::buildAdminFilterConditions($initValues);

        if(count($conditions))
            $sql .= ' WHERE ' . join(' AND ', $conditions);

        return self::_countBySql(get_class(), $sql, $initValues, 'counter', $force);
    }
    // End countByAdminView

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Bosselt die Filter-SQL-Fragmente f�r den Adminscreen zusammen
     */
    protected static function buildAdminFilterConditions(&$initValues)
    {
        $conditions = [];
        if (count($initValues))
        {
            $unset = [];
            foreach ($initValues as $column => $value)
            {
                if (is_null($value))
                {
                    $unset[$column] = $column;
                    continue;
                }

                switch ($column)
                {
                    case 'is_usergroup':
                        $initValues[$column] = ($value == 'true') ? true : false;
                        $conditions[] = StringFactory::unCamelCase($column) . " = :" . $column;
                    break;
                    default:
                        $initValues[$column] = '%' . $value . '%';
                        $conditions[] = StringFactory::unCamelCase($column) . " ILIKE :" . $column;
                    break;
                }
            }

            foreach($unset as $column)
                unset($initValues[$column]);
        }

        return $conditions;
    }
    // End buildAdminFilterConditions

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Dependency on HtmlTools\HtmlTableProperties ???
     */
//    public static function findByTableProperties( HtmlTableProperties $TableProperties)
//    {
//        $orderBy = $TableProperties->getOrderBy();
//        $sql = sprintf("SELECT * FROM %s WHERE is_usergroup = false"
//                        , Group::TABLE_NAME
//                        );
//
//        if ($filterSql = $TableProperties->getFilterSql())
//            $sql .= ' AND ' . $filterSql;
//        if(!is_null($TableProperties) && $orderView  = self::buildOrderView($orderBy, $TableProperties->getLimit(), $TableProperties->getOffset()))
//            $sql .= ' ' . $orderView;
//
//
//        return self::_findBySql(get_class(), $sql);
//    }
    // End findByTableProperties


    //////////////////////////////////////////////////////////////////////////////////////

    /**
     *
     */
    public static function countByTableProperties(HtmlTableProperties $TableProperties)
    {
        $sql = sprintf("SELECT count(*) FROM %s WHERE is_usergroup = false"
                       , Group::TABLE_NAME
                       );
        if ($filterSql = $TableProperties->getFilterSql())
            $sql .= ' AND ' . $filterSql;

        return self::_countBySql(get_class(), $sql, null, 'count');
    }
    // End countByTableProperties

    //////////////////////////////////////////////////////////////////////////////////////
}
// End class GroupSet
