<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs;

use DOMDocument;
use DOMElement;
use DOMNode;
use DOMText;

/**
 * A HtmlDOMFactory class
 *
 * @package blibs
 * @author Tobias Lode <tobias@beibob.de>
 * @author Fabian Möller <fab@beibob.de>
 *
 */
class HtmlDOMFactory
{
    /**
     * Current document
     * @var DOMDocument $Document
     */
    private $Document;

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Constructor
     *
     * @param  DOMDocument $DOMDocument
     * @return HtmlDOMFactory
     */
    public static function factory(DOMDocument $DOMDocument)
    {
        return new HtmlDOMFactory($DOMDocument);
    }
    // End factory

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Constructor
     *
     * @param  DOMDocument $DOMDocument
     * @return HtmlDOMFactory
     */
    public function __construct(DOMDocument $DOMDocument)
    {
        $this->Document = $DOMDocument;
    }
    // End __construct

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns a DOMText
     *
     * @param  string
     * @return DOMText
     */
    public function getText($text = '')
    {
        return $this->Document->createTextNode($text);
    }
    // End getText

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Liefert ein P-tag
     *
     * @param string $content - Der Textwert des p-tags
     * @param array $attributes - Zusätzliche Attribute
     * @return \DOMElement
     */
    public function getP($content, array $attributes = [])
    {
        return $this->Document->createElement('p', $content, $attributes);
    }
    // End getP

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Liefert ein Pre-tag
     *
     * @param string $content - Der Textwert des p-tags
     * @param array $attributes - Zusätzliche Attribute
     * @return \DOMElement
     */
    public function getPre($content, array $attributes = [])
    {
        return $this->Document->createElement('pre', $content, $attributes);
    }
    // End getPre

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Liefert ein h1-tag
     *
     * @param string $content - Der Textwert des h1-tags
     * @param array $attributes - Zusätzliche Attribute
     * @return \DOMElement
     */
    public function getH1($content, array $attributes = [])
    {
        return $this->Document->createElement('h1', $content, $attributes);
    }
    // End getH1

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Liefert ein h2-tag
     *
     * @param string $content - Der Textwert des h1-tags
     * @param array $attributes - Zusätzliche Attribute
     * @return \DOMElement
     */
    public function getH2($content, array $attributes = [])
    {
        return $this->Document->createElement('h2', $content, $attributes);
    }
    // End getH2

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Liefert ein h3-tag
     *
     * @param string $content - Der Textwert des h1-tags
     * @param array $attributes - Zusätzliche Attribute
     * @return \DOMElement
     */
    public function getH3($content, array $attributes = [])
    {
        return $this->Document->createElement('h3', $content, $attributes);
    }
    // End getH3

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Liefert ein h4-tag
     *
     * @param string $content - Der Textwert des h4-tags
     * @param array $attributes - Zusätzliche Attribute
     * @return \DOMElement
     */
    public function getH4($content, array $attributes = [])
    {
        return $this->Document->createElement('h4', $content, $attributes);
    }
    // End getH4

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Liefert ein h5-tag
     *
     * @param string $content - Der Textwert des h4-tags
     * @param array $attributes - Zusätzliche Attribute
     * @return \DOMElement
     */
    public function getH5($content, array $attributes = [])
    {
        return $this->Document->createElement('h5', $content, $attributes);
    }
    // End getH4

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Liefert ein span-tag
     *
     * @param string $content - Der Textwert des span-tags
     * @param array $attributes - Zusätzliche Attribute
     * @return \DOMElement
     */
    public function getSpan($content = null, array $attributes = [])
    {
        return $this->Document->createElement('span', $content, $attributes);
    }
    // End getSpan

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Liefert ein sub-tag
     *
     * @param string $content - Der Textwert des span-tags
     * @param array $attributes - Zusätzliche Attribute
     * @return \DOMElement
     */
    public function getSub($content = null, array $attributes = [])
    {
        return $this->Document->createElement('sub', $content, $attributes);
    }
    // End getSub

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Liefert ein sup-tag
     *
     * @param string $content - Der Textwert des span-tags
     * @param array $attributes - Zusätzliche Attribute
     * @return \DOMElement
     */
    public function getSup($content = null, array $attributes = [])
    {
        return $this->Document->createElement('sup', $content, $attributes);
    }
    // End getSup

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Liefert ein b-tag
     *
     * @param string $content - Der Textwert des b-tags
     * @param array $attributes - Zusätzliche Attribute
     * @return \DOMElement
     */
    public function getB($content = null, array $attributes = [])
    {
        return $this->Document->createElement('b', $content, $attributes);
    }
    // End getB

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Liefert ein i-tag
     *
     * @param string $content - Der Textwert des i-tags
     * @param array $attributes - Zusätzliche Attribute
     * @return \DOMElement
     */
    public function getI($content = null, array $attributes = [])
    {
        return $this->Document->createElement('i', $content, $attributes);
    }
    // End getI

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Liefert ein strong-tag
     *
     * @param string $content - Der Textwert des strong-tags
     * @param array $attributes - Zusätzliche Attribute
     * @return \DOMElement
     */
    public function getStrong($content = null, array $attributes = [])
    {
        return $this->Document->createElement('strong', $content, $attributes);
    }
    // End getStrong

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Liefert ein em-tag
     *
     * @param string $content - Der Textwert des strong-tags
     * @param array $attributes - Zusätzliche Attribute
     * @return \DOMElement
     */
    public function getEm($content = null, array $attributes = [])
    {
        return $this->Document->createElement('em', $content, $attributes);
    }
    // End getEm

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Liefert ein label-tag
     *
     * @param string $content - Der Textwert des tags
     * @param array $attributes - Zusätzliche Attribute
     * @return \DOMElement
     */
    public function getLabel($content = null, array $attributes = [])
    {
        return $this->Document->createElement('label', $content, $attributes);
    }
    // End getLabel

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Liefert ein nobr-tag
     *
     * @param string $content - Der Textwert des span-tags
     * @param array $attributes - Zusätzliche Attribute
     * @return \DOMElement
     */
    public function getNobr($content = null, array $attributes = [])
    {
        return $this->Document->createElement('nobr', $content, $attributes);
    }
    // End getNobr

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Liefert ein script-tag
     *
     * @param string $content - Der Textwert des script-tags
     * @param array $attributes - Zusätzliche Attribute
     * @return \DOMElement
     */
    public function getScript($content, array $attributes = [])
    {
        $defaultAttributes = ['type' => 'text/javascript'];
        foreach ($attributes as $k => $v)
            $defaultAttributes[$k] = $v;

        return $this->Document->createElement('script', $content, $defaultAttributes);
    }
    // End getScript

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Liefert ein style-tag
     *
     * @param string $content - Der Textwert des style-tags
     * @param array $attributes - Zusätzliche Attribute
     * @return \DOMElement
     */
    public function getStyle($content, array $attributes = [])
    {
        $defaultAttributes = ['type' => 'text/css'];
        foreach ($attributes as $k => $v)
            $defaultAttributes[$k] = $v;

        return $this->Document->createElement('style', $content, $defaultAttributes);
    }
    // End getScript

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Liefert ein form-tag
     *
     * @param array $attributes - Zusätzliche Attribute
     * @internal param string $content - Der Textwert des style-tags
     * @return \DOMElement
     */
    public function getForm(array $attributes = [])
    {
        $defaultAttributes = ['method' => 'POST'];
        foreach ($attributes as $k => $v)
            $defaultAttributes[$k] = $v;

        return $this->Document->createElement('form', null, $defaultAttributes);
    }
    // End getScript

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Liefert ein br-tag
     *
     * @param -
     * @return \DOMElement
     */
    public function getBr()
    {
        return $this->Document->createElement('br');
    }
    // End getBr

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Liefert ein select tag
     *
     * @param array $attributes - Zusätzliche Attribute
     * @return \DOMElement
     */
    public function getSelect(array $attributes = [])
    {
        return $this->Document->createElement('select', null, $attributes);
    }
    // End getSelect

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Liefert ein option-tag
     *
     * @param array $attributes - Zusätzliche Attribute
     * @param string $content - Inhalt des a-Tags, kann ein String sein.
     *
     * @return \DOMElement
     */
    public function getOption(array $attributes = [], $content = null)
    {
        $Tag = $this->Document->createElement('option', null, $attributes);

        if (is_null($content))
            return $Tag;

        $Tag->appendChild($this->Document->createTextNode($content));
        return $Tag;
    }
    // End getOption

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Liefert ein optgroup-tag
     *
     * @param array $attributes - Zusätzliche Attribute
     * @internal param string $content - Inhalt des a-Tags, kann ein String sein.
     *
     * @return \DOMElement
     */
    public function getOptGroup(array $attributes = [])
    {
        return $this->Document->createElement('optgroup', null, $attributes);
    }
    // End getOptGroup

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Liefert ein a-tag
     *
     * @param array $attributes - Zusätzliche Attribute
     * @param mixed $content - Inhalt des a-Tags, kann ein String oder ein Object (DOMNode)
     *                         string wird als DOMText angehängt, Object wird appended
     * @return \DOMElement
     */
    public function getA(array $attributes = [], $content = null)
    {
        $Tag = $this->Document->createElement('a', null, $attributes);

        if (is_null($content))
            return $Tag;

        if ($content instanceof DOMNode)
            $Tag->appendChild($content);
        else
            $Tag->appendChild($this->Document->createTextNode($content));

        return $Tag;
    }
    // End getA

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Liefert ein div-Tag
     *
     * @param array $attributes - Die Attribute für das Tag
     * @param DOMElement $Content - Der Inhalt des Tags
     * @return \DOMElement
     */
    public function getDiv(array $attributes = [], $Content = null)
    {
        $Tag = $this->Document->createElement('div', null, $attributes);

        if ($Content)
            $Tag->appendChild($Content);

        return $Tag;
    }
    // End getDiv

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Liefert ein ul-Tag
     *
     * @param array $attributes - Die Attribute f�r das Tag
     * @param DOMElement $Content - Der Inhalt des Tags
     * @return \DOMElement
     */
    public function getUl(array $attributes = [], DOMElement $Content = null)
    {
        $Tag = $this->Document->createElement('ul', null, $attributes);

        if ($Content)
            $Tag->appendChild($Content);

        return $Tag;
    }
    // End getUl

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Liefert ein ol-Tag
     *
     * @param array $attributes - Die Attribute f�r das Tag
     * @param DOMElement $Content - Der Inhalt des Tags
     * @return \DOMElement
     */
    public function getOl(array $attributes = [], DOMElement $Content = null)
    {
        $Tag = $this->Document->createElement('ol', null, $attributes);

        if ($Content)
            $Tag->appendChild($Content);

        return $Tag;
    }
    // End getOl

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Liefert ein li-Tag
     *
     * @param array $attributes - Die Attribute f�r das Tag
     * @param mixed $content - Der Inhalt des Tags, kann String oder DOMNode sein
     * @return \DOMElement
     */
    public function getLi(array $attributes = [], $content = null)
    {
        $Tag = $this->Document->createElement('li', null, $attributes);

        if (is_null($content))
            return $Tag;

        if ($content instanceof DOMNode)
            $Tag->appendChild($content);
        else
            $Tag->appendChild($this->Document->createTextNode($content));

        return $Tag;
    }
    // End getLi

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Liefert ein table-Tag
     *
     * @param array $attributes - Die Attribute für das Tag
     * @return \DOMElement
     */
    public function getTable(array $attributes = [])
    {
        return $this->Document->createElement('table', null, $attributes);
    }
    // End getTable

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Liefert ein table thead-Tag
     *
     * @param array $attributes - Die Attribute für das Tag
     * @return \DOMElement
     */
    public function getTHead(array $attributes = [])
    {
        return $this->Document->createElement('thead', null, $attributes);
    }
    // End getTHead

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Liefert ein table tfoot-Tag
     *
     * @param array $attributes - Die Attribute für das Tag
     * @return \DOMElement
     */
    public function getTFoot(array $attributes = [])
    {
        return $this->Document->createElement('tfoot', null, $attributes);
    }
    // End getTFoot

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Liefert ein table tbody-Tag
     *
     * @param array $attributes - Die Attribute für das Tag
     * @return \DOMElement
     */
    public function getTBody(array $attributes = [])
    {
        return $this->Document->createElement('tbody', null, $attributes);
    }
    // End getTBody

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Liefert ein tr-Tag
     *
     * @param array $attributes - Die Attribute f�r das Tag
     * @param DOMElement $Content - Der Inhalt des Tags
     * @return \DOMElement
     */
    public function getTr(array $attributes = [], DOMElement $Content = null)
    {
        $Tag = $this->Document->createElement('tr', null, $attributes);

        if ($Content)
            $Tag->appendChild($Content);

        return $Tag;
    }
    // End getTr

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Liefert ein td-Tag
     *
     * @param array $attributes - Die Attribute f�r das Tag
     * @param \DOMElement|\DOMNode $Content - Der Inhalt des Tags
     * @return \DOMElement
     */
    public function getTd(array $attributes = [], DOMNode $Content = null)
    {
        $Tag = $this->Document->createElement('td', null, $attributes);

        if ($Content)
            $Tag->appendChild($Content);

        return $Tag;
    }
    // End getTd

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Liefert ein th-Tag
     *
     * @param array $attributes - Die Attribute für das Tag
     * @param DOMNode $Content - Der Inhalt des Tags
     * @return \DOMElement
     */
    public function getTh(array $attributes = [], DOMNode $Content = null)
    {
        $Tag = $this->Document->createElement('th', null, $attributes);

        if ($Content)
            $Tag->appendChild($Content);

        return $Tag;
    }
    // End getTd

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Liefert ein button Tag
     *
     * @param array $attributes - Die Attribute f�r das Tag
     * @param DOMElement $Content - Der Inhalt des Tags
     * @return \DOMElement
     */
    public function getButton(array $attributes = [], DOMElement $Content = null)
    {
        $Tag = $this->Document->createElement('button', null, $attributes);

        if($Content)
            $Tag->appendChild($Content);

        return $Tag;
    }
    // End getButton

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Liefert ein dl-Tag
     *
     * @param array $attributes - Die Attribute f�r das Tag
     * @param DOMElement $Content - Der Inhalt des Tags
     * @return \DOMElement
     */
    public function getDl(array $attributes = [], DOMElement $Content = null)
    {
        $Tag = $this->Document->createElement('dl', null, $attributes);

        if($Content)
            $Tag->appendChild($Content);

        return $Tag;
    }
    // End getDl

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Liefert ein dt-Tag
     *
     * @param array $attributes - Die Attribute f�r das Tag
     * @param mixed $content - Der Inhalt des Tags, kann String oder DOMNode sein
     * @return \DOMElement
     */
    public function getDt(array $attributes = [], $content = null)
    {
        $Tag = $this->Document->createElement('dt', null, $attributes);

        if (is_null($content))
            return $Tag;

        if ($content instanceof DOMNode)
            $Tag->appendChild($content);
        else
            $Tag->appendChild($this->Document->createTextNode($content));

        return $Tag;
    }
    // End getDt

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Liefert ein dd-Tag
     *
     * @param array $attributes - Die Attribute f�r das Tag
     * @param mixed $content - Der Inhalt des Tags, kann String oder DOMNode sein
     * @return \DOMElement
     */
    public function getDd(array $attributes = [], $content = null)
    {
        $Tag = $this->Document->createElement('dd', null, $attributes);

        if (is_null($content))
            return $Tag;

        if ($content instanceof DOMNode)
            $Tag->appendChild($content);
        else
            $Tag->appendChild($this->Document->createTextNode($content));

        return $Tag;
    }
    // End getDd

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Liefert ein img-Tag
     *
     * @param array $attributes
     * @return \DOMElement
     */
    public function getImg(array $attributes = [])
    {
        $defaultAttributes = ['src' => '', 'alt' => ''];
        foreach ($attributes as $k => $v)
            $defaultAttributes[$k] = $v;

        return $this->Document->createElement('img', null, $defaultAttributes);
    }
    // End getImg

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Liefert ein input-Tag
     *
     * @param array $attributes
     * @return \DOMElement
     */
    public function getInput(array $attributes = [])
    {
        $defaultAttributes = ['type' => 'text'];
        foreach ($attributes as $k => $v)
            $defaultAttributes[$k] = $v;

        return $this->Document->createElement('input', null, $defaultAttributes);
    }
    // End getInput

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Liefert ein input-Tag
     *
     * @param array $attributes
     * @param string $content
     * @return \DOMElement
     */
    public function getTextarea(array $attributes = [], $content = '')
    {
        $Textarea = $this->Document->createElement('textarea', $content, $attributes);
        return $Textarea;
    }
    // End getTextarea

    //////////////////////////////////////////////////////////////////////////////////////
    // Helper
    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Adds a cssclass to the given elements class attribute
     *
     * @param DOMElement $Elt
     * @param  string $cssClass
     * @return void -
     */
    public function addClass(DOMElement $Elt, $cssClass)
    {
        if(!$this->hasClass($Elt, $cssClass))
            $Elt->setAttribute('class', trim($Elt->getAttribute('class').' '.$cssClass));
    }
    // End addClass

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns true if the given element has the cssclass in its class attribute
     *
     * @param DOMElement $Elt
     * @param  string $cssClass
     * @return  boolean
     */
    public function hasClass(DOMElement $Elt, $cssClass)
    {
        $parts = array_flip(preg_split('/\s/u', $Elt->getAttribute('class'), -1, PREG_SPLIT_NO_EMPTY));
        return isset($parts[$cssClass]);
    }
    // End hasClass

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Removes a cssclass from the elements class attribute
     *
     * @param DOMElement $Elt
     * @param  string $cssClass
     * @return void -
     */
    public function removeClass(DOMElement $Elt, $cssClass)
    {
        $parts = array_flip(preg_split('/\s/u', $Elt->getAttribute('class'), -1, PREG_SPLIT_NO_EMPTY));

        if(isset($parts[$cssClass]))
            unset($parts[$cssClass]);

        $Elt->setAttribute('class', join(' ', $parts));
    }
    // End removeClass

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Liefert ein Image mit der �bergebenen Source
     *
     * @param string $src - Die Source des Bildes
     * @param array $attributes - Zus�tzliche Attribute f�r das Bild
     * @return \DOMElement
     */
    public function getImage($src, array $attributes = [])
    {
        $attributes['src'] = $src;

        return $this->getImg($attributes);
    }
    // End getImage

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Liefert einen Div-Container mit der übergeben Klasse
     *
     * @param string $cssClass - Die CSS-Klasse die der Container tragen soll
     * @param DOMElement $Content - Der Inhalt des Containers
     * @return \DOMElement
     */
    public function getContainer($cssClass = null, DOMElement $Content = null)
    {
        $attributes = [];
        if ($cssClass)
            $attributes['class'] = $cssClass;

        return $this->getDiv($attributes, $Content);
    }
    // End getContainer

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Hängt den übergebene Text als p-Tags in den Container.
     */
    public function appendMultilineAsPTags(DOMNode $Container, $text, $ignoreEmptyParagraphs = true, $cssClass = null)
    {
        if ($ignoreEmptyParagraphs)
            $rows = explode("\n", StringFactory::removeHorizontalWhitespace($text));
        else
            $rows = explode("\n", $text);

        foreach ($rows as $rowContent)
        {
            $rowContent = trim($rowContent);
            if ($ignoreEmptyParagraphs && $rowContent == '')
                continue;

            $P = $Container->appendChild($this->getP($rowContent));

            if ($cssClass)
                $P->setAttribute('class', $cssClass);
        }
    }
    // End appendMultilineAsPTags

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Liefert ein Hidden-Input
     *
     * @param string $name - name-Attriute
     * @param string $value - value-Attriute
     * @return \DOMElement
     */
    public function getHiddenInput($name, $value)
    {
        return $this->getInput(['type' => 'hidden', 'name' => $name, 'value' => $value]);
    }
    // End getHiddenInput

    //////////////////////////////////////////////////////////////////////////////////////
}
// End HtmlDOMFactory
