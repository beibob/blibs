<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs;

/**
 * HttpResponse
 *
 * @package blibs
 * @author Tobias Lode <tobias@beibob.de>
 * @author     Fabian M�ller <fab@beibob.de>
 *
 */
class HttpResponse extends ResponseBase
{
    /**
     * Indicates if a header location was set
     */
    private $hasRedirect = false;

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Adds a header to the response
     *
     * @param  string $header
     * @param bool    $replace
     */
    public function setHeader($header, $replace = false)
    {
        header($header, $replace);

        /**
         * Check for header location and set status
         */
        if(!$this->hasRedirect && preg_match('/^location:/ui', $header))
            $this->hasRedirect = true;
    }
    // End setHeader

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the header Location
     *
     * @param  string $url
     * @return -
     */
    public function setHeaderLocation($url)
    {
        $this->setHeader('Location: '.$url);
    }
    // End setHeaderLocation

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Indicates if a header location was set
     *
     * @param
     * @return boolean
     */
    public function hasRedirect()
    {
        return $this->hasRedirect;
    }
    // End hasRedirect

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sends the data to the client
     *
     * @param  -
     * @return -
     */
    public function send()
    {
        /**
         * Handle http status
         */
        if($status = $this->getStatus())
            $this->setHeader(HttpStatus::getInstance($status)->getHttpHeader());

        return parent::send();
    }
    // End send

    //////////////////////////////////////////////////////////////////////////////////////
}
// End HttpResponse
