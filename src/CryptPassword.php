<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs;

/**
 * CryptPassword
 *
 * Helper for md5 crypted passwords
 *
 * This code was found on the internet.
 * Unfortunatley, the author is not known.
 *
 * @package blibs
 * @author unknown
 */
class CryptPassword
{
    protected $MAGIC = '$1$';
    protected $ITOA64 = './0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';

    ///////////////////////////////////////////////////////////////////////////

    /**
     *
     */
    protected function hex2bin($str)
    {
        $len = utf8_strlen($str);
        $nstr = "";
        for ($i=0;$i<$len;$i+=2)
        {
            $num = sscanf(utf8_substr($str,$i,2), "%x");
            $nstr.=chr($num[0]);
        }
        return $nstr;
    }
    // End __construct

    ///////////////////////////////////////////////////////////////////////////

    /**
     *
     */
    protected function to64($v, $n)
    {
        $ret = "";
        while (($n - 1) >= 0)
        {
            $n--;
            $ret .= $this->ITOA64[$v & 0x3f];
            $v = $v >> 6;
        }
        return $ret;
    }
    // End __construct

    ///////////////////////////////////////////////////////////////////////////

    /**
     *
     */
    public function md5crypt($pw, $salt, $magic="")
    {
        if ($magic == "") $magic = $this->MAGIC;
        $slist = explode("$", $salt);
        if ($slist[0] == "1") $salt = $slist[1];
        $salt = utf8_substr($salt, 0, 8);
        $ctx = $pw . $magic . $salt;
        $final = $this->hex2bin(md5($pw . $salt . $pw));
        for ($i=utf8_strlen($pw); $i>0; $i-=16)
        {
            if ($i > 16)
            {
                $ctx .= utf8_substr($final,0,16);
            }
            else
            {
                $ctx .= utf8_substr($final,0,$i);
            }
        }
        $i = utf8_strlen($pw);
        while ($i > 0)
        {
            if ($i & 1)
            {
                $ctx .= chr(0);
            }
            else
            {
                $ctx .= $pw[0];
            }
            $i = $i >> 1;
        }
        $final = $this->hex2bin(md5($ctx));

        # this is really stupid and takes too long
        for ($i=0;$i<1000;$i++)
        {
            $ctx1 = "";
            if ($i & 1)
            {
                $ctx1 .= $pw;
            }
            else
            {
                $ctx1 .= utf8_substr($final,0,16);
            }
            if ($i % 3) $ctx1 .= $salt;
            if ($i % 7) $ctx1 .= $pw;
            if ($i & 1)
            {
                $ctx1 .= utf8_substr($final,0,16);
            }
            else
            {
                $ctx1 .= $pw;
            }
            $final = $this->hex2bin(md5($ctx1));
        }
        $passwd = "";
        $passwd .= $this->to64( ( (ord($final[0]) << 16) | (ord($final[6]) << 8) | (ord($final[12])) ), 4);
        $passwd .= $this->to64( ( (ord($final[1]) << 16) | (ord($final[7]) << 8) | (ord($final[13])) ), 4);
        $passwd .= $this->to64( ( (ord($final[2]) << 16) | (ord($final[8]) << 8) | (ord($final[14])) ), 4);
        $passwd .= $this->to64( ( (ord($final[3]) << 16) | (ord($final[9]) << 8) | (ord($final[15])) ), 4);
        $passwd .= $this->to64( ( (ord($final[4]) << 16) | (ord($final[10]) << 8) | (ord($final[5])) ), 4);
        $passwd .= $this->to64( ord($final[11]), 2);
        return $magic . $salt . "$" . $passwd;
    }
    // End __construct

    ///////////////////////////////////////////////////////////////////////////
}
// End CryptPassword