<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs;

use PDO;
use PDOStatement;

/**
 * Simple database object list interface
 *
 * @todo       the complete dbObject, DataObject and DbObjectCache framework needs to be rewritten
 *             to get it in a right order (DbObjects are using DataObjects!), caching need to work
 *             for both
 *
 * @package blibs
 * @author Tobias Lode <tobias@beibob.de>
 * @author Fabian Möller <fab@beibob.de>
 * @abstract
 *
 */
class DataObjectSet implements \Iterator, \ArrayAccess, \Countable
{
    /**
     * DataObject array
     */
    private $dataObjects = [];

    /**
     * Prepared statement cache
     */
    private static $preparedStatements = [];

    /**
     * internal unique object id, helps identifying objects
     */
    private $objectId;

    /**
     * Hilfsvariablen für die sort-Funktionen
     */
    protected $sortKey;
    protected $sortAsc;



    /**
     * Adds an DataObject to the list
     *
     * @param  \stdClass $DataObject
     * @return \stdClass
     */
    public function add(\stdClass $DataObject)
    {
        return $this->dataObjects[] = $DataObject;
    }
    // End add



    /**
     * Removes an DataObject from the list
     *
     * @param  \stdClass $DataObject
     * @return \stdClass
     */
    public function remove(\stdClass $DataObject)
    {
        foreach($this->dataObjects as $index => $Object)
        {
            if($Object === $DataObject)
            {
                unset($this->dataObjects[$index]);
                return $Object;
            }
        }

        return null;
    }
    // End remove



    /**
     * Searches an element. First match returns
     *
     * @param  string $key
     * @param  mixed  $value
     * @param bool    $useRegEx
     * @return \stdClass
     */
    public function search($key, $value, $useRegEx = false)
    {
        /**
         * Copy to temporary list to avoid manipulating the
         * internal array pointer
         */
        $tmpList = $this->dataObjects;

        foreach($tmpList as $DataObject)
        {
            if($useRegEx)
            {
                if(preg_match($value, $DataObject->$key))
                    return $DataObject;
            }
            else
            {
                if($DataObject->$key == $value)
                    return $DataObject;
            }
        }

        return null;
    }
    // End search



    /**
     * Same as searchKey, but returns the elements and not the key
     *
     * @param        $property
     * @param  mixed $value
     * @param bool   $useRegEx
     * @return \stdClass
     */
    public function searchElement($property, $value, $useRegEx = false)
    {
        $index = $this->searchKey($property, $value, $useRegEx);
        if (!is_null($index) && isset($this->dataObjects[$index]))
            return $this->dataObjects[$index];

        return null;
    }
    // End searchElement


    
    /**
     * Searches an element and returns the key
     *
     * @param  string $key
     * @param  mixed  $value
     * @return DbObject
     */
    public function searchKey($property, $value, $useRegEx = false)
    {
        /**
         * Copy to temporary list to avoid manipulating the
         * internal array pointer
         */
        $tmpList = $this->dataObjects;

        foreach($tmpList as $index => $DbObject)
        {
            if($useRegEx)
            {
                if(preg_match($value, $DbObject->$property))
                    return $index;
            }
            else
            {
                if($DbObject->$property == $value)
                    return $index;
            }
        }

        return null;
    }
    // End searchKey
        


    /**
     * Returns true if the set is empty
     *
     * @param  -
     * @return boolean
     */
    public function isEmpty()
    {
        return empty($this->dataObjects);
    }
    // End isEmpty


    /**
     * Shifts the first element from the set
     *
     * @param  -
     * @return \stdClass
     */
    public function shift()
    {
        return array_shift($this->dataObjects);
    }
    // End shift


    /**
     * Joins each value of all elements in the list by key
     *
     * @param  string $seperator
     * @param  string $property
     * @param  string $keyProperty - see getArrayBy
     * @return string
     */
    public function join($seperator, $property = 'id', $keyProperty = null)
    {
        return join($seperator, $this->getArrayBy($property));
    }
    // End join



    /**
     * Returns an array of all values of a certain property
     *
     * @param  string $property
     * @param  mixed  $keyProperty - if true, use keeps the original key,
     *                               if set to a property name this will be used as key.
     *                               otherwise return an array with incremented index
     * @return array
     */
    public function getArrayBy($property = 'id', $keyProperty = null)
    {
        $list = [];

        foreach($this->dataObjects as $key => $Current)
        {
            if($keyProperty === true)
                $list[$key] = $Current->$property;

            elseif(is_string($keyProperty))
                $list[$Current->$keyProperty] = $Current->$property;

            else
                $list[] = $Current->$property;
        }

        return $list;
    }
    // End getArrayBy



    /**
     * Sortiert die Elemente in der Liste anhand des angebenen Keys
     * alphanumerisch auf- oder absteigend.
     *
     * @param  &array  $opList    - Referenz des Array auf dem sortiert werden soll
     * @param   string $key       - Name des Felds anhand dessen Sortiert wird
     * @param   bool   $ascending - wenn true, aufsteigende Sortierung, absteigend andernfalls
     * @returns bool              - true on success or false on failure
     */
    public function sortByKey($key, $ascending = true)
    {
        $this->sortKey = $key;
        $this->sortAsc = $ascending;

        $ret = usort($this->dataObjects, [$this, "_string_sort_cmpmethod"]);

        unset($this->sortKey);
        unset($this->sortAsc);

        return $ret;
    }
    // End sortElementsByKey



    /**
     * Sortiert die Elemente in der Liste anhand des angebenen Keys
     * alphanumerisch auf- oder absteigend.
     *
     * @param  &array  $opList    - Referenz des Array auf dem sortiert werden soll
     * @param   string $key       - Name des Felds anhand dessen Sortiert wird
     * @param   bool   $ascending - wenn true, aufsteigende Sortierung, absteigend andernfalls
     * @returns bool              - true on success or false on failure
     */
    public function numSortByKey($key, $ascending = true)
    {
        $this->sortKey = $key;
        $this->sortAsc = $ascending;

        $ret = usort($this->dataObjects, [$this, "_num_sort_cmpmethod"]);

        unset($this->sortKey);
        unset($this->sortAsc);

        return $ret;
    }
    // End sortElementsByKey



    /**
     * Liefert das Listenobjekt das den höchsten Wert der Eigenschaft property enthält
     *
     * @param string $property - Eine mit numerischen Werten gefüllte Eigenschaft der
     *                           Listenobjekte
     * @param DataObject
     */
    public function getMaxByKey($property)
    {
        $Clone = clone $this;
        $Clone->numSortByKey($property);
        return array_shift($Clone->dataObjects);
    }
    // End getMaxByKey


    /**
     * Liefert das Listenobjekt das dem niedrigsten Wert der Eigenschaft property enthält
     *
     * @param string $property   - Eine mit numerischen Werten gefüllte Eigenschaft der
     *                           Listenobjekte
     * @param        DataObject
     * @return null
     */
    public function getMinByKey($property)
    {
        if (count($this) < 2)
            return count($this) ? $this[0] : null;

        $Clone = clone $this;
        $Clone->numSortByKey($property, false);
        return $Clone[0];
    }
    // End getMaxByKey


    /**
     * Liefert die Summe aller Werte einer Property
     *
     * @param string $property   - Eine mit numerischen Werten gefüllte Eigenschaft der
     *                           Listenobjekte
     * @param        DataObject
     * @return int
     */
    public function getSumByKey($property)
    {
        $sum = 0;

        $Clone = clone $this;
        foreach ($Clone as $Current)
            $sum = $sum + $Current->$property;

        return $sum;
    }
    // End getSumByKey


    /**
     * Returns a copy of the array of objects
     *
     * @param bool $keyProperty
     * @return array
     */
    public function getArrayCopy($keyProperty = false)
    {
        if(!$keyProperty)
            return $this->dataObjects;

        $copy = [];
        foreach($this->dataObjects as $DO)
            $copy[$DO->$keyProperty] = $DO;

        return $copy;
    }


    public function filter(callable $callback = null, int $flag = 0)
    {
        $filtered = \array_filter($this->dataObjects, $callback, $flag);

        return new self($filtered);
    }

    public function map(callable $callback = null)
    {
        return \array_map($callback, $this->dataObjects);
    }

    /**
     * Reverses the list
     *
     * @return DataObjectSet
     */
    public function reverse()
    {
        $this->dataObjects = array_reverse($this->dataObjects);
        return $this;
    }
    // End reverse


    /**
     * Constructs the set - should not be called from the outside
     *
     * @param array $dataObjects
     * @return DataObjectSet -
     */
    public function __construct(array &$dataObjects = [])
    {
        $this->dataObjects = $dataObjects;
    }
    // End __construct


    // Interface Iterator


    public function rewind() {
        reset($this->dataObjects);
    }

    public function current() {
        return current($this->dataObjects);
    }

    public function key() {
        return key($this->dataObjects);
    }

	public function next() {
        next($this->dataObjects);
    }

    public function valid() {
        return isset($this->dataObjects[key($this->dataObjects)]);
    }


    // ArrayAccess


    public function offsetExists($key)
    {
        return isset($this->dataObjects[$key]);
    }
    // End offsetExists



    public function offsetGet($key)
    {
        return $this->dataObjects[$key];
    }
    // End offsetGet



    public function offsetSet($key, $value)
    {
        $this->dataObjects[$key] = $value;
    }
    // End offsetSet



    public function offsetUnset($key)
    {
        unset($this->dataObjects[$key]);
    }
    // End offsetGet


    // interface count


    /**
     * Returns the list count
     *
     * @return int
     */
    public function count()
    {
        return count($this->dataObjects);
    }
    // End count


    // protected


    /**
     * Lazy find
     *
     * @param        $dataObjectSetName
     * @param bool   $viewName
     * @param  array $initValues - key value array
     * @param  array $orderBy    - array map of columns on to directions array('id' => 'DESC')
     * @param  int   $limit      - limit on resultset
     * @param  int   $offset     - offset on resultset
     * @param  bool  $force      - bypass caching
     * @return DataObjectSet
     */
    protected static function _find($dataObjectSetName, $viewName = false, array $initValues = null, $orderBy = null, $limit = null, $offset = null, $force = false)
    {
        $conditions = self::buildConditions($initValues);
        $orderView  = self::buildOrderView($orderBy, $limit, $offset);

        $sql = sprintf('SELECT * FROM %s'
                       , $viewName
                       );

        if($conditions)
            $sql .= ' WHERE '. $conditions;

        if($orderView)
            $sql .= ' '. $orderView;

        return self::_findBySql($dataObjectSetName, $sql, $initValues, $force);
    }
    // End find


    /**
     * Lazy count
     *
     * @param        $dataObjectSetName
     * @param bool   $viewName
     * @param  array $initValues - key value array
     * @param  bool  $force      - bypass caching
     * @return int
     */
    protected static function _count($dataObjectSetName, $viewName = false, array $initValues = null, $force = false)
    {
        $conditions = self::buildConditions($initValues);

        $sql = sprintf('SELECT count(*) AS counter
                          FROM %s'
                       , $viewName
                       );

        if($conditions)
            $sql .= ' WHERE '. $conditions;

        return self::_countBySql($dataObjectSetName, $sql, $initValues, 'counter', $force);
    }
    // End _count


    /**
     * Inits the set by a sql statement. This may be an statement with placeholders.
     * Put all bind values in an associative array with parameter name in it keys
     *
     * @param          $dataObjectSetName
     * @param  string  $sql
     * @param  array   $initValues
     * @param  boolean $force
     * @throws Exception
     * @return DataObjectSet
     */
    protected static function _findBySql($dataObjectSetName, $sql, array $initValues = null, $force = false)
    {
        /**
         * First check if object already in cache
         */
        $signature = DbObjectCache::getSignature($dataObjectSetName, $initValues, $sql);

        if(!$force && DbObjectCache::has($signature))
            return DbObjectCache::get($signature);

        $Stmt = self::prepareStatement($sql, $initValues);

        if(!$Stmt->execute())
            throw new Exception(self::getSqlErrorMessage($sql, $initValues));

        /**
         * Get DataObjectSet instance
         */
        $DataObjectSet = new $dataObjectSetName();

        while($DataObject = $Stmt->fetchObject())
            $DataObjectSet->add($DataObject);

        return DbObjectCache::set($signature, $DataObjectSet);
    }
    // End findBySql


    /**
     * Count by sql query - assumes a column named 'counter'. This may be overwritten with argument countColumnName
     *
     * @param        $dataObjectSetName
     * @param        $sql
     * @param  array $initValues - key value array
     * @param string $countColumnName
     * @param  bool  $force      - bypass caching
     * @return int
     */
    protected static function _countBySql($dataObjectSetName, $sql, array $initValues = null, $countColumnName = 'counter', $force = false)
    {
        $signature = DbObjectCache::getSignature($dataObjectSetName, $initValues, $sql);

        if(!$force && DbObjectCache::has($signature))
            return DbObjectCache::get($signature);

        $counter = 0;

        $Stmt = self::prepareStatement($sql, $initValues);

        $Stmt->execute();
        $Stmt->bindColumn($countColumnName, $counter, PDO::PARAM_INT);
        $Stmt->fetch(PDO::FETCH_BOUND);

        return DbObjectCache::set($signature, $counter);
    }
    // End countBySql



    /**
     * Builds a condition query
     *
     * @param  array $initValues - key value array
     * @returns string
     */
    protected static function buildConditions(array &$initValues = null)
    {
        if(is_null($initValues))
            return false;

        $conditions = [];
        foreach($initValues as $name => $value)
        {
            if(is_int($name))
                $conditions[] = $value;
            else
            {
                if(is_null($value))
                {
                    $conditions[] = $name.' IS NULL';
                    unset($initValues[$name]);
                }
                else
                    $conditions[] = $name.' = :'.$name;
            }
        }

        return join(' AND ', $conditions);
    }
    // End buildConditions



    /**
     * Builds the ordering and limit/offset query string
     *
     * @param  array $orderBy    - array map of columns on to directions array('id' => 'DESC')
     * @param  int   $limit      - limit on resultset
     * @param  int   $offset     - offset on resultset
     * @return string
     */
    protected static function buildOrderView(array $orderBy = null, $limit = null, $offset = null)
    {
        $orderView = false;

        if(!is_null($orderBy) && count($orderBy))
        {
            $orders = [];

            foreach($orderBy as $column => $direction)
                $orders[] = $column .' '.$direction;

            $orderView = 'ORDER BY '. join(', ', $orders);
        }

        if(!is_null($limit) && $limit > 0)
            $orderView .= ' LIMIT '. $limit;

        if(!is_null($offset) && $offset > 0)
            $orderView .= ' OFFSET '. $offset;

        return $orderView;
    }
    // End buildOrderView


    /**
     * Returns a cached or new prepared statement
     *
     * @param  string $sql
     * @param array   $initValues
     * @return PDOStatement
     */
    final protected static function prepareStatement($sql, array $initValues = null)
    {
        $signature = md5($sql);

        if(!isset(self::$preparedStatements[$signature]) || !$Stmt = self::$preparedStatements[$signature])
            $Stmt = self::$preparedStatements[$signature] = DbHandle::getInstance()->prepare($sql);

        if(!is_null($initValues))
            foreach($initValues as $parameter => $value)
                $Stmt->bindValue($parameter, $value, DbHandle::getPDOType($value));

        return $Stmt;
    }
    // End prepareStatement


    /**
     * Handles a statement error
     *
     * @param  string $sql
     * @param array   $boundValues
     * @return false
     */
    final protected static function getSqlErrorMessage($sql, array $boundValues = null)
    {
        $errorInfo = self::prepareStatement($sql)->errorInfo();

        if(is_array($boundValues))
        {
            foreach($boundValues as $key => $value)
            {
                if(is_bool($value))
                    $sql = str_replace(':'.$key, '['.($value? 'true' : 'false') .'::'.gettype($value).']', $sql);

                elseif($value)
                    $sql = str_replace(':'.$key, '['.$value.'::'.gettype($value).']', $sql);

                elseif($value === null)
                    $sql = str_replace(':'.$key, '[NULL]', $sql);

                else
                    $sql = str_replace(':'.$key, '[empty]', $sql);
            }
        }

        return sprintf("%s - %s\n\n".
                       "Statement: %s\n"
                       , $errorInfo[0]
                       , $errorInfo[2]
                       , $sql
                       );
    }
    // End handleSqlError


    // protected


    /**
     * Sortfunktion fuer Methode sortByKey
     * Erwartet temporaer sortKey und sortAsc als Membervariablen
     *
     */
    protected function _string_sort_cmpmethod($a, $b)
    {
        $key = $this->sortKey;

        $aVal = is_array($a->$key)? current($a->$key) : $a->$key;
        $bVal = is_array($b->$key)? current($b->$key) : $b->$key;

        if($this->sortAsc)
            $cmp = strcmp($aVal, $bVal);
        else
            $cmp = strcmp($bVal, $aVal);

        return $cmp;
    }
    // End _sort_cmpmethod




    /**
     * Sortfunktion fuer Methode sortByKey
     * Erwartet temporaer sortKey und sortAsc als Membervariablen
     *
     */
    protected function _num_sort_cmpmethod($a, $b)
    {
        $key = $this->sortKey;

        if($this->sortAsc)
            return $b->$key - $a->$key;
        else
            return $a->$key - $b->$key;
    }
    // End _numsort_cmpmethod


}
// End class DataObjectSet
