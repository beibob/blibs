<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs;

/**
 * HtmlDocument
 *
 * @package blibs
 * @author Tobias Lode <tobias@beibob.de>
 * @author     Fabian Möller <fab@beibob.de>
 *
 */
class HtmlDocument extends XmlDocument
{
    /**
     * Constructs the document
     *
     * @param  string $charSet
     * @return HtmlDocument
     */
    public function __construct($module, $tplName, $charSet = 'utf-8')
    {
        parent::__construct($module, $tplName, '1.0', $charSet);
    }
    // End __construct

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the output of the document
     *
     * @param  -
     * @return string
     */
    public function __toString()
    {
        return $this->saveHtml();
    }
    // End getOutput
}
// End HtmlDocument
