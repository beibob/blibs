<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs;

use Beibob\Blibs\Interfaces\SessionSaveHandler;

/**
 * Saves session data into a database
 *
 * @package blibs
 * @author Tobias Lode <tobias@beibob.de>
 * @author     Fabian M�ller <fab@beibob.de>
 *
 */
class SessionSaveHandlerDb implements SessionSaveHandler
{
    /**
     * Session
     */
    private $Session;

    /**
     * DbSession
     */
    private $DbSession;

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Registers the Session
     *
     * @param  Session $Session
     * @return -
     */
    public function __construct(Session $Session)
    {
        $this->Session = $Session;
    }
    // End __construct

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Opens a session
     *
     * @param  string  $savePath
     * @param  integer $sessionName
     * @return boolean
     */
    public function open($savePath, $sessionName)
    {
        return true;
    }
    // End open

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Closes a session
     *
     * @param  -
     * @return boolean
     */
    public function close()
    {
        return true;
    }
    // End close

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Reads session data
     *
     * @param  string $sid
     * @return array
     */
    public function read($sid)
    {
        if(!$this->DbSession = DbSession::findById($sid))
            return false;

        if(!$this->DbSession->isValid())
            $this->DbSession = DbSession::create($sid, $this->Session->getLifeTime(), []);

        return $this->DbSession->getData();
    }
    // End read

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Writes session data
     *
     * @param  string $sid
     * @param  array $data
     * @return boolean
     */
    public function write($sid, $data)
    {
        if(!is_object($this->DbSession) ||
           !$this->DbSession->isValid() ||
           $this->DbSession->getId() != $sid)
            return false;

        $this->DbSession->setData($data);
        return $this->DbSession->update();
    }
    // End write

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Destroys a session
     *
     * @param  string $sid
     * @return boolean
     */
    public function destroy($sid)
    {
        if(!is_object($this->DbSession) ||
           !$this->DbSession->isValid())
            return false;

        return $this->DbSession->delete();
    }
    // End destroy

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Garbage collector method
     *
     * @param  int $maxLifeTime
     * @return -
     */
    public function gc($maxLifeTime)
    {
        DbSession::deleteExpired($maxLifeTime);
    }
    // End gc
}
// End class SessionSaveHandlerDb
