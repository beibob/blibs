<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs;

/**
 * Forward exception
 *
 * @package blibs
 * @author Tobias Lode <tobias@beibob.de>
 * @author     Fabian Möller <fab@beibob.de>
 *
 */
class ForwardException extends Exception
{
    /**
     * Forward controller name
     */
    private $ctrlName;

    /**
     * Forward action
     */
    private $action;

    /**
     * Forward query
     */
    private $query;

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Constructor
     *
     * @param  string $message
     * @param  int    $code
     * @param  string $ctrlname
     * @param  string $module
     * @param  string $action
     * @return -
     */
    public function __construct($message, $code, $ctrlName, $action, array $query = null)
    {
        $this->setControllerName($ctrlName);
        $this->setAction($action);
        $this->setQuery($query);
    }
    // End __construct

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the action Controller name
     *
     * @parm string
     */
    public function setControllerName($ctrlName)
    {
        $this->ctrlName = $ctrlName;
    }
    // End setControllerName

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the forward action
     *
     * @param string
     */
    public function setAction($action)
    {
        $this->action = $action;
    }
    // End setAction

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the forward init args
     *
     * @param array
     */
    public function setQuery(array $query = null)
    {
        $this->query = $query;
    }
    // End setQuery

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the action Controller name
     *
     * @return string
     */
    public function getControllerName()
    {
        return $this->ctrlName;
    }
    // End getControllerName

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the forward action
     *
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }
    // End getAction

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the forward init args
     *
     * @return array
     */
    public function getQuery()
    {
        return $this->query;
    }
    // End getQuery

    //////////////////////////////////////////////////////////////////////////////////////
}
// End ForwardException
