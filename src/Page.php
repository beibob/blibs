<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs;

use PDO;

/**
 *
 * @package blibs
 * @author Fabian Möller  <fab@beibob.de>
 * @author Tobias Lode <tobias@beibob.de>
 *
 */
class Page extends DbObject
{
    /**
     * Tablename
     */
    const TABLE_NAME = 'public.pages';
    const PROCEDURE_GET_PAGE_BY_RUBRIC_PATH = 'public.get_page_by_rubric_path';

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * nestedNodeId
     */
    private $nodeId;

    /**
     * caption
     */
    private $caption;

    /**
     * rubric path
     */
    private $rubric;

    /**
     * module
     */
    private $module;

    /**
     * controller
     */
    private $ctrlName;

    /**
     * controller action
     */
    private $ctrlAction;

    /**
     * visiblity
     */
    private $isVisible;

    /**
     * ssl flag
     */
    private $isSecure;

    /**
     * role required to access this page
     */
    private $roleId;

    /**
     * alternate page id on no access
     */
    private $denyPageId;

    /**
     * creation time
     */
    private $created;

    /**
     * modification time
     */
    private $modified;

    /**
     * Extensions
     */
    private $level;
    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * active marker
     */
    private $isActive;

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Primary key
     */
    private static $primaryKey = ['nodeId'];

    /**
     * Column types
     */
    private static $columnTypes = ['nodeId'         => PDO::PARAM_INT,
                                        'caption'        => PDO::PARAM_STR,
                                        'rubric'         => PDO::PARAM_STR,
                                        'module'         => PDO::PARAM_STR,
                                        'ctrlName'       => PDO::PARAM_STR,
                                        'ctrlAction'     => PDO::PARAM_STR,
                                        'isVisible'      => PDO::PARAM_BOOL,
                                        'isSecure'       => PDO::PARAM_BOOL,
                                        'roleId'         => PDO::PARAM_INT,
                                        'denyPageId'     => PDO::PARAM_INT,
                                        'created'        => PDO::PARAM_STR,
                                        'modified'       => PDO::PARAM_STR];

    /**
     * Extended column types
     */
    private static $extColumnTypes = ['active' => PDO::PARAM_BOOL,
                                           'level' => PDO::PARAM_INT];

    //////////////////////////////////////////////////////////////////////////////////////
    // public
    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Creates the object
     *
     * @param  integer  $nodeId    - nestedNodeId
     * @param  string   $caption   - caption
     * @param  string   $ctrlName  - controller
     * @param  string   $rubric    - rubric path
     * @param  string   $module     - the module name
     * @param  string   $ctrlAction - controller action
     * @param  boolean  $isVisible - visiblity
     * @param  integer  $roleId    - role required to access this page
     * @param  integer  $denyPageId - alternate page id on no access
     */
    public static function create($parentId, $caption, $ctrlName, $ident = null, $rubric = null, $ctrlAction = null, $isVisible = true, $roleId = null, $denyPageId = null, $module = null, $isSecure = false)
    {
        $Dbh = DbHandle::getInstance();

        try
        {
            $Dbh->beginTransaction();

            $ParentNode = NestedNode::findById($parentId);
            $PageNode   = NestedNode::createAsChildOf($ParentNode, $ident);

            $Page = new Page();
            $Page->setNodeId($PageNode->getId());
            $Page->setCaption($caption);
            $Page->setCtrlName($ctrlName);
            $Page->setModule($module);
            $Page->setRubric($rubric);
            $Page->setCtrlAction($ctrlAction);
            $Page->setIsVisible($isVisible);
            $Page->setIsSecure($isSecure);
            $Page->setRoleId($roleId);
            $Page->setDenyPageId($denyPageId);

            if(!$Page->getValidator()->isValid())
                throw new Exception('Page not valid');

            $Page->insert();

            $Dbh->commit();
        }
        catch(Exception $Exception)
        {
            $Dbh->rollback();
            throw $Exception;
        }

        return $Page;
    }
    // End create

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Creates the object as sibling of given nested node
     *
     * @param  NestedNode  $Sibling    - Sibling
     * @param  string      $caption   - caption
     * @param  string      $ctrlName  - controller
     * @param  string      $rubric    - rubric path
     * @param  string      $module     - the module name
     * @param  string      $ctrlAction - controller action
     * @param  boolean     $isVisible - visiblity
     * @param  integer     $roleId    - role required to access this page
     * @param  integer     $denyPageId - alternate page id on no access
     */
    public static function createRightFrom(NestedNode $Sibling, $caption, $ctrlName, $ident = null, $rubric = null, $ctrlAction = null, $isVisible = true, $roleId = null, $denyPageId = null, $module = null, $isSecure = false)
    {
        $Dbh = DbHandle::getInstance();

        try
        {
            $Dbh->beginTransaction();

            $PageNode = NestedNode::createRightFrom($Sibling, $ident);

            $Page = new Page();
            $Page->setNodeId($PageNode->getId());
            $Page->setCaption($caption);
            $Page->setCtrlName($ctrlName);
            $Page->setModule($module);
            $Page->setRubric($rubric);
            $Page->setCtrlAction($ctrlAction);
            $Page->setIsVisible($isVisible);
            $Page->setIsSecure($isSecure);
            $Page->setRoleId($roleId);
            $Page->setDenyPageId($denyPageId);

            if(!$Page->getValidator()->isValid())
                throw new Exception();

            $Page->insert();

            $Dbh->commit();
        }
        catch(Exception $Exception)
        {
            $Dbh->rollback();
            throw $Exception;
        }

        return $Page;
    }
    // End createRightFrom

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Creates the object as sibling of given nested node
     *
     * @param  NestedNode  $Sibling    - Sibling
     * @param  string      $caption   - caption
     * @param  string      $ctrlName  - controller
     * @param  string      $rubric    - rubric path
     * @param  string      $module     - the module name
     * @param  string      $ctrlAction - controller action
     * @param  boolean     $isVisible - visiblity
     * @param  integer     $roleId    - role required to access this page
     * @param  integer     $denyPageId - alternate page id on no access
     */
    public static function createLeftFrom(NestedNode $Sibling, $caption, $ctrlName, $ident = null, $rubric = null, $ctrlAction = null, $isVisible = true, $roleId = null, $denyPageId = null, $module = null, $isSecure = false)
    {
        $Dbh = DbHandle::getInstance();

        try
        {
            $Dbh->beginTransaction();

            $PageNode   = NestedNode::createLeftFrom($Sibling, $ident);

            $Page = new Page();
            $Page->setNodeId($PageNode->getId());
            $Page->setCaption($caption);
            $Page->setCtrlName($ctrlName);
            $Page->setModule($module);
            $Page->setRubric($rubric);
            $Page->setCtrlAction($ctrlAction);
            $Page->setIsVisible($isVisible);
            $Page->setIsSecure($isSecure);
            $Page->setRoleId($roleId);
            $Page->setDenyPageId($denyPageId);

            if(!$Page->getValidator()->isValid())
                throw new Exception();

            $Page->insert();

            $Dbh->commit();
        }
        catch(Exception $Exception)
        {
            $Dbh->rollback();
            throw $Exception;
        }

        return $Page;
    }
    // End createLeftFrom

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Creates the object
     *
     * @param  integer  $nodeId    - nestedNodeId
     * @param  string   $caption   - caption
     * @param  string   $ctrlName  - controller
     * @param  string   $rubric    - rubric path
     * @param  string   $module    - module path
     * @param  string   $ctrlAction - controller action
     * @param  boolean  $isVisible - visiblity
     * @param  integer  $roleId    - role required to access this page
     * @param  integer  $denyPageId - alternate page id on no access
     */
    public static function createRoot($caption, $ctrlName, $ident = null, $rubric = null, $ctrlAction = null, $isVisible = true, $roleId = null, $denyPageId = null, $module = null, $isSecure = false)
    {
        $Dbh = DbHandle::getInstance();

        try
        {
            $Dbh->beginTransaction();

            /**
             * Create a node with id = rootId and lft = 1
             */
            $Node = NestedNode::createRoot($ident);

            $Page = new Page();
            $Page->setNodeId($Node->getId());
            $Page->setCaption($caption);
            $Page->setCtrlName($ctrlName);
            $Page->setRubric($rubric);
            $Page->setModule($module);
            $Page->setCtrlAction($ctrlAction);
            $Page->setIsVisible($isVisible);
            $Page->setIsSecure($isSecure);
            $Page->setRoleId($roleId);
            $Page->setDenyPageId($denyPageId);

            if(!$Page->getValidator()->isValid())
                throw new Exception();

            $Page->insert();

            $Dbh->commit();
        }
        catch(Exception $Exception)
        {
            $Dbh->rollback();
            throw $Exception;
        }

        return $Page;
    }
    // End createRoot

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Inits a `Page' by its primary key
     *
     * @param  integer  $nodeId - nestedNodeId
     * @param  boolean  $force - Bypass caching
     * @return Page
     */
    public static function findByNodeId($nodeId, $force = false)
    {
        if(!$nodeId)
            return new Page();

        $sql = sprintf("SELECT node_id
                             , caption
                             , rubric
                             , module
                             , ctrl_name
                             , ctrl_action
                             , is_visible
                             , is_secure
                             , role_id
                             , deny_page_id
                             , created
                             , modified
                          FROM %s
                         WHERE node_id = :nodeId"
                       , self::TABLE_NAME
                       );

        return self::findBySql(get_class(), $sql, ['nodeId' => $nodeId], $force);
    }
    // End findByNodeId

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Inits a `Page' by its primary key
     *
     * @param  integer  $nodeId - nestedNodeId
     * @param  boolean  $force - Bypass caching
     * @return Page
     */
    public static function findByNodeIdent($rootId, $ident, $force = false)
    {
        if(!$rootId || !$ident)
            return new Page();

        $sql = sprintf("SELECT node_id
                             , caption
                             , rubric
                             , module
                             , ctrl_name
                             , ctrl_action
                             , is_visible
                             , is_secure
                             , role_id
                             , deny_page_id
                             , created
                             , modified
                          FROM %s
                         WHERE root_id = :rootId
                           AND ident   = :ident"
                       , PageSet::VIEW_NAME
                       );

        return self::findBySql(get_class(), $sql, ['rootId' => $rootId, 'ident' => $ident], $force);
    }
    // End findByNodeId

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Inits a `Page' by its primary key
     *
     * @param  integer  $nodeId - nestedNodeId
     * @param  boolean  $force - Bypass caching
     * @return Page
     */
    public static function findParent(Page $Page, $force = false)
    {
        if(!$Page->isInitialized())
            return new Page();

        $sql = sprintf("SELECT node_id
                             , caption
                             , rubric
                             , module
                             , ctrl_name
                             , ctrl_action
                             , is_visible
                             , is_secure
                             , role_id
                             , deny_page_id
                             , created
                             , modified
                          FROM %s
                         WHERE root_id = :rootId
                           AND :lft BETWEEN lft AND rgt
                           AND level = :level"
                       , PageSet::VIEW_NAME
                       );

        $Node = $Page->getNode();
        return self::findBySql(get_class(), $sql,
                               ['rootId' => $Node->getRootId(),
                                     'lft' =>   $Node->getLft(),
                                     'level' => $Node->getLevel() - 1], $force);
    }
    // End findByNodeId

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Inits a `Page' by its rubric path
     *
     * @param  integer  $path
     * @param  boolean  $force - Bypass caching
     * @return Page
     */
    public static function findByRubricPath($rootId, $rubricPath, $force = false)
    {
        if(!$rootId || empty($rubricPath))
            return new Page();

        $sql = sprintf("SELECT *
                          FROM %s(:rubricPath, :rootId)"
                       , self::PROCEDURE_GET_PAGE_BY_RUBRIC_PATH
                       );

        $Object = self::findBySql(get_class(), $sql, ['rubricPath' => $rubricPath,
                                                           'rootId'     => $rootId], $force);

        if (!$Object->getNodeId())
            $Object->setInitialized(false);

        return $Object;
    }
    // End findByRubricPath

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the property nodeId
     *
     * @param  integer  $nodeId - nestedNodeId
     * @return
     */
    public function setNodeId($nodeId)
    {
        if(!$this->getValidator()->assertNotEmpty('nodeId', $nodeId))
            return;

        $this->nodeId = (int)$nodeId;
    }
    // End setNodeId

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the property caption
     *
     * @param  string   $caption - caption
     * @return
     */
    public function setCaption($caption)
    {
        if(!$this->getValidator()->assertNotEmpty('caption', $caption))
            return;

        if(!$this->getValidator()->assertMaxLength('caption', 200, $caption))
            return;

        $this->caption = $caption;
    }
    // End setCaption

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the property rubric
     *
     * @param  string   $rubric - rubric path
     * @return
     */
    public function setRubric($rubric = null)
    {
        if(!$this->getValidator()->assertMaxLength('rubric', 200, $rubric))
            return;

        $this->rubric = self::cleanupRubricName($rubric);
    }
    // End setRubric

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the property module
     *
     * @param  string   $module - the module
     * @return
     */
    public function setModule($module = null)
    {
        if(!$this->getValidator()->assertMaxLength('module', 200, $module))
            return;

        $this->module = $module;
    }
    // End setModule

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the property ctrlName
     *
     * @param  string   $ctrlName - controller
     * @return
     */
    public function setCtrlName($ctrlName)
    {
        if(!$this->getValidator()->assertNotEmpty('ctrlName', $ctrlName))
            return;

        if(!$this->getValidator()->assertMaxLength('ctrlName', 120, $ctrlName))
            return;

        $this->ctrlName = $ctrlName;
    }
    // End setCtrlName

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the property ctrlAction
     *
     * @param  string   $ctrlAction - controller action
     * @return
     */
    public function setCtrlAction($ctrlAction = null)
    {
        if(!$this->getValidator()->assertMaxLength('ctrlAction', 120, $ctrlAction))
            return;

        $this->ctrlAction = $ctrlAction;
    }
    // End setCtrlAction

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the property isVisible
     *
     * @param  boolean  $isVisible - visiblity
     * @return
     */
    public function setIsVisible($isVisible = true)
    {
        $this->isVisible = (bool)$isVisible;
    }
    // End setIsVisible

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the property isSecure
     *
     * @param  boolean  $isSecure - ssl flag
     * @return
     */
    public function setIsSecure($isSecure = false)
    {
        $this->isSecure = (bool)$isSecure;
    }
    // End setIsSecure

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the property roleId
     *
     * @param  integer  $roleId - role required to access this page
     * @return
     */
    public function setRoleId($roleId = null)
    {
        $this->roleId = $roleId;
    }
    // End setRoleId

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the property denyPageId
     *
     * @param  integer  $denyPageId - alternate page id on no access
     * @return
     */
    public function setDenyPageId($denyPageId = null)
    {
        $this->denyPageId = $denyPageId;
    }
    // End setDenyPageId

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the property isVisible
     *
     * @param  boolean  $isVisible - visiblity
     * @return
     */
    public function setIsActive($isActive = true)
    {
        $this->isActive = (bool)$isActive;
    }
    // End setIsActive

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the property nodeId
     *
     * @return integer
     */
    public function getNodeId()
    {
        return $this->nodeId;
    }
    // End getNodeId

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the associated NestedNode by property nodeId
     *
     * @param  boolean  $force
     * @return NestedNode
     */
    public function getNode($force = false)
    {
        return NestedNode::findById($this->nodeId, $force);
    }
    // End getNode

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the property caption
     *
     * @return string
     */
    public function getCaption()
    {
        return $this->caption;
    }
    // End getCaption

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the property rubric
     *
     * @return string
     */
    public function getRubric()
    {
        return $this->rubric;
    }
    // End getRubric

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the property module
     *
     * @return string
     */
    public function getModule()
    {
        return $this->module;
    }
    // End getModule

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the property ctrlName
     *
     * @return string
     */
    public function getCtrlName()
    {
        return $this->ctrlName;
    }
    // End getCtrlName

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the property ctrlAction
     *
     * @return string
     */
    public function getCtrlAction()
    {
        return $this->ctrlAction;
    }
    // End getCtrlAction

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the property isVisible
     *
     * @return boolean
     */
    public function isVisible()
    {
        return (bool)$this->isVisible;
    }
    // End isVisible

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the property isSecure
     *
     * @return boolean
     */
    public function isSecure()
    {
        return (bool)$this->isSecure;
    }
    // End isSecure

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the property roleId
     *
     * @return integer
     */
    public function getRoleId()
    {
        return $this->roleId;
    }
    // End getRoleId

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the associated Role by property roleId
     *
     * @param  boolean  $force
     * @return Role
     */
    public function getRole($force = false)
    {
        return Role::findByNodeId($this->roleId, $force);
    }
    // End getRole

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the property denyPageId
     *
     * @return integer
     */
    public function getDenyPageId()
    {
        return $this->denyPageId;
    }
    // End getDenyPageId

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the associated Page by property denyPageId
     *
     * @param  boolean  $force
     * @return Page
     */
    public function getDenyPage($force = false)
    {
        return Page::findByNodeId($this->denyPageId, $force);
    }
    // End getDenyPage

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the property created
     *
     * @return string
     */
    public function getCreated()
    {
        return $this->created;
    }
    // End getCreated

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the property modified
     *
     * @return string
     */
    public function getModified()
    {
        return $this->modified;
    }
    // End getModified

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the property isActive
     *
     * @return  boolean
     */
    public function isActive()
    {
        return (bool)$this->isActive;
    }
    // End isActive

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     *  returns extended property level
     */
    public function getLevel()
    {
        if (isset($this->level))
            return $this->level;

           return $this->level = $this->getNode()->getLevel();
    }
    // End getLevel

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns true if this page has child nodes
     *
     * @return boolean
     */
    public function hasChildNodes()
    {
        return $this->getNode()->hasChildNodes();
    }
    // End hasChildNodes

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns true if this page has at least one visible child
     *
     * @return boolean
     */
    public function hasVisibleChildNodes()
    {
        if (!$this->getNode()->hasChildNodes())
            return false;

        foreach ($this->getChildNodes() as $ChildNode)
            if ($ChildNode->isVisible())
                return true;

        return false;
    }
    // End hasVisibleChildNodes

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns a set of child nodes
     *
     * @param  boolean $force
     * @return PageSet
     */
    public function getChildNodes($force = false, $initValues = [])
    {
        return PageSet::findByParent($this->getNode(), $initValues, $force);
    }
    // End getChildNodes

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Checks, if the object exists
     *
     * @param  integer  $nodeId - nestedNodeId
     * @param  boolean  $force - Bypass caching
     * @return boolean
     */
    public static function exists($nodeId, $force = false)
    {
        return self::findByNodeId($nodeId, $force)->isInitialized();
    }
    // End exists

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Updates the object in the table
     *
     * @return boolean
     */
    public function update()
    {
        $this->modified = self::getCurrentTime();

        $sql = sprintf("UPDATE %s
                           SET caption        = :caption
                             , rubric         = :rubric
                             , module         = :module
                             , ctrl_name      = :ctrlName
                             , ctrl_action    = :ctrlAction
                             , is_visible     = :isVisible
                             , is_secure      = :isSecure
                             , role_id        = :roleId
                             , deny_page_id   = :denyPageId
                             , created        = :created
                             , modified       = :modified
                         WHERE node_id = :nodeId"
                       , self::TABLE_NAME
                       );

        return $this->updateBySql($sql,
                                  ['nodeId'        => $this->nodeId,
                                        'caption'       => $this->caption,
                                        'rubric'        => $this->rubric,
                                        'module'        => $this->module,
                                        'ctrlName'      => $this->ctrlName,
                                        'ctrlAction'    => $this->ctrlAction,
                                        'isVisible'     => $this->isVisible,
                                        'isSecure'      => $this->isSecure,
                                        'roleId'        => $this->roleId,
                                        'denyPageId'    => $this->denyPageId,
                                        'created'       => $this->created,
                                        'modified'      => $this->modified]
                                  );
    }
    // End update

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Deletes the object from the table
     *
     * @return boolean
     */
    public function delete()
    {
        $sql = sprintf("DELETE FROM %s
                              WHERE node_id = :nodeId"
                       , self::TABLE_NAME
                      );

        return $this->deleteBySql($sql,
                                  ['nodeId' => $this->nodeId]);
    }
    // End delete

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns an array with the primary key properties and
     * associates its values, if it's a valid object
     *
     * @param  boolean  $propertiesOnly
     * @return array
     */
    public function getPrimaryKey($propertiesOnly = false)
    {
        if($propertiesOnly)
            return self::$primaryKey;

        $primaryKey = [];

        foreach(self::$primaryKey as $key)
            $primaryKey[$key] = $this->$key;

        return $primaryKey;
    }
    // End getPrimaryKey

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the tablename constant. This is used
     * as interface for other objects.
     *
     * @return string
     */
    public static function getTablename()
    {
        return self::TABLE_NAME;
    }
    // End getTablename

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the columns with their types. The columns may also return extended columns
     * if the first argument is set to true. To access the type of a single column, specify
     * the column name in the second argument
     *
     * @param  boolean  $extColumns
     * @param  mixed    $column
     * @return mixed
     */
    public static function getColumnTypes($extColumns = false, $column = false)
    {
        $columnTypes = $extColumns? array_merge(self::$columnTypes, self::$extColumnTypes) : self::$columnTypes;

        if($column)
            return $columnTypes[$column];

        return $columnTypes;
    }
    // End getColumnTypes

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Cleans up a rubric name
     *
     * @param  string
     * @return string
     */
    public static function cleanupRubricName($rubric)
    {
        $cleaned = preg_replace(['/ /i', '/[äÄ]/', '/[öÖ]/', '/[üÜ]/i', '/ß/', '/[^a-z0-9\_\-]/iu'],
                                ['-', 'ae', 'oe', 'ue', 'ss', ''],
                                $rubric
                                );

        return strtolower(str_replace('--','-', $cleaned));
    }
    // End cleanupRubricName

    //////////////////////////////////////////////////////////////////////////////////////
    // protected
    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Inserts a new object in the table
     *
     * @return boolean
     */
    protected function insert()
    {
        $this->created        = self::getCurrentTime();
        $this->modified       = null;

        $sql = sprintf("INSERT INTO %s (node_id, caption, rubric, module, ctrl_name, ctrl_action, is_visible, is_secure,role_id, deny_page_id, created, modified)
                               VALUES  (:nodeId, :caption, :rubric, :module, :ctrlName, :ctrlAction, :isVisible, :isSecure, :roleId, :denyPageId, :created, :modified)"
                       , self::TABLE_NAME
                       );

        return $this->insertBySql($sql,
                                  ['nodeId'        => $this->nodeId,
                                        'caption'       => $this->caption,
                                        'rubric'        => $this->rubric,
                                        'module'        => $this->module,
                                        'ctrlName'      => $this->ctrlName,
                                        'ctrlAction'    => $this->ctrlAction,
                                        'isVisible'     => $this->isVisible,
                                        'isSecure'      => $this->isSecure,
                                        'roleId'        => $this->roleId,
                                        'denyPageId'    => $this->denyPageId,
                                        'created'       => $this->created,
                                        'modified'      => $this->modified]
                                  );
    }
    // End insert

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Inits the object with row values
     *
     * @param  \stdClass $DO - Data object
     * @return boolean
     */
    protected function initByDataObject(\stdClass $DO = null)
    {
        $this->nodeId         = (int)$DO->node_id;
        $this->caption        = $DO->caption;
        $this->rubric         = $DO->rubric;
        $this->module         = $DO->module;
        $this->ctrlName       = $DO->ctrl_name;
        $this->ctrlAction     = $DO->ctrl_action;
        $this->isVisible      = (bool)$DO->is_visible;
        $this->isSecure       = (bool)$DO->is_secure;
        $this->roleId         = $DO->role_id;
        $this->denyPageId     = $DO->deny_page_id;
        $this->created        = $DO->created;
        $this->modified       = $DO->modified;

        /**
         * Set extensions
         */
        if (isset($DO->level)) $this->level = (int) $DO->level;
    }
    // End initByDataObject
}
// End class Page
