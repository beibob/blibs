<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs;

/**
 * CliRequest accessor
 *
 * @package blibs
 * @author Tobias Lode <tobias@beibob.de>
 * @author Fabian Möller <fab@beibob.de>
 *
 */
class CliRequest extends RequestBase
{
    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Constructs the instance
     *
     * @param -
     */
    public function __construct(array $request = null)
    {
        parent::__construct(!is_null($request)? $request : $_SERVER['argv']);
    }
    // End __construct

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the URI
     *
     * @return  string
     */
    public function getUri()
    {
        return $this->request[0];
    }
    // End getUri

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the host
     *
     * @param   -
     * @return  string
     */
    public function getHost()
    {
        return isset($_SERVER['HOST'])? $_SERVER['HOST'] : gethostname();
    }
    // End getHost

    //////////////////////////////////////////////////////////////////////////////////////
}
// End class CliRequest
