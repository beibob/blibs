<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs;

use Beibob\Blibs\Interfaces\Filter;
use Beibob\Blibs\Interfaces\Request;
use Beibob\Blibs\Interfaces\Response;

/**
 * Filter for handling errors / exceptions.
 *
 * @package blibs
 * @author Tobias Lode <tobias@beibob.de>
 *
 */
class PostErrorFilter implements Filter
{
    /**
     *  The filter function, that sets the content to the exceptions messages
     *
     *  @param Request $Request
     *  @param Response $Response
     */
    public function filter(Request $Request, Response $Response, HttpRouter $Router = null)
    {
        if(!$Response->hasExceptions())
            return;

        $Config = Environment::getInstance()->getConfig();

        /**
         * Avoid class not found errors if requested
         */
        if($Response->hasException('AutoLoaderException', AutoLoaderException::CLASS_NOT_FOUND) &&
           $Config->postErrorFilter->ignoreClassNotFoundErrors)
            return;

        /**
         * Add special error header
         */
        if(!headers_sent())
            header('X-PHP-ERROR: true');

        /** Checking if the errors should be screen displayed or/and mailed to the
         *  address hold by the postErrorFilter.sendMailTo attribute in the config
         *  file.
         */
        if($Config->php->display_errors)
        {
            $Response->unsetContent();
            $Response->setContent($this->buildExceptionMessage($Request, $Response));
            $Response->setHeader('Content-Type: text/html', true, 500);
        }

        foreach($Response->getExceptions() as $index => $Exception)
        {
            Log::getDefault()->fatal($Exception->getMessage(), get_class($Exception));
            Log::getDefault()->fatal($Exception->getTraceAsString(), get_class($Exception));
        }

        if(!isset($Config->postErrorFilter->sendMailTo))
            return;

        $mailSubject = "BLibsException (".$Request->getHost();

        foreach($Response->getExceptions() as $index => $Exceptions)
            $mailSubject .= " ".get_class($Exceptions);

        $mailSubject .= ")";

        $mailContent = $this->buildExceptionMessage($Request, $Response);

        $mailHeader  = 'MIME-Version: 1.0' . "\r\n";
        $mailHeader .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

        foreach($Config->postErrorFilter->toList('sendMailTo') as $emailAddress)
            $mailAccepted = mail($emailAddress, $mailSubject, $mailContent, $mailHeader);
    }
    // End filter

    //////////////////////////////////////////////////////////////////////////////////////
    // protected
    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Build html exception message
     *
     * @param Request $Request
     * @param Response $Response
     * @return string
     */
    protected function buildExceptionMessage(Request $Request, Response $Response)
    {
        if(Environment::getInstance()->getApplicationType() == APP_TYPE_WEB)
        {
            $content = "<html><head></head><body><div style=\"border-style:solid; border-width:thin; border-color:#FF0000; padding:1cm\"><font color=#0000FF><b>BlibsException</b></font color><br><br>";
            $content .= "<font color=#0000FF>HttpRequest:</font color><br>";
            $content .= "Protocol: ".$Request->getServerProtocol()."<br>";
            $content .= "Method: ".$Request->getMethod()."<br>";
            $content .= "URI: <a href=".$Request->getHost().$Request->getRequestURI().">".$Request->getHost().$Request->getRequestURI()."</a><br>";
            $content .= "Redirect URL: <a href=".$Request->getHost().$Request->getRedirectURL().">".$Request->getHost().$Request->getRedirectURL()."</a><br>";
            $content .= "Remote Address: ".$Request->getRemoteAddress()."<br>";
            $content .= "Referer: <a href=".$Request->getReferer().">".$Request->getReferer()."</a><br>";
            $content .= "<pre>".print_r($_REQUEST, true)."</pre>";

            foreach($Response->getExceptions() as $index => $exception)
            {
                $content .= "<font color=#0000FF>Exception Message:</font color><br>";
                $content .= $exception->getMessage()."<br>";
                $content .= "<pre>". $exception->getFile() .' '. $exception->getLine() ."</pre><br>";
                $content .= "<pre>".$exception->getTraceAsString()."</pre>";
            }
            $content .= "<div></body></html>";
        }
        else
        {
            $content = [];
            $content[] = "--------------------------------------------------------------------------------";
            $content[] = "BlibsException";
            $content[] = "CliRequest:";
            $content[] = "URI: ".$Request->getHost() . $Request->getURI();
            $content[] = print_r($Request->getAsArray(), true);
            $content[] = "";

            foreach($Response->getExceptions() as $index => $exception)
            {
                $content[] = "Exception Message:";
                $content[] = $exception->getMessage();
                $content[] = "";
                $content[] = $exception->getTraceAsString();
                $content[] = "";
            }
            $content[] = "--------------------------------------------------------------------------------";
            $content[] = "";

            $content = join("\n", $content);
        }

        return $content;
    }
    // End buildExceptionMessage
}
// End PostErrorFilter
