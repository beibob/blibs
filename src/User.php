<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs;

use PDO;

/**
 *
 * @package blibs
 * @author Fabian Möller  <fab@beibob.de>
 * @author Tobias Lode <tobias@beibob.de>
 *
 */
class User extends DbObject
{
    /**
     * Tablename
     */
    const TABLE_NAME = 'public.users';

    /**
     * View names
     */
    const VIEW_NAME = 'public.users_v';

    /**
     * Registration state
     */
    const STATUS_LEGACY    = -1;
    const STATUS_REQUESTED = 0;
    const STATUS_CONFIRMED = 1;

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Liste von Nutzernamen die offensichtlich immer da sein müssen und somit
     * eine Sonderrolle spielen
     */
    public static $systemAuthNames = ['root', 'Gast'];

    /**
     * userId
     */
    private $id;

    /**
     * authName
     */
    private $authName;

    /**
     * authKey
     */
    private $authKey;

    /**
     * authMethod
     */
    private $authMethod;

    /**
     * userGroupId
     */
    private $groupId;

    /**
     * @var isLocked
     */
    private $isLocked = false;

    /**
     * @var status
     */
    private $status;

    /**
     * creation time
     */
    private $created;

    /**
     * modification time
     */
    private $modified;

    /**
     * login time
     */
    private $loginTime;

    /**
     * company
     */
    private $company;

    /**
     * @var string gender
     */
    private $gender;

    /**
     * firstname
     */
    private $firstname;

    /**
     * lastname
     */
    private $lastname;

    /**
     * email address
     */
    private $email;

    /**
     * email address
     */
    private $candidateEmail;

    /**
     * birthday
     */
    private $birthday;

    /**
     * notice
     */
    private $notice;

    /**
     * ext: fullname
     */
    private $fullname;

    /**
     * Primary key
     */
    private static $primaryKey = ['id'];

    /**
     * Column types
     */
    private static $columnTypes = ['id'          => PDO::PARAM_INT,
                                        'authName'    => PDO::PARAM_STR,
                                        'authKey'     => PDO::PARAM_STR,
                                        'authMethod'  => PDO::PARAM_INT,
                                        'groupId'     => PDO::PARAM_INT,
                                        'isLocked'    => PDO::PARAM_BOOL,
                                        'status'      => PDO::PARAM_INT,
                                        'created'     => PDO::PARAM_STR,
                                        'modified'    => PDO::PARAM_STR,
                                        'loginTime'   => PDO::PARAM_STR];

    /**
     * Extended columns types
     */
    private static $extColumnTypes = ['company'        => PDO::PARAM_STR,
                                      'gender'         => PDO::PARAM_STR,
                                      'firstname'      => PDO::PARAM_STR,
                                      'lastname'       => PDO::PARAM_STR,
                                      'email'          => PDO::PARAM_STR,
                                      'candidateEmail' => PDO::PARAM_STR,
                                      'notice'         => PDO::PARAM_STR,
                                      'birthday'       => PDO::PARAM_STR,
                                      'fullname'       => PDO::PARAM_STR];

    /**
     * Creates the object
     *
     * @param  Auth $Auth
     */
    public static function create(Auth $Auth, $status = self::STATUS_REQUESTED)
    {
        $User = new User();
        $User->setAuthentication($Auth);
        $User->setLoginTime(null);
        $User->setStatus($status);

        $Validator = $User->getValidator();

        if($Validator->isValid())
        {
            DbHandle::getInstance()->beginTransaction();

            $Group = Group::create($Auth->getAuthName(), true);

            if($Validator->assertTrue('groupId', $Group->isValid()))
            {
                $User->groupId = $Group->getId();
                $User->insert();
                DbHandle::getInstance()->commit();
            }
            else
                DbHandle::getInstance()->rollback();
        }

        return $User;
    }
    // End create


    /**
     * Inits a User by id
     *
     * @param  int  $id    - userId
     * @param  boolean  $force - Bypass caching
     * @return User
     */
    public static function findById($id, $force = false)
    {
        if(!$id)
            return new User();

        $sql = sprintf("SELECT id
                             , auth_name
                             , auth_key
                             , auth_method
                             , group_id
                             , is_locked
                             , status
                             , created
                             , modified
                             , login_time
                          FROM %s
                         WHERE id = :id"
                       , self::TABLE_NAME
                       );

        return self::findBySql(get_class(), $sql, ['id' => $id], $force);
    }
    // End findById


    /**
     * Inits a User by user's groupId
     *
     * @param  numeric  $groupId
     * @param  boolean  $force - Bypass caching
     * @return User
     */
    public static function findByGroupId($groupId, $force = false)
    {
        if(!$groupId)
            return new User();

        $sql = sprintf("SELECT id
                             , auth_name
                             , auth_key
                             , auth_method
                             , group_id
                             , is_locked
                             , status
                             , created
                             , modified
                             , login_time
                          FROM %s
                         WHERE group_id = :groupId"
                       , self::TABLE_NAME
                       );

        return self::findBySql(get_class(), $sql, ['groupId' => $groupId], $force);
    }
    // End findByGroupId


    /**
     * Inits a User by id
     *
     * @param  numeric  $id    - userId
     * @param  boolean  $force - Bypass caching
     * @return User
     */
    public static function findExtendedById($id, $force = false)
    {
        if(!$id)
            return new User();

        $sql = sprintf("SELECT id
                             , auth_name
                             , auth_key
                             , auth_method
                             , group_id
                             , is_locked
                             , status
                             , created
                             , modified
                             , login_time
                             , company
                             , gender
                             , firstname
                             , lastname
                             , email
                             , candidate_email
                             , notice
                             , birthday
                          FROM %s
                         WHERE id = :id"
                       , self::VIEW_NAME
                       );

        return self::findBySql(get_class(), $sql, ['id' => $id], $force);
    }
    // End findById


    /**
     * Inits a User by email
     *
     * @param  numeric  $email   - email
     * @param  boolean  $force - Bypass caching
     * @return User
     */
    public static function findExtendedByEmail($email, $force = false)
    {
        if(!$email)
            return new User();

        $sql = sprintf("SELECT id
                             , auth_name
                             , auth_key
                             , auth_method
                             , group_id
                             , is_locked
                             , status
                             , created
                             , modified
                             , login_time
                             , company
                             , gender
                             , firstname
                             , lastname
                             , email
                             , candidate_email
                             , notice
                             , birthday
                          FROM %s
                         WHERE email = :email"
            , self::VIEW_NAME
        );

        return self::findBySql(get_class(), $sql, ['email' => utf8_strtolower(utf8_trim($email))], $force);
    }
    // End findExtendedByEmail

    /**
     * Inits a User by email
     *
     * @param  numeric  $email   - email
     * @param  boolean  $force - Bypass caching
     * @return User
     */
    public static function findExtendedByCandidateEmail($email, $force = false)
    {
        if(!$email)
            return new User();

        $sql = sprintf("SELECT id
                             , auth_name
                             , auth_key
                             , auth_method
                             , group_id
                             , is_locked
                             , status
                             , created
                             , modified
                             , login_time
                             , company
                             , gender
                             , firstname
                             , lastname
                             , email
                             , candidate_email
                             , notice
                             , birthday
                          FROM %s
                         WHERE candidate_email = :email"
            , self::VIEW_NAME
        );

        return self::findBySql(get_class(), $sql, ['email' => utf8_strtolower(utf8_trim($email))], $force);
    }
    // End findExtendedByEmail


    /**
     * Inits the User by name
     *
     * @param  string $authName
     * @return -
     */
    public static function findByAuthName($authName, $caseSensitive = true, $force = false)
    {
        if(!$authName)
            return new User();

        $authName = utf8_trim($authName);
        if (!$caseSensitive)
            $authName = utf8_strtolower($authName);

        $sql = sprintf('SELECT id
                             , auth_name
                             , auth_key
                             , auth_method
                             , group_id
                             , is_locked
                             , status
                             , created
                             , modified
                             , login_time
                          FROM %s
                         WHERE %s = :authName'
                        , self::getTablename()
                        , $caseSensitive ? 'auth_name' : 'lower(auth_name)'
                       );

        return self::findBySql(get_class(), $sql, ['authName' => $authName], $force);
    }
    // End findByAuthName


    /**
     * Inits a user by crypted id
     *
     * @return string - Die verschlüsselte Id
     */
    public static function findByCryptId($token, $tsProperty = 'created')
    {
        $deCrypt = IdFactory::decryptId($token);
        if (!is_array($deCrypt) || count($deCrypt) != 2)
            return new User();

        $User = self::findById($deCrypt[0]);
        if (!$User->isInitialized())
            return $User;

        $ts = $User->$tsProperty;
        $DateTime = new BlibsDateTime($ts);
        if ($deCrypt[1] == $DateTime->getUnixtime())
            return $User;

        return new User();
    }
    // End findByCryptId


    /**
     * Authenticates the user
     *
     * @param Auth $Auth
     * @return boolean
     */
    public function authenticate(Auth $Auth)
    {
        if(!$Auth->validate($this))
            return false;

        $this->loginTime = self::getCurrentTime();
        $this->update();

        return true;
    }
    // End authenticate


    /**
     * Sets the authentication
     *
     * @param  Auth $Auth
     * @return boolean
     */
    public function setAuthentication(Auth $Auth)
    {
        if(!$this->getValidator()->assertTrue('authName', $Auth->isValid())) return false;
        if(!$this->getValidator()->assertLength('authName', 100, 3, $Auth->getAuthName())) return false;
        if(isset($Auth->authKey) && !$this->getValidator()->assertLength('authKey', 100, 3, $Auth->getAuthKey())) return false;

        if($Auth->getAuthName())
            $this->authName   = $Auth->getAuthName();

        if($Auth->getAuthKey())
            $this->authKey    = $Auth->getAuthKey();

        if($Auth->getAuthMethod())
            $this->authMethod = $Auth->getAuthMethod();

        return true;
    }
    // End setExpires


    /**
     * Checks if this user has a certain role
     *
     * @param  string $rootIdent
     * @param  string $roleIdent
     * @return -
     */
    public function hasRole($rootIdent, $roleIdent, $force = false)
    {
        return RoleMember::isGranted(Role::findByIdent($rootIdent, $roleIdent, $force)->getNodeId(), $this->getGroupId());
    }
    // End hasRole


    /**
     * Sets the property loginTime
     *
     * @param  integer  $loginTime - login time
     * @return
     */
    public function setLoginTime($loginTime = null)
    {
        $this->loginTime = $loginTime;
    }
    // End setLoginTime


    /**
     * @param mixed $isLocked
     */
    public function setIsLocked($isLocked)
    {
        $this->isLocked = $isLocked;
    }


    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }


    /**
     * Returns the property id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    // End getId


    /**
     * Verschlüsselt die id des Users
     *
     * @return string - Die verschlüsselte Id
     */
    public function getCryptId($tsProperty = 'created')
    {
        $ts = $this->$tsProperty;
        $DateTime = new BlibsDateTime($ts);
        return IdFactory::encryptId($this->id, $DateTime->getUnixtime());
    }
    // End getCryptId


    /**
     * Returns the property authName
     *
     * @return string
     */
    public function getAuthName()
    {
        return $this->authName;
    }
    // End getAuthName


    /**
     * Returns the property authKey
     *
     * @return string
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }
    // End getAuthKey


    /**
     * Returns the property authMethod
     *
     * @return integer
     */
    public function getAuthMethod()
    {
        return $this->authMethod;
    }
    // End getAuthMethod


    /**
     * Returns the property groupId
     *
     * @return int
     */
    public function getGroupId()
    {
        return $this->groupId;
    }
    // End getGroupId


    /**
     * Returns the group
     *
     * @param  -
     * @return Group
     */
    public function getGroup()
    {
        return Group::findById($this->groupId);
    }
    // End getGroupId

    /**
     * @return bool
     */
    public function getIsLocked()
    {
        return (bool) $this->isLocked;
    }

    /**
     * Alias of getIsLocked
     *
     * @return bool
     */
    public function isLocked()
    {
        return (bool) $this->getIsLocked();
    }


    /**
     * Returns the registration state
     *
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }


    /**
     * Returns the property created
     *
     * @return integer
     */
    public function getCreated()
    {
        return $this->created;
    }
    // End getCreated


    /**
     * Returns the property modified
     *
     * @return integer
     */
    public function getModified()
    {
        return $this->modified;
    }
    // End getModified


    /**
     * Returns the property loginTime
     *
     * @return integer
     */
    public function getLoginTime()
    {
        return $this->loginTime;
    }
    // End getLoginTime


    /**
     * Returns the extension property company
     *
     * @return string
     */
    public function getCompany()
    {
        if(isset($this->company))
            return $this->company;

        return $this->company = $this->getProfile()->getCompany();
    }
    // End getCompany


    /**
     * Returns the extension property gender
     *
     * @return string
     */
    public function getGender()
    {
        if(isset($this->gender))
            return $this->gender;

        return $this->gender = $this->getProfile()->getGender();
    }
    // End getGender


    /**
     * Returns the full name or username
     *
     * @return string
     */
    public function getIdentifier($lastnameFirst = false)
    {
        $firstname = $this->getFirstname();
        $lastname = $this->getLastname();

        if($lastname && $lastnameFirst)
            $fullname = trim($lastname.', '.$firstname);
        else
            $fullname = trim($firstname.' '.$lastname);

        if(!$fullname)
            $fullname = $this->getAuthName();

        return $fullname;
    }
    // End getIdentifier


    /**
     * Returns the extension property firstname
     *
     * @return string
     */
    public function getFirstname()
    {
        if(isset($this->firstname))
            return $this->firstname;

        return $this->firstname = $this->getProfile()->getFirstname();
    }
    // End getFirstname


    /**
     * Returns the property lastname
     *
     * @return string
     */
    public function getLastname()
    {
        if(isset($this->lastname))
            return $this->lastname;

        return $this->lastname = $this->getProfile()->getLastname();
    }
    // End getLastname


    /**
     * Returns the property email
     *
     * @return string
     */
    public function getEmail()
    {
        if(isset($this->email))
            return $this->email;

        return $this->email = $this->getProfile()->getEmail();
    }
    // End getEmail

    /**
     * Returns the property candidateEmail
     *
     * @return string
     */
    public function getCandidateEmail($force = false)
    {
        if(isset($this->candidateEmail) && !$force)
            return $this->candidateEmail;

        return $this->candidateEmail = $this->getProfile($force)->getCandidateEmail();
    }
    // End getCandidateEmail

    /**
     * Returns the extension property notice
     *
     * @return string
     */
    public function getNotice()
    {
        if(isset($this->notice))
            return $this->notice;

        return $this->notice = $this->getProfile()->getNotice();
    }
    // End getNotice

    /**
     * Returns the extension property fullname
     *
     * @return string
     */
    public function getFullname()
    {
        if(isset($this->fullname))
            return $this->fullname;

        return $this->fullname = $this->getIdentifier();
    }
    // End getFullname


    /**
     * Returns the property birthday
     *
     * @return string
     */
    public function getBirthday()
    {
        if(isset($this->birthday))
            return $this->birthday;

        return $this->birthday = $this->getProfile()->getBirthday();
    }
    // End getBirthday


    /**
     * Returns the profile
     *
     * @return  UserProfile
     */
    public function getProfile($force = false)
    {
        return UserProfile::findByUserId($this->getId(), $force);
    }
    // End getProfile


    /**
     * Checks, if the object exists
     *
     * @param  numeric  $id    - userId
     * @param  boolean  $force - Bypass caching
     * @return boolean
     */
    public static function exists($id, $force = false)
    {
        return self::findById($id, $force)->isInitialized();
    }
    // End exists


    /**
     * Checks, if the object exists
     *
     * @param  numeric  $id    - userId
     * @param  boolean  $force - Bypass caching
     * @return boolean
     */
    public static function nameExists($authName, $force = false)
    {
        return self::findByAuthName($authName, $force)->isInitialized();
    }
    // End exists

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Updates the object in the table
     *
     * @return boolean
     */
    public function update()
    {
        $this->modified = self::getCurrentTime();

        $sql = sprintf("UPDATE %s
                           SET auth_name   = :authName
                             , auth_key    = :authKey
                             , auth_method = :authMethod
                             , group_id    = :groupId
                             , is_locked   = :isLocked
                             , status      = :status
                             , created     = :created
                             , modified    = :modified
                             , login_time  = :loginTime
                         WHERE id          = :id"
                       , self::TABLE_NAME
                       );

        return $this->updateBySql($sql,
                                  ['id'         => $this->id,
                                        'authName'   => $this->authName,
                                        'authKey'    => $this->authKey,
                                        'authMethod' => $this->authMethod,
                                        'groupId'    => $this->groupId,
                                        'isLocked'   => $this->isLocked,
                                        'status'     => $this->status,
                                        'created'    => $this->created,
                                        'modified'   => $this->modified,
                                        'loginTime'  => $this->loginTime]
                                  );
    }
    // End update


    /**
     * Deletes the object from the table
     *
     * @return boolean
     */
    public function delete()
    {
        try
        {
            $this->beginTransaction();

            $sql = sprintf("DELETE FROM %s
                                  WHERE id = :id"
                           , self::TABLE_NAME
                           );

            $result = $this->deleteBySql($sql, ['id' => $this->id]);

            $Group = $this->getGroup();
            $Group->delete();

            DbObjectCache::freeByObject($this);
            $this->_initialized = false;

            $this->commit();
        }
        catch(Exception $Exception)
        {
            $this->rollback();
            throw $Exception;
        }

        return $result;
    }
    // End delete


    /**
     * Returns an array with the primary key properties and
     * associates its values, if it's a valid object
     *
     * @return array
     */
    public function getPrimaryKey($propertiesOnly = false)
    {
        if($propertiesOnly)
            return self::$primaryKey;

        $primaryKey = [];

        foreach(self::$primaryKey as $key)
            $primaryKey[$key] = $this->$key;

        return $primaryKey;
    }
    // End getPrimaryKey


    /**
     * Returns the tablename constant. This is used
     * as interface for other objects.
     *
     * @return string
     */
    public static function getTablename()
    {
        return self::TABLE_NAME;
    }
    // End getTablename


    /**
     * Returns the columns with their types. The columns may also return extended columns
     * if the first argument is set to true. To access the type of a single column, specify
     * the column name in the second argument
     *
     * @param  boolean  $extColumns
     * @param  mixed    $column
     * @return mixed
     */
    public static function getColumnTypes($extColumns = false, $column = false)
    {
        $columnTypes = $extColumns? array_merge(self::$columnTypes, self::$extColumnTypes) : self::$columnTypes;

        if($column)
            return $columnTypes[$column];

        return $columnTypes;
    }
    // End getColumnTypes


    /**
     * Inserts a new object in the table
     *
     * @return boolean
     */
    protected function insert()
    {
        $this->id         = $this->getNextSequenceValue();
        $this->created    = self::getCurrentTime();
        $this->modified   = null;

        $sql = sprintf("INSERT INTO %s (id, auth_name, auth_key, auth_method, group_id, is_locked, status, created, modified, login_time)
                               VALUES  (:id, :authName, :authKey, :authMethod, :groupId, :isLocked, :status, :created, :modified, :loginTime)"
                       , self::TABLE_NAME
                       );

        return $this->insertBySql($sql, ['id'          => $this->id,
                                              'authName'    => $this->authName,
                                              'authKey'     => $this->authKey,
                                              'authMethod'  => $this->authMethod,
                                              'groupId'     => $this->groupId,
                                              'isLocked'    => $this->isLocked,
                                              'status'      => $this->status,
                                              'created'     => $this->created,
                                              'modified'    => $this->modified,
                                              'loginTime'   => $this->loginTime
                                              ]);
    }
    // End insert


    /**
     * Inits the object with row values
     *
     * @param  object   $DO - Data object
     */
    protected function initByDataObject(\stdClass $DO = null)
    {
        $this->id          = $DO->id;
        $this->authName    = $DO->auth_name;
        $this->authKey     = $DO->auth_key;
        $this->authMethod  = (int)$DO->auth_method;
        $this->groupId     = $DO->group_id;
        $this->isLocked    = (bool) $DO->is_locked;
        $this->status      = (int) $DO->status;
        $this->created     = $DO->created;
        $this->modified    = $DO->modified;
        $this->loginTime   = $DO->login_time;

        /**
         * Set extensions
         */
        if(isset($DO->company))         $this->company         = $DO->company;
        if(isset($DO->gender))          $this->gender          = $DO->gender;
        if(isset($DO->firstname))       $this->firstname       = $DO->firstname;
        if(isset($DO->lastname))        $this->lastname        = $DO->lastname;
        if(isset($DO->email))           $this->email           = $DO->email;
        if(isset($DO->candidate_email)) $this->candidateEmail  = $DO->candidate_email;
        if(isset($DO->notice))          $this->notice          = $DO->notice;
        if(isset($DO->birthday))        $this->birthday        = $DO->birthday;
        if(isset($DO->fullname))        $this->fullname        = $DO->fullname;
    }
    // End initByDataObject
}
// End class User