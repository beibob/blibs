<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs;

/**
 * Handles a set of DbSessions
 *
 * @package blibs
 * @author Tobias Lode <tobias@beibob.de>
 * @author     Fabian M�ller <fab@beibob.de>
 * @abstract
 *
 */
class DbSessionSet extends DbObjectSet
{
    /**
     * Finds all expired sessions
     *
     * @param  -
     * @return DbSessionSet
     */
    public static function findSomething()
    {
        $sql = sprintf("SELECT *
                          FROM %s
                         WHERE id = 'test'"
                       , DbSession::getTablename()
                       );

        return self::findBySql(get_class(), $sql, ['now' => time()]);
    }
    // End findExpired
}
// End class DbSessionSet
