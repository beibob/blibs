<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs;

use DateTime;

/**
 * Validator
 *
 * @package blibs
 * @author Tobias Lode <tobias@beibob.de>
 * @author Fabian Möller <fab@beibob.de>
 *
 */
class Validator
{
    /**
     * Error constants
     */
    const ERR_EMPTY                    = 'err_empty';
    const ERR_NOT_A_NUMBER             = 'err_not_a_number';
    const ERR_NOT_AN_EMAIL             = 'err_not_an_email';
    const ERR_NOT_A_DATE               = 'err_not_a_date';
    const ERR_INVALID                  = 'err_invalid';
    const ERR_INVALID_CALL             = 'err_invalid_call';
    const ERR_UPLOAD                   = 'err_upload';
    const ERR_UPLOAD_MIME_TYPE         = 'err_upload_mime_type';
    const ERR_EXISTS                   = 'err_exists';
    const ERR_NOT_EXISTS               = 'err_not_exists';
    const ERR_LENGTH                   = 'err_length';
    const ERR_MAX_LENGTH               = 'err_length_max';
    const ERR_MIN_LENGTH               = 'err_length_min';
    const ERR_RFC822_VIOLATION         = 'err_rfc822_violation';
    const ERR_URL_INVALID              = 'err_url_invalid';
    const ERR_SELECTION                = 'err_selection';
    const ERR_NOT_UNIQUE               = 'err_not_unique';
    const ERR_NOT_EQUAL                = 'err_not_equal';
    const ERR_NOT_A_TIME               = 'err_not_a_time';
    const ERR_UNKNOWN                  = 'err_unknown';
    const ERR_NOT_UTF8                 = 'err_not_utf8';

    
    
    /**
     * RegEx to validate an url
     * 
     * @see: https://gist.github.com/dperini/729294
     * @see: http://stackoverflow.com/questions/161738/#9284473
     *
     * NOTE: The Flags /iuS needs to be concatinated to PHP Version.
     *       JS version needs only /i
     */
    const REGEX_URL_PHP = '_^(?:(?:https?|ftp)://)(?:\S+(?::\S*)?@)?(?:(?!10(?:\.\d{1,3}){3})(?!127(?:\.\d{1,3}){3})(?!169\.254(?:\.\d{1,3}){2})(?!192\.168(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\x{00a1}-\x{ffff}0-9]+-?)*[a-z\x{00a1}-\x{ffff}0-9]+)(?:\.(?:[a-z\x{00a1}-\x{ffff}0-9]+-?)*[a-z\x{00a1}-\x{ffff}0-9]+)*(?:\.(?:[a-z\x{00a1}-\x{ffff}]{2,})))(?::\d{2,5})?(?:/[^\s]*)?$_';
    const REGEX_URL_JS = '/^(?:(?:https?|ftp):\/\/)(?:\S+(?::\S*)?@)?(?:(?!10(?:\.\d{1,3}){3})(?!127(?:\.‌​\d{1,3}){3})(?!169\.254(?:\.\d{1,3}){2})(?!192\.168(?:\.\d{1,3}){2})(?!172\.(?:1[‌​6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1‌​,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00‌​a1-\uffff0-9]+-?)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]+-?)*[a-z\u‌​00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/[^\s]*)?$/';
    
    /**
     * RegEx to validate a phonenumber
     * 
     * NOTE: The Flag /u needs to be concatinated before using. (jQuery-compatiblity, utf8 or not)
     */
    const REGEX_PHONENUMBER = '/^[0-9\-\+\.\(\)\ \/]{9,30}$/';
    
    /**
     * RegEx to validate a email
     * 
     * NOTE: The Flag /ui needs to be concatinated before using. (jQuery-compatiblity, utf8 or not)
     */
    const REGEX_EMAIL =  '/^[_\.0-9a-z\+\-]+@([0-9a-z][0-9a-z\-]*\.)+[a-z]{2,6}$/';
    
    
    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * data
     */
    public $DataObject;

    /**
     * Errors
     */
    protected $errors = [];

    /**
     * Asserted properties
     */
    protected $asserted = [];

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Constructor
     *
     * @param  array $data
     * @return -
     */
    public function __construct($DataObject = null)
    {
        $this->setDataObject($DataObject);
    }
    // End __construct

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns true if a DataObject was set and the error list is empty
     *
     * @param  -
     * @return boolean
     */
    public function isValid()
    {
        return !$this->hasErrors();
    }
    // End isValid

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns true if the error list is not empty
     *
     * @param  -
     * @return integer
     */
    public function hasErrors()
    {
        return (bool) count($this->errors);
    }
    // End hasErrors

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns true if the property is in the error list
     *
     * @param  -
     * @return boolean
     */
    public function hasError($property)
    {
        return isset($this->errors[$property]);
    }
    // End hasError

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns true if the property is in the error list
     *
     * @param string $property - Eine Eigenschaft
     * @return string
     */
    public function getError($property)
    {
        return $this->errors[$property];
    }
    // End getError

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns all properties with errors and it's error messages
     *
     * @param  -
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }
    // End getErrors

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Resets all states
     *
     * @param  -
     * @return -
     */
    public function clearErrors()
    {
        $this->errors = [];
    }
    // End reset

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Removes an error
     * Returns always false
     *
     * @param  string  $property
     * @return
     */
    public function unsetError($property)
    {
        unset($this->errors[$property]);
    }
    // End unsetError

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns true if the property is in asserted list
     *
     * @param  -
     * @return boolean
     */
    public function isAsserted($property)
    {
        return isset($this->asserted[$property]);
    }
    // End hasError

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Asserts that all values are not empty
     *
     * @param  string  $property
     * @param  string  $error
     * @return boolean
     */
    public function assertNotEmpty($property, $value = null, $error = self::ERR_EMPTY)
    {
        $value = $this->getValue($property, $value);

        if($value == '' && !is_numeric($value) && !is_bool($value))
            return $this->setError($property, $error);

        return $this->setAsserted($property);
    }
    // End assertNotEmpty

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Asserts that all values are not empty
     *
     * @param  string  $property
     * @param  string  $error
     * @return boolean
     */
    public function assertUniqueness($property, $value = null, $callback, array $parameters = [], $error = self::ERR_NOT_UNIQUE)
    {
        $oldValue = $this->getValue($property);
        $newValue = $this->getValue($property, $value);

        if($oldValue != $newValue && call_user_func_array($callback, $parameters))
            return $this->setError($property, $error);

        return $this->setAsserted($property);
    }
    // End assertNotEmpty

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Asserts the value is not empty or not the 'NULL' string.
     * This assertions is meant for selectbox form elements
     *
     * @param  string  $property
     * @param  string  $error
     * @return boolean
     */
    public function assertSelection($property, $value = null, $error = self::ERR_SELECTION)
    {
        $value = $this->getValue($property, $value);

        if($value == 'NULL' || ($value == "" && !is_numeric($value)))
            return $this->setError($property, $error);

        return $this->setAsserted($property);
    }
    // End assertSelection

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Asserts the given boolean value to be true
     *
     * @param  string  $property
     * @param  string  $error
     * @return boolean
     */
    public function assertTrue($property, $value = null, $error = self::ERR_INVALID)
    {
        if(!(bool)$this->getValue($property, $value))
            return $this->setError($property, $error);

        return $this->setAsserted($property);
    }
    // End assertTrue

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Asserts that the specified values is numeric
     *
     * @param  string  $property
     * @param  string  $error
     * @return boolean
     */
    public function assertNumber($property, $value = null, $error = self::ERR_NOT_A_NUMBER)
    {
        $value = $this->getValue($property, $value);

        if($value != '' && !is_numeric($value))
            return $this->setError($property, $error);

        return $this->setAsserted($property);
    }
    // End assertNumber

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Asserts that the specified values is Utf8
     *
     * @param  string  $property
     * @param  string  $error
     * @return boolean
     */
    public function assertUtf8($property, $value = null, $error = self::ERR_NOT_UTF8)
    {
        $value = $this->getValue($property, $value);

        if($value != '' && !StringFactory::isUTF8($value))
            return $this->setError($property, $error);

        return $this->setAsserted($property);
    }
    // End assertUtf8

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Asserts that the specified arguments are a valid date
     *
     * @param  string  $property
     * @param  mixed   $value
     * @param  string  $error
     * @return boolean
     */
    public function assertDateTime($property, $value = null, $error = null)
    {
        $value = $this->getValue($property, $value);

        try
        {
            $TimeStamp = new DateTime($value);
        }
        catch(Exception $Exception)
        {
            return $this->setError($property, is_null($error)? $Exception->getMessage() : $error .': '.$Exception->getMessage() );
        }

        return $this->setAsserted($property);
    }
    // End assertDateTime

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Asserts that the specified arguments are a valid date
     *
     * @param  string  $property
     * @param  string  $propertyMonth
     * @param  string  $propertyDay
     * @param  string  $error
     * @return boolean
     */
    public function assertDate($propertyYear, $propertyMonth, $propertyDay, $valueYear = null, $valueMonth = null, $valueDay = null, $error = self::ERR_NOT_A_DATE)
    {
        $year  = $this->getValue($propertyYear, $valueYear);
        $month = $this->getValue($propertyMonth, $valueMonth);
        $day   = $this->getValue($propertyDay, $valueDay);

        if(!(!$year && !$month && !$day))
        {
            $Date = new BlibsDate();
            $Date->setDateParts($year, $month, $day);

            if(!$Date->isValid())
            {
                $this->setError($propertyYear,  $error);
                $this->setError($propertyMonth, $error);
                $this->setError($propertyDay,   $error);
                return false;
            }
        }

        $this->setAsserted($propertyYear);
        $this->setAsserted($propertyMonth);
        $this->setAsserted($propertyDay);
        return true;
    }
    // End assertDate

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Asserts that the specified arguments are a valid time
     *
     * @param  string  $propertyHour
     * @param  string  $propertyMinute
     * @param  string  $propertySecond
     * @param  string  $error
     * @return boolean
     */
    public function assertTime($propertyHour, $propertyMinute, $propertySecond, $valueHour = null, $valueMinute = null, $valueSecond = null, $error = self::ERR_NOT_A_TIME)
    {
        $hour   = $this->getValue($propertyHour, $valueHour);
        $minute = $this->getValue($propertyMinute, $valueMinute);
        $second = $this->getValue($propertySecond, $valueSecond);


        if(!(!$hour && !$minute && !$second))
        {
            $BlibsTime = new BlibsTime();
            $BlibsTime->setTimeParts($hour, $minute, $second);
            if(!$BlibsTime->isValid())
            {
                $this->setError($propertyHour, $error);
                $this->setError($propertyMinute, $error);
                $this->setError($propertySecond, $error);
                return false;
            }

            if( $hour < 0 || $hour > 23 || $minute < 0 || $minute > 59 || $second < 0 || $second > 59 )
            {
                if($hour < 0 || $hour > 23)
                    $this->setError($propertyHour, $error);

                if($minute < 0 || $minute > 59)
                    $this->setError($propertyMinute, $error);

                if($second < 0 || $second > 59)
                    $this->setError($propertySecond, $error);

                return false;
            }

        }

        $this->setAsserted($propertyHour);
        $this->setAsserted($propertyMinute);
        $this->setAsserted($propertySecond);

        return true;
    }
    // End assertTime

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Asserts that the value of the property is an email address
     *
     * @param string  $name       - property name containing an email address
     * @param string  $error
     * @param boolean $validateMX - check DNS MX record for the email domain
     * @return -
     */
    public function assertEmail($property, $value = null, $validateMX = false, $error = self::ERR_NOT_AN_EMAIL)
    {
        if(($email = $this->getValue($property, $value)) == '')
            return $this->setAsserted($property);

        if(!self::isEmail($email))
            return $this->setError($property, $error);

        elseif($validateMX && !self::validateEmail($email))
            return $this->setError($property, $error);

        return $this->setAsserted($property);
    }
    // End assertEmail

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Asserts that the min length of the property value
     *
     * @param string  $property
     * @param integer $minLength
     * @param string  $error
     */
    public function assertLength($property, $maxLength, $minLength = 0, $value = null, $error = self::ERR_LENGTH)
    {
        if(!$this->assertMaxLength($property, $maxLength, $value, $error))
            return false;

        if(!$this->assertMinLength($property, $minLength, $value, $error))
            return false;

        return true;
    }
    // End assertLength

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Asserts that the min length of the property value
     *
     * @param string  $property
     * @param integer $minLength
     * @param string  $error
     */
    public function assertMaxLength($property, $maxLength, $value = null, $error = self::ERR_MAX_LENGTH)
    {
        $value = utf8_strlen((string)$this->getValue($property, $value));

        if($value > $maxLength)
            return $this->setError($property, $error);

        return $this->setAsserted($property);
    }
    // End assertLength

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Asserts that the min length of the property value
     *
     * @param string  $property
     * @param integer $minLength
     * @param string  $error
     */
    public function assertMinLength($property, $minLength = 0, $value = null, $error = self::ERR_MIN_LENGTH)
    {
        $value = utf8_strlen((string)$this->getValue($property, $value));

        if($value < $minLength)
            return $this->setError($property, $error);

        return $this->setAsserted($property);
    }
    // End assertLength

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Asserts the specified regex on the value of a property
     *
     * @param  string  $property
     * @param  string  $regex - regular expression, e.g. /[a-z]+/i
     * @param  string  $error
     * @return boolean
     */
    public function assertRegEx($property, $regExp, $value = null, $error = self::ERR_INVALID)
    {
        if(($value = $this->getValue($property, $value)) == '' || $regExp == '')
            return $this->setAsserted($property);

        if(!preg_match($regExp, $value))
            return $this->setError($property, $error);

        return $this->setAsserted($property);
    }
    // End assertRegEx

    //////////////////////////////////////////////////////////////////////////////////////
    
    /**
     * Asserts that the value of the given property is a url
     *
     * @param  string $name  - property name
     * @return -
     */
    public function assertUrl($property, $value = null, $error = self::ERR_URL_INVALID)
    {
        if (strpos($this->getValue($property, $value), 'http://localhost') === 0)
            return $this->setAsserted($property);
        
        return $this->assertRegEx($property, self::REGEX_URL_PHP . 'ui', $value, $error);
    }    
    // End assertValidUrl
    
    //////////////////////////////////////////////////////////////////////////////////////
    
    /**
     * 
     * @param type $property
     * @param type $value
     * @param type $error
     */
    public function assertPhoneNumber($property, $value = null, $error = self::ERR_INVALID)
    {
        return $this->assertRegEx($property, self::REGEX_PHONENUMBER . 'ui', $value, $error);        
    }    
    // assertPhoneNumber
    
    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the data object
     *
     * @param  object $DataObject
     * @return -
     */
    public function setDataObject($DataObject)
    {
        if(!is_object($DataObject))
            return;

        $this->DataObject = $DataObject;
    }
    // End setDataObject

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the value for the property
     *
     * @param  string $property
     * @return string
     */
    public function getValue($property, $value = null)
    {
        if(!is_null($value) || !is_object($this->DataObject))
            return $value;

        return $this->DataObject->$property;
    }
    // End setData

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Marks a property with the error.
     * Returns always false
     *
     * @param  string  $property
     * @param  string  $error
     * @return boolean
     */
    public function setAsserted($property)
    {
        $this->asserted[$property] = $property;

        return true;
    }
    // End setAsserted

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Marks a property with the error.
     * Returns always false
     *
     * @param  string  $property
     * @param  string  $error
     * @return boolean
     */
    public function setError($property, $error = self::ERR_INVALID)
    {
        $this->errors[$property] = $error;
        return false;
    }
    // End setError

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Prueft, die Gueltigkeit einer Email
     *
     * @param  string $email - Email Adresse
     * @return boolean       - Wahr, wenn ok, false wenn nicht ok
     */
    public static function isEmail($email)
    {
        return preg_match(self::REGEX_EMAIL . 'ui', $email);
    }
    // End isEmail

    ///////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Prueft, ob die Email Adresse Valide ist
     *
     * @param  string $email - Email Adresse
     * @return boolean       - Wahr, wenn ok, false wenn nicht ok
     */
    public static function validateEmail($email)
    {
        list($user, $domain) = explode('@', $email);

        $mxrecords = null;
        if (getmxrr($domain, $mxrecords))
            return true;

        return false;
    }
    // End validateEmail

    ///////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Magic method __toString
     *
     * @param  -
     * @return string
     */
    public function __toString()
    {
        if($this->isValid())
            return 'No errors';

        $out = [];

        foreach($this->getErrors() as $property => $error)
            $out[] = $property.': '.$error;

        return join(", ", $out);
    }
    // End toString
}
// End HtmlFormValidator
