<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs;

/**
 * Handles mime types
 *
 * @package blibs
 * @author Tobias Lode <tobias@beibob.de>
 * @author Fabian Möller <fab@beibob.de>
 *
 */
class MimeType
{
    /**
     * Liefert den Content-type fuer den gegebenen Dateinamen (falls mit Extensions)
     *
     * @param   string $filename
     * @returns string
     */
    public static function getByFilename($filename)
    {
        if(!preg_match("/^.+\.(.+?)$/u", $filename, $matches))
            return false;

        if(!array_key_exists(utf8_strtolower($matches[1]), self::$mimeTypes))
            return false;

        return self::$mimeTypes[utf8_strtolower($matches[1])];
    }
    // End getByFilename

    ///////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Gets the mime type by
     */
    public static function getByFilepath($filepath)
    {
        return trim(shell_exec("file -b --mime-type \"" . $filepath . "\""));
    }
    // End getByFilepath

    ///////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Liefert den Content-type fuer die gegebenen Erweiterung
     *
     * @param   string $extension
     * @returns string
     */
    public static function getByExtension($extension)
    {
        if(!array_key_exists($extension, self::$mimeTypes))
            return false;

        return self::$mimeTypes[$extension];
    }
    // End getByExtension

    ///////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Liefert die Extension zu einem Contenttype
     *
     * @param  string $contenttype
     * @return string  - extension
     */
    public static function getExtension($contentType)
    {
        return array_search($contentType, self::$mimeTypes);
    }
    // End getExtension

    ///////////////////////////////////////////////////////////////////////////////////////////
    // MIME Types
    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * MIME Typen
     */
    public static $mimeTypes = ['ez' => 'application/andrew-inset',
                                     'atom' => 'application/atom+xml',
                                     'hqx' => 'application/mac-binhex40',
                                     'cpt' => 'application/mac-compactpro',
                                     'mathml' => 'application/mathml+xml',
                                     'doc' => 'application/msword',
                                     'bin' => 'application/octet-stream',
                                     'dms' => 'application/octet-stream',
                                     'lha' => 'application/octet-stream',
                                     'lzh' => 'application/octet-stream',
                                     'exe' => 'application/octet-stream',
                                     'class' => 'application/octet-stream',
                                     'so' => 'application/octet-stream',
                                     'dll' => 'application/octet-stream',
                                     'dmg' => 'application/octet-stream',
                                     'oda' => 'application/oda',
                                     'ogg' => 'application/ogg',
                                     'pdf' => 'application/pdf',
                                     'ai' => 'application/postscript',
                                     'eps' => 'application/postscript',
                                     'ps' => 'application/postscript',
                                     'rdf' => 'application/rdf+xml',
                                     'smi' => 'application/smil',
                                     'smil' => 'application/smil',
                                     'gram' => 'application/srgs',
                                     'grxml' => 'application/srgs+xml',
                                     'mif' => 'application/vnd.mif',
                                     'xul' => 'application/vnd.mozilla.xul+xml',
                                     'xls' => 'application/vnd.ms-excel',
                                     'ppt' => 'application/vnd.ms-powerpoint',
                                     'pptx' => 'application/vnd.ms-powerpoint',
                                     'wbxml' => 'application/vnd.wap.wbxml',
                                     'wmlc' => 'application/vnd.wap.wmlc',
                                     'wmlsc' => 'application/vnd.wap.wmlscriptc',
                                     'vxml' => 'application/voicexml+xml',
                                     'bcpio' => 'application/x-bcpio',
                                     'vcd' => 'application/x-cdlink',
                                     'pgn' => 'application/x-chess-pgn',
                                     'cpio' => 'application/x-cpio',
                                     'csh' => 'application/x-csh',
                                     'dcr' => 'application/x-director',
                                     'dir' => 'application/x-director',
                                     'dxr' => 'application/x-director',
                                     'dvi' => 'application/x-dvi',
                                     'spl' => 'application/x-futuresplash',
                                     'gtar' => 'application/x-gtar',
                                     'hdf' => 'application/x-hdf',
                                     'js' => 'application/x-javascript',
                                     'skp' => 'application/x-koan',
                                     'skd' => 'application/x-koan',
                                     'skt' => 'application/x-koan',
                                     'skm' => 'application/x-koan',
                                     'latex' => 'application/x-latex',
                                     'nc' => 'application/x-netcdf',
                                     'cdf' => 'application/x-netcdf',
                                     'sh' => 'application/x-sh',
                                     'shar' => 'application/x-shar',
                                     'swf' => 'application/x-shockwave-flash',
                                     'sit' => 'application/x-stuffit',
                                     'sv4cpio' => 'application/x-sv4cpio',
                                     'sv4crc' => 'application/x-sv4crc',
                                     'tar' => 'application/x-tar',
                                     'tcl' => 'application/x-tcl',
                                     'tex' => 'application/x-tex',
                                     'texinfo' => 'application/x-texinfo',
                                     'texi' => 'application/x-texinfo',
                                     't' => 'application/x-troff',
                                     'tr' => 'application/x-troff',
                                     'roff' => 'application/x-troff',
                                     'man' => 'application/x-troff-man',
                                     'me' => 'application/x-troff-me',
                                     'ms' => 'application/x-troff-ms',
                                     'ustar' => 'application/x-ustar',
                                     'src' => 'application/x-wais-source',
                                     'xhtml' => 'application/xhtml+xml',
                                     'xht' => 'application/xhtml+xml',
                                     'xslt' => 'application/xslt+xml',
                                     'xml' => 'application/xml',
                                     'xsl' => 'application/xml',
                                     'dtd' => 'application/xml-dtd',
                                     'zip' => 'application/zip',
                                     'au' => 'audio/basic',
                                     'snd' => 'audio/basic',
                                     'mid' => 'audio/midi',
                                     'midi' => 'audio/midi',
                                     'kar' => 'audio/midi',
                                     'mpga' => 'audio/mpeg',
                                     'mp2' => 'audio/mpeg',
                                     'mp3' => 'audio/mpeg',
                                     'aif' => 'audio/x-aiff',
                                     'aiff' => 'audio/x-aiff',
                                     'aifc' => 'audio/x-aiff',
                                     'm3u' => 'audio/x-mpegurl',
                                     'ram' => 'audio/x-pn-realaudio',
                                     'ra' => 'audio/x-pn-realaudio',
                                     'rm' => 'application/vnd.rn-realmedia',
                                     'wav' => 'audio/x-wav',
                                     'pdb' => 'chemical/x-pdb',
                                     'xyz' => 'chemical/x-xyz',
                                     'bmp' => 'image/bmp',
                                     'cgm' => 'image/cgm',
                                     'gif' => 'image/gif',
                                     'ief' => 'image/ief',
                                     'jpeg' => 'image/jpeg',
                                     'jpg' => 'image/jpeg',
                                     'jpe' => 'image/jpeg',
                                     'png' => 'image/png',
                                     'svg' => 'image/svg+xml',
                                     'tiff' => 'image/tiff',
                                     'tif' => 'image/tiff',
                                     'djvu' => 'image/vnd.djvu',
                                     'djv' => 'image/vnd.djvu',
                                     'wbmp' => 'image/vnd.wap.wbmp',
                                     'ras' => 'image/x-cmu-raster',
                                     'ico' => 'image/x-icon',
                                     'pnm' => 'image/x-portable-anymap',
                                     'pbm' => 'image/x-portable-bitmap',
                                     'pgm' => 'image/x-portable-graymap',
                                     'ppm' => 'image/x-portable-pixmap',
                                     'rgb' => 'image/x-rgb',
                                     'xbm' => 'image/x-xbitmap',
                                     'xpm' => 'image/x-xpixmap',
                                     'xwd' => 'image/x-xwindowdump',
                                     'igs' => 'model/iges',
                                     'iges' => 'model/iges',
                                     'msh' => 'model/mesh',
                                     'mesh' => 'model/mesh',
                                     'silo' => 'model/mesh',
                                     'wrl' => 'model/vrml',
                                     'vrml' => 'model/vrml',
                                     'ics' => 'text/calendar',
                                     'ifb' => 'text/calendar',
                                     'css' => 'text/css',
                                     'html' => 'text/html',
                                     'htm' => 'text/html',
                                     'txt' => 'text/plain',
                                     'asc' => 'text/plain',
                                     'csv' => 'text/csv',
                                     'rtx' => 'text/richtext',
                                     'rtf' => 'text/rtf',
                                     'sgml' => 'text/sgml',
                                     'sgm' => 'text/sgml',
                                     'tsv' => 'text/tab-separated-values',
                                     'wml' => 'text/vnd.wap.wml',
                                     'wmls' => 'text/vnd.wap.wmlscript',
                                     'etx' => 'text/x-setext',
                                     'mpeg' => 'video/mpeg',
                                     'mpg' => 'video/mpeg',
                                     'mpe' => 'video/mpeg',
                                     'qt' => 'video/quicktime',
                                     'mov' => 'video/quicktime',
                                     'mxu' => 'video/vnd.mpegurl',
                                     'm4u' => 'video/vnd.mpegurl',
                                     'm4v' => 'video/mp4',
                                     'mp4' => 'video/mp4',
                                     'avi' => 'video/x-msvideo',
                                     'movie' => 'video/x-sgi-movie',
                                     'wmv' => 'video/x-ms-asf', 
                                     'ice' => 'x-conference/x-cooltalk'
        
                                     // New microsoft mimetypes
                                     , 'doc' =>'application/msword'
                                     , 'dot' =>'application/msword'
                                     , 'docx' =>'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
                                     , 'dotx' =>'application/vnd.openxmlformats-officedocument.wordprocessingml.template'
                                     , 'docm' =>'application/vnd.ms-word.document.macroEnabled.12'
                                     , 'dotm' =>'application/vnd.ms-word.template.macroEnabled.12'
                                     , 'xls' =>'application/vnd.ms-excel'
                                     , 'xlt' =>'application/vnd.ms-excel'
                                     , 'xla' =>'application/vnd.ms-excel'
                                     , 'xlsx' =>'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
                                     , 'xltx' =>'application/vnd.openxmlformats-officedocument.spreadsheetml.template'
                                     , 'xlsm' =>'application/vnd.ms-excel.sheet.macroEnabled.12'
                                     , 'xltm' =>'application/vnd.ms-excel.template.macroEnabled.12'
                                     , 'xlam' =>'application/vnd.ms-excel.addin.macroEnabled.12'
                                     , 'xlsb' =>'application/vnd.ms-excel.sheet.binary.macroEnabled.12'
                                     , 'ppt' =>'application/vnd.ms-powerpoint'
                                     , 'pot' =>'application/vnd.ms-powerpoint'
                                     , 'pps' =>'application/vnd.ms-powerpoint'
                                     , 'ppa' =>'application/vnd.ms-powerpoint'
                                     , 'pptx' =>'application/vnd.openxmlformats-officedocument.presentationml.presentation'
                                     , 'potx' =>'application/vnd.openxmlformats-officedocument.presentationml.template'
                                     , 'ppsx' =>'application/vnd.openxmlformats-officedocument.presentationml.slideshow'
                                     , 'ppam' =>'application/vnd.ms-powerpoint.addin.macroEnabled.12'
                                     , 'pptm' =>'application/vnd.ms-powerpoint.presentation.macroEnabled.12'
                                     , 'potm' =>'application/vnd.ms-powerpoint.presentation.macroEnabled.12'
                                     , 'ppsm' =>'application/vnd.ms-powerpoint.slideshow.macroEnabled.12'        
                                     , 'msg' => 'application/vnd.ms-outlook'
        
                                     ];
}
// End class MimeType