<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs;

use Beibob\Blibs\Interfaces\LogAdapter;

/**
 * @package blibs
 * @author Tobias Lode <tobias@beibob.de>
 * @author     Fabian M�ller <fab@beibob.de>
 *
 */
class LogFirePHP implements LogAdapter
{
    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Log severity
     */
    protected $severity;

    ///////////////////////////////////////////////////////////////////////////

    /**
     * Constructs the logger object and opens the logfile if logDirectory is specified
     *
     * @param
     * @return Logger
     */
    public function __construct(Config $LogConfig)
    {
        $Config = Environment::getInstance()->getConfig();
        $this->severity = $LogConfig->get('severity', LOG_NONE);
    }
    // End __construct

    ///////////////////////////////////////////////////////////////////////////

    /**
     * Writes a protocolline into the logfile
     *
     * @param  int    $severity
     * @param  string $message  - message text
     * @param  string $label    - additional label
     * @return -
     */
    public function log($severity, $issue, $label = null)
    {
        if(!function_exists('fb') ||
           !($this->severity & $severity))
            return;

        fb($issue, $label);
    }
    // End log

    //////////////////////////////////////////////////////////////////////////////////////
}
// End class LogFile
