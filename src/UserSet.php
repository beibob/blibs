<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs;

/**
 * Handles a set of User
 *
 * @package blibs
 * @author Fabian Möller  <fab@beibob.de>
 * @author Tobias Lode <tobias@beibob.de>
 *
 */
class UserSet extends DbObjectSet
{
    //////////////////////////////////////////////////////////////////////////////////////
    // public
    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Lazy find
     *
     * @param  array    $initValues - key value array
     * @param  array    $orderBy   - array map of columns on to directions array('id' => 'DESC')
     * @param  integer  $limit     - limit on resultset
     * @param  integer  $offset    - offset on resultset
     * @param  boolean  $force     - Bypass caching
     * @return UserSet
     */
    public static function find(array $initValues = null, array $orderBy = null, $limit = null, $offset = null, $force = false)
    {
        return self::_find(get_class(), User::VIEW_NAME, $initValues, $orderBy, $limit, $offset, $force);
    }
    // End find

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Finds all users of a certain groupId
     *
     * @param  string   $groupId
     * @param  array    $orderBy   - array map of columns on to directions array('id' => 'DESC')
     * @param  integer  $limit     - limit on resultset
     * @param  integer  $offset    - offset on resultset
     * @param  boolean  $force     - Bypass caching
     * @return UserSet
     */
    public static function findByGroupId($groupId, array $orderBy = null, $limit = null, $offset = null, $force = false)
    {
        if(!$groupId)
            return new UserSet();

        $sql = sprintf('SELECT u.*
                          FROM %s u
                          JOIN %s m ON u.id = m.user_id
                         WHERE m.group_id = :groupId'
                       , User::VIEW_NAME
                       , GroupMember::TABLE_NAME
                       );

        if($orderView = self::buildOrderView($orderBy, $limit, $offset))
            $sql .= ' '.$orderView;

        return self::_findBySql(get_class(), $sql, ['groupId' => $groupId], $force);
    }
    // End findByGroupId

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Lazy count
     *
     * @param  array    $initValues - key value array
     * @param  boolean  $force     - Bypass caching
     * @return int
     */
    public static function dbCount(array $initValues = null, $force = false)
    {
        return self::_count(get_class(), User::VIEW_NAME, $initValues, $force);
    }
    // End count

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Lazy find
     *
     * @param  array    $initValues - key value array
     * @param  array    $orderBy   - array map of columns on to directions array('id' => 'DESC')
     * @param  integer  $limit     - limit on resultset
     * @param  integer  $offset    - offset on resultset
     * @param  boolean  $force     - Bypass caching
     * @return UserSet
     */
    public static function findByAdminView(array $initValues = null, array $orderBy = null, $limit = null, $offset = null, $hideSystemAuthNames = false)
    {
        $orderView  = self::buildOrderView($orderBy, $limit, $offset);

        $sql = 'SELECT * FROM ' . User::VIEW_NAME;

        $conditions = self::buildAdminFilterConditions($initValues);

        if ($hideSystemAuthNames)
            $conditions[] = "auth_name NOT IN ('" . join("', '", User::$systemAuthNames) . "')";

        if(count($conditions))
            $sql .= ' WHERE ' . join(' AND ', $conditions);

        if($orderView)
            $sql .= ' ' . $orderView;

        return self::_findBySql(get_class(), $sql, $initValues);
    }
    // End findByAdminView

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Z�hlt wieviele Datens�tze die Admin-Tabelle enth�lt
     */
    public static function countByAdminView(array $initValues = null, $hideSystemAuthNames = false)
    {
        $sql = 'SELECT count(*) AS counter FROM ' . User::VIEW_NAME;

        $conditions = self::buildAdminFilterConditions($initValues);

        if ($hideSystemAuthNames)
            $conditions[] = "auth_name NOT IN ('" . join("', '", User::$systemAuthNames) . "')";

        if(count($conditions))
            $sql .= ' WHERE ' . join(' AND ', $conditions);

        return self::_countBySql(get_class(), $sql, $initValues, 'counter');
    }
    // End countByAdminView

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Bosselt die Filter-SQL-Fragmente f�r den Adminscreen zusammen
     */
    protected static function buildAdminFilterConditions(&$initValues)
    {


        $conditions = [];
        if (count($initValues))
        {
            $unset = [];
            foreach ($initValues as $column => $value)
            {
                if (is_null($value))
                {
                    $unset[$column] = $column;
                    continue;
                }
                switch ($column)
                {
                    default:
                        $initValues[$column] = '%' . $value . '%';
                        $conditions[] = StringFactory::unCamelCase($column) . " ILIKE :" . $column;
                    break;
                }
            }

            foreach($unset as $column)
                unset($initValues[$column]);
        }

        return $conditions;
    }
    // End buildAdminFilterConditions

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Dependency on HtmlTools\HtmlTableProperties ??
     */
//    public static function findByTableProperties( HtmlTableProperties $TableProperties)
//    {
//        $orderBy = $TableProperties->getOrderBy();
//        $sql = sprintf("SELECT * FROM (SELECT *, firstname ||' '|| lastname AS name FROM %s) AS x ", User::VIEW_NAME );
//
//        if ($filterSql = $TableProperties->getFilterSql())
//            $sql .= 'WHERE ' . $filterSql;
//        if(!is_null($TableProperties) && $orderView  = self::buildOrderView($orderBy, $TableProperties->getLimit(), $TableProperties->getOffset()))
//            $sql .= ' ' . $orderView;
//
//
//        return self::_findBySql(get_class(), $sql);
//    }
    // End findByTableProperties

   //////////////////////////////////////////////////////////////////////////////////////

    /**
     *
     */
    public static function countByTableProperties(HtmlTableProperties $TableProperties)
    {
        $sql = sprintf("SELECT count(*) FROM (SELECT *, firstname ||' '|| lastname AS name FROM %s) AS x "
                       , User::VIEW_NAME
                       );
        if ($filterSql = $TableProperties->getFilterSql())
            $sql .= 'WHERE ' . $filterSql;

        return self::_countBySql(get_class(), $sql, null, 'count');
    }
    // End countByTableProperties

   //////////////////////////////////////////////////////////////////////////////////////
}
// End class UserSet
