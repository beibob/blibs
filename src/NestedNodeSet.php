<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs;

/**
 * Handles a set of NestedNodes
 *
 * @package blibs
 * @author Tobias Lode <tobias@beibob.de>
 * @author     Fabian M�ller <fab@beibob.de>
 * @abstract
 *
 */
class NestedNodeSet extends DbObjectSet
{
    /**
     * Finds all direct child nodes of the given parent node
     *
     * @param  -
     * @return NestedNodeSet
     */
    public static function findByParent(NestedNode $Node, $force = false)
    {
        if(!$Node->isValid())
            return new NestedNodeSet();

        $sql = sprintf("SELECT *
                          FROM %s
                         WHERE root_id = :rootId
                           AND lft BETWEEN :lft AND :rgt
                           AND level = :level + 1
                         ORDER BY lft"
                       , NestedNode::getTablename());

        return self::_findBySql(get_class(), $sql, ['rootId' => $Node->getRootId(),
                                                        'lft'    => $Node->getLft(),
                                                        'rgt'    => $Node->getRgt(),
                                                        'level'  => $Node->getLevel()
                                                         ], $force
                                );
    }
    // End findByParent

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Finds a list of all Root nodes.
     *
     * @param  -
     * @return -
     */
    public static function findRootNodes($force = false)
    {
        $sql = sprintf("SELECT *
                          FROM %s
                          WHERE id = root_id"
                       , NestedNode::getTablename()
                       );

        return self::_findBySql(get_class(), $sql, null, $force);
    }
    // End findRootNodes

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Inits a list of all subordinate nodes of the given parent node
     *
     * @param  NestedNode $Parent
     * @return NestedNodeSet
     */
    public static function findSubordinatesByParent(NestedNode $Node, $force = false)
    {
        if(!$Node->isValid())
            return new NestedNodeSet();

        $sql = sprintf("SELECT *
                          FROM %s
                         WHERE root_id = :rootId
                           AND lft BETWEEN :lft AND :rgt
                           AND level > :level
                         ORDER BY lft"
                       , NestedNode::getTablename());

        return self::_findBySql(get_class(), $sql, ['rootId' => $Node->getRootId(),
                                                        'lft'    => $Node->getLft(),
                                                        'rgt'    => $Node->getRgt(),
                                                        'level'  => $Node->getLevel()
                                                         ], $force);
    }
    // End initSubordinatesByParent

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Inits a list of parent nodes of a given subordinate
     *
     * @param  NestedNode $Subordinate
     * @param  bool $excludeSubordinate  - exclude the start node itself
     * @return NestedNodeSet
     */
    public static function findParentsOf(NestedNode $Subordinate, $excludeSubordinate = false, $force = false)
    {
        if(!$Subordinate->isValid())
            return new NestedNodeSet();

        $initValues = ['rootId' => $Subordinate->getRootId(),
                            'lft'    => $Subordinate->getLft()
                            ];

        if($excludeSubordinate)
        {
            $excludeSql = "AND level < :level";
            $initValues['level'] = $Subordinate->getLevel();
        }

        $sql = sprintf("SELECT *
                          FROM %s
                         WHERE root_id = :rootId
                           AND :lft BETWEEN lft AND rgt
                            %s
                      ORDER BY lft"
                       , NestedNode::getTablename()
                       , $excludeSubordinate? $excludeSql : ''
                       );


        return self::_findBySql(get_class(), $sql, $initValues, $force);
    }
    // End findParentsOf

    //////////////////////////////////////////////////////////////////////////////////////
}
// End class NestedNodeSet
