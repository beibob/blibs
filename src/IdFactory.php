<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs;

/**
 * Factory for creating various kinds of ids
 *
 * @package blibs
 * @author Tobias Lode <tobias@beibob.de>
 * @author     Fabian M�ller <fab@beibob.de>
 *
 */
class IdFactory
{
    /**
     * BLibs Epoche ``Sat Jan 1 00:00:00 CET 2005``
     *
     * Konstante notwendig fuer die Generierung von SequelIDs
     */
    const BLIBS_EPOCH = 1104534000;

    /**
     * sequelId lengths
     */
    const MIN_SEQUEL_ID_LENGTH = 10;
    const MAX_SEQUEL_ID_LENGTH = 20;


    const ID_CRYPT_WISCHIWASCHI = 3;

    /**
     * unique xml ids
     */
    private static $xmlId = 1;

    ///////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Generiert SequelIDs.
     * WICHTIG! IDs muessen als numerischer String in PHP
     *          verwendet und verarbeitet werden!
     *
     * @return string  numerische SequelId
     */
    public static function getSequelId()
    {
        $currtime = gettimeofday();

        $sec  = $currtime["sec"] - self::BLIBS_EPOCH;
        $usec = $currtime["usec"];

        return sprintf("%d%06d", $sec, $usec);
    }
    // End getSequelId

    ///////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Generiert md5 basierte unique IDs
     *
     * @return string  alphanumeische UniqueID
     */
    public static function getUniqueId()
    {
        return md5(uniqid(rand(),1));
    }
    // End getUniqueId

    ///////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns a unique and valid xml id
     *
     * @return string
     */
    public static function getUniqueXmlId()
    {
        return chr(rand(97, 122)) . (string)(self::$xmlId++);
    }
    // End getUniqueId

    ///////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Liefert aus allen mitgelieferten Merkmalen einen Fingerabdruck
     * Wenn kein Argument angegeben wurde, liefert die Methode eine UniqueID
     *
     * @param   string - variable Argumentliste
     * @returns string
     */
    public static function getFingerprint()
    {
        if(func_num_args() > 0)
        {
            $attributes = func_get_args();
            $fingerprint = md5(join('', $attributes));
        }
        else
        {
            $fingerprint = self::getUniqueId();
        }

        return $fingerprint;
    }
    // End getFingerprint

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Assert a sequelId
     *
     * @param  numeric $sequelId
     * @return boolean
     */
    public static function isSequelId($sequelId)
    {
        if(!is_numeric($sequelId) ||
           utf8_strlen($sequelId) < self::MIN_SEQUEL_ID_LENGTH ||
           utf8_strlen($sequelId) > self::MAX_SEQUEL_ID_LENGTH)
            return false;

        return true;
    }
    // End isSequelId

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * macht ein wischiwaschi-crypten einer Id
     */
    public static function encryptId($id, $timeStamp)
    {
        return ($id * self::ID_CRYPT_WISCHIWASCHI) . '.' . ($id ^ ($timeStamp));
    }
    // End encryptId

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * macht ein wischiwaschi-decrypten einer Id
     */
    public static function decryptId($token)
    {
        if (!$token)
            return;

        $parts = explode('.', $token);

        if (count($parts) < 2)
            return;

        if (count($parts) == 3)
        {
            $id = (int) (($parts[0] . '.' . $parts[1]) / self::ID_CRYPT_WISCHIWASCHI);
            $cryptedTs = (int) $parts[2];
        }
        else
        {
            $id = (int) ($parts[0] / self::ID_CRYPT_WISCHIWASCHI);
            $cryptedTs = (int) $parts[1];
        }

        $timeStamp = ($cryptedTs ^ $id);

        return [$id, $timeStamp];
    }
    // End decryptId

    //////////////////////////////////////////////////////////////////////////////////////
}
// End IdFactory
