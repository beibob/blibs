<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs;

/**
 * Redirect exception
 *
 * @package blibs
 * @author Tobias Lode <tobias@beibob.de>
 * @author     Fabian M�ller <fab@beibob.de>
 *
 */
class RedirectException extends Exception
{
    /**
     * Redirect location
     */
    private $location;

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Constructor
     *
     * @param  string $message
     * @param  int    $code
     * @param  string $ctrlname
     * @param  string $module
     * @param  string $action
     * @return -
     */
    public function __construct($location, $code = null)
    {
        $this->setLocation($location);
    }
    // End __construct

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the location
     *
     * @parm string
     */
    public function setLocation($location)
    {
        $this->location = $location;
    }
    // End setLocation

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the location url
     *
     * @return string
     */
    public function getLocation()
    {
        return $this->location;
    }
    // End getLocation

    //////////////////////////////////////////////////////////////////////////////////////
}
// End RedirectException
