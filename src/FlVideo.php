<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs;

/**
 * @package blibs
 * @author Fabian Möller  <fab@beibob.de>
 * @author Dakam Franck <franck@beibob.de>
 *
 */
class FlVideo extends File
{
    /**
     * GetId3 Object
     */
    private $Id3;

    /**
     *
     * @MetaInformation about flv ArrayObject
     */
    private $Info;

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Constructs a file instance. opens a filehandle
     * only if mode is specified
     *
     * @param  string $filepath
     * @param  string $filemode
     * @return -
     */
    public function __construct($filepath = false, $filemode = false)
    {
        parent::__construct($filepath, $filemode);
        $this->getInfo();
    }
    // End __construct

    ////////////////////////////////////////////////////////////////////////////

    /**
     *
     * @param int $maxWidth
     * @param int $maxHeight
     * @param int $count
     * @param int $offsetTime
     *
     * @returns one GdImage object or list of GdImage objects
     */
    public function getScaledSnapshots($maxWidth, $maxHeight, $count = 1, $offsetTime = 1)
    {
        if($count == 1)
        {
            $Image = $this->getSnapshot($offsetTime);
            $GdImage = GdImage::factory($Image->getFilepath());
            $GdImage->scaleToMaxSize($maxWidth, $maxHeight);
            $GdImage->save($Image->getFilepath());
            return $Image;
        }
        else
        {
            $unScaledimageList = $this->getSnapshots($count);
            $imageList = [];
            for ($i=0 ; $i < $count ; $i++)
            {
                $GdImage = GdImage::factory($unScaledimageList[$i]->getFilepath());
                $GdImage->scaleToMaxSize($maxWidth, $maxHeight);
                $GdImage->save($unScaledimageList[$i]->getFilepath());
                $imageList[] = $unScaledimageList[$i];
            }
            return $imageList;
        }
    }

    /**
     *
     * @param int $count
     * @param int $ratio
     * @param int $offsetTime
     *
     * @return one GdImage object or list of GdImage objects
     */

    public function getCroppedSnapshots($count = 1, $ratio = 1 , $offsetTime = 1)
    {
        if($count == 1)
        {
            $Image = $this->getSnapshot($offsetTime);
            $GdImage = GdImage::factory($Image->getFilepath());
            $GdImage->crop($ratio);
            $GdImage->save($Image->getFilepath());
            return $GdImage;
        }
        else
        {
            $unScaledimageList = $this->getSnapshots($count);
            $imageList = [];
            for ($i=0 ; $i < $count ; $i++)
            {
                $GdImage = GdImage::factory($unScaledimageList[$i]->getFilepath());
                $GdImage->crop($ratio);
                $GdImage->save($unScaledimageList[$i]->getFilepath());
                $imageList[] = $unScaledimageList[$i];
            }

            return $imageList;
        }
    }

    /**
     * @param integer $offsetTime - Time in seconds where grab should happens
     * @return File $Image
     */
    public function getSnapshot($offsetTime = 1)
    {
        $ImageFile = new File();
        $ImageFile->createTemporaryFile();

        $cmd = sprintf('%s -i %s -f rawvideo -vframes 1 -vcodec png -ss %d  -y %s'
                       , Environment::getInstance()->getConfig()->ffmpeg->path
                       , $this->getFilepath()
                       , $offsetTime
                       , $ImageFile->getFilepath());

        $output = @system($cmd);

        if (MimeType::getByFilepath($ImageFile->getFilepath()) == MimeType::$mimeTypes['png'])
            return $ImageFile;
        else
            throw new Exception("ffmpeg error!\n\nMessage:\n" . $output);
    }
    // End getSnapshot

    ////////////////////////////////////////////////////////////////////////////

    /**
     * Get video Snapshots
     *
     * @return array list of ImageFiles
     */
    public function getSnapshots($count, $timeOffset= false)
    {
        $snapTime = $snapTime2 = $this->getVideoDuration() / ($count +1);

        $imageFiles = [];

        for ($i = 0; $i < $count; $i++)
        {
            try
            {
                $imageFiles[] = $this->getSnapshot($snapTime2);
            }
            catch (Exception $Exception)
            {
                foreach ($imageFiles as $File)
                    $File->delete();

                throw $Exception;
            }

            $snapTime2 = $snapTime2 + $snapTime;
        }

        return $imageFiles;
    }
    //End getSnapshots

    ////////////////////////////////////////////////////////////////////////////

    /**
     * Returns meta information about flv
     */
    public function getInfo()
    {
        if (is_object($this->Info))
            return $this->Info;

        return $this->Info = new ArrayObject(@$this->getId3()->analyze($this->getFilepath()));
    }
    // End getInfo

    ////////////////////////////////////////////////////////////////////////////

    /*
     * Get The Video Duration
     */
    public function getVideoDuration()
    {
        return $this->Info['meta']['onMetaData']['duration'];
    }
    //End getVideoDuration

    ////////////////////////////////////////////////////////////////////////////

    /*
     * Get The Video Width
     */
    public function getVideoWidth()
    {
        return $this->Info['meta']['onMetaData']['width'];
    }
    //End getVideoWidth

    ////////////////////////////////////////////////////////////////////////////

    /*
     * Get The Video Width
     */
    public function getVideoHeight()
    {
        return $this->Info['meta']['onMetaData']['height'];
    }
    //End getVideoHeight

    ////////////////////////////////////////////////////////////////////////////

    /**
     * Returns Id3 object
     */
    protected function getId3()
    {
        return $this->Id3 = new GetId3();
    }
    // End getId3

    ////////////////////////////////////////////////////////////////////////////
}
// End FlVideo
