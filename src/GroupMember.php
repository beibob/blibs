<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs;

use PDO;

/**
 *
 * @package blibs
 * @author     Fabian M�ller  <fab@beibob.de>
 * @author Tobias Lode <tobias@beibob.de>
 *
 */
class GroupMember extends DbObject
{
    /**
     * Tablename
     */
    const TABLE_NAME = 'public.group_members';

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * userId
     */
    private $userId;

    /**
     * groupId
     */
    private $groupId;

    /**
     * Primary key
     */
    private static $primaryKey = ['groupId', 'userId'];

    /**
     * Column types
     */
    private static $columnTypes = ['userId'         => PDO::PARAM_INT,
                                        'groupId'        => PDO::PARAM_INT];

    /**
     * Extended column types
     */
    private static $extColumnTypes = [];

    //////////////////////////////////////////////////////////////////////////////////////
    // public
    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Creates the object
     *
     * @param  integer  $userId - userId
     * @param  integer  $groupId - groupId
     */
    public static function create($userId, $groupId)
    {
        $GroupMember = new GroupMember();
        $GroupMember->setUserId($userId);
        $GroupMember->setGroupId($groupId);

        if($GroupMember->getValidator()->isValid())
            $GroupMember->insert();

        return $GroupMember;
    }
    // End create

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Inits a `GroupMember' by its primary key
     *
     * @param  integer  $groupId - groupId
     * @param  integer  $userId - userId
     * @param  boolean  $force  - Bypass caching
     * @return GroupMember
     */
    public static function findByPk($groupId, $userId, $force = false)
    {
        if(!$groupId || !$userId)
            return new GroupMember();

        $sql = sprintf("SELECT user_id
                             , group_id
                          FROM %s
                         WHERE group_id = :groupId
                           AND user_id = :userId"
                       , self::TABLE_NAME
                       );

        return self::findBySql(get_class(), $sql, ['groupId' => $groupId, 'userId' => $userId], $force);
    }
    // End findByPk

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the property userId
     *
     * @param  integer  $userId - userId
     * @return
     */
    public function setUserId($userId)
    {
        if(!$this->getValidator()->assertNotEmpty('userId', $userId))
            return;

        $this->userId = (int)$userId;
    }
    // End setUserId

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the property groupId
     *
     * @param  integer  $groupId - groupId
     * @return
     */
    public function setGroupId($groupId)
    {
        if(!$this->getValidator()->assertNotEmpty('groupId', $groupId))
            return;

        $this->groupId = (int)$groupId;
    }
    // End setGroupId

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the property userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return (int)$this->userId;
    }
    // End getUserId

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the associated User by property userId
     *
     * @param  boolean  $force
     * @return User
     */
    public function getUser($force = false)
    {
        return User::findById($this->userId, $force);
    }
    // End getUser

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the property groupId
     *
     * @return integer
     */
    public function getGroupId()
    {
        return (int)$this->groupId;
    }
    // End getGroupId

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the associated Group by property groupId
     *
     * @param  boolean  $force
     * @return Group
     */
    public function getGroup($force = false)
    {
        return Group::findById($this->groupId, $force);
    }
    // End getGroup

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Checks, if the object exists
     *
     * @param  integer  $groupId - groupId
     * @param  integer  $userId - userId
     * @param  boolean  $force  - Bypass caching
     * @return boolean
     */
    public static function exists($groupId, $userId, $force = false)
    {
        return self::findByPk($groupId, $userId, $force)->isInitialized();
    }
    // End exists

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Updates the object in the table
     *
     * @return boolean
     */
    public function update()
    {
        /**
         * Nothing to update
         */
    }
    // End update

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Deletes the object from the table
     *
     * @return boolean
     */
    public function delete()
    {
        $sql = sprintf("DELETE FROM %s
                              WHERE group_id = :groupId
                                AND user_id = :userId"
                       , self::TABLE_NAME
                      );

        return $this->deleteBySql($sql,
                                  ['groupId' => $this->groupId, 'userId' => $this->userId]);
    }
    // End delete

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns an array with the primary key properties and
     * associates its values, if it's a valid object
     *
     * @param  boolean  $propertiesOnly
     * @return array
     */
    public function getPrimaryKey($propertiesOnly = false)
    {
        if($propertiesOnly)
            return self::$primaryKey;

        $primaryKey = [];

        foreach(self::$primaryKey as $key)
            $primaryKey[$key] = $this->$key;

        return $primaryKey;
    }
    // End getPrimaryKey

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the tablename constant. This is used
     * as interface for other objects.
     *
     * @return string
     */
    public static function getTablename()
    {
        return self::TABLE_NAME;
    }
    // End getTablename

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the columns with their types. The columns may also return extended columns
     * if the first argument is set to true. To access the type of a single column, specify
     * the column name in the second argument
     *
     * @param  boolean  $extColumns
     * @param  mixed    $column
     * @return mixed
     */
    public static function getColumnTypes($extColumns = false, $column = false)
    {
        $columnTypes = $extColumns? array_merge(self::$columnTypes, self::$extColumnTypes) : self::$columnTypes;

        if($column)
            return $columnTypes[$column];

        return $columnTypes;
    }
    // End getColumnTypes

    //////////////////////////////////////////////////////////////////////////////////////
    // protected
    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Inserts a new object in the table
     *
     * @return boolean
     */
    protected function insert()
    {

        $sql = sprintf("INSERT INTO %s (user_id, group_id)
                               VALUES  (:userId, :groupId)"
                       , self::TABLE_NAME
                       );

        return $this->insertBySql($sql,
                                  ['userId'  => $this->userId,
                                        'groupId' => $this->groupId]
                                  );
    }
    // End insert

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Inits the object with row values
     *
     * @param  \stdClass $DO - Data object
     * @return boolean
     */
    protected function initByDataObject(\stdClass $DO = null)
    {
        $this->userId         = (int)$DO->user_id;
        $this->groupId        = (int)$DO->group_id;

        /**
         * Set extensions
         */
    }
    // End initByDataObject
}
// End class GroupMember
