<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs;

use PDO;
use PDOStatement;

/**
 * Abstract active record interface for DataObjects
 *
 * @package blibs
 * @author Tobias Lode <tobias@beibob.de>
 * @author Fabian Möller <fab@beibob.de>
 * @abstract
 *
 */
abstract class DbObject extends \stdClass
{
    /**
     * Database handle
     */
    protected $Dbh;

    /**
     * Validator
     */
    protected $Validator;

    /**
     * Prepared statement cache
     */
    private static $preparedStatements = [];

    /**
     * object initialization state
     */
    private $_initialized = false;

    /**
     * internal unique object id, helps identifying objects
     */
    private $objectId;

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Saves the object to table and returns true on success
     *
     * @param  -
     * @return boolean
     */
    abstract public function update();

    /**
     * Deletes the object from the table
     *
     * @param  -
     * @return boolean
     */
    abstract public function delete();

    /**
     * Returns an array with the primary key properties and,
     * if it's a valid object, associated its values
     *
     * @param  -
     * @return array
     */
    abstract public function getPrimaryKey();

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns a DbObject Instance
     *
     * @param  -
     * @return DbObject
     */
    public static function getInstance($dbObjectName, $DataObject = null)
    {
        /**
         * @todo: use late static binding to get primary key.
         *        then look it up in the object cache
         *         *  $primaryKey = static::getPrimaryKey();
         */
        $DbObject = new $dbObjectName($DataObject);

        if($CachedObject = DbObjectCache::getDbObject(DbObjectCache::getSignature($dbObjectName, $DbObject->getPrimaryKey())))
        {
            /**
             * Overwrite values with new DataObject
             */
            $CachedObject->initByDataObject($DataObject);
            return $CachedObject;
        }

        return $DbObject;
    }
    // End getInstance

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns true if the object is initialized
     *
     * @param  -
     * @return boolean
     */
    public function isInitialized()
    {
        return $this->_initialized;
    }
    // End isInitialized

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Setzt das initialized-Flag
     */
    public function setInitialized($initialized = true)
    {
        return $this->_initialized = $initialized;
    }
    // setInitialized

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns true if the object is initialized and it's validator
     * was either never instantiated or validates to true
     *
     * @param  -
     * @return boolean
     */
    public function isValid()
    {
        return $this->isInitialized() &&
            (!is_object($this->Validator) || is_object($this->Validator) && $this->Validator->isValid());
    }
    // End isValid

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the validator
     *
     * @param  -
     * @return Validator
     */
    public function getValidator()
    {
        if(!$this->Validator instanceOf Validator)
            $this->Validator = new Validator($this);

        return $this->Validator;
    }
    // End setValue

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns a data object
     *
     * @param  -
     * @return object
     */
    public function getDataObject($extColumns = false)
    {
        $DataObject = new \stdClass();

        foreach($this->getColumnTypes($extColumns) as $name => $type)
            $DataObject->$name = $this->$name;

        return $DataObject;
    }
    // End getDataObject

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the DbHandle
     *
     * @param  -
     * @return DbHandle
     */
    public function getDbHandle()
    {
        return DbHandle::getInstance();
    }
    // End getDbHandle

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Magic method __isset
     *
     * @param  string $property
     * @return boolean
     */
    public function __isset($property)
    {
        /**
         * @todo: use late static binding
         */
        $properties = $this->getColumnTypes(true);

        /**
         * Just check if property is a valid column
         */
        return isset($properties[$property]);
    }
    // End __isset

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Magic method __get to provide read-only property access
     *
     * @param  string   $property
     * @return mixed
     */
    public function __get($property)
    {
        if(!isset($this->$property))
            return null;

        /**
         * Prefere get method
         */
        $methodName = self::getMethodName($property);
        if(method_exists($this, $methodName))
            return $this->$methodName();

        return $this->$property;
    }
    // End __get

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Magic method __set to call the specific property setter
     *
     * @param  string   $property     the property to set
     * @param  mixed    $value        the value to set
     */
    public function __set($property, $value)
    {
        if(!isset($this->$property))
            return null;

        /**
         * Prefere set method
         */
        $methodName = self::getMethodName($property, 'set');
        if(method_exists($this, $methodName))
            return $this->$methodName($value);

        $this->$property = $value;;
    }
    // End __set

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the method name of the given property
     *
     * @param  string $property
     * @return string
     */
    public static function getMethodName($property, $prefix = 'get')
    {
        if((substr($property, 0, 2) == 'is' ||
           substr($property, 0, 3) == 'has') && $prefix == 'get')
            $method = $property;

        else
            $method = $prefix . ucfirst($property);

        return $method;
    }
    // End getMethodName

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns a cached or new prepared statement
     *
     * @param  string $sql
     * @return PDOStatement
     */
    final public static function prepareStatement($sql, array $initValues = null)
    {
        $signature = md5($sql);

        if(!isset(self::$preparedStatements[$signature]) || !$Stmt = self::$preparedStatements[$signature])
            $Stmt = self::$preparedStatements[$signature] = DbHandle::getInstance()->prepare($sql);

        if(!is_null($initValues))
        {
            foreach($initValues as $parameter => $value)
                $Stmt->bindValue($parameter, $value, DbHandle::getPDOType($value));
        }

        return $Stmt;
    }
    // End prepareStatement

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Handles a statement error
     *
     * @param  string $sql
     * @return false
     */
    final public static function getSqlErrorMessage($dbObjectName, $sql, array $boundValues = null)
    {
        $errorInfo = self::prepareStatement($sql)->errorInfo();

        if(is_array($boundValues))
        {
            foreach($boundValues as $key => $value)
            {
                if(is_bool($value))
                    $sql = str_replace(':'.$key, '['.($value? 'true' : 'false') .'::'.gettype($value).']', $sql);

                elseif($value)
                    $sql = str_replace(':'.$key, '['.$value.'::'.gettype($value).']', $sql);

                elseif($value === null)
                    $sql = str_replace(':'.$key, '[NULL]', $sql);

                else
                    $sql = str_replace(':'.$key, '[empty]', $sql);
            }
        }

        return sprintf("%s - %s\n\n".
                       "Statement: %s\n"
                       , $errorInfo[0]
                       , $errorInfo[2]
                       , $sql
                       );
    }
    // End handleSqlError

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the objectId
     *
     * @param  -
     * @return string
     */
    public function getObjectId()
    {
        return $this->objectId;
    }
    // End getObjectId

    //////////////////////////////////////////////////////////////////////////////////////
    // protected
    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Contructor
     *
     * @param  -
     * @return -
     */
    final protected function __construct(\stdClass $DataObject = null)
    {
        /**
         * Debugging
         */
        $this->objectId = spl_object_hash($this);

        /**
         * Init database handle
         */
        $this->Dbh = $this->getDbHandle();

        if(!$DataObject)
            return;

        /**
         * Init object and mark it valid
         */
        $this->initByDataObject($DataObject);
        $this->_initialized = true;
    }
    // End __construct

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Inits the data object
     *
     * @param  \StdClass $DataObject
     * @return -
     */
    abstract protected function initByDataObject(\StdClass $DataObject = null);

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Inserts a new object in the table
     *
     * @param  -
     * @return boolean
     */
    abstract protected function insert();

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Inserts a new object into the table by sql statement
     *
     * @param  string $sql
     * @param  array  $bindValues
     * @return boolean
     */
    final protected function insertBySql($sql, array $boundValues, array &$returnValues = null, $fetchStyle = PDO::FETCH_OBJ)
    {
        $Stmt = self::prepareStatement($sql, $boundValues);

        if(!$Stmt->execute())
            throw new Exception(self::getSqlErrorMessage(get_class($this), $sql, $boundValues));

        if(!is_null($returnValues))
            $returnValues = $Stmt->fetchAll($fetchStyle);

        DbObjectCache::setDbObject(DbObjectCache::getSignature(get_class($this), $this->getPrimaryKey()), $this);
        return $this->_initialized = true;
    }
    // End insertBySql

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Updates the object in the table by sql statement
     *
     * @param  string $sql
     * @param  array  $boundValues
     * @param  array  $initValues
     * @return boolean
     */
    final protected function updateBySql($sql, array $boundValues, array &$returnValues = null, $fetchStyle = PDO::FETCH_OBJ)
    {
        if(!$this->isValid())
            return false;

        $Stmt = self::prepareStatement($sql, $boundValues);

        if(!$Stmt->execute())
            throw new Exception(self::getSqlErrorMessage(get_class($this), $sql, $boundValues));

        if(!is_null($returnValues))
            $returnValues = $Stmt->fetchAll($fetchStyle);

        return true;
    }
    // End updateBySql

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Deletes the object from the table by sql statement
     *
     * @param  string $sql
     * @param  array  $initValues
     * @return boolean
     */
    final protected function deleteBySql($sql, array $initValues = null, array &$returnValues = null, $fetchStyle = PDO::FETCH_OBJ)
    {
        if(!$this->isInitialized())
            return false;

        $Stmt = self::prepareStatement($sql, $initValues);

        if(!$Stmt->execute())
            throw new Exception(self::getSqlErrorMessage(get_class($this), $sql, $initValues));

        if(!is_null($returnValues))
            $returnValues = $Stmt->fetchAll($fetchStyle);

        DbObjectCache::freeByObject($this);
        $this->_initialized = false;
        return true;
    }
    // End deleteBySql

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Queries the given sql which
     *
     * @param  string $sql
     * @param  array  $bindValues
     * @return boolean
     */
    final protected static function executeSql($dbObjectName, $sql, array $boundValues = null, array &$returnValues = null, $fetchStyle = PDO::FETCH_OBJ, $colIndex = null)
    {
        $Stmt = self::prepareStatement($sql, $boundValues);

        if(!$Stmt->execute())
            throw new Exception(self::getSqlErrorMessage($dbObjectName, $sql, $boundValues));

        if(!is_null($returnValues))
            $returnValues = !is_null($colIndex)? $Stmt->fetchAll($fetchStyle, $colIndex) : $Stmt->fetchAll($fetchStyle);

        return true;
    }
    // End executeSql

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Performs the given sql and returns the affected rows
     *
     * @param  string $sql
     * @param  array  $bindValues
     * @return int
     */
    final protected static function performSql($dbObjectName, $sql, array $boundValues = null)
    {
        $Stmt = self::prepareStatement($sql, $boundValues);

        if(!$Stmt->execute())
            throw new Exception(self::getSqlErrorMessage($dbObjectName, $sql, $boundValues));

        return true;
    }
    // End querySql

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Inits the object by a sql statement. This may be an statement with placeholders.
     * Put all bind values in an associative array with parameter name in it keys
     *
     * @param  string  $sql
     * @param  array   $initValues
     * @param  boolean $force
     * @return boolean
     */
    final protected static function findBySql($dbObjectName, $sql, array $initValues = null, $force = false)
    {
        /**
         * Check required arguments
         */
        if(!$dbObjectName || !$sql)
            return null;

        /**
         * Check if object already in cache
         */
        if(!$force && $DbObject = DbObjectCache::getDbObject(DbObjectCache::getSignature($dbObjectName, $initValues, $sql)))
            return $DbObject;

        $Stmt = self::prepareStatement($sql);

        if(!is_null($initValues))
            foreach($initValues as $parameter => $value)
                $Stmt->bindValue($parameter, $value, DbHandle::getPDOType($value));

        if(!$Stmt->execute())
            throw new Exception(self::getSqlErrorMessage($dbObjectName, $sql, $initValues));

        if(!$DataObject = $Stmt->fetchObject())
            return new $dbObjectName();

        /**
         * Get DbObject instance by identity map
         */
        $DbObject = DbObject::getInstance($dbObjectName, $DataObject);

        /**
         * Cache it.
         */
        DbObjectCache::setDbObject(DbObjectCache::getSignature($dbObjectName, $initValues, $sql), $DbObject);

        return $DbObject;
    }
    // End findBySql

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the next value of the given sequence
     *
     * @param  -
     * @return mixed
     */
    final protected function getNextSequenceValue($seqName = false)
    {
        /**
         * @todo: replace call to getTablename with late static binding feature
         */
        if(!$seqName)
        {
            if(count($pks = $this->getPrimaryKey(true)) != 1)
                throw new Exception('Need explicit sequence name on objects with more than one unique columns');

            $seqName = sprintf('%s_%s_seq'
                               , $this->getTablename()
                               , $pks[0]);
        }

        $values = [];

        if(!self::executeSql(get_class($this), 'SELECT nextval(:seqName) AS value', ['seqName' => $seqName], $values))
            return false;

        if(!$Obj = array_shift($values))
            return false;

        return $Obj->value;
    }
    // End getNextSequenceValue

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Queries an expression and returns its value
     *
     *  e.g. queryExpression('uuid_generate_v4()')
     *   returns uuidv4 string
     *
     * @param  string $expr
     * @param array   $boundValues
     * @return null|string
     * @throws Exception
     */
    final protected function queryExpression($expr, array $boundValues = null)
    {
        $values = [];

        if(!self::executeSql(get_class($this), 'SELECT '.$expr.' AS value', $boundValues, $values))
            return null;

        if(!$Obj = array_shift($values))
            return null;

        return $Obj->value;
    }
    // End queryExpression

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the current date time as string
     *
     * @param  string $format
     * @return string
     */
    protected static function getCurrentTime($format = false)
    {
        return (string)new BlibsDateTime(false, $format);
    }
    // End getCurrentTime

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Begins a transaction
     *
     * @param  -
     * @return -
     */
    protected function beginTransaction()
    {
        $this->Dbh->beginTransaction();
    }
    // End beginTransaction

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Commits a transaction
     *
     * @param  -
     * @return -
     */
    protected function commit()
    {
        $this->Dbh->commit();
    }
    // End commit

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Rollback a transaction
     *
     * @param  -
     * @return -
     */
    protected function rollback()
    {
        $this->Dbh->rollback();
    }
    // End rollback

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the dbObjectName tablename
     *
     * @param  string
     * @return string
     */
    protected static function getDbInstanceName($dbObjectName)
    {
        if(isset(self::$dbHandleInstanceNames[$dbObjectName]))
            return self::$dbHandleInstanceNames[$dbObjectName];

        return self::$dbHandleInstanceNames[$dbObjectName] = call_user_func([$dbObjectName, 'getDbInstanceName']);
    }
    // End getDbObjectName

    //////////////////////////////////////////////////////////////////////////////////////
    // private
    //////////////////////////////////////////////////////////////////////////////////////
}
// End DbObject
