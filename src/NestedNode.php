<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs;

use PDO;

/**
 * A nested node
 *
 * @package blibs
 * @author Tobias Lode <tobias@beibob.de>
 * @author Fabian Möller <fab@beibob.de>
 *
 */
class NestedNode extends DbObject
{
    /**
     * Table name
     */
    const TABLE_NAME    = 'public.nested_nodes';
    const SEQUENCE_NAME = 'public.nested_nodes_seq';

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * id
     */
    private $id;

    /**
     * rootNodeId
     */
    private $rootId;

    /**
     * left value
     */
    private $lft;

    /**
     * right value
     */
    private $rgt;

    /**
     * node level
     */
    private $level;

    /**
     * ident
     */
    private $ident;

    /**
     * Primary key
     */
    private static $primaryKey = ['id'];

    /**
     * Columns and their types
     */
    private static $columnTypes = ['id'     => PDO::PARAM_INT,
                                        'rootId' => PDO::PARAM_INT,
                                        'lft'    => PDO::PARAM_INT,
                                        'rgt'    => PDO::PARAM_INT,
                                        'level'  => PDO::PARAM_INT,
                                        'ident'  => PDO::PARAM_STR
                                        ];

    /**
     * Extended columns and their types
     */
    private static $extColumnTypes = [];

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Creates a new root node
     *
     * @param  string $ident [optional]
     * @return NestedNode
     */
    public static function createRoot($ident = null)
    {
        /**
         * Create a node with id = rootId and lft = 1
         */
        return self::create(false, 1, 2, 0, $ident);
    }
    // End create

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Creates a new node as child of another node
     *
     * @param  NestedNode $ParentNode
     * @return NestedNode
     */
    public static function createAsChildOf(NestedNode $ParentNode, $ident = null)
    {
        if(!$ParentNode->isValid())
            return null;

        $Dbh = DbHandle::getInstance();

        $Dbh->beginTransaction();
        $affectedNodes = self::spreadForChildRightInsert($ParentNode->rootId, $ParentNode->rgt);

        if($ChildNode = self::create($ParentNode->rootId, $ParentNode->rgt, $ParentNode->rgt + 1, $ParentNode->level + 1, $ident))
            $Dbh->commit();
        else
            $Dbh->rollback();

        /**
         * Refresh cache
         */
        self::refreshCachedObjects($affectedNodes);
        return $ChildNode;
    }
    // End create

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Creates a new node left from another node
     *
     * @param  NestedNode $Node
     * @return NestedNode
     */
    public static function createLeftFrom(NestedNode $Node, $ident = null)
    {
        if(!$Node->isValid())
            return null;

        /**
         * Create space left besides $Node and insert the new
         * with lft = Node lft and rgt with lft + 1 as leaf
         */
        $Dbh = DbHandle::getInstance();

        $Dbh->beginTransaction();
        $affectedNodes = self::spreadForLeftInsert($Node->rootId, $Node->lft);
        $SiblingNode   = self::create($Node->rootId, $Node->lft, $Node->lft + 1, $Node->level, $ident);
        $Dbh->commit();

        /**
         * Refresh cache
         */
        self::refreshCachedObjects($affectedNodes);
        return $SiblingNode;
    }
    // End createLeftFrom

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Creates a new node right from another node
     *
     * @param  NestedNode $Node
     * @return NestedNode
     */
    public static function createRightFrom(NestedNode $Node, $ident)
    {
        if(!$Node->isValid())
            return null;

        /**
         * Create space right besides $Node and insert the new
         * with lft = Node lft+1 and rgt with lft + 2 as leaf
         */
        $Dbh = DbHandle::getInstance();

        $Dbh->beginTransaction();
        $affectedNodes = self::spreadForRightInsert($Node->rootId, $Node->rgt);
        $SiblingNode   = self::create($Node->rootId, $Node->rgt + 1, $Node->rgt + 2, $Node->level, $ident);
        $Dbh->commit();

        /**
         * Refresh cache
         */
        self::refreshCachedObjects($affectedNodes);
        return $SiblingNode;
    }
    // End createRightFrom

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Finds a node by id
     *
     * @param  string  $id
     * @param  boolean $force - bybass caching
     * @return NestedNode
     */
    public static function findById($id, $force = false)
    {
        if(!$id)
            return new NestedNode();

        $sql = 'SELECT *
                  FROM '.self::getTablename().'
                 WHERE id = :id';

        return parent::findBySql(get_class(), $sql, ['id' => $id], $force);
    }
    // End findById

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Finds the parent node of a child node
     *
     * @param  NestedNode $Node
     * @param  boolean $force - bybass caching
     * @return NestedNode
     */
    public static function findParentOf(NestedNode $Node, $force = false)
    {
        $sql = 'SELECT *
                  FROM '.self::getTablename().'
                 WHERE :lft BETWEEN lft AND rgt
                   AND level = :level
                   AND root_id = :rootId';

        return parent::findBySql(get_class(), $sql,
                                 ['lft'    => $Node->lft,
                                       'level'  => $Node->level - 1,
                                       'rootId' => $Node->rootId,
                                       ],
                                 $force);
    }
    // End findRootOf

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Finds a node by its ident
     *
     * @param  string  $id
     * @param  boolean $force - bybass caching
     * @return NestedNode
     */
    public static function findRootByIdent($ident, $force = false)
    {
        if(!$ident)
            return new NestedNode();

        $sql = 'SELECT *
                  FROM '.self::TABLE_NAME.'
                 WHERE root_id = id
                   AND ident = :ident';

        return parent::findBySql(get_class(), $sql, ['ident'  => $ident]
                                 , $force);
    }
    // End findRootByIdent

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Finds a node by its ident
     *
     * @param  string  $id
     * @param  boolean $force - bybass caching
     * @return NestedNode
     */
    public static function findByIdent($rootId, $ident, $force = false)
    {
        if(!$rootId || !$ident)
            return new NestedNode();

        $sql = 'SELECT *
                  FROM '.self::TABLE_NAME.'
                 WHERE root_id = :rootId
                   AND ident = :ident';

        return parent::findBySql(get_class(), $sql, ['rootId' => $rootId,
                                                          'ident'  => $ident]
                                 , $force);
    }
    // End findById

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Liefert die Elternnode
     */
    public function getParentNode()
    {
        return NestedNode::findParentOf($this);
    }
    // End getParentNode

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns id
     *
     * @param  -
     * @return numeric
     */
    public function getId()
    {
        return $this->id;
    }
    // End getId

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns rootId
     *
     * @param  -
     * @return numeric
     */
    public function getRootId()
    {
        return $this->rootId;
    }
    // End getRootId

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns rootId
     *
     * @param  -
     * @return NestedNode
     */
    public function getRoot()
    {
        return self::findById($this->rootId);
    }
    // End getRoot

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the left value
     *
     * @param  -
     * @return int
     */
    public function getLft()
    {
        return $this->lft;
    }
    // End getLft

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the right value
     *
     * @param  -
     * @return int
     */
    public function getRgt()
    {
        return $this->rgt;
    }
    // End getRgt

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the level
     *
     * @param  -
     * @return int
     */
    public function getLevel()
    {
        return $this->level;
    }
    // End getLevel

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns ident
     *
     * @param  -
     * @return numeric
     */
    public function getIdent()
    {
        return $this->ident;
    }
    // End getIdent

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the ident
     *
     * @param  string $ident
     * @return -
     */
    public function setIdent($ident = null)
    {
        $this->ident = $ident;
    }
    // End setIdent

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns true if the node has child nodes
     *
     * @param  -
     * @return boolean
     */
    public function hasChildNodes()
    {
        return ($this->getRgt() - $this->getLft()) > 1;
    }
    // End hasChildNodes

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns a set of child nodes
     *
     * @param  boolean $force
     * @return NestedNodeSet
     */
    public function getChildNodes($force = false, $initValues = [])
    {
        return NestedNodeSet::findByParent($this, $initValues, $force);
    }
    // End getChildNodes

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the first child of the node
     *
     * @param  boolean $force
     * @return NestedNode
     */
    public function getFirstChildNode($force = false)
    {
        if (!$this->hasChildNodes())
            return NestedNode::findById(null);

        $ChildNodes = NestedNodeSet::findByParent($this, [], $force);
        return $ChildNodes[0];
    }
    // End getFirstChildNode

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the last child of the node
     *
     * @param  boolean $force
     * @return NestedNode
     */
    public function getLastChildNode($force = false)
    {
        if (!$this->hasChildNodes())
            return NestedNode::findById(null);

        $ChildNodes = NestedNodeSet::findByParent($this, [], $force);
        return $ChildNodes[$ChildNodes->count() - 1];
    }
    // End getLastChildNode

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the countSubordinates-Counter
     *
     * @param  -
     * @return boolean
     */
    public function countSubordinates()
    {
        if ($this->hasChildNodes())
            return ($this->getRgt() - $this->getLft() - 1) / 2;
        else
            return 0;
    }
    // End countSubordinates

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Swaps this node with its sibling
     *
     * @param  Node $Sibling - rightmost or leftmost sibling
     * @return -
     */
    public function swapWith(NestedNode $Sibling)
    {
        if(!$this->isValid() || !$Sibling->isValid())
            return;

        /**
         * Determine if sibling stands right or left from us
         */
        if($this->rgt < $Sibling->lft)
            $affectedNodes = self::swapSiblings($this->rootId, $this, $Sibling);
        else
            $affectedNodes = self::swapSiblings($this->rootId, $Sibling, $this);

        /**
         * Refresh cache
         */
        self::refreshCachedObjects($affectedNodes);
    }
    // End swapWith

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Copies all child nodes of one node to another node
     *
     * @param  NestedNode - $DstParentNode
     * @return -
     */
    public function copyChildNodesTo($DstParentNode)
    {
        if($this->rootId == $DstParentNode->rootId &&
           $this->lft    >= $DstParentNode->lft &&
           $this->rgt    <= $DstParentNode->rgt)
            throw new Exception("Can't copy a subtree under itself!");

        $NestedNodeSet = NestedNodeSet::findSubordinatesByParent($this);

        if(!$NestedNodeSet->count())
            return false;

        $this->Dbh->beginTransaction();
        $affectedNodes = self::spreadForChildLeftInsert($DstParentNode->rootId, $DstParentNode->lft, $NestedNodeSet->count());

        /**
         * Refresh cache
         */
        self::refreshCachedObjects($affectedNodes);

        foreach($NestedNodeSet as $Subordinate)
        {
            /**
             * Prepare subordinate for new insertion
             */
            $NewNode = clone $Subordinate;

            $NewNode->id     = $this->getNextSequenceValue(self::SEQUENCE_NAME);
            $NewNode->rootId = $DstParentNode->rootId;
            $NewNode->lft    = $DstParentNode->lft + $Subordinate->lft - $this->lft;
            $NewNode->rgt    = $DstParentNode->lft + $Subordinate->rgt - $this->lft;
            $NewNode->level  = $DstParentNode->level + ($Subordinate->level - $this->level);

            $NewNode->insert();
        }

        $this->Dbh->commit();
        return $NestedNodeSet->count();
    }
    // End copyChildNodesTo

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Saves the object to table and returns true on success
     *
     * @param  -
     * @return boolean
     */
    public function update()
    {
        $sql = sprintf("UPDATE %s
                           SET root_id = :rootId
                             , lft     = :lft
                             , rgt     = :rgt
                             , level   = :level
                             , ident   = :ident
                         WHERE id = :id"
                       , self::getTablename()
                      );

        return $this->updateBySql($sql,
                                  ['id'     => $this->id,
                                        'rootId' => $this->rootId,
                                        'lft'    => $this->lft,
                                        'rgt'    => $this->rgt,
                                        'level'  => $this->level,
                                        'ident'  => $this->ident
                                        ]);
    }
    // End update

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Deletes the object from the table
     *
     * @param  -
     * @return boolean
     */
    public function delete()
    {
        try
        {
            $this->beginTransaction();

            $sql = sprintf("DELETE FROM %s
                              WHERE lft BETWEEN :lft AND :rgt
                                AND root_id = :rootId
                                 RETURNING *"
                           , self::getTablename()
                           );

            $deletedNodes = [];
            if($result = $this->deleteBySql($sql,
                                            ['rootId' => $this->rootId,
                                                  'lft'    => $this->lft,
                                                  'rgt'    => $this->rgt
                                                  ],
                                            $deletedNodes
                                            ))
            {
                /**
                 * Free cache
                 */
                self::freeCachedObjects($deletedNodes);

                /**
                 * close up the gap left by the subtree
                 */
                $sql = sprintf("UPDATE %s
                                   SET lft = CASE WHEN lft > :lft1::int
                                                  THEN lft - (:rgt1::int - :lft2::int + 1)
                                                  ELSE lft
                                             END
                                     , rgt = CASE WHEN rgt > :lft3::int
                                                  THEN rgt - (:rgt2::int - :lft4::int + 1)
                                                  ELSE lft
                                             END
                                     WHERE root_id = :rootId::int
                                       AND (lft > :lft5::int
                                        OR rgt > :lft6::int
                                           )
                                 RETURNING *"
                               , self::TABLE_NAME
                               );

                $affectedNodes = [];
                if(self::executeSql(get_class($this), $sql,
                                    ['lft1'   => $this->lft,
                                          'lft2'   => $this->lft,
                                          'lft3'   => $this->lft,
                                          'lft4'   => $this->lft,
                                          'lft5'   => $this->lft,
                                          'lft6'   => $this->lft,
                                          'rgt1'   => $this->rgt,
                                          'rgt2'   => $this->rgt,
                                          'rootId' => $this->rootId
                                          ],
                                    $affectedNodes
                                    ))
                {
                    /**
                     * Refresh cache
                     */
                    self::refreshCachedObjects($affectedNodes);
                }
            }

            $this->commit();
        }
        catch(Exception $Exception)
        {
            $this->rollback();
            throw $Exception;
        }

        return $result;
    }
    // End delete

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns an array with the primary key properties and,
     * if it's a valid object, associated its values
     *
     * @param  -
     * @return array
     */
    public function getPrimaryKey($propertiesOnly = false)
    {
        if($propertiesOnly)
            return self::$primaryKey;

        $primaryKey = [];

        foreach(self::$primaryKey as $key)
            $primaryKey[$key] = $this->$key;

        return $primaryKey;
    }
    // End getPrimaryKey

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the name of the table the object is stored in
     *
     * @param  -
     * @return string
     */
    public static function getTablename()
    {
        return self::TABLE_NAME;
    }
    // End getTablename

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the columns with their types. The columns may also return extended columns
     * if the first argument is set to true. To access the type of a single column, specify
     * the column name in the second argument
     *
     * @param  boolean $extColumns
     * @param  string $column
     * @return mixed
     */
    public static function getColumnTypes($extColumns = false, $column = false)
    {
        $columnTypes = $extColumns? array_merge(self::$columnTypes, self::$extColumnTypes) : self::$columnTypes;

        if(!$column)
            return $columnTypes;

        return $columnTypes[$column];
    }
    // End getColumnTypes

    //////////////////////////////////////////////////////////////////////////////////////
    // protected
    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Creates the object
     *
     * @param  numeric  $rootId - id of the root node
     * @param  integer  $lft   - left number
     * @param  integer  $rgt   - right number
     * @param  integer  $level - level
     */
    protected static function create($rootId, $lft, $rgt, $level, $ident = null)
    {
        $Node = new NestedNode();
        $Node->id           = $Node->getNextSequenceValue(self::SEQUENCE_NAME);
        $Node->rootId       = $rootId? $rootId : $Node->id;
        $Node->lft          = $lft;
        $Node->rgt          = $rgt;
        $Node->level        = $level;
        $Node->setIdent($ident);
        $Node->insert();

        return $Node;
    }
    // End create

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Inserts a new object in the table
     *
     * @param  -
     * @return boolean
     */
    protected function insert()
    {
        $sql = sprintf("INSERT INTO %s (id, root_id, lft, rgt, level, ident)
                               VALUES  (:id, :rootId, :lft, :rgt, :level, :ident)"
                       , self::getTablename()
                       );

        return $this->insertBySql($sql, ['id'     => $this->id,
                                              'rootId' => $this->rootId,
                                              'lft'    => $this->lft,
                                              'rgt'    => $this->rgt,
                                              'level'  => $this->level,
                                              'ident'  => $this->ident
                                              ]);
    }
    // End insert

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Inits the object by an data object
     *
     * @param  \stdClass $TDO
     * @return -
     */
    protected function initByDataObject(\stdClass $DO = null)
    {
        $this->id      = $DO->id;
        $this->rootId  = $DO->root_id;
        $this->lft     = (int)$DO->lft;
        $this->rgt     = (int)$DO->rgt;
        $this->level   = (int)$DO->level;
        $this->ident   = $DO->ident;
    }
    // End initByDataObject

    //////////////////////////////////////////////////////////////////////////////////////
    // private
    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Creates space for _one_ node left from another
     *
     * @param  numeric $rootId - id of the root node
     * @param  numeric $lft    - lft value of the sibling on the right
     * @param  numeric $nNodes    - number of nodes to spread space for
     * @return -
     */
    private static function spreadForLeftInsert($rootId, $lft, $nNodes = 1)
    {
        $sql = sprintf("UPDATE %s
                           SET lft = CASE WHEN lft >= :lft1
                                          THEN lft + :nSpace1
                                          ELSE lft
                                     END
                             , rgt = CASE WHEN rgt > :lft2
                                          THEN rgt + :nSpace2
                                          ELSE rgt
                                     END
                         WHERE rgt > :lft3
                           AND root_id = :rootId
                     RETURNING *"
                       , self::getTablename()
                       );

        $affectedNodes = [];

        parent::executeSql(get_class(), $sql,
                           ['lft1' => $lft,
                                 'lft2' => $lft,
                                 'lft3' => $lft,
                                 'nSpace1' => $nNodes * 2,
                                 'nSpace2' => $nNodes * 2,
                                 'rootId' => $rootId],
                           $affectedNodes
                           );

        return $affectedNodes;
    }
    // End spreadForLeftInsert

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Creates space for _one_ node right from another
     *
     * @param  numeric $rootId - id of the root node
     * @param  numeric $rgt    - rgt value of the sibling on the left
     * @param  numeric $nNodes    - number of nodes to spread space for
     * @return -
     */
    private static function spreadForRightInsert($rootId, $rgt, $nNodes = 1)
    {
        $sql = sprintf("UPDATE %s
                           SET lft = CASE WHEN lft > :rgt1
                                          THEN lft + :nSpace1
                                          ELSE lft
                                     END
                             , rgt = CASE WHEN rgt > :rgt2
                                          THEN rgt + :nSpace2
                                          ELSE rgt
                                     END
                         WHERE rgt > :rgt3
                           AND root_id = :rootId
                     RETURNING *"
                       , self::getTablename());

        $affectedNodes = [];
        parent::executeSql(get_class(), $sql,
                          ['rgt1' => $rgt,
                                'rgt2' => $rgt,
                                'rgt3' => $rgt,
                                'nSpace1' => $nNodes * 2,
                                'nSpace2' => $nNodes * 2,
                                'rootId' => $rootId],
                          $affectedNodes
                          );

        return $affectedNodes;
    }
    // End spreadForRightInsert

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Creates space for n nodes right from another
     *
     * @param  numeric $rootId    - id of the root node
     * @param  numeric $rgt       - rgt value of the sibling on the left
     * @param  numeric $nNodes    - number of nodes to spread space for
     * @param
     * @return -
     */
    private static function spreadForChildRightInsert($rootId, $parentRgt, $nNodes = 1)
    {
        $sql = sprintf("UPDATE %s
                           SET lft = CASE WHEN lft > :parentRgt1
                                          THEN lft + :nSpace1
                                          ELSE lft
                                     END
                             , rgt = CASE WHEN rgt >= :parentRgt2
                                          THEN rgt + :nSpace2
                                          ELSE rgt
                                     END
                         WHERE rgt >= :parentRgt3
                           AND root_id = :rootId
                     RETURNING *"
                       , self::getTablename()
                       );

        $affectedNodes = [];
        parent::executeSql(get_class(), $sql,
                           ['parentRgt1' => $parentRgt,
                                 'parentRgt2' => $parentRgt,
                                 'parentRgt3' => $parentRgt,
                                 'nSpace1'    => $nNodes * 2,
                                 'nSpace2'    => $nNodes * 2,
                                 'rootId'     => $rootId],
                          $affectedNodes
                          );

        return $affectedNodes;
    }
    // End spreadForChildRightInsert

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Creates space for n nodes as leftmost childs of a parent node
     *
     * @param  numeric $rootId    - id of the root node
     * @param  numeric $lft       - lft value of the parent node
     * @param  numeric $nNodes    - number of nodes to spread space for
     * @param
     * @return -
     */
    private static function spreadForChildLeftInsert($rootId, $parentLft, $nNodes = 1)
    {
        $sql = sprintf("UPDATE %s
                           SET lft = CASE WHEN lft > :parentLft1
                                          THEN lft + :nSpace1
                                          ELSE lft
                                     END
                             , rgt = CASE WHEN rgt > :parentLft2
                                          THEN rgt + :nSpace2
                                          ELSE rgt
                                     END
                         WHERE rgt > :parentLft3
                           AND root_id = :rootId
                     RETURNING *"
                       , self::getTablename()
                       );

        $affectedNodes = [];
        parent::executeSql(get_class(), $sql,
                          ['parentLft1' => $parentLft,
                                'parentLft2' => $parentLft,
                                'parentLft3' => $parentLft,
                                'nSpace1'    => $nNodes * 2,
                                'nSpace2'    => $nNodes * 2,
                                'rootId'     => $rootId],
                          $affectedNodes
                          );

        return $affectedNodes;
    }
    // End spreadForChildLeftInsert

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Swap to nodes within a tree
     *
     * @param  numeric $rootId  - id of the root node
     * @param  NestedNode $LeftNode   - leftmost sibling
     * @param  NestedNode $RightNode  - rightmost sibling
     * @return -
     */
    private static function swapSiblings($rootId, NestedNode $LeftNode, NestedNode $RightNode)
    {
        $sql = sprintf("UPDATE %s
                           SET lft = CASE WHEN lft BETWEEN :lLft1 AND :lRgt1
                                          THEN :rRgt1::integer + lft - :lRgt2::integer

                                          WHEN lft BETWEEN :rLft1 AND :rRgt2
                                          THEN :lLft2::integer + lft - :rLft2::integer

                                          ELSE :lLft3::integer + :rRgt3::integer + lft - :lRgt3::integer - :rLft3::integer
                                     END
                             , rgt = CASE WHEN rgt BETWEEN :lLft4 AND :lRgt4
                                          THEN :rRgt4::integer + rgt - :lRgt5::integer

                                          WHEN rgt BETWEEN :rLft4 AND :rRgt5
                                          THEN :lLft5::integer + rgt - :rLft5::integer

                                          ELSE :lLft6::integer + :rRgt6::integer + rgt - :lRgt6::integer - :rLft6::integer
                                     END
                         WHERE lft BETWEEN :lLft7 AND :rRgt7
                           AND root_id = :rootId
                     RETURNING *"
                       , self::getTablename());

        $affectedNodes = [];
        parent::executeSql(get_class(), $sql,
                          ['lLft1' => $LeftNode->lft, 'lRgt1' => $LeftNode->rgt,
                                'lLft2' => $LeftNode->lft, 'lRgt2' => $LeftNode->rgt,
                                'lLft3' => $LeftNode->lft, 'lRgt3' => $LeftNode->rgt,
                                'lLft4' => $LeftNode->lft, 'lRgt4' => $LeftNode->rgt,
                                'lLft5' => $LeftNode->lft, 'lRgt5' => $LeftNode->rgt,
                                'lLft6' => $LeftNode->lft, 'lRgt6' => $LeftNode->rgt,
                                'lLft7' => $LeftNode->lft,

                                'rLft1' => $RightNode->lft, 'rRgt1' => $RightNode->rgt,
                                'rLft2' => $RightNode->lft, 'rRgt2' => $RightNode->rgt,
                                'rLft3' => $RightNode->lft, 'rRgt3' => $RightNode->rgt,
                                'rLft4' => $RightNode->lft, 'rRgt4' => $RightNode->rgt,
                                'rLft5' => $RightNode->lft, 'rRgt5' => $RightNode->rgt,
                                'rLft6' => $RightNode->lft, 'rRgt6' => $RightNode->rgt,
                                'rRgt7' => $RightNode->rgt,

                                'rootId' => $rootId],
                          $affectedNodes
                          );

        return $affectedNodes;
    }
    // End spreadForLeftInsert

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Refreshes all cached objects with the given array of data objects
     *
     * @param  array $affectedNodes
     * @return -
     */
    private static function refreshCachedObjects(array $affectedNodes)
    {
        /**
         * Free cache for affected nodes
         */
        foreach($affectedNodes as $DataObject)
        {
            $signature = DbObjectCache::getSignature(get_class(), [$DataObject->id]);

            if(!$DbObject = DbObjectCache::getDbObject($signature))
                continue;

            $DbObject->initByDataObject($DataObject);
        }
    }
    // End refreshCachedObjects

     //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Frees all cached objects with the given array of data objects
     *
     * @param  array $deletedNodes
     * @return -
     */
    private static function freeCachedObjects(array $deletedNodes)
    {
        /**
         * Free cache for affected nodes
         */
        foreach($deletedNodes as $DataObject)
        {
            $signature = DbObjectCache::getSignature(get_class(), [$DataObject->id]);
            DbObjectCache::freeDbObject($signature);
        }
    }
    // End deleteCachedObjects
}
// End NestedNode
