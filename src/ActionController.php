<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs;

use Beibob\Blibs\Interfaces\Viewable;

/**
 * Abstract Action Controller
 *
 * @package blibs
 * @author Tobias Lode <tobias@beibob.de>
 * @author Fabian Möller <fab@beibob.de>
 * @abstract
 *
 */
abstract class ActionController
{
    /**
     * @var FrontController $FrontController
     */
    protected $FrontController;

    /**
     * @var RequestBase $Request
     */
    protected $Request;

    /**
     * @var ResponseBase $Response
     */
    protected $Response;

    /**
     * Dependency injection container
     */
    protected $container;

    /**
     * @var Viewable $View
     */
    private $View;

    /**
     * @var Viewable $BaseView
     */
    private $BaseView;

    /**
     * @var array $additionalViews
     */
    private $additionalViews = [];

    /**
     * @var string $module Module this Controller is defined in
     */
    private $module;

    /**
     * @var string $action Invoked action
     */
    private $action;

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Constructs the controller and handles an action
     *
     * @param  -
     * @return ActionController
     */
    public function __construct()
    {
        $this->FrontController = FrontController::getInstance();

        $this->Request  = $this->FrontController->getRequest();
        $this->Response = $this->FrontController->getResponse();

        $this->container =  $this->FrontController->getEnvironment()->getContainer();
        $this->setModule();
    }
    // End __construct

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Processes this controller.
     *
     * Calls all necessary methods and hooks in the proper order
     * and returns the view. if renderView is not set to false,
     * the view also will be rendered.
     *
     * @throws Exception - if no view could be resolved
     * @param  string $action
     * @param  array $initArgs - init arguments for the controller and view
     * @param  bool $renderView - controlls rendering of the view
     * @return  View
     */
    final public function process($action = null, array $query = [], $renderView = true)
    {
        $this->setAction($action);
        $this->init($query);
        $this->invokeActionMethod();
        $this->finalize();

        if($this->hasBaseView())
            $View = $this->getBaseView();

        elseif($this->hasView())
            $View = $this->getView();

        else
            return null;

        if($renderView)
        {
            $View->process([], $this->module);

            if($this->hasAdditionalViews())
            {
                $MultiPartView = new MultiPartView();
                $MultiPartView->add($View);

                foreach($this->getAdditionalViews() as $AddView)
                {
                    $AddView->process([], $this->module);
                    $MultiPartView->add($AddView);
                }

                $View = $MultiPartView;
            }
        }

        return $View;
    }
    // End process

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns a href string to a controller
     *
     * @param  string $action
     * @param  array  $arguments
     * @return -
     */
    public function getActionLink($action = null, array $arguments = null)
    {
        return $this->FrontController->getUrlTo(null, $action, $arguments);
    }
    // End getLinkTo

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns a href string to a controller
     *
     * @param  string $action
     * @param  array  $arguments
     * @return -
     */
    public function getLinkTo($controller = null, $action = null, array $arguments = null)
    {
        return $this->FrontController->getUrlTo($controller, $action, $arguments);
    }
    // End getLinkTo

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the module
     *
     * @param  -
     * @return string
     */
    public function getModule()
    {
        return $this->module;
    }
    // End getModule

    //////////////////////////////////////////////////////////////////////////////////////
    // protected
    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Will be called on initialization.
     *
     * @param  array $args
     * @return -
     */
    protected function init(array $args = [])
    {
    }
    // End init

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Will be called after invoking an action
     *
     * @param  -
     * @return -
     */
    protected function finalize()
    {
    }
    // End finalize

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns true if the controller can return a view
     *
     * @param  -
     * @return boolean
     */
    final protected function hasView()
    {
        return !is_null($this->View);
    }
    // End hasView

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns true if the controller can return additional views
     *
     * @param  -
     * @return boolean
     */
    final protected function hasAdditionalViews()
    {
        return !empty($this->additionalViews);
    }
    // End hasAdditionalViews

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the view
     *
     * @param  -
     * @return View
     */
    final protected function getView()
    {
        return $this->View;
    }
    // End getView

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the additional views
     *
     * @param  -
     * @return array
     */
    final protected function getAdditionalViews()
    {
        return $this->additionalViews;
    }
    // End getAdditionalViews

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns true if the controller can return a base view
     *
     * @param  -
     * @return boolean
     */
    final protected function hasBaseView()
    {
        return !is_null($this->BaseView);
    }
    // End hasBaseView

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the base view
     *
     * @param  -
     * @return View
     */
    final protected function getBaseView()
    {
        return $this->BaseView;
    }
    // End getBaseView

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the action
     *
     * @param  string $action
     * @return -
     */
    protected function setAction($action)
    {
        $this->action = $action;
    }
    // End setAction

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Invokes an action
     *
     * @param  string $action
     * @return -
     */
    protected function invokeActionMethod($action = null)
    {
        $this->_invokeActionMethod(!is_null($action)? $action : $this->action);
    }
    // End invokeAction

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Default action - the default is no action
     *
     * @param  -
     * @return -
     */
    protected function defaultAction()
    {
    }
    // End defaultAction

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Registers a view
     *
     * @param  View $View
     * @return -
     */
    protected function setView(Viewable $View = null)
    {
        return $this->View = $View;
    }
    // End setView

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Registers a view
     *
     * @param  View $View
     * @return -
     */
    protected function addView(Viewable $View)
    {
        if(!$this->hasView())
            return $this->setView($View);

        return $this->additionalViews[get_class($View)] = $View;
    }
    // End addView

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Registers a base view
     *
     * @param  View $BaseView
     * @return -
     */
    final protected function setBaseView(Viewable $BaseView)
    {
        return $this->BaseView = $BaseView;
    }
    // End setView

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Forwards to another controller and sets its views for the current one
     *
     * @param  string $ctrlName
     * @param  string $action
     * @param  string $module
     * @param  array  $initArgs
     * @return -
     */
    protected function forward($ctrlName = null, $action = null, $module = null, array $initArgs = [])
    {
        if(is_null($ctrlName))
            $ctrlName = get_class($this);

        if(!class_exists($ctrlName))
            return;

        $ActionController = new $ctrlName();
        $ActionController->process($initArgs, $action, false);

        if($ActionController->hasBaseView())
            $this->setBaseView($ActionController->getBaseView());

        if($ActionController->hasView())
            $this->setView($ActionController->getView());
    }
    // End defaultAction

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Forwards processing to another application controller. This stops processing
     * of the current controller immediately.
     *
     * @param  string $ctrlName
     * @param  string $action
     * @param  string $module
     * @return -
     */
    protected function forwardReset($ctrlName, $action = null, array $query = null)
    {
        throw new ForwardException('Hard reset', 0, $ctrlName, $action, $query);
    }
    // End forwardReset

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Hard redirect to another controller
     *
     * @param  string $ctrlName
     * @param  string $action
     * @param  string $module
     * @return -
     */
    protected function redirect($ctrlName = null, $action = null, array $arguments = null)
    {
        throw new RedirectException($this->FrontController->getUrlTo($ctrlName, $action, $arguments));
    }
    // End redirect

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the invoked action
     *
     * @param  -
     * @return string
     */
    protected function getAction()
    {
        return is_null($this->action)? FrontController::getInstance()->getAction() : $this->action;
    }
    // End getAction

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the ActionController`s identifier string
     *
     * @param -
     * @returns string
     */
    protected function ident()
    {
        return get_class($this);
    }
    // End ident


    /**
     * @param string $serviceIdent
     * @return mixed
     */
    protected function get($serviceIdent)
    {
        return $this->container->get($serviceIdent);
    }

    //////////////////////////////////////////////////////////////////////////////////////
    // private
    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Constructs the controller and handles an action
     *
     * @param  -
     * @return ActionController
     */
    private function setModule()
    {
        if($parts = explode('\\', get_class($this)))
            $this->module = strtolower($parts[0]);
    }
    // End setModule

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Invokes the action methods
     *
     * @param  string $action
     * @return -
     */
    private function _invokeActionMethod($action = null)
    {
        $this->action = $action;

        if(is_null($action))
            return $this->defaultAction();

        $actionMethod = $action . 'Action';

        if(!method_exists($this, $actionMethod))
            return $this->defaultAction();

        $this->$actionMethod();
    }
    // End invokeActionMethod
}
// End ActionController
