<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs;

use PDO;

/**
 * Abstract database object list interface
 *
 * @package blibs
 * @author Tobias Lode <tobias@beibob.de>
 * @author Fabian Möller <fab@beibob.de>
 * @abstract
 *
 */
class DbObjectSet extends DataObjectSet
{
    /**
     * Sortable order interval
     */
    const ORDER_INTERVAL = 100;



    /**
     * DbObject table name cache
     */
    private static $tablenames = [];

    /**
     * internal unique object id, helps identifying objects
     */
    private $objectId;



    /**
     * Constructs the set
     */
    public function __construct()
    {
        parent::__construct();

        /**
         * Debugging
         */
        $this->objectId = spl_object_hash($this);
    }
    // End __construct



    /**
     * Returns the objectId
     *
     * @param  -
     * @return string
     */
    public function getObjectId()
    {
        return $this->objectId;
    }
    // End getObjectId


    /**
     * Returns a list of simple DataObjects
     *
     * @param  -
     * @param bool $extProperties
     * @return array -
     */
    public function getDataObjectList($keyProperty = null, $extProperties = false)
    {
        $DOList = [];

        foreach($this as $Current)
        {
            if($keyProperty)
                $DOList[$Current->__get($keyProperty)] = $Current->getDataObject($extProperties);
            else
                $DOList[] = $Current->getDataObject($extProperties);
        }

        return $DOList;
    }
    // End getDataObjectlist



    /**
     * Wenn ein Element das Interface Sortable implementiert hat,
     * kann ueber diese Methode die Ordnungszahl eines Elements
     * erhoeht und damit die Sortierreihenfolge geaendert werden.
     *
     * Bei Erfolg wird true zurueck gegeben.
     *
     * Wichtig: Erwartet eine nach der orderProperty aufsteigend sortierte und initialisierte List
     *
     * @param  integer $index           - Index position of element to move up
     * @return boolean
     */
    public function moveElementUp($index)
    {
        /**
         * Works only on classes which implements interface Sortable
         */
        if (version_compare(PHP_VERSION, '5.1.0') === 1)
        {
            // PHP 5.1.0 added the option to pass the class parameter as a string to function class_implements
            if(in_array('Sortable', class_implements($this->getDbObjectName(get_class($this)))) == false)
                return false;
        }
        else
        {
            // PHP < 5.1.0 needs an Instance of 'elementClassname' to check for implemented interfaces
            $elementClassname = $this->getDbObjectName(get_class());
            $Element = new $elementClassname();
            if(in_array('Sortable', class_implements($Element)) == false)
                return false;
        }

        /**
         * Find Element in current list
         */
        if($index < 0 || $index >= count($this) - 1)
            return false;

        $Element = $this[$index];

        /**
         * Set Elements orderProperty to nextposition
         */
        $Element->setOrdinalNumber($this->getNextPosition($index + 1));

        /**
         * Swap both elements if there is an successor
         */
        if(count($this) > $index + 1)
        {
            $this[$index]     = $this[$index + 1];
            $this[$index + 1] = $Element;
        }

        return true;
    }
    // End moveElementUp



    /**
     * Wenn eine Element das Interface Sortable implementiert hat,
     * kann ueber diese Methode die Ordnungszahl eines Elements
     * gemindert und damit die Sortierreihenfolge geaendert werden.
     *
     * Bei Erfolg wird true zurueck gegeben.
     *
     * Wichtig: Erwartet eine nach der orderProperty aufsteigend sortierte und initialisierte List
     *
     * @param  integer $index           - Index position of element to move down
     * @return boolean
     */
    public function moveElementDown($index)
    {
        /**
         * Works only on classes which implements interface Sortable
         */
        if (version_compare(PHP_VERSION, '5.1.0') === 1)
        {
            // PHP 5.1.0 added the option to pass the class parameter as a string to function class_implements
            if(in_array('Sortable', class_implements($this->getDbObjectName(get_class($this)))) == false)
                return false;
        }
        else
        {
            // PHP < 5.1.0 needs an Instance of 'elementClassname' to check for implemented interfaces
            $elementClassname = $this->getDbObjectName(get_class());
            $Element = new $elementClassname();
            if(in_array('Sortable', class_implements($Element)) == false)
                return false;
        }

        /**
         * Find Element in current list
         */
        if($index <= 0 || $index >= count($this))
            return false;

        $Element = $this[$index];

        /**
         * Set Elements orderProperty to nextposition
         */
        $Element->setOrdinalNumber($this->getPreviousPosition($index - 1));

        /**
         * Swap both elements if there is an predecessor
         */
        if($index > 0)
        {
            $this[$index]     = $this[$index - 1];
            $this[$index - 1] = $Element;
        }

        return true;
    }
    // End moveElementDown



    /**
     * Wenn ein Element das Interface Sortable implementiert hat,
     * kann ueber diese Methode die Ordnungszahl eines Elements
     * bestimmt werden die auf die aktuelle Ordnungszahl folgt
     *
     * Wichtig: Erwartet eine nach der orderProperty aufsteigend sortierte und initialisierte List
     *
     * @param  integer $index           - Index position of element from which get next position
     * @return boolean
     */
    public function getNextPosition($index)
    {
        /**
         * In case list is empty
         */
        if(($count = count($this)) == 0)
            return self::ORDER_INTERVAL;

        /**
         * Find Element in current list
         */
        if($index < 0 || $index >= $count)
            return false;

        $Element = $this[$index];

        if($count > $index + 1)
        {
            /**
             * check the space between two positions. if there is no
             * space left anymore ($diff < 2), renumber the list
             * and try again
             */
            $diff = $this[$index + 1]->getOrdinalNumber() - $this[$index]->getOrdinalNumber();
            if($diff < 2)
            {
                $this->renumberOrderedList();
                return $this->getNextPosition($index);
            }

            $nextPosition = $this[$index]->getOrdinalNumber() + (int)floor($diff / 2);
        }
        else
        {
            /**
             * move to end position
             */
            $nextPosition = $this[$count - 1]->getOrdinalNumber() + self::ORDER_INTERVAL;
        }
        return $nextPosition;
    }
    // End getNextPosition



    /**
     * Wenn eine Element das Interface Sortable implementiert hat,
     * kann ueber diese Methode die Ordnungszahl eines Elements
     * ermittelt werden die der aktuellen Ordnungszahl vorraus geht
     *
     * Wichtig: Erwartet eine nach der orderProperty aufsteigend sortierte und initialisierte List
     *
     * @param  integer $index           - Index position of element from which get previous position
     * @return boolean
     */
    public function getPreviousPosition($index)
    {
        /**
         * In case list is empty
         */
        if(($count = count($this)) == 0)
            return self::ORDER_INTERVAL;

        /**
         * Find Element in current list
         */
        if($index < 0 || $index >= count($this))
            return false;

        $Element = $this[$index];

        if($index > 0)
        {
            /**
             * check the space between two positions. if there is no
             * space left anymore ($diff < 2), renumber the list
             * and try again
             */
            $diff = $this[$index]->getOrdinalNumber() - $this[$index - 1]->getOrdinalNumber();

            if($diff < 2)
            {
                $this->renumberOrderedList();
                return $this->getPreviousPosition($index);
            }

            $prevPosition = $this[$index]->getOrdinalNumber() - (int)floor($diff / 2);
            return $prevPosition;
        }
        else
        {
            /**
             * calculate top position
             */
            $prevPosition = (int)floor($this[0]->getOrdinalNumber() / 2);

            if($prevPosition < 2)
            {
                $this->renumberOrderedList();
                return $this->getPreviousPosition($index);
            }

            return $prevPosition;
        }
    }
    // End getPreviousPostion


    // protected


    /**
     * Normalisert die Nummerierung einer sortierbaren Liste.
     *
     * @return void -
     */
    protected function renumberOrderedList()
    {
        if(count($this) == 0)
            return;

        $newOrder = self::ORDER_INTERVAL;

        foreach($this as $DO)
        {
            $DO->setOrdinalNumber($newOrder);
            $newOrder += self::ORDER_INTERVAL;
        }
    }
    // End renumberOrderedList



    /**
     * Sortfunktion fuer Methode sortByKey
     * Erwartet temporaer sortKey und sortAsc als Membervariablen
     *
     */
    protected function _string_sort_cmpmethod($a, $b)
    {
        $key = $this->sortKey;

        $aVal = is_array($a->$key)? current($a->$key) : $a->$key;
        $bVal = is_array($b->$key)? current($b->$key) : $b->$key;

        if($this->sortAsc)
            $cmp = strcmp($aVal, $bVal);
        else
            $cmp = strcmp($bVal, $aVal);

        return $cmp;
    }
    // End _sort_cmpmethod



    /**
     * Sortfunktion fuer Methode sortByKey
     * Erwartet temporaer sortKey und sortAsc als Membervariablen
     *
     */
    protected function _num_sort_cmpmethod($a, $b)
    {
        $key = $this->sortKey;

        if($this->sortAsc)
            return $b->$key - $a->$key;
        else
            return $a->$key - $b->$key;
    }
    // End _numsort_cmpmethod


    /**
     * Lazy find
     *
     * @param        $dbObjectSetName
     * @param bool   $viewName
     * @param  array $initValues - key value array
     * @param  array $orderBy    - array map of columns on to directions array('id' => 'DESC')
     * @param  int   $limit      - limit on resultset
     * @param  int   $offset     - offset on resultset
     * @param  bool  $force      - bypass caching
     * @return static
     */
    protected static function _find($dbObjectSetName, $viewName = false, array $initValues = null, $orderBy = null, $limit = null, $offset = null, $force = false)
    {
        $conditions = self::buildConditions($initValues);
        $orderView  = self::buildOrderView($orderBy, $limit, $offset);

        $sql = sprintf('SELECT * FROM %s'
                       , $viewName? $viewName : self::getTablename(self::getDbObjectName($dbObjectSetName))
                       );

        if($conditions)
            $sql .= ' WHERE '. $conditions;

        if($orderView)
            $sql .= ' '. $orderView;

        return self::_findBySql($dbObjectSetName, $sql, $initValues, $force);
    }
    // End find


    /**
     * Lazy count
     *
     * @param        $dbObjectSetName
     * @param bool   $viewName
     * @param  array $initValues - key value array
     * @param  bool  $force      - bypass caching
     * @return int
     */
    protected static function _count($dbObjectSetName, $viewName = false, array $initValues = null, $force = false)
    {
        $conditions = self::buildConditions($initValues);

        $sql = sprintf('SELECT count(*) AS counter
                          FROM %s'
                       , $viewName? $viewName : self::getTablename(self::getDbObjectName($dbObjectSetName))
                       );

        if($conditions)
            $sql .= ' WHERE '. $conditions;

        return self::_countBySql($dbObjectSetName, $sql, $initValues, 'counter', $force);
    }
    // End _count


    /**
     * Lazy sum
     *
     * @param        $dbObjectSetName
     * @param        $sumColumn
     * @param bool   $viewName
     * @param  array $initValues - key value array
     * @param null   $orderBy
     * @param null   $limit
     * @param null   $offset
     * @param  bool  $force      - bypass caching
     * @return int
     */
    protected static function _sum($dbObjectSetName, $sumColumn , $viewName = false, array $initValues = null, $orderBy = null, $limit = null, $offset = null, $force = false)
    {
        //$conditions = self::buildConditions($initValues);
        $orderView  = self::buildOrderView($orderBy, $limit , $offset);

        $innerSql = sprintf('SELECT %s AS counter
                          FROM %s'
                       , $sumColumn
                       , $viewName ? $viewName : self::getTablename(self::getDbObjectName($dbObjectSetName))
                       );

        if($orderView)
            $innerSql .= ' '. $orderView;

        $sql = sprintf('SELECT sum(x.counter) FROM (%s) as x', $innerSql);

        return self::_sumBySql($dbObjectSetName, $sql, $initValues, 'sum', $force);
    }
    // End _sum


    /**
     * Lazy sum
     *
     * @param        $dataObjectSetName
     * @param        $sql
     * @param  array $initValues - key value array
     * @param string $sumColumnName
     * @param  bool  $force      - bypass caching
     * @return int
     */
    protected static function _sumBySql($dataObjectSetName, $sql, array $initValues = null, $sumColumnName = 'sum', $force = false)
    {
        $signature = DbObjectCache::getSignature($dataObjectSetName, $initValues, $sql);

        if(!$force && DbObjectCache::has($signature))
            return DbObjectCache::get($signature);

        $sum = 0;

        $Stmt = DbObject::prepareStatement($sql);

        $Stmt->execute();
        $Stmt->bindColumn($sumColumnName, $sum, PDO::PARAM_INT);
        $Stmt->fetch(PDO::FETCH_BOUND);

        return DbObjectCache::set($signature, $sum);
    }
    // End find


    /**
     * Inits the set by a sql statement. This may be an statement with placeholders.
     * Put all bind values in an associative array with parameter name in it keys
     *
     * @param          $dbObjectSetName
     * @param  string  $sql
     * @param  array   $initValues
     * @param  boolean $force
     * @throws Exception
     * @return static
     */
    protected static function _findBySql($dbObjectSetName, $sql, array $initValues = null, $force = false)
    {
        /**
         * First check if object already in cache
         */
        $signature = DbObjectCache::getSignature($dbObjectSetName, $initValues, $sql);

        if(!$force && $DbObjectSet = DbObjectCache::getObjectSet($signature)) {
            $DbObjectSet->rewind();
            return $DbObjectSet;
        }

        $dbObjectName = self::getDbObjectName($dbObjectSetName);

        $Stmt = DbObject::prepareStatement($sql, $initValues);

        if(!$Stmt->execute())
            throw new Exception(DbObject::getSqlErrorMessage($dbObjectName, $sql, $initValues));

        /**
         * Get DbObjectSet instance
         */
        $DbObjectSet = new $dbObjectSetName();

        while($DataObject = $Stmt->fetchObject())
        {
            /**
             * Get DbObject instance and cache object
             */
            $DbObject = DbObject::getInstance($dbObjectName, $DataObject);

            if(!$DbObject->isInitialized())
                continue;

            $DbObjectSet->add($DbObject);
            DbObjectCache::setDbObject(DbObjectCache::getSignature($dbObjectName, $DbObject->getPrimaryKey()), $DbObject);
        }

        return DbObjectCache::setObjectSet($signature, $DbObjectSet);
    }
    // End findBySql


    /**
     * Count by sql query - assumes a column named 'counter'. This may be overwritten with argument countColumnName
     *
     * @param        $dbObjectSetName
     * @param        $sql
     * @param  array $initValues - key value array
     * @param string $countColumnName
     * @param  bool  $force      - bypass caching
     * @return int
     */
    protected static function _countBySql($dbObjectSetName, $sql, array $initValues = null, $countColumnName = 'counter', $force = false)
    {
        $signature = DbObjectCache::getSignature($dbObjectSetName, $initValues, $sql);

        if(!$force && DbObjectCache::has($signature))
            return DbObjectCache::get($signature);

        $counter = 0;

        $Stmt = DbObject::prepareStatement($sql, $initValues);

        $Stmt->execute();
        $Stmt->bindColumn($countColumnName, $counter, PDO::PARAM_INT);
        $Stmt->fetch(PDO::FETCH_BOUND);

        return DbObjectCache::set($signature, $counter);
    }
    // End _countBySql



    /**
     * Builds the ordering and limit/offset query string
     *
     * @param  array $orderBy    - array map of columns on to directions array('id' => 'DESC')
     * @param  int   $limit      - limit on resultset
     * @param  int   $offset     - offset on resultset
     * @return string
     */
    public static function buildOrderView(array $orderBy = null, $limit = null, $offset = null)
    {
        $orderView = false;

        if(!is_null($orderBy) && count($orderBy))
        {
            $orders = [];

            foreach($orderBy as $column => $direction)
                $orders[] = $column .' '.$direction;

            $orderView = 'ORDER BY '. join(', ', $orders);
        }

        if(!is_null($limit) && $limit > 0)
            $orderView .= ' LIMIT '. $limit;

        if(!is_null($offset) && $offset > 0)
            $orderView .= ' OFFSET '. $offset;

        return $orderView;
    }
    // End buildOrderView



    /**
     * Returns the dbObjectName
     *
     * @param  string
     * @return string
     */
    protected static function getDbObjectName($dbObjectSetName)
    {
        return utf8_substr($dbObjectSetName, 0, -3);
    }
    // End getDbObjectName



    /**
     * Returns the dbObjectName tablename
     *
     * @param  string
     * @return string
     */
    protected static function getTablename($dbObjectName)
    {
        if(isset(self::$tablenames[$dbObjectName]))
            return self::$tablenames[$dbObjectName];

        return self::$tablenames[$dbObjectName] = call_user_func([$dbObjectName, 'getTablename']);
    }
    // End getDbObjectName


}
// End class DbObjectSet
