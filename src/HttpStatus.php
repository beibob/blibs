<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs;

/**
 * HTTP Status class
 *
 * @package blibs
 * @author Tobias Lode <tobias@beibob.de>
 * @author Fabian Moeller <fab@beibob.de>
 *
 */
class HttpStatus
{
    /**
     * Status code
     */
    private $statusCode;

    /**
     * Instance
     */
    private static $instances = [];

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the instance
     *
     * @param  int $code
     * @return HttpStatus
     */
    public static function getInstance($statusCode)
    {
        if(isset(self::$instances[$statusCode]))
            return self::$instances[$statusCode];

        return self::$instances[$statusCode] = new HttpStatus($statusCode);
    }
    // End getInstance

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the status code
     *
     * @return int
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }
    // End getStatusCode

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the status message by code
     *
     * @return string
     */
    public function getStatusMessage()
    {
        return self::$codes[$this->statusCode][1];
    }
    // End getStatusMessage

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the status message by code
     *
     * @param  int $status
     * @return string
     */
    public function getStatusVersion()
    {
        return self::$codes[$this->statusCode][0];
    }
    // End getStatusVersion

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the http header string
     *
     * @return string
     */
    public function getHttpHeader()
    {
        return sprintf("%s %d %s"
                       , $this->getStatusVersion()
                       , $this->getStatusCode()
                       , $this->getStatusMessage()
                       );
    }
    // End getHttpHeader

    //////////////////////////////////////////////////////////////////////////////////////
    // private
    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Constructs the object
     *
     * @param  int $code
     */
    private function __construct($statusCode)
    {
        $this->statusCode = $statusCode;
    }
    // End __construct

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * All status codes
     */
    private static $codes = [100 => ['HTTP/1.1', 'Continue'],
                                  101 => ['HTTP/1.1', 'Switching Protocols'],
                                  200 => ['HTTP/1.0', 'OK'],
                                  201 => ['HTTP/1.0', 'Created'],
                                  202 => ['HTTP/1.0', 'Accepted'],
                                  203 => ['HTTP/1.0', 'Non-Authoritative Information'],
                                  204 => ['HTTP/1.0', 'No Content'],
                                  205 => ['HTTP/1.0', 'Reset Content'],
                                  206 => ['HTTP/1.0', 'Partial Content'],
                                  300 => ['HTTP/1.0', 'Multiple Choices'],
                                  301 => ['HTTP/1.0', 'Permanently at another address - consider updating link'],
                                  302 => ['HTTP/1.1', 'Found at new location - consider updating link'],
                                  303 => ['HTTP/1.1', 'See Other'],
                                  304 => ['HTTP/1.0', 'Not Modified'],
                                  305 => ['HTTP/1.0', 'Use Proxy'],
                                  306 => ['HTTP/1.0', 'Switch Proxy'], // No longer used, but reserved
                                  307 => ['HTTP/1.0', 'Temporary Redirect'],
                                  400 => ['HTTP/1.0', 'Bad Request'],
                                  401 => ['HTTP/1.0', 'Authorization Required'],
                                  402 => ['HTTP/1.0', 'Payment Required'],
                                  403 => ['HTTP/1.0', 'Forbidden'],
                                  404 => ['HTTP/1.0', 'Not Found'],
                                  405 => ['HTTP/1.0', 'Method Not Allowed'],
                                  406 => ['HTTP/1.0', 'Not Acceptable'],
                                  407 => ['HTTP/1.0', 'Proxy Authentication Required'],
                                  408 => ['HTTP/1.0', 'Request Timeout'],
                                  409 => ['HTTP/1.0', 'Conflict'],
                                  410 => ['HTTP/1.0', 'Gone'],
                                  411 => ['HTTP/1.0', 'Length Required'],
                                  412 => ['HTTP/1.0', 'Precondition Failed'],
                                  413 => ['HTTP/1.0', 'Request Entity Too Large'],
                                  414 => ['HTTP/1.0', 'Request-URI Too Long'],
                                  415 => ['HTTP/1.0', 'Unsupported Media Type'],
                                  416 => ['HTTP/1.0', 'Requested Range Not Satisfiable'],
                                  417 => ['HTTP/1.0', 'Expectation Failed'],
                                  449 => ['HTTP/1.0', 'Retry With'], // Microsoft extension
                                  500 => ['HTTP/1.0', 'Internal Server Error'],
                                  501 => ['HTTP/1.0', 'Not Implemented'],
                                  502 => ['HTTP/1.0', 'Bad Gateway'],
                                  503 => ['HTTP/1.0', 'Service Unavailable'],
                                  504 => ['HTTP/1.0', 'Gateway Timeout'],
                                  505 => ['HTTP/1.0', 'HTTP Version Not Supported'],
                                  509 => ['HTTP/1.0', 'Bandwidth Limit Exceeded'] // not an official HTTP status code
                                  ];

    //////////////////////////////////////////////////////////////////////////////////////
}
// End HttpStatus
