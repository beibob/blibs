<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs;

/**
 * Manages configurations
 *
 * @package blibs
 * @author Tobias Lode <tobias@beibob.de>
 * @author Fabian Möller <fab@beibob.de>
 *
 */
class Config
{
    /**
     * Constructor
     *
     * @param  string $path
     * @param  string $section
     * @return Config
     */
    public function __construct(array $config = [])
    {
        foreach($config as $key => $value)
            $this->$key = $value;
    }
    // End __construct

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the value
     *
     * @param  string $key
     * @return mixed
     */
    public function get($key, $default = null)
    {
        return isset($this->$key)? $this->$key : $default;
    }
    // End get

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Treats the value as an directory path. Directory pathes are always
     * returned with trailing slash.
     *
     * @param  string  $key
     * @param  boolean $removeLeadingSlash
     * @return string
     */
    public function toDir($key, $removeLeadingSlash = false, $default = null)
    {
        if(!is_string($path = $this->$key))
            return $default;

       /**
         * remove leading /
         */
        if($path{0} == '/' && $removeLeadingSlash)
            $path = utf8_substr($path, 1);

        /**
         * add trailing /
         */
        if(utf8_substr($path, -1, 1) != '/')
            $path .= '/';

        return $path;
    }
    // End toDir

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the value as array. If this entry has subentries
     * the returned array will be multi-dimensional
     *
     * @param  string $key
     * @return array
     */
    public function toArray($key)
    {
        return (array)$this->$key;
    }
    // End toArray

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Explodes the value to return it as array
     *
     * @param  string $key
     * @param  string $seperator
     * @return array
     */
    public function parseArray($key, $seperator = null, $trimValues = false)
    {
        $value = isset($this->$key) ? trim($this->$key) : false;
        if (!$value)
            return [];

        if (is_null($seperator))
        {
            if (strpos($value, ',') !== false)
                $seperator = ',';
            elseif (strpos($value, ';') !== false)
                $seperator = ';';
            elseif (strpos($value, ' ') !== false)
                $seperator = ' ';
            else
                return [$value];
        }

        $array = explode($seperator, $value);
        if ($trimValues)
        {
            foreach ($array as $key => $value)
                $array[$key] = trim($value);
            return $array;
        }
        else
            return $array;
    }
    // End parseArray

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the value as boolean.
     *
     * @param  string $key
     * @return bool
     */
    public function toBoolean($key)
    {
        return (bool)$this->$key;
    }
    // End toBoolean

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * If the value is a string separated by comma or space it will be splitted
     * and returned as array.
     * An undefined value will still return an (empty) array
     *
     * @param  string $key
     * @return array
     */
    public function toList($key)
    {
        if(!isset($this->$key) ||
           !is_string($this->$key))
            return [];

        return (array)preg_split('/[,\s]+/u', $this->$key, -1, PREG_SPLIT_NO_EMPTY);
    }
    // End toList

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Magic method __isset
     *
     * @param  string $key
     * @return boolean
     */
    public function __isset($key)
    {
        return isset($this->$key);
    }
    // End __isset

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Magic method __get
     *
     * @param  string $key
     * @return mixed
     */
    public function __get($key)
    {
        return isset($this->$key)? $this->$key : null;
    }
    // End __get

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Magic method __set
     *
     * @param  string $key
     * @param  mixed  $value
     * @return -
     */
    public function __set($key, $value)
    {
        if(is_array($value))
            $this->$key = new Config($value);

        else
            $this->$key = $value;
    }
    // End __set

    //////////////////////////////////////////////////////////////////////////////////////
}
// End class Config
