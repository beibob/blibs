<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs;

use PDO;

/**
 * Group object
 *
 * @package blibs
 * @author     Fabian M�ller  <fab@beibob.de>
 * @author Tobias Lode <tobias@beibob.de>
 *
 */
class Group extends DbObject
{
    /**
     * Tablename
     */
    const TABLE_NAME = 'public.groups';

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * groupId
     */
    private $id;

    /**
     * name
     */
    private $name;

    /**
     * isUserGroup
     */
    private $isUsergroup;

    /**
     * creation time
     */
    private $created;

    /**
     * modification time
     */
    private $modified;

    /**
     * Primary key
     */
    private static $primaryKey = ['id'];

    /**
     * Column types
     */
    private static $columnTypes = ['id'          => PDO::PARAM_INT,
                                        'name'        => PDO::PARAM_STR,
                                        'isUsergroup' => PDO::PARAM_BOOL,
                                        'created'     => PDO::PARAM_STR,
                                        'modified'    => PDO::PARAM_STR];

    /**
     * Extended columns types
     */
    private static $extColumnTypes = [];

    //////////////////////////////////////////////////////////////////////////////////////
    // public
    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Creates the object
     *
     * @param  string   $name       - name
     * @param  boolean  $isUsergroup - isUserGroup
     */
    public static function create($name, $isUsergroup = false)
    {
        $Group = new Group();
        $Group->setName($name);
        $Group->setIsUsergroup($isUsergroup);

        if($Group->getValidator()->isValid())
            $Group->insert();

        return $Group;
    }
    // End create

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Inits a User by id
     *
     * @param  numeric  $id    - groupId
     * @param  boolean  $force - Bypass caching
     * @return Group
     */
    public static function findById($id, $force = false)
    {
        if(!$id)
            return new Group();

        $sql = sprintf("SELECT id
                             , name
                             , is_usergroup
                             , created
                             , modified
                          FROM %s
                         WHERE id = :id"
                       , self::TABLE_NAME
                       );

        return self::findBySql(get_class(), $sql, ['id' => $id], $force);
    }
    // End findById

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Inits a User by name
     *
     * @param  string   $name
     * @param  boolean  $force - Bypass caching
     * @return Group
     */
    public static function findByName($name, $force = false)
    {
        if(!$name)
            return new Group();

        $sql = sprintf("SELECT id
                             , name
                             , is_usergroup
                             , created
                             , modified
                          FROM %s
                         WHERE name = :name"
                       , self::TABLE_NAME
                       , $name
                       );

        return self::findBySql(get_class(), $sql, ['name' => $name], $force);
    }
    // End findByName

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Checks if this group has a certain role
     *
     * @param  string $rootIdent
     * @param  string $roleIdent
     * @return -
     */
    public function hasRole($rootIdent, $roleIdent, $force = false)
    {
        return RoleMember::isGranted(Role::findByIdent($rootIdent, $roleIdent, $force)->getNodeId(), $this->getId());
    }
    // End hasRole

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the property name
     *
     * @param  string   $name  - name
     * @return
     */
    public function setName($name)
    {
        if(!$this->getValidator()->assertNotEmpty('name', $name))
            return;

        if(!$this->getValidator()->assertLength('name', 200, 1, $name))
            return;

        if(!$this->getValidator()->assertUniqueness('name', $name, [get_class(), 'nameExists'], [$name]))
            return;

        $this->name = $name;
    }
    // End setName

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the property isUsergroup
     *
     * @param  boolean  $isUsergroup - isUserGroup
     * @return
     */
    public function setIsUsergroup($isUsergroup = false)
    {
        $this->isUsergroup = $isUsergroup;
    }
    // End setIsUsergroup

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the property id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    // End getId

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the property name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    // End getName

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the property isUsergroup
     *
     * @return boolean
     */
    public function isUsergroup()
    {
        return $this->isUsergroup;
    }
    // End isUsergroup

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the property created
     *
     * @return BlibsDateTime
     */
    public function getCreated($format = false)
    {
        return new BlibsDateTime($this->created);
    }
    // End getCreated

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the property modified
     *
     * @return BlibsDateTime
     */
    public function getModified($format = false)
    {
        return new BlibsDateTime($this->modified);
    }
    // End getModified

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Add a user to this group
     *
     * @param  User $User
     * @return User
     */
    public function addUser(User $User)
    {
        if($this->isInitialized())
            GroupMember::create($User->getId(), $this->getId());

        return $User;
    }
    // End addUser

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Checks, if the object exists
     *
     * @param  numeric  $id    - groupId
     * @param  boolean  $force - Bypass caching
     * @return boolean
     */
    public static function exists($id, $force = false)
    {
        return self::findById($id, $force)->isInitialized();
    }
    // End exists

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Checks, if the object exists by name
     *
     * @param  string   $name
     * @param  boolean  $force - Bypass caching
     * @return boolean
     */
    public static function nameExists($name, $force = false)
    {
        return self::findByName($name, $force)->isInitialized();
    }
    // End exists

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Updates the object in the table
     *
     * @return boolean
     */
    public function update()
    {
        $this->modified = self::getCurrentTime();

        $sql = sprintf("UPDATE %s
                           SET name        = :name
                             , is_usergroup = :isUsergroup
                             , created     = :created
                             , modified    = :modified
                         WHERE id          = :id"
                       , self::TABLE_NAME
                       );

        return $this->updateBySql($sql,
                                  ['id'          => $this->id,
                                        'name'        => $this->name,
                                        'isUsergroup' => $this->isUsergroup,
                                        'created'     => $this->created,
                                        'modified'    => $this->modified]
                                  );
    }
    // End update

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Deletes the object from the table
     *
     * @return boolean
     */
    public function delete()
    {
        $sql = sprintf("DELETE FROM %s
                              WHERE id = :id"
                       , self::TABLE_NAME
                      );

        return $this->deleteBySql($sql, ['id' => $this->id]);
    }
    // End delete

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns an array with the primary key properties and
     * associates its values, if it's a valid object
     *
     * @return array
     */
    public function getPrimaryKey($propertiesOnly = false)
    {
        if($propertiesOnly)
            return self::$primaryKey;

        $primaryKey = [];

        foreach(self::$primaryKey as $key)
            $primaryKey[$key] = $this->$key;

        return $primaryKey;
    }
    // End getPrimaryKey

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the tablename constant. This is used
     * as interface for other objects.
     *
     * @return string
     */
    public static function getTablename()
    {
        return self::TABLE_NAME;
    }
    // End getTablename

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the columns with their types. The columns may also return extended columns
     * if the first argument is set to true. To access the type of a single column, specify
     * the column name in the second argument
     *
     * @param  boolean  $extColumns
     * @param  mixed    $column
     * @return mixed
     */
    public static function getColumnTypes($extColumns = false, $column = false)
    {
        $columnTypes = $extColumns? array_merge(self::$columnTypes, self::$extColumnTypes) : self::$columnTypes;

        if($column)
            return $columnTypes[$column];

        return $columnTypes;
    }
    // End getColumnTypes

    //////////////////////////////////////////////////////////////////////////////////////
    // protected
    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Inserts a new object in the table
     *
     * @return boolean
     */
    protected function insert()
    {
        $this->id       = $this->getNextSequenceValue();
        $this->created  = self::getCurrentTime();
        $this->modified = null;

        $sql = sprintf("INSERT INTO %s (id, name, is_usergroup, created, modified)
                               VALUES  (:id, :name, :isUsergroup, :created, :modified)"
                       , self::TABLE_NAME
                       );

        return $this->insertBySql($sql, ['id'          => $this->id,
                                              'name'        => $this->name,
                                              'isUsergroup' => $this->isUsergroup,
                                              'created'     => $this->created,
                                              'modified'    => $this->modified
                                              ]);
    }
    // End insert

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Inits the object with row values
     *
     * @param  object   $DO - Data object
     */
    protected function initByDataObject(\stdClass $DO = null)
    {
        $this->id          = $DO->id;
        $this->name        = $DO->name;
        $this->isUsergroup = $DO->is_usergroup;
        $this->created     = $DO->created;
        $this->modified    = $DO->modified;
    }
    // End initByDataObject

    //////////////////////////////////////////////////////////////////////////////////////
    // private
    //////////////////////////////////////////////////////////////////////////////////////

}
// End class Group
