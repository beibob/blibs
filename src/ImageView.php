<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs;

/**
 * A simple ImageView
 *
 * @package blibs
 * @author Tobias Lode <tobias@beibob.de>
 * @author Fabian Möller <fab@beibob.de>
 *
 */
class ImageView extends View
{
    /**
     * Media
     */
    private $ImageFile;

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Constructs the view
     *
     * @param  Media $Media
     * @return -
     */
    public function __construct(File $ImageFile = null)
    {
        if(!is_null($ImageFile))
            $this->setImageFile($ImageFile);
    }
    // End __construct

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the media
     *
     * @param  Media $Media
     * @return -
     */
    public function setImageFile(File $ImageFile)
    {
        $this->ImageFile = $ImageFile;
    }
    // End setImageFile

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the content as string
     *
     * @param  -
     * @return string
     */
    public function __toString()
    {
        if(!$this->ImageFile->exists())
            return '';

        return file_get_contents($this->ImageFile->getFilePath());
    }
    // End __toString

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Will be called on initialization. Assigns the given argument array to this view
     *
     * @param  array $args
     * @return -
     */
    public function process(array $args = [], $module = null) {}
}
// End ImageView
