<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs;

/**
 * Global AutoLoader
 *
 * @package blibs
 * @author Thorsten Mürell <thorsten.muerell@tmarts.de>
 */
class AutoLoader
{
    const CONTROLLERS_PATH    = 'Controller';
    const INTERFACES_PATH     = 'Interface';
    const MODELS_PATH         = 'models';
    const VIEWS_PATH          = 'View';
    const VIEW_HELPERS_PATH   = 'View/Helpers';

    /**
     * Cached include paths array
     */
    private static $includePaths = [];

    /**
     * Cached included classes in array
     */
    private static $includedClasses = [];

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Set include pathes for libraries
     *
     * @param  string $libDir
     * @return string
     */
    public static function registerLibDir($libDir)
    {
        return self::$includePaths[basename($libDir)] = $libDir . DIRECTORY_SEPARATOR;
    }
    // End requireLib

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Set include pathes for modules
     *
     * @param  string $moduleDir
     * @return string
     */
    public static function registerModuleDir($moduleDir)
    {
        return self::$includePaths[basename($moduleDir)] = $moduleDir . DIRECTORY_SEPARATOR;
    }
    // End requireModules

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Auto loader callback
     *
     * @param  string $cName
     * @return boolean
     */
    public static function resolve($cName)
    {
        $parts = explode('\\', $cName);
        if(count($parts) < 2)
            throw new AutoLoaderException('Unqualified class name: '. $cName, AutoLoaderException::CLASS_NAME_NOT_QUALIFIED);

        $ident = array_shift($parts);
        $baseDir = self::$includePaths[$ident];
        $classGlobPath = $baseDir . join(DIRECTORY_SEPARATOR, $parts) . '.*php';

        //fb($classGlobPath, 'classGlobPath');

        if($matches = glob($classGlobPath))
        {
            self::$includedClasses[$cName] = $cPath = array_shift($matches);

            require_once($cPath);
            return true;
        }

        // Do not throw AutoLoaderException in case there are other autoloaders active!!!
        if(count(spl_autoload_functions()) > 1)
            return false;

        throw new AutoLoaderException('Class `'.$cName .'\' not found', AutoLoaderException::CLASS_NOT_FOUND);
    }
    // End resolve

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Prüft ob eine Klasse existiert
     *
     * @deprecated
     */
    public static function getClassPath($className)
    {
        if (isset(self::$includedClasses[$className]))
            return self::$includedClasses[$className];

        return null;
    }
    // End classExists

    //////////////////////////////////////////////////////////////////////////////////////
}
// End AutoLoader
