<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs;

use Beibob\Blibs\Interfaces\LogAdapter;

/**
 * @package blibs
 * @author Tobias Lode <tobias@beibob.de>
 * @author     Fabian M�ller <fab@beibob.de>
 *
 */
class LogFile implements LogAdapter
{
    /**
     * Log path and name
     */
    const LOG_DEFAULT_DIR = 'logs/';
    const LOG_DEFAULT_PREFIX = 'app.log.';

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Handle to logfile resource
     */
    protected $logfileHandle;

    /**
     * Path to logfile
     */
    protected $logfilePath;

    /**
     * Log severity
     */
    protected $severity;

    /**
     * Number of entries
     */
    protected $numEntries = 0;

    ///////////////////////////////////////////////////////////////////////////

    /**
     * Constructs the logger object and opens the logfile if logDirectory is specified
     *
     * @param
     * @return Logger
     */
    public function __construct(Config $LogConfig)
    {
        $Config = Environment::getInstance()->getConfig();

        $this->severity = $LogConfig->get('severity', LOG_NONE);
        $this->logfilePath = $Config->toDir('baseDir')
            . $LogConfig->toDir('dir', true, self::LOG_DEFAULT_DIR)
            . $LogConfig->get('prefix', self::LOG_DEFAULT_PREFIX)
            . date('Y-m-d');

        $this->open();
    }
    // End __construct

    ///////////////////////////////////////////////////////////////////////////

    /**
     * Destructor
     *
     * @param  -
     * @return -
     */
    public function __destruct()
    {
        $this->close();
    }
    // End __destruct

    ///////////////////////////////////////////////////////////////////////////

    /**
     * Writes a protocolline into the logfile
     *
     * @param  int    $severity
     * @param  mixed $issue
     * @param  string $label    - method name
     * @return -
     */
    public function log($severity, $issue, $label = null)
    {
        if(!is_resource($this->logfileHandle) ||
           !($this->severity & $severity))
            return;

        /**
         * Format message
         */
        list($usec, $sec) = explode(' ',microtime());

        $severityString = Log::getSeverityString($severity);
        $date = date('Y-m-d H:i:s', $sec);

        /**
         * Show miliseconds when in debug mode
         */
        if($this->severity & LOG_DBG)
            $date .= '.'. str_pad(intval($usec * 1000), 3, '0', STR_PAD_LEFT);

        $logMsg = sprintf('%s - %s [%-7s] ', $date, Environment::getInstance()->getApplicationType() == APP_TYPE_WEB? Environment::getRemoteAddress() : Environment::getInstance()->getName(), $severityString);

        if($label)
            $logMsg .= $label .' - ';

        $logMsg .= print_r($issue, true) . "\n";

        fputs($this->logfileHandle, $logMsg);
        $this->numEntries++;
    }
    // End log

    //////////////////////////////////////////////////////////////////////////////////////
    // protected
    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Opens the logfile
     *
     * @param  -
     * @return -
     */
    protected function open()
    {
        if(!$this->logfilePath)
            throw new Exception('Could not open logfile. No path set!');

        $this->logfileHandle = fopen($this->logfilePath, 'a+');

        if(!is_resource($this->logfileHandle))
            throw new Exception('Could not open logfile at \'' . $this->logfilePath . '\' for writing');
    }
    // End open

    ///////////////////////////////////////////////////////////////////////////

    /**
     * Closes the logger. this is called by the destructor
     *
     * @param  bool $insertCloseMsg - should a close message be written into the logfile?
     * @param  text $specialLastMsg - Something to add?
     * @return -
     */
    protected function close()
    {
        if(!is_resource($this->logfileHandle))
            return;

        if($this->numEntries > 0)
            fputs($this->logfileHandle, "\n");

        fclose($this->logfileHandle);
    }
    // End close

    ///////////////////////////////////////////////////////////////////////////
}
// End class LogFile
