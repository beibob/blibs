<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs;

/**
 * Manages configurations
 *
 * @package blibs
 * @author Tobias Lode <tobias@beibob.de>
 * @author     Fabian Möller <fab@beibob.de>
 *
 */
class ConfigIni extends Config
{
    /**
     * Constructor
     *
     * @param  string|array $path
     * @param  string $section
     * @return Config
     */
    public function __construct($paths = null, $section = null, array $config = [])
    {
        if ($paths && $section) {
            if (!is_array($paths)) {
                $paths = [$paths];
            }

            if ($config) {
                $config = \array_merge(self::readConfigs($paths, $section), $config);
            }
            else {
                $config = self::readConfigs($paths, $section);
            }
        }

        parent::__construct($config);
    }
    // End __construct

    //////////////////////////////////////////////////////////////////////////////////////
    // private
    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Reads and parses a config ini file
     *
     * @param  string $path
     * @return -
     */
    private static function readConfigs(array $configPaths, $readSection)
    {
        $config = null;
        $allSections = self::parseIniFiles($configPaths);

        $resolvedSections = [];

        /**
         * Resolve inherited sections
         */
        foreach(array_keys($allSections) as $expr)
        {
            /**
             * split by colon and merge sections
             */
            if(strpos($expr, ':') === false) {
                $resolvedSections[$expr] = $allSections[$expr];
            } else {
                list($section, $templateSection) = explode(':', $expr);
                $section = trim($section);
                $templateSection = trim($templateSection);
                if (!array_key_exists($templateSection, $resolvedSections)) {
                    throw new Exception("TemplateSection '$templateSection' not found!");
                }

                $resolvedSections[$section] = array_merge($resolvedSections[$templateSection], $allSections[$expr]);
            }
        }

        /**
         * Fallback to default if no config was found
         */
        if(array_key_exists($readSection, $resolvedSections))
            $config = $resolvedSections[$readSection];
        else
            $config = $allSections['default'];

        /**
         * Resolve dot nesting
         */
        foreach($config as $key => $value)
        {
            if(strpos($key, '.') === false)
                continue;

            $groups = explode('.', $key);
            $current =& $config;

            foreach($groups as $group)
                $current =& $current[$group];

            $current = trim($value, " \t\n\r\0\x0B'");
            unset($current);
            unset($config[$key]);
        }

        return $config;
    }

    /**
     * @param $configPath
     * @return array|bool
     */
    private static function parseIniFiles(array $configPaths)
    {
        $allSections = [];
        foreach ($configPaths as $configPath) {
            $allSections = array_merge($allSections, parse_ini_file($configPath, true));
        }

        return $allSections;
    }
    // End readConfig
}
// End class ConfigIni
