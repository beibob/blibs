<?php
/**
 * This file is part of blibs - mvc development framework
 * 
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 * 
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs;

/**
 * An ActionController class that handles ajax calls
 *
 * @package blibs
 * @author Tobias Lode <tobias@beibob.de>
 * @author     Fabian Möller <fab@beibob.de>
 *
 */
class AjaxController extends PageActionController
{
    /**
     * indicates if this is an ajax call
     */
    private $isAjax = false;

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Constructs the controller and handles an action
     *
     * @param  -
     * @return ActionController
     */
    public function __construct()
    {
        parent::__construct();

        $this->isAjax = Environment::isXmlHttpRequest();
    }
    // End __construct

    //////////////////////////////////////////////////////////////////////////////////////
    // protected
    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns true if this controller is called asynchron
     *
     * @returns boolean
     */
    protected function isAjax()
    {
        return $this->isAjax;
    }
    // End isAjax

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Hard redirect to another controller
     *
     * @param  string $ctrlName
     * @param  string $action
     * @param  string $module
     * @return -
     */
    protected function redirect($ctrlName = null, $action = null, array $arguments = null)
    {
        if(!$this->isAjax())
            parent::redirect($ctrlName, $action, $arguments);

        $this->forwardReset($ctrlName, $action, $arguments);
    }
    // End redirect

    //////////////////////////////////////////////////////////////////////////////////////
}
// End AjaxController
