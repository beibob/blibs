<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs;

use Beibob\Blibs\Interfaces\SessionSaveHandler;

/**
 * Manages session data and storage of the data
 *
 * @package blibs
 * @author Tobias Lode <tobias@beibob.de>
 * @author Fabian Möller <fab@beibob.de>
 *
 */
class Session
{
    /**
     * Name
     */
    private $name;

    /**
     * Save Handler
     */
    private $SaveHandler;

    /**
     * Expire interval
     */
    private $lifeTime;

    /**
     * Client session enabled
     */
    private $clientSession = false;

    /**
     * Client session enabled
     */
    private $clientSessionLifeTime;

    /**
     * Session Identifier
     */
    private $sid;

    /**
     * Namespaces
     */
    private $namespaces = [];

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Default expire interval
     */
    const DEFAULT_SESSION_NAME    = 'sid';
    const DEFAULT_LIFE_TIME       = 7200;

    /**
     * Scopes
     */
    const SCOPE_REQUEST    = 0;
    const SCOPE_PERSISTENT = 1;

    ////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Constructs the object
     *
     * @param  string $name
     * @param  mixed  $expires - false means never expire
     * @param  string $saveHandler
     * @param  string $savePath
     * @param  boolean $clientSession
     * @return -
     */
    public function __construct($name = false, $lifeTime = self::DEFAULT_LIFE_TIME, $saveHandlerName = false, $savePath = false, $clientSession = false, $clientSessionLifeTime = 0)
    {
        /**
         * Save configs
         */
        $this->name                  = $name? $name : self::DEFAULT_SESSION_NAME;
        $this->savePath              = $savePath;
        $this->lifeTime              = $lifeTime;
        $this->clientSession         = $clientSession;
        $this->clientSessionLifeTime = $clientSessionLifeTime;

        $this->setSaveHandler($saveHandlerName);

        $this->_init();
    }
    // End __construct

    ////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Inits the session
     *
     * @param  -
     * @return -
     */
    public function start()
    {
        /**
         * Start the session
         */
        session_start();

        /**
         * Fix cookie lifeTime on client side
         */
        if($this->clientSession)
        {
            $cLifeTime = $this->clientSessionLifeTime? time() : 0;
            setcookie(session_name(), session_id(), $cLifeTime, '/');
        }

        /**
         * Create namespaces
         */
        foreach($_SESSION as $namespace => $DataObj)
            if(is_null($DataObj->expires) || time() < $DataObj->expires)
                $this->namespaces[$namespace] = new SessionNamespace($namespace, self::SCOPE_PERSISTENT, $DataObj->expires, $DataObj->data);
        /**
         * Save sessionId
         */
        $this->sid = session_id();
    }
    // End init

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the session name
     *
     * @param  -
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    // End getSessionName

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the session id
     *
     * @param  -
     * @return numeric
     */
    public function getSid()
    {
        return $this->sid;
    }
    // End getSid

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the session life time
     *
     * @param  -
     * @return int
     */
    public function getLifeTime()
    {
        return $this->lifeTime;
    }
    // End getLifeTime

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns true if the named namespace exists
     *
     * @param  string $name
     * @return boolean
     */
    public function hasNamespace($name)
    {
        return isset($this->namespaces[$name]);
    }
    // End hasNamespace

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns a SessionNamespace instance by name
     *
     * @param  string $name
     * @param  int $scope
     * @param  int $expires
     * @return SessionNamespace
     */
    public function getNamespace($name, $scope = false, $lifeTime = null, array $data = [])
    {
        if(!isset($this->namespaces[$name]))
            $this->namespaces[$name] = new SessionNamespace($name, $scope, !is_null($lifeTime)? time() + $lifeTime : null, $data);

        else
        {
            if($scope)
                $this->namespaces[$name]->setScope($scope);

            if($lifeTime)
                $this->namespaces[$name]->setExpireTime(time() + $lifeTime);
        }

        return $this->namespaces[$name];
    }
    // End getNamespace

    public function addNamespace(SessionNamespace $Namespace)
    {
        if (!$this->hasNamespace($Namespace->getName()))
            return $this->namespaces[$Namespace->getName()] = $Namespace;

        $Existing = $this->getNamespace($Namespace->getName());

        foreach ($Namespace->getData() as $key => $value)
            $Existing->$key = $value;
    }

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Saves the data
     *
     * @param  -
     * @return -
     */
    public function save()
    {
        $data = [];
        if (is_array($this->namespaces))
        {
            foreach($this->namespaces as $name => $Namespace)
            {
                switch($Namespace->getScope())
                {
                    case self::SCOPE_PERSISTENT:
                        /**
                        * Skip if namespace is expired
                        */
                        if($Namespace->isExpired() ||
                           $Namespace->isEmpty())
                        {
                            unset($_SESSION[$name]);
                            break;
                        }

                        $_SESSION[$name] = new \stdClass();
                        $_SESSION[$name]->expires = $Namespace->getExpireTime();
                        $_SESSION[$name]->data    = $Namespace->getData();
                        break;

                    case self::SCOPE_REQUEST:
                    default:
                        unset($this->namespaces[$name]);
                        break;
                }
            }
        }
        session_write_close();
    }
    // End save

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Kills the session
     *
     * @param  -
     * @return -
     */
    public function destroy()
    {
        if($this->clientSession && isset($_COOKIE[$this->name]))
            setcookie($this->name, '', -1, '/');

        foreach($this->namespaces as $Namespace)
            $Namespace->freeData();

        /**
         * Clear sid and data
         */
        $this->sid = null;
        $this->namespaces = null;
        $_SESSION = [];

        /**
         * Call destroy
         */
        session_destroy();
    }
    // End killSession

    ////////////////////////////////////////////////////////////////////////////////////////
    // private
    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Inits the session
     *
     * @param  -
     * @return -
     */
    private function _init()
    {
        ini_set("session.gc_maxlifetime", $this->lifeTime);

        session_name($this->name);

        if(!empty($this->savePath))
            session_save_path($this->savePath);

        ini_set("session.use_cookies", (int)$this->clientSession);

        if($this->clientSession)
            session_set_cookie_params($this->clientSessionLifeTime);

        /**
         * Set save handler if available
         */
        if($this->SaveHandler instanceOf SessionSaveHandler)
            session_set_save_handler([$this->SaveHandler, 'open'],
                                     [$this->SaveHandler, 'close'],
                                     [$this->SaveHandler, 'read'],
                                     [$this->SaveHandler, 'write'],
                                     [$this->SaveHandler, 'destroy'],
                                     [$this->SaveHandler, 'gc']);
    }
    // End init

    //////////////////////////////////////////////////////////////////////////////////////
    // private
    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the SaveHandler
     *
     * @param  string $saveHandler
     * @return -
     */
    private function setSaveHandler($saveHandlerName)
    {
        if(!$saveHandlerName)
            return;

        $this->SaveHandler = new $saveHandlerName($this);
    }
    // End setSaveHandler
}
// End class Session
