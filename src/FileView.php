<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs;

/**
 * A file view
 *
 * @package blibs
 * @author Tobias Lode <tobias@beibob.de>
 * @author Fabian Möller <fab@beibob.de>
 *
 */
class FileView extends View
{
    /**
     * filePath
     */
    private $filePath;

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Constructs the view
     *
     * @param  File $File
     * @return -
     */
    public function __construct($filePath = null)
    {
        if(!is_null($filePath))
            $this->setFilePath($filePath);
    }
    // End __construct

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the file path
     *
     * @param  string $filePath
     * @return -
     */
    public function setFilePath($filePath)
    {
        $this->filePath = $filePath;
    }
    // End setFilePath

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the content as string
     *
     * @param  -
     * @return string
     */
    public function __toString()
    {
        if(empty($this->filePath))
            return '';

        return file_get_contents($this->filePath);
    }
    // End __toString

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Processes the view
     *
     * @param  -
     * @return -
     */
    public function process(array $initArgs = [], $module = null) {}

    //////////////////////////////////////////////////////////////////////////////////////
}
// End Viewable
