<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs;

/**
 * @package blibs
 * @author Fabian Möller  <fab@beibob.de>
 * @author Tobias Lode <tobias@beibob.de>
 *
 */
class UserStore
{
    /**
     * SessionNamespace names
     */
    const AUTH_NAMESPACE = 'blibs.userStore';

    /**
     * Admin group
     */
    const ADMIN_GROUP_NAME = 'admins';

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Auth namespace name
     */
    protected $authNamespaceName = self::AUTH_NAMESPACE;

    /**
     * Admin group name
     */
    protected $adminGroupName = self::ADMIN_GROUP_NAME;

    /**
     * Singleton instance
     */
    private static $Instance;

    /**
     * Current logged in user
     */
    private $User;

    /**
     * AuthNamespace
     */
    private $AuthNamespace;

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the singelton
     *
     * @param  int $lifeTime
     * @return UserStore
     */
    public static function getInstance($lifeTime = null)
    {
        if(!self::$Instance)
            self::$Instance = new UserStore($lifeTime);

        return self::$Instance;
    }
    // End getInstance

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the current logged in user
     *
     * @param  -
     * @return User
     */
    public function getUser()
    {
        return $this->User;
    }
    // End getUser

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the current logged in userId
     *
     * @param  -
     * @return int
     */
    public function getUserId()
    {
        return $this->User->getId();
    }
    // End getUserId

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns true if a logged in user exists
     *
     * @param  -
     * @return bool
     */
    public function hasUser()
    {
        return (is_object($this->getUser()) && $this->getUser()->isInitialized());
    }
    // End hasUser

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Compares the given user with the logged in one
     *
     * @param  User $User
     * @return bool
     */
    public function isUser(User $User)
    {
        if (!$User->isInitialized() || !$this->hasUser())
            return false;

        return ($this->getUser()->getId() == $User->getId());
    }
    // End isUser

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the current logged in user
     *
     * @param  User $User
     * @return User
     */
    public function setUser(User $User)
    {
        $this->AuthNamespace->userId = $User->getId();
        $this->User = $User;

        return $User;
    }
    // End setUser

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Removes the current user from the store
     *
     * @param  -
     * @return -
     */
    public function unsetUser()
    {
        unset($this->AuthNamespace->userId);
        $this->User = null;
    }
    // End unsetUser

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Checks if the current user has admin privileges.
     *
     * @return boolean
     */
    public function hasAdminPrivileges()
    {
        if(!$this->hasUser())
            return false;

        $User = $this->getUser();

        /**
         * check if in admin group
         */
        $AdminGroup = Group::findByName($this->adminGroupName);
        return GroupMember::exists($AdminGroup->getId(), $User->getId());
    }
    // End hasAdminPrivileges

    //////////////////////////////////////////////////////////////////////////////////////
    // protected
    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Constructor
     */
    protected function __construct($lifeTime = null)
    {
        $Session = Environment::getInstance()->getSession();
        $this->AuthNamespace = $Session->getNamespace($this->authNamespaceName, true, $lifeTime);

        $this->setUser(User::findExtendedById($this->AuthNamespace->userId));
    }
    // End __construct

    //////////////////////////////////////////////////////////////////////////////////////
}
// End UserStore
