/**
 * Blibs js framework / inspired by balupton's ajaxy http://balupton.github.com/jquery-ajaxy/demo/
 */
(function(window, $, undefined) {
    
    var jBlibs = (window.jBlibs = window.jBlibs || {});

    jBlibs.App = {

        /**
         * Root url
         */
        rootUrl: null,        

        /**
         * config options
         */
        options: {
            debug: false,
            controllers: {} 
        },
    
        /**
         *  Global initialization. Called once per request
         */
        init: function(opts) {
            var me = this;

            /**
             * Merge-in options
             */
            $.extend(true, this.options, opts || {});

            /**
             * Setup history
             */
            this._setupHistory();

            /**
             * Helper for internal links
             * Internal links should not start with a protocol
             */ 
            $.extend($.expr[':'], {
                internal: function(obj) {
                    return !/^.+?:\/\//.test($(obj).attr('href'));
                }
            });
            
            /**
             * Init rootUrl
             */
            this.rootUrl = window.location.protocol + '//' + window.location.hostname;
            
            /**
             * Call global initialize
             */
            if($.isFunction(this.options.initialize))
                this.options.initialize();
                
            /**
             * Initialize all controllers
             */ 
            $.each(this.options.controllers, function(name) {
                this._urlMatch = false;

                // check if this controller matches the current url
                if(!me._urlMatches(this))
                    return;
                    
                this._urlMatch = true;

                if(!$.isFunction(this.initialize))
                    return;
                    
                if(me.options.debug)
                    window.console.log('Initialize '+name);
                
                this.initialize();
            });

            /**
             * Prepare the document
             */
            this._prepare($(document));
        
            /**
             * Chain
             */ 
            return this;
        },
    
        /**
         * Prepare the view
         */
        _prepare: function($view, view) {
            var me = this;

            /**
             * Let all matching controllers prepare the current context
             */
            $.each(this.options.controllers, function(ctrlName, controller) { 
                // check if this controller matches the current url
                //if(!me._urlMatches(controller))
                if(!controller._urlMatch)
                    return;

                /**
                 * If a selector is specified, bind all controls within that context
                 */ 
                var $context = this.selector? $(this.selector) : $view;

                /**
                 * Register all controls within the given context
                 */
                if(this.controls) {
                    // iterate over selectors within controls
                    $.each(this.controls, function(selector) {
                        // iterate over events
                        $.each(this, function(eventName, target) {
                            // bind selector to specified event
                            $(selector, $context).on(eventName, function(e) {
                                // determine target url
                                var url;
                                switch(typeof target) {
                                    case 'function': // target is returned by function
                					    url = target.call(this, e);
                                        break;
                                    case 'string':  // target is defined by attribute
                                        url = $(this).attr(target);
                                        break;
                                }

                                // decide wether or not to push the new state
                                if($(this).hasClass('no-history')) {
                                    jBlibs.App.query(url);
                                }
                                else {
                                    History.pushState(null, null, url);
                                }
                                e.preventDefault();
                            });
                        });
                    });
                }
                
                // prepare registered views
                $.each(controller.views, function(selector, fn) {
                    var $viewContext;

                    // check if view is within current context
                    if($context.is(selector))
                        $viewContext = $context;
                    else
                        $viewContext = $(selector, $context);

                    // no view? nothing to do here
                    if(!$viewContext.length || $viewContext.is(':empty'))
                        return;

                    // some debug output
                    if(me.options.debug) {
                        window.console.log('Prepare view '+ selector + ' within context', $viewContext);
                    }

                    // call view function
                    switch(typeof fn) {
                        case 'function':
                            fn.call(controller, $viewContext);
                            break;

                        case 'string':
                            if($.isFunction(controller[fn])) {
                                controller[fn].call(controller, $viewContext);
                            }
                            break;
                    }
                });
            });

            /**
             * Chain
             */ 
            return this;
        },
        // End _prepare
    
        query: function(href, data, opts) {
            var me = this;
            if(!opts)
                opts = {};
            
            /**
             * if href is given, catch some html.
             * if there is also a response, it will be added, too
             */
            return $.ajax({
                url: href,
                data: data,
                type: opts.httpMethod || 'GET',
                beforeSend: function(xhr) {
                    me._startLoading();
                },
                success: function(response, status, xhr) {
                    me._replaceView(response);
                    me._stopLoading();
                },
                error: function(xhr, textStatus) { 
                    me._stopLoading();
                }
            });
        },
        // End query

        /**
         * Checks if the ctrl matches the configured urls
         */
        _urlMatches: function(ctrl) {
            // no specified urls matchs always
            if(!ctrl.urls)
                return true;
            
            var url = History.getState().url.replace(this.rootUrl, ''),
                matches = ctrl.urls,
                isAMatch = false;
                
			switch(typeof matches) {
				// Objects
				case 'object':
					if (matches.test || false && matches.exec || false ) {
						// Regular Expression
						isAMatch = matches.test(url);
						break;
					}
				case 'array':
				    var me = this;
					$.each(matches, function(i, match){
						isAMatch = match === url;
						if(isAMatch)
						    return false;
					});
					break;

				// Exact
				case 'number':
				case 'string':
					isAMatch = (String(matches) === url);
					break;
			}

			return isAMatch;
        },

        
        /**
         * Expects an response object with multiple views
         * Each view must have an outer container with an id within the current document.
         * This container will be replaced by the reponse view content
         */
        _replaceView: function(response) {
            if(!response || typeof response !== 'object')
                return;
            
            for(var name in response)
            {
                switch(name)
                {
                    case 'redirect':
                        window.location.replace(response[name]);
                        break;

                    case 'run':
                        eval(response[name]);
                        break;

                    case 'title':
                        break;

                    default:
                        var view = $($.trim(response[name]));

                        if(!view.attr('id'))
                            continue;

                        var wrapperId = view.attr('id');
                        $("#"+wrapperId).replaceWith(view);
                        this._prepare(view, name);
                        break;
                }
            }
            
            /**
             * Chain
             */ 
            return this;
        },
    
        // Bind to StateChange Event
        _setupHistory: function() {
            var me = this;
            
            History.Adapter.bind(window, 'statechange',function() { 
                me.query(History.getState().url);
            });
        },
        // End _setupHistory
        
        _loadingTimer: null,
        
        _startLoading: function() {
            this._loadingTimer = window.setTimeout(function() {
                $('body').addClass('loading');
            }, 500);
        },
        // End startLoading

        _stopLoading: function() {
           // $('body').removeClass('loading');
            window.clearTimeout(this._loadingTimer);
        }

    // End stopLoading
    };
        
}(window, jQuery));
