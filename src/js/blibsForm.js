/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * This enables assigning object methods to event handlers
 *
 * Usage (within object):
 *
 *       element.onclick = this.myMethod.bindEventListener(this, arg1, arg2,...);
 *
 */
function argsToArray(pseudoArray) {
    var result = new Array();
    for (var i = 0; i < pseudoArray.length; i++)
        result.push(pseudoArray[i]);
    return result;
};

Function.prototype.bind = function (object) {
    var method = this;
    var oldArguments = argsToArray(arguments).slice(1);
    return function () {
        var newArguments = argsToArray(arguments);
        return method.apply(object, oldArguments.concat(newArguments));
    };
};

Function.prototype.bindEventListener = function (object) {
    var method = this;
    var oldArguments = argsToArray(arguments).slice(1);
    return function (event) {
        var newArguments = argsToArray(arguments);
        return method.apply(object, newArguments.concat(oldArguments));
    };
};

////////////////////////////////////////////////////

var blibs;
if(!blibs) blibs = {};

blibs.EventHelper = function()
{
    this.initEventHelper = function()
    {
        var clickableElements = new Array('A');
        for (var tagI in clickableElements)
        {
            tag = clickableElements[tagI];
            RegisterAtHandlerList = document.getElementsByTagName(tag);

            for (var i in RegisterAtHandlerList)
            {
                RegisterAtHandlerList[i].onclick = this.onClickhandler.bindEventListener(this) + RegisterAtHandlerList[i].onclick;
            }
        }
    }

    this.onClickhandler = function(e)
    {
        if (!e) e = window.event;
        if (e.stopPropagation) e.stopPropagation()
        else e.cancelBubble = true;
    }

    this.initEventHelper();
};




blibs.FormHelper = function()
{
   this.setActionAndSubmit = function(e, actionUrl, formname)
   {
      var form = this.getForm(e, formname);

      form.action = actionUrl;
      form.submit();

      return false;
   }

   this.setActionAndSubmitValue = function(e, actionUrl, hiddenFields, formname)
   {
      var form = this.getForm(e, formname);

      for(var name in hiddenFields)
      {
         input = document.createElement('input');
         input.type = 'hidden';
         input.name = name;
         input.value = hiddenFields[name];
         form.appendChild(input);
      }

      form.action = (actionUrl) ? actionUrl : form.action;
      form.submit();

      return false;
   }

    this.copySelectedItemsToBox = function (sourceName, destName, formname)
    {
        Sourcebox = eval("document." + formname + "." + sourceName);
        Destbox = eval("document." + formname + "." + destName);
        for(var i=0; i < Sourcebox.length; i++)
        {
            if(Sourcebox.options[i].selected == true)
            {
                if (!this.item_exists_in_box(formname, destName, Sourcebox.options[i]))
                {
                    NeuerEintrag = new Option(Sourcebox.options[i].text, Sourcebox.options[i].value, false,false);
                    Destbox.options[Destbox.length] = NeuerEintrag;
                }
            }
        }
    }
    // End copySelectedItemsToBox

/////////////////////////////////////////////////////////////////////////

    this.item_exists_in_box = function (formname, boxname, OptionElt)
    {
        Box = eval("document." + formname + "." + boxname);
        retval = false;
        for(var k=0; k < Box.length; k++)
        {
            if (Box.options[k].value == OptionElt.value)
            {
                retval = true;
            }
        }
        return retval;
    }
    // End item_exists_in_box

/////////////////////////////////////////////////////////////////////////

    this.removeSelectedItemsFromBox = function (destName, formname)
    {
        Destbox = eval("document." + formname + "." + destName);

        zaehler = Destbox.length;
        for(var i=0; i < zaehler; i++)
        {
            if(Destbox.options[i].selected)
            {
                Destbox.options[i] = null;
                i = 0;
                zaehler = Destbox.length;
            }
        }
    }

/////////////////////////////////////////////////////////////////////////

    this.copyMultiselectItems = function (formname, box)
    {
        box = eval("document." + formname + "." + box);
        if (box)
        {
            for(var i=0; i < box.length; i++)
            {
                box.options[i].selected = true;
            }
            box.name += "[]";
        }
        return false;
    }

/////////////////////////////////////////////////////////////////////////

   this.getForm = function(e, formname)
   {
      var form;

      if(formname)
         form = eval("document.forms." + formname);

      // Wenn kein formname übergeben, finde die nächste Form dessen Kind ich bin
      if(!form)
         form = this.getFormByEvent(e);

      // ...sonst, liefere die erste Form im Document
      if(!form)
         form = this.getFirstFormInDocument();

      if(!form)
      {
         // Im Dokument existiert kein Formular. Jetzt bauen wir eins.
         form = document.getElementsByTagName("body").item(0).appendChild(document.createElement("form"));
         form.setAttribute("method", "POST");
         form.setAttribute("name", "blibsDefaultForm");
      }

      return form;
   }

   this.getFormByEvent = function(e)
   {
      var event = e || window.event;
      var target = event.target || event.srcElement;

      return target.form? target.form : this.getParentFormByElement(target);
   }

   this.getParentFormByElement = function(elt)
   {
      if (elt.tagName.toLowerCase() != 'form')
         return elt.tagName.toLowerCase() != 'body'? this.getParentFormByElement(elt.parentNode) : false;

      return elt;
   }

   this.getFirstFormInDocument = function()
   {
      return document.forms[0];
   }

};
var blibsForm = new blibs.FormHelper();

