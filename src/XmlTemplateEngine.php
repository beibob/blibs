<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs;

use DOMElement;
use DOMNode;

/**
 * Template Engine parsing XML based templates
 *
 * @package blibs
 * @author Tobias Lode <tobias@beibob.de>
 * @author Fabian Möller <fab@beibob.de>
 *
 */
class XmlTemplateEngine extends TemplateEngine
{
    /**
     * templateData cache index
     */
    protected $dataCacheIndex = 0;

    /**
     * templateData cache
     */
    protected $dataCache = [];

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Replace embedded templates
     *
     * @param  View $View
     * @return -
     */
    public function process()
    {
        $this->processEmbeddedContent();
    }
    // End process

    //////////////////////////////////////////////////////////////////////////////////////
    // protected
    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Processes embedded content
     *
     * @param  -
     * @return -
     */
    protected function processEmbeddedContent(DOMNode $ContextNode = null)
    {
        /**
         * Replace assigned template variables
         */

        $xPathConditions = ['//text()[contains(., "$$")]', '//@*[contains(., "$$")]'];

        $Nodelist = $this->View->query(join('|', $xPathConditions), $ContextNode);

        foreach($Nodelist as $Item)
        {
            switch($Item->nodeType)
            {
                case XML_TEXT_NODE:
                case XML_CDATA_SECTION_NODE:
                    $Item->replaceData(0, $Item->length, $this->substituteTemplateData($Item->data));
                    break;

                case XML_ATTRIBUTE_NODE:
                    $Item->ownerElement->setAttribute($Item->name,
                                                      $this->substituteTemplateData($Item->ownerElement->getAttribute($Item->name))
                                                      );
                    break;
            }
        }

        /**
         * Replace embedded content objects
         */
        $Nodelist = $this->View->query('//include[@name]', $ContextNode);

        foreach($Nodelist as $Item)
        {
            if($Item->nodeType != XML_ELEMENT_NODE)
                continue;

            if($Embedder = $this->resolveEmbeddedContent($Item))
                $Item->parentNode->replaceChild($Embedder, $Item);

            else
                $Item->parentNode->removeChild($Item);
        }
    }
    // End processEmbeddedContent

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Embedds a view within a view
     *
     * @param  DOMElement
     * @return -
     */
    protected function resolveEmbeddedContent(DOMElement $Element)
    {
        $includeName = $Element->getAttribute('name');
        $moduleName  = $Element->hasAttribute('module')? $Element->getAttribute('module') : $this->View->getModule();

        /**
         * Assign arguments
         */
        $args = [];
        foreach($Element->attributes as $DOMAttr)
        {
            /**
             * Lookup cached values
             */
            if(preg_match('/^\$\$\{(\d+)\}\$\$$/u', $DOMAttr->value, $matches) &&
               isset($this->dataCache[$matches[1]]))
                $args[$DOMAttr->name] = $this->dataCache[$matches[1]];

            else
                $args[$DOMAttr->name] = $DOMAttr->value;
        }

        return self::getContentNode($moduleName, $includeName, $args, $this->View, isset($args['action'])? $args['action'] : null);
    }
    // End resolveEmbeddedContent

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Liefert eine ContentNode zum übergebenen Inhaltsnamen
     * @param -
     */
    public static function getContentNode($moduleName, $name, $args, XmlDocument $Document, $action = null)
    {
        /**
         * Check if name is a full qualified class, otherwise assume a template name
         */
        if(class_exists($name))
        {
            $Embedder = new $name();

            if($Embedder instanceOf ActionController)
            {
                $EmbeddedNode = null;

                try
                {
                    $EmbedView = $Embedder->process($action ? $action : FrontController::getInstance()->getAction(), $args);
                }
                catch(Exception $Exception)
                {
                    /**
                     * this is the case when neither a view nor base view
                     * is set
                     */
                    return null;
                }

                if($EmbedView)
                    $EmbeddedNode = $Document->importNode($EmbedView? $EmbedView->getContentNode() : $Document->createTextnode(''), true);
            }
            else
            {
                $Embedder->process($args, $moduleName);
                $EmbeddedNode = $Document->importNode($Embedder->getContentNode(), true);
            }
        }
        else
        {
            $EmbedView = new XmlDocument($name, $moduleName);
            $EmbedView->process($args, $moduleName);
            $EmbeddedNode = $Document->importNode($EmbedView->getContentNode(), true);
        }
        return $EmbeddedNode;
    }
    // End getContentNode

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Substitute callback method
     *
     * @param  array $matches
     * @return string
     * @todo encoding muss aus View kommen
     */
    protected function substituteCallback(array $matches)
    {
        $substitution = $this->resolveSubstitution($matches[1]);

        if(!is_array($substitution) && !is_object($substitution))
            return $substitution;

        $this->dataCache[$this->dataCacheIndex] =& $substitution;
        return '$${'.$this->dataCacheIndex++.'}$$';
    }
    // End substituteCallback

    //////////////////////////////////////////////////////////////////////////////////////
}
// End XmlTemplateEngine
