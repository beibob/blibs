<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs;

use Beibob\Blibs\Interfaces\Viewable;

/**
 * Template Engine parsing text files
 *
 * @package blibs
 * @author Tobias Lode <tobias@beibob.de>
 * @author Fabian Möller <fab@beibob.de>
 *
 */
class TemplateEngine
{
    /**
     * Template suffix
     */
    const TPL_SUFFIX = '.tpl';

    /**
     * Template paths
     */
    const TEMPLATE_PATH = 'templates';

    /**
     * View
     */
    protected $View;

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Contructor
     *
     * @param  View $View
     * @return TemplateEngine
     */
    public function __construct(Viewable $View)
    {
        $this->View = $View;

        if (method_exists(Environment::getInstance(),'getContainer') && Environment::getInstance()->getContainer()->has('templateEngine.substitutions'))
        {
            foreach (Environment::getInstance()->getContainer()->get('templateEngine.substitutions') as $substitution) {
                $substitution->setView($View);
            }
        }
    }
    // End __construct

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Replace embedded templates
     *
     * @param  View $View
     * @return -
     */
    public function process()
    {
        return $this->substituteTemplateData($this->View);
    }
    // End process

    //////////////////////////////////////////////////////////////////////////////////////
    // protected
    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Substitutes template data
     *
     * @param  string $content
     * @return string
     */
    protected function substituteTemplateData($content)
    {
        return preg_replace_callback('/\$\$(.+?)\$\$/u', [$this, 'substituteCallback'], $content);
    }
    // End substituteTemplateData

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Substitute callback method
     *
     * @param  array $matches
     * @return string
     * @todo encoding muss aus View kommen
     */
    protected function substituteCallback(array $matches)
    {
        return $this->resolveSubstitution($matches[1]);
    }
    // End substituteCallback

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Resolve substitution
     *
     * @param  string $expresion
     * @return string
     */
    protected function resolveSubstitution($expression, $explode = ":")
    {
        $parameters = $this->psplit($explode, $expression);

        $current    = $this->View->get(array_shift($parameters));

        foreach($parameters as $param)
        {
            if(is_object($current)) {
                // Two actions can be performed on objects.
                // Function call or get
                if (preg_match('/^  ([^\(\)]+)  \((.+)?\)$/ux', $param, $matches)) {
                    $function = $matches[1];
                    if (isset($matches[2]))
                        $params = array_map([$this, 'resolveParameter'], $this->psplit(',', $matches[2]));
                    else
                        $params = [];

                    $current = call_user_func_array([$current, $function], $params);
                } else {
                    // Simple get
                    $current = $current->$param;
                }
            }

            elseif(is_array($current))
                $current =& $current[$param];

            elseif(is_string($current))
                break;
        }

        /**
         * Skip convertion if no string
         */
        if(is_object($current) || is_array($current))
            return $current;

        return $current;
    }
    // End resolveSubstitution

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Resolve parameter
     *
     * @param  string $parameter
     * @return string
     */
    protected function resolveParameter($expression)
    {
        // Three cases for resolving call parameters
        // 1. String literal '...'
        if (preg_match("/^\s*'(.+)'\s*$/u", $expression, $matches))
        {
            return $matches[1];
        }
        // 2. Number
        elseif (preg_match("/^\s*(\d+)\s*$/u", $expression, $matches))
        {
            return $matches[1];
        }
        // 3. Expression of the form object.value or object.object.value ...
        elseif (preg_match("/^\s*(.+)\s*$/u", $expression, $matches))
        {
            return $this->resolveSubstitution($matches[1], "\\.");
        }
    }
    // End substituteCallback

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Preserves string literals after character split
     *
     * @param  string $split_character - the character to split at
     * @param  string $text - the text to split
     * @return array
     */
    protected function psplit($split_character, $text)
    {
        $parameters = mb_split($split_character, $text);

        $ret = [];
        $i = 0;
        foreach ($parameters as $p) {
            if (!isset($ret[$i]) || !$ret[$i]) {
                $ret[$i] = $p;
            } else {
                if (substr_count($ret[$i], "'") % 2 == 1) {
                    $ret[$i] .= ($split_character . $p);
                } else {
                    $ret[++$i] = $p;
                }
            }
        }
        return $ret;
    }

    // End substituteCallback

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the path to a template
     *
     * @param   string  $tplName
     * @param   module  $module
     * @returns string
     */
    public static function getTemplatePath($tplName, $module = false, $checkIfExists = false)
    {
        $FrontController = FrontController::getInstance();

        $Config  = $FrontController->getConfig();

        if(!$module)
            $module = $FrontController->getModule();

        //if(!isset($Config->$module))
        //throw new Exception('Default module "'.$module.'" not found. Check your module configuration.');

        $baseDir = $Config->appDir . DIRECTORY_SEPARATOR;

        $tplPath = $baseDir . ucfirst($module) . DIRECTORY_SEPARATOR . AutoLoader::VIEWS_PATH . DIRECTORY_SEPARATOR . self::TEMPLATE_PATH . DIRECTORY_SEPARATOR . $tplName . self::TPL_SUFFIX;

        if($checkIfExists)
        {
            if(!file_exists($tplPath))
            {
                if(($defaultModule = $FrontController->getDefaultModule()) != $module)
                {
                    $tplPath = $baseDir . $defaultModule . $Config->$defaultModule->templates . DIRECTORY_SEPARATOR
                        . $tplName . self::TPL_SUFFIX;

                    if(!file_exists($tplPath))
                        return false;
                }
            }
        }

        return $tplPath;
    }
    // End getTemplatePath

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Loads the content of a template and returns it
     *
     * @param   string  $tplName
     * @returns string
     */
    public static function loadTemplate($tplName, $module = false)
    {
        if(!$tplPath = self::getTemplatePath($tplName, $module, true))
        {
            $errorInfo = error_get_last();
            throw new Exception('Error loading template '.$module.'::'.$tplName.': '. $errorInfo['message'], $errorInfo['type']);
        }

        if(!$fileArr = @file($tplPath))
        {
            $errorInfo = error_get_last();
            throw new Exception('Error loading template '.$module.'::'.$tplName.': '. $errorInfo['message'], $errorInfo['type']);
        }


        $content = join('', $fileArr);

        if (method_exists(Environment::getInstance(),'getContainer') && Environment::getInstance()->getContainer()->has('templateEngine.substitutions')) {
            foreach (Environment::getInstance()->getContainer()->get('templateEngine.substitutions') as $substitution) {
                $content = $substitution->substituteTemplateData($content);
            }
        }

        return $content;
    }
    // End getTemplateByName

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Liefert Content zum übergebenen Inhaltsnamen
     * @param -
     */
    public static function getContent($moduleName, $name, $args, $action = null)
    {
        if(class_exists($name))
        {
            $Embedder = new $name();

            if($Embedder instanceOf ActionController)
            {
                try
                {
                    $EmbedView = $Embedder->process($args, $action ? $action : FrontController::getInstance()->getAction());
                }
                catch(Exception $Exception)
                {
                    /**
                     * this is the case when neither a view nor base view
                     * is set
                     */
                    return null;
                }

                $Content = (string) $EmbedView;
            }
            else
            {
                $Embedder->process($args, $moduleName);
                $Content = (string) $Embedder;
            }
        }
        else
        {
            $EmbedView = new TextView($name, $moduleName);
            $EmbedView->process($args, $moduleName);
            $Content = (string) $EmbedView;
        }
        return $Content;
    }
    // End getContent

    //////////////////////////////////////////////////////////////////////////////////////
}
// End XmlTemplateEngine
