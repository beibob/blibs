<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs;

use Beibob\Blibs\Interfaces\Viewable;
use Beibob\Blibs\Interfaces\ViewPostProcessingPlugin;

/**
 * Bindet JS-Dateien ein
 *
 * @package blibs
 * @author Tobias Lode <tobias@beibob.de>
 * @author     Fabian M�ller <fab@beibob.de>
 *
 */
class JsLoader implements ViewPostProcessingPlugin
{
    /**
     * Singleton instance
     */
    private static $Instance;

    /**
     * Liste registrierter JS-Dateien.
     */
    protected static $registeredFiles = [];

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Constructs the JsLoader singleton
     *
     * @return JsLoader
     */
    public static function getInstance()
    {
        if(!self::$Instance)
            self::$Instance = new JsLoader();

        return self::$Instance;
    }
    // End getInstance

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Registriert eine JS-Datei
     *
     * @param string $module - Das Modul das die JS-Datei liefert
     * @param string $jsUrl  - Die Url zur einzubindenen JS-Datei.
     *                         Relativ zur baseUrl aus der config
     */
    public function register($module, $jsUrl)
    {
        $Config = Environment::getInstance()->getConfig();
        $jsUrl = join('/', [$Config->jsUrl, $module, $jsUrl]);
        return self::$registeredFiles[$jsUrl] = $jsUrl;
    }
    // End register

    /**
     *
     */
    public function unregisterAll()
    {
        self::$registeredFiles = [];
    }

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * H�ngt alle registierten JS-Dateien als script-Tags in den
     * Head der �bergeben Seite. Wird von der Plugin-Schnittstelle
     * des FrontControllers aufgerufen
     *
     * @param  Viewable $View
     * @return -
     */
    public function doViewPostProcessing(Viewable $View)
    {
        if ($View instanceof XmlDocument)
        {
            $Head = $View->getElementsByTagName('head')->item(0);
            if (!$Head)
                return;

            foreach (self::$registeredFiles as $jsUrl)
            {
                $Script = $View->createElement('script');
                $Script->setAttribute('type', 'text/javascript');
                $Script->setAttribute('src', $jsUrl);
                $Head->appendChild($Script);
                $Head->appendChild($View->createTextNode("\n"));
            }
        }
    }
    // End doViewPostProcessing

    //////////////////////////////////////////////////////////////////////////////////////
    // protected
    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Constructor
     * Registriert sich selbst an der Plugin-Schnittstelle des JSLoaders
     */
    protected function __construct()
    {
        $FrontController = FrontController::getInstance();
        $FrontController->registerViewPostProcessingPlugin($this);
    }
    // End __construct

    //////////////////////////////////////////////////////////////////////////////////////
}
// End JsLoader
