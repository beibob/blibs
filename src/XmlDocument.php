<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs;

use Beibob\Blibs\Interfaces\Viewable;
use DOMDocument;
use DOMElement;
use DOMNode;
use DOMXPath;

/**
 * XmlDocument
 *
 * @package blibs
 * @author Tobias Lode <tobias@beibob.de>
 * @author Fabian Möller <fab@beibob.de>
 *
 */
class XmlDocument extends DOMDocument implements Viewable
{
    /**
     * Keeps the module for this view
     */
    private $module;

    /**
     * Template name
     */
    protected $tplName;

    /**
     * Template data
     */
    private $templateData = [];

    /*
     * Marks the view as processed
     */
    private $isProcessed = array();

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Constructs the Document
     *
     * @param  string $xmlName
     * @return -
     */
    public function __construct($tplName = false, $module = null, $version = '1.0', $charSet = 'UTF-8')
    {
        parent::__construct($version, $charSet);

        $this->encoding = $charSet;
        $this->validateOnParse  = false;
        $this->resolveExternals = false;

        if($tplName)
            $this->setTplName($tplName, $module);
    }
    // End __construct

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the module
     */
    public function setModule($module)
    {
        $this->module = $module;
    }
    // End setModule

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the template name and module
     */
    public function setTplName($tplName, $module = null)
    {
        $this->tplName = $tplName;

        if(!is_null($module))
            $this->module = $module;
    }
    // End setTplName

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the template name and module
     */
    public function getTplName()
    {
        return ['tplName' => $this->tplName, 'module' => $this->module];
    }
    // End getTplName

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Assigns a variable to the template for substitution
     *
     * @param  string $key
     * @param  mixed $value
     * @return mixed
     */
    public function assign($key, $value = true)
    {
        return $this->templateData[$key] = $value;
    }
    // End assign

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns an assigned substitution variable
     *
     * @param  string $key
     * @return mixed
     */
    public function get($key, $defaultValue = null)
    {
        return isset($this->templateData[$key])? $this->templateData[$key] : $defaultValue;
    }
    // End get

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns all assigned substitution variables
     *
     * @return array
     */
    public function getAssignedValues()
    {
        return $this->templateData;
    }
    // End getAssignedValues

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns true if a assigned substitution variable exists
     *
     * @param  string $key
     * @return boolean
     */
    public function has($key)
    {
        return isset($this->templateData[$key]);
    }
    // End has

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * @see has()
     *
     * @param  string $key
     * @return boolean
     */
    public function exists($key)
    {
        return $this->has($key);
    }
    // End exists

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Processes the view
     *
     * Calls all necessary methods and hooks in the proper order to load,
     * initialize and render this view.
     *
     * @param  array $initArgs - init arguments
     * @param  string $module - module of the view if still unset
     * @return XmlDocument
     */
    public function process(array $initArgs = [], $module = null)
    {
        if ($this->isProcessed($initArgs, $this->templateData))
            return $this;

        if($module && !$this->module)
            $this->setModule($module);

        $this->init($initArgs);

        $this->beforeLoading();
        $this->loadTemplate();
        $this->afterLoading();

        $this->beforeRender();
        $XmlTemplateEngine = new XmlTemplateEngine($this);
        $XmlTemplateEngine->process();
        $this->afterRender();

        $this->isProcessed[md5(json_encode($initArgs) . json_encode($this->templateData))] = true;

        return $this;
    }
    // End process

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns true if the view is processed
     */
    public function isProcessed($initArgs = [], $templateData = [])
    {
        return isset($this->isProcessed[md5(json_encode($initArgs) . json_encode($templateData))]);
    }
    // End isProcessed

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the content
     *
     * @param  -
     * @return DOMElement
     */
    public function getContentNode()
    {
        return $this->firstChild;
    }
    // End getContentNode

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Appends this document (the ContentNode) to the given element.
     *
     * @param  DOMNode $Node
     * @return DOMNode
     */
    public function appendTo(DOMNode $Node)
    {
        return $Node->appendChild($Node->ownerDocument->importNode($this->getContentNode(), true));
    }
    // End appendTo

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Creates an element
     *
     * @param  string $name
     * @param  array $attributes
     * @param  string $strValue
     * @return DOMElement
     */
    public function createElement($name, $strValue = null, array $attributes = null)
    {
        $Element = parent::createElement($name);

        if(!is_null($attributes))
            foreach($attributes as $attrName => $attrValue)
                $Element->setAttribute($attrName, $attrValue);

        if(!is_null($strValue))
            $Element->appendChild($this->createTextNode($strValue));

        return $Element;
    }
    // End createElement;

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns an DOMElement by id
     *
     * @param  string $id
     * @param  boolean $removeAttribute
     * @return DOMElement
     */
    public function getElementById($id, $removeAttribute = false)
    {
        $Element = $this->getElementsByAttr('id', $id)->item(0);

        if($removeAttribute && is_object($Element) && $Element->hasAttribute('id'))
            $Element->removeAttribute('id');

        return $Element;
    }
    // End getElementById

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns an DOMElement by id
     *
     * @param  string $id
     * @return DOMNodelist
     */
    public function getElementsByAttr($attribute, $value, DOMNode $ContextNode = null)
    {
        return $this->query('//*[@'. $attribute .'="'. $value .'"]', $ContextNode);
    }
    // End getElementsByAttr

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * XPath Query
     *
     * @param  string $query
     * @return DOMNodelist
     */
    public function query($query, DOMNode $ContextNode = null)
    {
        $XPath = new DOMXPath($this);

        if($ContextNode)
            return $XPath->query($query, $ContextNode);

        return $XPath->query($query);
    }
    // End query

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Entfernt alle childNodes des Elements
     *
     * @param DOMElement - Das Element dessen Kinder entfernt werden sollen
     */
    public function removeChildNodes(DOMElement $DOMElement)
    {
        while($DOMElement->hasChildNodes())
            $DOMElement->removeChild($DOMElement->childNodes->item(0));
    }
    // End removeChildNodes

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the output of the document
     *
     * @param  -
     * @return string
     */
    public function __toString()
    {
        return $this->saveXml();
    }
    // End getOutput

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the input encoding
     *
     * @param  -
     * @return string
     */
    public function getInputEncoding()
    {
        return 'UTF-8';
    }
    // End getInputEncoding

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the output encoding
     *
     * @param  -
     * @return string
     */
    public function getOutputEncoding()
    {
        return $this->encoding;
    }
    // End getOutputEncoding

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the module
     *
     * @param  -
     * @return string
     */
    public function getModule()
    {
        return $this->module;
    }
    // End getModule

    //////////////////////////////////////////////////////////////////////////////////////
    // protected
    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Inits the view
     *
     * @param  array $args
     * @return -
     */
    protected function init(array $args = [])
    {
        if(is_null($args))
            return;

        foreach($args as $name => $value)
        {
            switch ($name)
            {
                case 'tplName':
                    $this->tplName = $value;
                    break;
                default:
                    $this->assign($name, $value);
                    break;
            }
        }
    }
    // End init

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Callback triggered before rendering the template
     *
     * @param  -
     * @return -
     */
    protected function beforeRender() {}
    // End beforeRender

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Callback triggered after rendering the template
     *
     * @param  -
     * @return -
     */
    protected function afterRender() {}
    // End afterRender

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Callback triggered before loading the template
     *
     * Note: Nothing is sure to be initialized here!
     *
     * @param  -
     * @return -
     */
    protected function beforeLoading() {}
    // End beforeLoading

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Prepares the given template.
     *
     * Each template call which has an appropiate handler installed
     * will be replaced by the replacement content they provide.
     *
     */
    protected function loadTemplate()
    {
        if(!$this->tplName)
            return;

        $content = TemplateEngine::loadTemplate($this->tplName, $this->module);

        if(!@$this->loadXML($content))
            if(!@$this->loadXML("<div>" . $content . "</div>"))
                $this->loadXML("<p style=\"color: red; font-weight: bold;\">Error loading template!</p>");
    }
    // End loadTemplate

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Callback triggered after loading the template
     *
     * Note: Basic functionality is initialized here. Template is loaded
     *
     * @param  -
     * @return -
     */
    protected function afterLoading() {}
    // End afterLoading

    //////////////////////////////////////////////////////////////////////////////////////
}
// End XmlDocument
