<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs;

use PDO;

/**
 * Session database object
 *
 * @package blibs
 * @author Tobias Lode <tobias@beibob.de>
 * @author     Fabian M�ller <fab@beibob.de>
 *
 */
class DbSession extends DbObject
{
    /**
     * Table name
     */
    const TABLE_NAME = 'public.sessions';

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * id
     */
    private $id;

    /**
     * created
     */
    private $created;

    /**
     * modified
     */
    private $modified;

    /**
     * expires
     */
    private $expires;

    /**
     * data
     */
    private $data;

    /**
     * Primary key
     */
    private static $primaryKey = ['id'];

    /**
     * Columns and their types
     */
    private static $columnTypes = ['id'       => PDO::PARAM_STR,
                                        'created'  => PDO::PARAM_STR,
                                        'modified' => PDO::PARAM_STR,
                                        'expires'  => PDO::PARAM_INT,
                                        'data'     => PDO::PARAM_STR
                                        ];

    /**
     * Extended columns and their types
     */
    private static $extColumnTypes = [];

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Creates a session
     *
     * @param  -
     * @return -
     */
    public static function create($id, $expires = null, $data = '')
    {
        $Session = new DbSession();
        $Session->setId($id);
        $Session->setExpires($expires);
        $Session->setData($data);

        if($Session->getValidator()->isValid())
            $Session->insert();

        return $Session;
    }
    // End create

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Inits the Session by id
     *
     * @param  string $id
     * @return -
     */
    public static function findById($id, $force = false)
    {
        if(!$id)
            return new DbSession();

        $sql = sprintf('SELECT *
                          FROM %s
                         WHERE id = :id'
                       , self::getTablename()
                       );

        return self::findBySql(get_class(), $sql, ['id' => $id], $force);
    }
    // End findById

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Deletes expired session entries
     *
     * @param  int $maxLifeTime
     * @return -
     */
    public static function deleteExpired($maxLifeTime)
    {
        $sql = sprintf("DELETE FROM %s
                              WHERE (modified IS NOT NULL
                                AND modified + (expires||' seconds')::interval < now()
                                    )
                                 OR (modified IS NULL
                                AND created + (expires||' seconds')::interval < now()
                                    )"
                       , self::getTablename()
                       );

        self::performSql(get_class(), $sql);
    }
    // End deleteExpired

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns id
     *
     * @param  -
     * @return numeric
     */
    public function getId()
    {
        return $this->id;
    }
    // End getId

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns created
     *
     * @param  -
     * @return BlibsDateTime
     */
    public function getCreated($format = false)
    {
        return new BlibsDateTime($this->created, $format);
    }
    // End getCreated

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns modified
     *
     * @param  -
     * @return int
     */
    public function getModified($format = false)
    {
        return new BlibsDateTime($this->modified, $format);
    }
    // End getModified

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns expires
     *
     * @param  -
     * @return int
     */
    public function getExpires()
    {
        return $this->expires;
    }
    // End getExpires

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns data
     *
     * @param  -
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }
    // End getData

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets id
     *
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }
    // End setExpires

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets expires
     *
     * @param int
     */
    public function setExpires($expires)
    {
        $this->expires = $expires;
    }
    // End setExpires

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets data
     *
     * @param string
     */
    public function setData($data)
    {
        $this->data = $data;
    }
    // End setData

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Saves the object to table and returns true on success
     *
     * @param  -
     * @return boolean
     */
    public function update()
    {
        $this->modified = self::getCurrentTime();

        $sql = sprintf("UPDATE %s
                           SET modified = :modified
                             , expires  = :expires
                             , data     = :data
                         WHERE id = :id"
                       , self::getTablename()
                       );

        return $this->updateBySql($sql,
                                  ['modified' => $this->modified,
                                        'expires'  => $this->expires,
                                        'data'     => $this->data,
                                        'id'       => $this->id
                                        ]
                                  );
    }
    // End update

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Deletes the object from the table
     *
     * @param  -
     * @return boolean
     */
    public function delete()
    {
        $sql = sprintf("DELETE FROM %s
                              WHERE id = :id"
                       , self::getTablename()
                       );

        return $this->deleteBySql($sql, ['id' => $this->id]);
    }
    // End delete

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns an array with the primary key properties and,
     * if it's a valid object, associated its values
     *
     * @param  -
     * @return array
     */
    public function getPrimaryKey($propertiesOnly = false)
    {
        if($propertiesOnly)
            return self::$primaryKey;

        $primaryKey = [];

        foreach(self::$primaryKey as $key)
            $primaryKey[$key] = $this->$key;

        return $primaryKey;
    }
    // End getPrimaryKey

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the name of the table the object is stored in
     *
     * @param  -
     * @return string
     */
    public static function getTablename()
    {
        return self::TABLE_NAME;
    }
    // End getTablename

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the columns with their types. The columns may also return extended columns
     * if the first argument is set to true. To access the type of a single column, specify
     * the column name in the second argument
     *
     * @param  boolean $extColumns
     * @param  string $column
     * @return mixed
     */
    public static function getColumnTypes($extColumns = false, $column = false)
    {
        $columnTypes = $extColumns? array_merge(self::$columnTypes, self::$extColumnTypes) : self::$columnTypes;

        if(!$column)
            return $columnTypes;

        return $columnTypes[$column];
    }
    // End getColumnTypes

    //////////////////////////////////////////////////////////////////////////////////////
    // protected
    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Inserts a new object in the table
     *
     * @param  -
     * @return boolean
     */
    protected function insert()
    {
        $this->created  = self::getCurrentTime();
        $this->modified = null;

        $sql = sprintf("INSERT INTO %s (id, created, modified, expires, data)
                                VALUES (:id, :created, :modified, :expires, :data)"
                       , self::getTablename()
                       );

        return $this->insertBySql($sql, ['id'       => $this->id,
                                              'created'  => $this->created,
                                              'modified' => $this->modified,
                                              'expires'  => $this->expires,
                                              'data'     => $this->data
                                              ]);
    }
    // End insert

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Inits the object by an data object
     *
     * @param  \stdClass $TDO
     * @return -
     */
    protected function initByDataObject(\stdClass $DO = null)
    {
        $this->id       = $DO->id;
        $this->created  = $DO->created;
        $this->modified = $DO->modified;
        $this->expires  = $DO->expires;
        $this->data     = $DO->data;
    }
    // End initByDataObject

    //////////////////////////////////////////////////////////////////////////////////////
}
// End DbSession
