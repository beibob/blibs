<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs;

use Beibob\Blibs\Interfaces\ApplicationController;

/**
 * Media Controller
 *
 * @package blibs
 * @author Tobias Lode <tobias@beibob.de>
 * @author Fabian Möller <fab@beibob.de>
 *
 */
class MediaController extends ActionController implements ApplicationController
{
    /**
     * Requested media
     */
    private $Media;

    /**
     * Die Id des Medias
     */
    private $mediaId;

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the mediaId
     */
    public function setMediaId($mediaId = false)
    {
        $this->mediaId = $mediaId ? $mediaId : $this->getAction();
        return $this->getMediaId();
    }
    // End setMediaId

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the mediaId
     */
    public function getMediaId()
    {
        return $this->mediaId;
    }
    // End getMediaId

    //////////////////////////////////////////////////////////////////////////////////////
    // protected
    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Default action - the default is no action
     *
     * @param  -
     * @return -
     */
    protected function defaultAction()
    {
        $mediaId = $this->getMediaId();
        if(!$mediaId)
            $mediaId = $this->setMediaId();

        if(!is_numeric($mediaId))
            return;

        $Media = Media::findById($mediaId);

        if(!$Media->isInitialized())
            return;

        /**
         * Disabling headers pragma and expires
         */
        $this->Response->setHeader('Pragma: ');
        $this->Response->setHeader('Expires: ');

        /**
         * Tell client to revalidate
         */
        $this->Response->setHeader('Cache-Control: must-revalidate');

        if($mimeType = $Media->getMimeType())
            $this->Response->setHeader('Content-Type: '. $mimeType);

        /**
         * Compare last modified timestamp with client-side cache
         */
        $this->Request->validateCacheHeaderLastModified($Media->getCreatedDateTime()->getUnixtime(), $this->Response);

        /**
         * Set the view
         */
        $this->setView(new MediaView($Media));
    }
    // End defaultAction

    //////////////////////////////////////////////////////////////////////////////////////
}
// End MediaController
