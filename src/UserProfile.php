<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs;

use PDO;

/**
 *
 * @package blibs
 * @author     Fabian M�ller  <fab@beibob.de>
 * @author Tobias Lode <tobias@beibob.de>
 *
 */
class UserProfile extends DbObject
{
    /**
     * Tablename
     */
    const TABLE_NAME = 'public.user_profiles';


    /**
     * Valid values for property gender
     */
    const GENDER_MALE = 'm';
    const GENDER_FEMALE = 'f';

    /**
     * userId
     */
    private $userId;

    /**
     * company
     */
    private $company;

    /**
     * @var string gender (null, m or f)
     */
    private $gender;

    /**
     * firstname
     */
    private $firstname;

    /**
     * lastname
     */
    private $lastname;

    /**
     * email address
     */
    private $email;

    /**
     * candidate email address
     */
    private $candidateEmail;

    /**
     * birthday
     */
    private $birthday;

    /**
     * @var string notice
     */
    private $notice;

    /**
     * Primary key
     */
    private static $primaryKey = ['userId'];

    /**
     * Column types
     */
    private static $columnTypes = ['userId'         => PDO::PARAM_INT,
                                        'company'        => PDO::PARAM_STR,
                                        'gender'         => PDO::PARAM_STR,
                                        'firstname'      => PDO::PARAM_STR,
                                        'lastname'       => PDO::PARAM_STR,
                                        'email'          => PDO::PARAM_STR,
                                        'candidateEmail' => PDO::PARAM_STR,
                                        'birthday'       => PDO::PARAM_STR,
                                        'notice'         => PDO::PARAM_STR];

    /**
     * Extended column types
     */
    private static $extColumnTypes = [];


    /**
     * Creates the object
     *
     * @param  integer  $userId   - userId
     * @param  string   $company  - company
     * @param  string   $firstname - firstname
     * @param  string   $lastname - lastname
     * @param  string   $email    - email address
     * @param  string   $birthday - birthday
     */
    public static function create($userId, $company = '', $gender = null, $firstname = '', $lastname = '', $email = '', $notice = '', $birthday = null)
    {
        $UserProfile = new UserProfile();
        $UserProfile->setUserId($userId);
        $UserProfile->setCompany($company);
        $UserProfile->setGender($gender);
        $UserProfile->setFirstname($firstname);
        $UserProfile->setLastname($lastname);
        $UserProfile->setCandidateEmail($email);
        $UserProfile->setNotice($notice);
        $UserProfile->setBirthday($birthday);

        if($UserProfile->getValidator()->isValid())
            $UserProfile->insert();

        return $UserProfile;
    }
    // End create


    /**
     * Inits a `UserProfile' by its primary key
     *
     * @param  integer  $userId - userId
     * @param  boolean  $force - Bypass caching
     * @return UserProfile
     */
    public static function findByUserId($userId, $force = false)
    {
        if(!$userId)
            return new UserProfile();

        $sql = sprintf("SELECT user_id
                             , company
                             , gender
                             , firstname
                             , lastname
                             , email
                             , candidate_email
                             , notice
                             , birthday
                          FROM %s
                         WHERE user_id = :userId"
                       , self::TABLE_NAME
                       );

        return self::findBySql(get_class(), $sql, ['userId' => $userId], $force);
    }
    // End findByUserId


    /**
     * Inits a `UserProfile' by its primary key
     *
     * @param  integer  $userId - userId
     * @param  boolean  $force - Bypass caching
     * @return UserProfile
     */
    public static function findByEmail($email, $force = false)
    {
        if(!$email)
            return new UserProfile();

        $sql = sprintf("SELECT user_id
                             , company
                             , gender
                             , firstname
                             , lastname
                             , email
                             , candidate_email
                             , notice
                             , birthday
                          FROM %s
                         WHERE email = :email"
                       , self::TABLE_NAME
                       );

        return self::findBySql(get_class(), $sql, ['email' => $email], $force);
    }
    // End findByEmail


    /**
     * Sets the property userId
     *
     * @param  integer  $userId - userId
     * @return
     */
    public function setUserId($userId)
    {
        if(!$this->getValidator()->assertNotEmpty('userId', $userId))
            return;

        $this->userId = (int)$userId;
    }
    // End setUserId


    /**
     * Sets the property company
     *
     * @param  string   $company - company
     * @return
     */
    public function setCompany($company = '')
    {
        if(!$this->getValidator()->assertMaxLength('company', 100, $company))
            return;

        $this->company = $company;
    }
    // End setCompany


    /**
     * Sets the property gender
     *
     * @param  string   $gender - gender. On of the GENDER_*-Constants or null
     * @return
     */
    public function setGender($gender = null)
    {
        if(!$this->getValidator()->assertTrue('gender', is_null($gender) || in_array($gender, [self::GENDER_MALE, self::GENDER_FEMALE]), $gender))
            return;

        $this->gender = $gender;
    }
    // End setGender

    /**
     * Sets the property firstname
     *
     * @param  string   $firstname - firstname
     * @return
     */
    public function setFirstname($firstname = '')
    {
        if(!$this->getValidator()->assertMaxLength('firstname', 100, $firstname))
            return;

        $this->firstname = $firstname;
    }
    // End setFirstname


    /**
     * Sets the property lastname
     *
     * @param  string   $lastname - lastname
     * @return
     */
    public function setLastname($lastname = '')
    {
        if(!$this->getValidator()->assertMaxLength('lastname', 100, $lastname))
            return;

        $this->lastname = $lastname;
    }
    // End setLastname


    /**
     * Sets the property email
     *
     * @param  string   $email - email address
     * @return
     */
    public function setEmail($email = '')
    {
        if(!$this->getValidator()->assertMaxLength('email', 100, $email))
            return;

        $this->email = $email;
    }
    // End setEmail

    /**
     * Sets the property candidateEmail
     *
     * @param  string   $candidateEmail - email address
     * @return
     */
    public function setCandidateEmail($candidateEmail = '')
    {
        if(!$this->getValidator()->assertMaxLength('candidateEmail', 100, $candidateEmail))
            return;

        $this->candidateEmail = $candidateEmail;
    }
    // End setCandidateEmail



    /**
     * @param string $notice
     */
    public function setNotice($notice)
    {
        $this->notice = $notice;
    }
    // End setNotice


    /**
     * Sets the property birthday
     *
     * @param  string   $birthday - birthday
     * @return
     */
    public function setBirthday($birthday = null)
    {
        if ($birthday instanceof BlibsDate && $birthday->isValid())
            $this->birthday  = $birthday->getDateString();
        elseif ($birthday instanceof BlibsDate)
            $this->birthday = null;
        else
            $this->birthday = $birthday;
    }
    // End setBirthday


    /**
     * Returns the property userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return (int)$this->userId;
    }
    // End getUserId


    /**
     * Returns the associated User by property userId
     *
     * @param  boolean  $force
     * @return User
     */
    public function getUser($force = false)
    {
        return User::findById($this->userId, $force);
    }
    // End getUser


    /**
     * Returns the property company
     *
     * @return string
     */
    public function getCompany()
    {
        return $this->company;
    }
    // End getCompany


    /**
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }
    // End getGender


    /**
     * Returns the property firstname
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }
    // End getFirstname


    /**
     * Returns the property lastname
     *
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }
    // End getLastname


    /**
     * Returns the property email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }
    // End getEmail

    /**
     * Returns the property candidateEmail
     *
     * @return string
     */
    public function getCandidateEmail()
    {
        return $this->candidateEmail;
    }
    // End getCandidateEmail


    /**
     * @return string
     */
    public function getNotice()
    {
        return $this->notice;
    }
    // End getNotice


    /**
     * Returns the property birthday
     *
     * @return string
     */
    public function getBirthday()
    {
        return $this->birthday;
    }
    // End getBirthday


    /**
     * Checks, if the object exists
     *
     * @param  integer  $userId - userId
     * @param  boolean  $force - Bypass caching
     * @return boolean
     */
    public static function exists($userId, $force = false)
    {
        return self::findBy($userId, $force)->isInitialized();
    }
    // End exists


    /**
     * Updates the object in the table
     *
     * @return boolean
     */
    public function update()
    {
        $sql = sprintf("UPDATE %s
                           SET company         = :company
                             , gender          = :gender
                             , firstname       = :firstname
                             , lastname        = :lastname
                             , email           = :email
                             , candidate_email = :candidateEmail
                             , notice          = :notice
                             , birthday        = :birthday
                         WHERE user_id = :userId"
                       , self::TABLE_NAME
                       );

        return $this->updateBySql($sql,
                                  ['userId'        => $this->userId,
                                        'company'        => $this->company,
                                        'gender'         => $this->gender,
                                        'firstname'      => $this->firstname,
                                        'lastname'       => $this->lastname,
                                        'email'          => $this->email,
                                        'candidateEmail' => $this->candidateEmail,
                                        'notice'         => $this->notice,
                                        'birthday'       => $this->birthday]
                                  );
    }
    // End update


    /**
     * Deletes the object from the table
     *
     * @return boolean
     */
    public function delete()
    {
        $sql = sprintf("DELETE FROM %s
                              WHERE user_id = :userId"
                       , self::TABLE_NAME
                      );

        return $this->deleteBySql($sql,
                                  ['userId' => $this->userId]
                                  );
    }
    // End delete


    /**
     * Returns an array with the primary key properties and
     * associates its values, if it's a valid object
     *
     * @param  boolean  $propertiesOnly
     * @return array
     */
    public function getPrimaryKey($propertiesOnly = false)
    {
        if($propertiesOnly)
            return self::$primaryKey;

        $primaryKey = [];

        foreach(self::$primaryKey as $key)
            $primaryKey[$key] = $this->$key;

        return $primaryKey;
    }
    // End getPrimaryKey


    /**
     * Returns the tablename constant. This is used
     * as interface for other objects.
     *
     * @return string
     */
    public static function getTablename()
    {
        return self::TABLE_NAME;
    }
    // End getTablename


    /**
     * Returns the columns with their types. The columns may also return extended columns
     * if the first argument is set to true. To access the type of a single column, specify
     * the column name in the second argument
     *
     * @param  boolean  $extColumns
     * @param  mixed    $column
     * @return mixed
     */
    public static function getColumnTypes($extColumns = false, $column = false)
    {
        $columnTypes = $extColumns? array_merge(self::$columnTypes, self::$extColumnTypes) : self::$columnTypes;

        if($column)
            return $columnTypes[$column];

        return $columnTypes;
    }
    // End getColumnTypes


    /**
     * Inserts a new object in the table
     *
     * @return boolean
     */
    protected function insert()
    {

        $sql = sprintf("INSERT INTO %s (user_id, company, gender, firstname, lastname, email, candidate_email, notice, birthday)
                               VALUES  (:userId, :company, :gender, :firstname, :lastname, :email, :candidateEmail, :notice, :birthday)"
                       , self::TABLE_NAME
                       );

        return $this->insertBySql($sql,
                                  ['userId'        => $this->userId,
                                        'company'        => $this->company,
                                        'gender'         => $this->gender,
                                        'firstname'      => $this->firstname,
                                        'lastname'       => $this->lastname,
                                        'email'          => $this->email,
                                        'candidateEmail' => $this->candidateEmail,
                                        'notice'         => $this->notice,
                                        'birthday'       => $this->birthday]
                                  );
    }
    // End insert


    /**
     * Inits the object with row values
     *
     * @param  \stdClass $DO - Data object
     * @return boolean
     */
    protected function initByDataObject(\stdClass $DO = null)
    {
        $this->userId         = (int)$DO->user_id;
        $this->company        = $DO->company;
        $this->gender         = $DO->gender;
        $this->firstname      = $DO->firstname;
        $this->lastname       = $DO->lastname;
        $this->email          = $DO->email;
        $this->candidateEmail = $DO->candidate_email;
        $this->notice         = $DO->notice;
        $this->birthday       = $DO->birthday;

        /**
         * Set extensions
         */
    }
    // End initByDataObject
}
// End class UserProfile
