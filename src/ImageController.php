<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs;

use Beibob\Blibs\Interfaces\ApplicationController;

/**
 * Image Controller
 *
 * @package blibs
 * @author Tobias Lode <tobias@beibob.de>
 * @author Fabian Möller <fab@beibob.de>
 *
 */
class ImageController extends ActionController implements ApplicationController
{
    /**
     * Requested media
     */
    private $ImageFile;
    private $imagePath;

    protected $forceSaveAs = false;
    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the mediaId
     */
    public function setImageFile(File $ImageFile)
    {
        if (!$ImageFile->exists() && !is_file($ImageFile->getFilepath()))
            return;

        $this->ImageFile = $ImageFile;
        $this->imagePath = $this->ImageFile->getFilepath();
        return $this->getImageFile();
    }
    // End setMediaId

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the mediaId
     */
    public function setImagePath($imagePath)
    {
        $ImageFile = new File($imagePath);
        if (!$ImageFile->exists() && !is_file($ImageFile->getFilepath()))
            return;

        $this->ImageFile = $ImageFile;
        $this->imagePath = $imagePath;
        return $this->getImagePath();
    }
    // End setMediaId

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the ImageFile
     */
    public function getImageFile()
    {
        return $this->ImageFile;
    }
    // End getImageFile

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the imagePath
     */
    public function getImagePath()
    {
        if ($this->ImageFile instanceof File)
            return $this->ImageFile->getFilepath();

        return;
    }
    // End getImagePath

    //////////////////////////////////////////////////////////////////////////////////////
    // protected
    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * pukes out the image
     *
     * @param  -
     * @return -
     */
    protected function flushImage()
    {
        if (is_null($this->getImageFile()))
            return;

        if (!$this->ImageFile->exists() || !is_file($this->ImageFile->getFilepath()))
            return;

        /** @TODO Prüfung ob es ein Image ist bauen **/

        /**
         * Disabling headers pragma and expires
         */
        $this->Response->setHeader('Pragma: ');

        if ($this->forceSaveAs)
        {
            //  Tell client to revalidate

            $this->Response->setHeader('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            $this->Response->setHeader('Content-Type: application/octet-stream');
            $this->Response->setHeader("Content-Disposition: attachment; filename=\"" . basename($this->ImageFile->getFilepath()) . "\"");
            $this->Response->setHeader("Content-Length: " . File::getFilesize($this->ImageFile->getFilepath()));
        }
        elseif($mimeType = $this->getMimeType())
        {
            $this->Response->setHeader('Cache-Control: must-revalidate');
            $this->Response->setHeader("Content-Type: " . $mimeType. "; name=\"" . basename($this->ImageFile->getFilepath()) . "\"");
        }

        /**
         * Compare last modified timestamp with client-side cache
         */
        $this->Request->validateCacheHeaderLastModified($this->ImageFile->getMTime(), $this->Response);

        /**
         * Set the view
         */
        $this->setView(new ImageView($this->ImageFile));
    }
    // End flushImage

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     *
     */
    protected function getMimeType()
    {
        $imageInfo = getimagesize($this->getImagePath());
        return $imageInfo['mime'];
    }
    // End getMimeType

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     *
     */
    protected function defaultAction()
    {
        $this->flushImage();
    }
    // End defaultAction

    //////////////////////////////////////////////////////////////////////////////////////
}
// End ImageController
