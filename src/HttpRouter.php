<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs;

use Beibob\Blibs\Interfaces\Request;

/**
 * Resolves an ActionController to build the content
 *
 * @package blibs
 * @author Tobias Lode <tobias@beibob.de>
 * @author Fabian Möller <fab@beibob.de>
 */
class HttpRouter implements Interfaces\Router
{
    const CONTROLLERS_PATH    = 'Controller';
    const INTERFACES_PATH     = 'Interfaces';
    const MODELS_PATH         = 'Db';
    const VIEWS_PATH          = 'View';


    /**
     * FrontController
     */
    protected $FrontController;

    /**
     * Controller name
     */
    protected $ctrlName;

    /**
     * Invoked action
     */
    protected $action;

    /**
     * Invoked query
     */
    protected $query;

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Constructor
     *
     * @param  FrontController $FrontController
     */
    public function __construct(FrontController $FrontController)
    {
        $this->FrontController = $FrontController;
    }
    // End __construct

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the action Controller name
     *
     * @return string
     */
    public function getInvokedControllerName()
    {
        return $this->ctrlName;
    }
    // End getInvokedControllerName

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the invoked module
     *
     * @return string
     */
    public function getInvokedModule()
    {
        return $this->module;
    }
    // End getInvokedModule

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the invoked action
     *
     * @return string
     */
    public function getInvokedAction()
    {
        return $this->action;
    }
    // End getInvokedAction

    /**
     * Resolves and returns the name of the invoked ActionController,
     * the action and the query string by the given Request
     *
     * 0: /                          = ('<defaultmodule>\<defaultctrl>', null, [query])
     * 1: /module/                   = ('module', null, [query])
     * 1: /ctrl/                     = ('<defaultmodule>\ctrl', null, [query])
     * 2: /module/ctrl/              = ('module\ctrl', null, [query])
     * 2: /ctrl/action/              = ('<defaultmodule>\ctrl', 'action', [query])
     * 2: /sub/ctrl/                 = ('<defaultmodule>\sub\ctrl', null, [query])
     * 3: /module/sub/ctrl/          = ('module\sub\ctrl', null, [query])
     * 3: /sub/ctrl/action/          = ('<defaultmodule>\sub\ctrl', 'action', [query])
     * 3: /module/ctrl/action/       = ('module\ctrl', 'action', [query])
     * 4: /module/sub/ctrl/action/   = ('module\sub\ctrl', 'action', [query])
     * 4: /sub/sub2/sub3/ctrl/       = ('<defaultmodule>\sub\sub2\sub3\ctrl', null, [query])
     * 4: /sub/sub2/ctrl/action/     = ('<defaultmodule>\sub\sub2\ctrl', 'action', [query])
     *
     * @param Request $Request
     * @return array
     */
    public function resolve(Request $Request)
    {
        if(!$reqUrl = $Request->getUri())
            throw new Exception('Missing a request URI');

        $Url  = Url::parse($reqUrl);
        $path = preg_split('/\//u', $Url->getScriptName(), -1, PREG_SPLIT_NO_EMPTY);

        if(($n = count($path)) > 0)
        {
            $registeredModules = $this->FrontController->getEnvironment()->getModules();

            if(isset($registeredModules[$path[0]]))
                $module = array_shift($path);

            else
                $module = $this->FrontController->getDefaultModule();

            if($path)
            {
                $this->ctrlName = $this->buildNsClassName($path, $module);

                if (class_exists($this->ctrlName)) {
                    $this->action = null;
                }
                else {
                    $this->action = array_pop($path);
                    $this->ctrlName = $this->buildNsClassName($path, $module);
                }
            }
            else
                $this->ctrlName = $this->FrontController->getDefaultActionControllerName($module);
        }
        else
            $this->ctrlName = $this->FrontController->getDefaultActionControllerName();

        $this->query = $Request->getAsArray();

        return [$this->ctrlName, $this->action, $this->query];
    }
    // End resolve

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Build the url to the given name of an ActionController,
     * an optional action and optional a query string
     *
     * @param string  $actionControllerName
     * @param string  $action
     * @param array   $query
     * @return string
     */
    public function getUrlTo($actionControllerName, $action = null, array $query = null)
    {
        $parts = [];

        if(!is_null($actionControllerName) && $this->buildNsRequestName($actionControllerName))
            $parts[] =  $this->buildNsRequestName($actionControllerName);

        if(!is_null($action))
            $parts[] = $action;

        $scriptName = '/' . join('/', $parts);
        if ($scriptName != '/')
            $scriptName .= '/';

        return (string)Url::factory($scriptName, $query);
    }
    // End getUrlTo

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the last resolved name of an action controller
     *
     * @return string
     */
    public function getActionController()
    {
        return $this->ctrlName;
    }
    // End getActionController

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the last resolved action
     *
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }
    // End getAction

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the last resolved request query
     *
     * @return array
     */
    public function getQuery()
    {
        return $this->query;
    }
    // End getQuery

    //////////////////////////////////////////////////////////////////////////////////////
    // protected
    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Builds the namespaced class name
     */
    protected function buildNsClassName(array $path, $module = null)
    {
        $className = StringFactory::camelCase(array_pop($path), '-') . 'Ctrl';

        if(!$module)
            $module = $this->FrontController->getDefaultModule();

        $ns = '';
        foreach($path as $part)
            $ns .= StringFactory::camelCase($part, '-') . '\\';

        return ucfirst($module) .'\\'. self::CONTROLLERS_PATH .'\\'. $ns . $className;
    }
    // End buildNsClassName

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Builds the namespaced request name
     */
    protected function buildNsRequestName($className)
    {
        // Skip if className is the default action controller
        if($className == $this->FrontController->getDefaultActionControllerName())
            return '';

        $parts = array_flip(explode('\\', $className));
        unset($parts[self::CONTROLLERS_PATH]);
        $parts = array_flip($parts);

        $module = lcfirst(array_shift($parts));
        $className = array_pop($parts);
        // remove legacy module prefix
        //$className = preg_replace('/^'. preg_quote($module) .'/i', '', $className);

        $path = $module !== $this->FrontController->getDefaultModule()
            ? $module . '/'
            : '';

        foreach($parts as $part)
            $path .=  StringFactory::unCamelCase($part, '-') .'/';

        return $path . StringFactory::unCamelCase(utf8_substr($className, 0, -4), '-');
    }
    // End buildNsRequestName

    //////////////////////////////////////////////////////////////////////////////////////
}
// End HttpRouter
