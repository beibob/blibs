<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs;

use Beibob\Blibs\Interfaces\Request;

/**
 * Request implementatioin
 *
 * @package blibs
 * @author Tobias Lode <tobias@beibob.de>
 * @author Fabian Möller <fab@beibob.de>
 *
 */
abstract class RequestBase implements Request
{
    /**
     * request
     */
    protected $request;

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Constructs the instance
     *
     * @param -
     */
    public function __construct(array $request = null)
    {
        $this->request = $request;
    }
    // End __construct

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the uri
     *
     * @return  string
     */
    abstract public function getUri();

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns a value from the request by key
     *
     * @param  string  $key   - request key
     * @return mixed
     */
    public function get($key)
    {
        if(!isset($this->request[$key]))
            return null;

        if(is_numeric($this->request[$key]))
            return $this->request[$key];

        return self::unescape($this->request[$key]);
    }
    // End get

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the request as array
     *
     * @param  -
     * @return array
     */
    public function getAsArray()
    {
        return self::unescapeArray($this->request);
    }
    // End getAsArray

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns a numeric value or null
     *
     * @param  -
     * @return numeric
     */
    public function getNumeric($key)
    {
		if(!isset($this->request[$key]))
			return null;

        return is_numeric($this->request[$key])? $this->request[$key] : null;
    }
    // End getNummeric

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns a trimmed string for the given $key property or null
     *
     * @param  string $key
     * @return string
     */
    public function getString($key, $dontTrim = false)
    {
		if(!isset($this->request[$key]))
			return null;

        return $dontTrim? (string)$this->request[$key] : utf8_trim((string)$this->request[$key]);
    }
    // End getString

    //////////////////////////////////////////////////////////////////////////////////////

   /**
     * Returns array property
     *
     * @param  -
     * @return array
     */
    public function getArray($key)
    {
        if(!isset($this->request[$key]) || !is_array($this->request[$key]))
            return [];

        return $this->request[$key];
    }
    // End getArray

    //////////////////////////////////////////////////////////////////////////////////////

   /**
     * Returns a DateTime property
     *
     * @param  string $key
     * @param  string $format
     * @return \DateTime
     */
    public function getDateTime($key, $format = 'Y-m-d')
    {
        if(!isset($this->request[$key]) || !is_string($this->request[$key]))
            return null;

        return \DateTime::createFromFormat($format, $this->request[$key]);
    }
    // End getDateTime

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Allows setting a value
     *
     * @param  string  $key   - request key
     * @param  mixed   $value
     */
    public function set($key, $value)
    {
        $this->request[$key] = $value;
    }
    // End get

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Has
     *
     * @param  string $key
     * @return boolean
     */
    public function has($key)
    {
        return isset($this->request[$key]);
    }
    // End __isset

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Magic method __isset
     *
     * @param  string $key
     * @return boolean
     */
    public function __isset($key)
    {
        return $this->has($key);
    }
    // End __isset

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Magic method __get
     *
     * @param  string $key
     * @return mixed
     */
    public function __get($key)
    {
        if(!isset($this->request[$key]))
            return null;

        return $this->get($key);
    }
    // End __get

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Magic method __set
     *
     * @param  string $key
     * @param  mixed  $value
     */
    public function __set($key, $value)
    {
        $this->set($key, $value);
    }
    // End __set

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Magic method __sleep
     *
     * @param  -
     * @return array
     */
    public function __sleep()
    {
        return ['request', 'requestMethod'];
    }
    // End __sleep

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * magic __toString method
     *
     * @param  -
     * @return string
     */
    public function __toString()
    {
        return print_r($this->request, true);
    }
    // End __toString

    ///////////////////////////////////////////////////////////////////////////////////////////
    // Interface Iterator
    ///////////////////////////////////////////////////////////////////////////////////////////

    public function rewind()
    {
        if(!is_array($this->request))
            return false;

        reset($this->request);
    }
    // End rewind

    ///////////////////////////////////////////////////////////////////////////////////////////

    public function current()
    {
        if(!is_array($this->request))
            return false;

        $current = current($this->request);
        return $current;
    }
    // End current

    ///////////////////////////////////////////////////////////////////////////////////////////

    public function key()
    {
        if(!is_array($this->request))
            return false;

        $key = key($this->request);
        return $key;
    }
    // End key

    ///////////////////////////////////////////////////////////////////////////////////////////

    public function next()
    {
        if(!is_array($this->request))
            return false;

        $next = next($this->request);
        return $next;
    }
    // End next

    ///////////////////////////////////////////////////////////////////////////////////////////

    public function valid()
    {
        $valid = $this->current() !== false;
        return $valid;
    }
    // End valid

    ///////////////////////////////////////////////////////////////////////////////////////////
    // protected
    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Unescapes a string or an array
     *
     * @param   mixed
     * @returns mixed
     */
    protected static function unescape($mixed)
    {
        if(is_array($mixed))
            return self::unescapeArray($mixed);

        return self::unescapeString($mixed);
    }
    // End unescapeValue

    ///////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Unescapes all values of an array
     *
     * @param   array $values
     * @returns array
     */
    protected static function unescapeArray(array $values)
    {
        $cleaned = [];

        foreach($values as $key => $value)
            $cleaned[$key] = self::unescape($value);

        return $cleaned;
    }
    // End unescapeValue

    ///////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Unescapes a string
     *
     * @param  string
     * @return string
     */
    protected static function unescapeString($value)
    {
        $value = strtr($value, ['\"' => '"',
                                   "\\'" => "'",
                                   '\`' => "`",
                                   '\'' => "'",
                                   '\\\\' => chr(92)]);

        return $value;
    }
    // End unescapeString
}
// End class RequestBase
