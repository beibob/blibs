<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs;

/**
 * A class representing date and time
 *
 * @package blibs
 * @author Fabian Möller  <fab@beibob.de>
 * @author Tobias Lode <tobias@beibob.de>
 *
 */
class BlibsDateTime
{
    /**
     * Date
     */
    protected $Date = null;

    /**
     * Time
     */
    protected $Time = null;

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * ISO datetime string format
     */
    const FORMAT_ISO_DATETIME = 'Y-m-d H:i:sP';

    /**
     * RFC2822 datetime string format
     */
    const FORMAT_RFC2822_DATETIME = 'D, j M Y H:i:s O';

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * DateTime Factory
     *
     * @param  mixed   $datetime   - may either a BlibsDateTime object, a datetime string
     * @param  string  $format - [only if $datetime is a date string]
     * @return BlibsDateTime
     */
    public static function factory($dateTime = false, $format = false)
    {
        return new BlibsDateTime($dateTime, $format);
    }
    // End factory

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Constructs the object
     *
     * @param  mixed   $datetime   - may either a BlibsDateTime object, a datetime string
     * @param  string  $format - [only if $datetime is a date string]
     * @return object
     */
    public function __construct($dateTime = false, $format = false)
    {
        if(is_string($dateTime))
            $this->setDateTimeString($dateTime, $format);

        elseif(is_array($dateTime))
            $this->setDateTimeArray($dateTime);

        elseif(is_object($dateTime))
            $this->setDateTime($dateTime);

        elseif(is_numeric($dateTime))
            $this->setTimestamp($dateTime);

        elseif($dateTime === false)
            $this->setTimestamp();

        else
            $this->setInvalid();
    }
    // End __construct

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the current iso (or format specific) date time
     *
     * @param  string $format
     * @return string
     */
    public static function now($format = false)
    {
        $Now = new BlibsDateTime(false, $format);
        return $Now->getDateTimeString($format);
    }
    // End now

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Inits the object by tosecondss time
     *
     * @param  -
     * @return boolean
     */
    public function setNow()
    {
        return $this->setTimestamp();
    }
    // End setNow

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Inits the object by another BlibsDateTime object
     *
     * @param  BlibsDateTime $BlibsDateTime
     * @return boolean
     */
    public function setDateTime(BlibsDateTime $BlibsDateTime)
    {
        if(!$BlibsDateTime->isValid())
            return false;

        return $this->setDateTimeObjects($BlibsDateTime->getDate(), $BlibsDateTime->getTime());
    }
    // End initByDateTime

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Inits the object by a datetime string. the format string expects
     * the format conventions (see FORMAT_* constants above and in Date and Time classes)
     *
     * @param  string $dateTimeStr
     * @param  string $format
     * @return boolean
     */
    public function setDateTimeString($dateTimeStr, $format = false)
    {
        if(!$format)
            return $this->setISODateTimeString($dateTimeStr);

        if(empty($dateTimeStr))
            return false;

        list($Date, $Time) = $this->parseDateTimeString($dateTimeStr, $format);

        return $this->setDateTimeObjects($Date, $Time);
    }
    // End initByTimeString

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets datetime by an array. The array must contain the keys year, month, day, hour, minutes and seconds, optional timezone
     *
     * @param  array $dataArray
     * @return boolean
     */
    public function setDateTimeArray(array $dateTimeArray)
    {
        if(!$this->checkValidDateTimeArray($dateTimeArray))
            return false;

        $Date = new BlibsDate($dateTimeArray);
        $Time = new BlibsTime($dateTimeArray);

        return $this->setDateTimeObjects($Date, $Time);
    }
    // End setDateTimeArray

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Inits the object by a iso time string format YYYY-MM-DD[T]HH24-MM-SS[TZ]
     *
     * @param  string $dateTimeStr
     * @return boolean
     */
    public function setISODateTimeString($dateTimeStr)
    {
        if(empty($dateTimeStr))
            return false;

        list($Date, $Time) = $this->parseISODateTimeString($dateTimeStr);

        return $this->setDateTimeObjects($Date, $Time);
    }
    // End initByISODateTimeString

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Inits the object by an unix timestamp
     *
     * @param  integer $timestamp - optional, if false time() is used
     * @return boolean
     */
    public function setTimestamp($timestamp = false)
    {
        $Date = new BlibsDate($timestamp);
        $Time = new BlibsTime($timestamp);

        return $this->setDateTimeObjects($Date, $Time);
    }
    // End initByTimestamp

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the dateTime parts
     * Standard behaviour is to check the dateTime parts. if any of them is invalid, the method
     * returns false without setting the dateTime.
     *
     * @param  mixed  $year
     * @param  mixed  $month
     * @param  mixed  $day
     * @param  mixed  $hour
     * @param  mixed  $minutes
     * @param  mixed  $seconds
     * @param  mixed  $timezone  - +1:00 or Z or GMT or UTC
     * @return boolean
     */
    public function setDateTimeParts($year = false, $month = false, $day = false, $hour = false, $minutes = false, $seconds = false, $timezone = false)
    {
        $Date = new BlibsDate();
        $Date->setDateParts($year, $month, $day);

        $Time = new BlibsTime();
        $Time->setTimeParts($hour, $minutes, $seconds, $timezone);

        return $this->setDateTimeObjects($Date, $Time);
    }
    // End setDateTimeParts

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets both objects, Date and Time. Date has to be at least a valid object
     *
     * @param  BlibsDate $Date
     * @param  BlibsTime $Time
     * @return boolean
     */
    public function setDateTimeObjects(BlibsDate $Date, BlibsTime $Time)
    {
        if(!is_object($Date) || !$Date->isValid())
        {
            $this->setInvalid();
            return false;
        }

        $this->Date = $Date;
        $this->Time = is_object($Time) && $Time->isValid()? $Time : new BlibsTime();
    }
    // End setDateTimeObjects

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Unsets the Date and Time objects. This makes it invalid
     *
     * @param  -
     * @return -
     */
    public function setInvalid()
    {
        $this->Date = null;
        $this->Time = null;
    }
    // End setInvalid

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns a formated time string
     *
     * @param  string $format
     * @return string
     */
    public function getDateTimeString($format = false, $language = null)
    {
        if(!$this->isValid())
            return '';

        if(!$format)
            return $this->getISODateTimeString();

        if($language)
            $this->Date->setLanguage($language);

        return strtr( $format, [BlibsTime::FORMAT_HOUR_24        => $this->Time->getHour('H'),
                                     BlibsTime::FORMAT_HOUR_24_INT    => $this->Time->getHour('G'),
                                     BlibsTime::FORMAT_HOUR_12        => $this->Time->getHour('h'),
                                     BlibsTime::FORMAT_HOUR_12_INT    => $this->Time->getHour('g'),
                                     BlibsTime::FORMAT_MINUTES        => $this->Time->getMinutes('i'),
                                     BlibsTime::FORMAT_SECONDS        => $this->Time->getSeconds('s'),
                                     BlibsTime::FORMAT_MERIDIEM       => $this->Time->getMeridiem('A'),
                                     BlibsTime::FORMAT_MERIDIEM_LC    => $this->Time->getMeridiem('a'),
                                     BlibsTime::FORMAT_TIMEZONE       => $this->Time->getTimezone('P'),
                                     BlibsTime::FORMAT_TIMEZONE_SMALL => $this->Time->getTimezone('O'),
                                     BlibsDate::FORMAT_YEAR           => $this->Date->getYear('Y'),
                                     BlibsDate::FORMAT_YEAR_SMALL     => $this->Date->getYear('y'),
                                     BlibsDate::FORMAT_MONTH          => $this->Date->getMonth('m'),
                                     BlibsDate::FORMAT_MONTH_INT      => $this->Date->getMonth('n'),
                                     BlibsDate::FORMAT_MONTH_TEXT     => $this->Date->getMonth(BlibsDate::FORMAT_MONTH_TEXT),
                                     BlibsDate::FORMAT_MONTH_FULLTEXT => $this->Date->getMonth(BlibsDate::FORMAT_MONTH_FULLTEXT),
                                     BlibsDate::FORMAT_DAY            => $this->Date->getDay('d'),
                                     BlibsDate::FORMAT_DAY_INT        => $this->Date->getDay('j'),
                                     BlibsDate::FORMAT_DAY_TEXT       => $this->getDay(BlibsDate::FORMAT_DAY_TEXT),
                                     BlibsDate::FORMAT_DAY_FULLTEXT   => $this->getDay(BlibsDate::FORMAT_DAY_FULLTEXT)
                                     ]
                      );
    }
    // End getDateTimeString

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns a iso formated datetime string
     *
     * @param  -
     * @return string
     */
    public function getISODateTimeString($glueT = false)
    {
        if(!$this->isValid())
            return false;

        $glueT = $glueT? 'T' : ' ';

        return $this->Date->getISODateString() . $glueT . $this->Time->getISOTimeString();
    }
    // End getISODateTimeString

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns a datetime array
     *
     * @param  -
     * @return array
     */
    public function getDateTimeArray()
    {
        if(!$this->isValid())
            return false;

        return array_merge($this->Date->getDateArray(), $this->Time->getTimeArray());
    }
    // End getDateTimeArray

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns a unix timestamp if possible
     *
     * @param  -
     * @return int/false
     */
    public function getUnixtime()
    {
        if(!$this->isValid())
            return false;

        return mktime($this->Time->getHour(),
                      $this->Time->getMinutes(),
                      $this->Time->getSeconds(),
                      $this->Date->getMonth(),
                      $this->Date->getDay(),
                      $this->Date->getYear()
                      );
    }
    // End getUnixtime

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the year part of the date, if either format is empty or
     * contains a year format specifier
     *
     * @param  string $format
     * @return string
     */
    public function getYear($format = false)
    {
        if(!$this->isValid())
            return false;

        return $this->Date->getYear($format);
    }
    // End getYear

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the month part of the date, if either format is empty or contains a month format specifier
     *
     * @param  string $format - output format
     * @return mixed
     */
    public function getMonth($format = false)
    {
        if(!$this->isValid())
            return false;

        return $this->Date->getMonth($format);
    }
    // End getMonth

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the month part of the date as String according to the given language
     *
     * @param  string $language - language
     * @return mixed
     */
    public function getMonthString($language = "de")
    {
        if(!$this->isValid())
            return false;

        return $this->Date->getMonthString($language);
    }
    // End getMonthString

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the day part of the date, if either format is empty or
     * contains a day format specifier
     *
     * @param  string $format
     * @return mixed
     */
    public function getDay($format = false)
    {
        if(!$this->isValid())
            return false;

        return $this->Date->getDay($format);
    }
    // End getDay

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the hour part of the time, if either format is empty or
     * contains a hour format specifier
     *
     * @param  string $format
     * @return string
     */
    public function getHour($format = false)
    {
        if(!$this->isValid())
            return false;

        return $this->Time->getHour($format);
    }
    // End getHour

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the meridiem part
     *
     * @param  string $format
     * @return string
     */
    public function getMeridiem($format = false)
    {
        if(!$this->isValid())
            return false;

        return $this->Time->getMeridiem($format);
    }
    // End getMeridiem

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the minutes part of the time, if either format is empty or contains a minutes format specifier
     *
     * @param  string $format - output format
     * @return mixed
     */
    public function getMinutes($format = false)
    {
        if(!$this->isValid())
            return false;

        return $this->Time->getMinutes($format);
    }
    // End getMinutes

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the seconds part of the time, if either format is empty or
     * contains a seconds format specifier
     *
     * @param  string $format
     * @return mixed
     */
    public function getSeconds($format = false)
    {
        if(!$this->isValid())
            return false;

        return $this->Time->getSeconds($format);
    }
    // End getSeconds

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the timezone
     *
     * @param  string $format
     * @return string
     */
    public function getTimezone($format = false)
    {
        if(!$this->isValid())
            return false;

        return $this->Time->getTimezone($format);
    }
    // End getTimezone

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the Date object
     *
     * @param  -
     * @return Date
     */
    public function getDate()
    {
        if(!$this->isValid())
            return new BlibsDate();

        return $this->Date;
    }
    // End getDate

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the Time object
     *
     * @param  -
     * @return Time
     */
    public function getTime()
    {
        if(!$this->isValid())
            return new BlibsTime();

        return $this->Time;
    }
    // End getDate

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * returns the weekNumber of the first week in the month
     */
    public function getFirstWeekOfMonth()
    {
        return $this->Date->getFirstWeekOfMonth();
    }
    // End getFirstWeekOfMonth

    ///////////////////////////////////////////////////////////////////////////

    /**
     * returns the weekNumber of the last week in the month
     */
    public function getLastWeekOfMonth()
    {
        return $this->Date->getLastWeekOfMonth();
    }
    // End getLastWeekOfMonth

    ///////////////////////////////////////////////////////////////////////////

    /**
     * Checks if the BlibsDateTime object represents a valid. this is at least when
     * Date is a valid object
     *
     * @param  -
     * @return boolean
     */
    public function isValid()
    {
        return is_object($this->Date) && $this->Date->isValid();
    }
    // End isValid

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns true if the date is today
     *
     * @param  -
     * @return boolean
     */
    public function isToday()
    {
        return $this->Date->isToday();
    }
    // End isToday

    //////////////////////////////////////////////////////////////////////////////////////
    // static
    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * return a DateTime-Object by a given weekNumber
     *
     * @param  int $weekNumber
     * @param  int $day
     * @param  int $year
     * @return BlibsDateTime
     */
    public static function getDateTimeByWeekNumber($weekNumber, $day = 1, $year = false)
    {
        $Date = BlibsDate::getDateByWeekNumber($weekNumber, $day, $year);
        $Time = new BlibsTime(['hour' => 0, 'minutes' => 0, 'seconds' => 0]);

        $DateTime = new BlibsDateTime();
        $DateTime->setDateTimeParts($Date, $Time);

        return $DateTime;
    }
    // End getDateTimeByWeekNumber

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Overwrites the php-method __toString for converting an object to string
     *
     * @param  --
     * @return string
     */
    public function __toString()
    {
        return (string)$this->getISODateTimeString();
    }
    // End __toString

    //////////////////////////////////////////////////////////////////////////////////////
    // protected
    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Parses a datetime string by a given format
     *
     * not really performant
     *
     * @param  string $dateTimeStr
     * @param  string $format
     * @return array
     */
    protected function parseDateTimeString($dateTimeStr, $format)
    {
        $found   = [];
        $pattern = '';

        for($position = 0; $position < utf8_strlen($format); $position++)
        {
            switch($format{$position})
            {
                case  BlibsDate::FORMAT_YEAR:
                    $found[]  = BlibsDate::FORMAT_YEAR;
                    $pattern .= '(\d{4})';
                    break;

                case BlibsDate::FORMAT_YEAR_SMALL:
                    $found[]  = BlibsDate::FORMAT_YEAR_SMALL;
                    $pattern .= '(\d{2})';
                    break;

                case BlibsDate::FORMAT_MONTH:
                    $found[]  = BlibsDate::FORMAT_MONTH;
                    $pattern .= '(\d{2})';
                    break;

                case BlibsDate::FORMAT_MONTH_INT:
                    $found[]  = BlibsDate::FORMAT_MONTH_INT;
                    $pattern .= '(\d{1,2})';
                    break;

                case BlibsDate::FORMAT_MONTH_FULLTEXT:
                    $found[]  = BlibsDate::FORMAT_MONTH_FULLTEXT;
                    $pattern .= '(\w+?)';
                    break;

                case BlibsDate::FORMAT_MONTH_TEXT:
                    $found[]  = BlibsDate::FORMAT_MONTH_TEXT;
                    $pattern .= '(\w{3})';
                    break;

                case BlibsDate::FORMAT_DAY:
                    $found[]  = BlibsDate::FORMAT_DAY;
                    $pattern .= '(\d{2})';
                    break;

                case BlibsDate::FORMAT_DAY_INT:
                    $found[]  = BlibsDate::FORMAT_DAY_INT;
                    $pattern .= '(\d{1,2})';
                    break;

                case BlibsDate::FORMAT_DAY_TEXT:
                    $pattern .= '\w{3}';
                    break;

                case BlibsDate::FORMAT_DAY_FULLTEXT:
                    $pattern .= '\w+?';
                    break;

                case BlibsDate::FORMAT_UNIXTIME:
                    $found[]  = BlibsDate::FORMAT_UNIXTIME;
                    $pattern .= '(\d+)';
                    break;

                case BlibsTime::FORMAT_HOUR_24:
                    $found[]  = BlibsTime::FORMAT_HOUR_24;
                    $pattern .= '(\d{2})';
                    break;

                case BlibsTime::FORMAT_HOUR_12:
                    $found[]  = BlibsTime::FORMAT_HOUR_12;
                    $pattern .= '(\d{2})';
                    break;

                case BlibsTime::FORMAT_HOUR_24_INT:
                    $found[]  = BlibsTime::FORMAT_HOUR_24_INT;
                    $pattern .= '(\d{1,2})';
                    break;

                case BlibsTime::FORMAT_HOUR_12_INT:
                    $found[]  = BlibsTime::FORMAT_HOUR_12_INT;
                    $pattern .= '(\d{1,2})';
                    break;

                case BlibsTime::FORMAT_MERIDIEM:
                    $found[]  = BlibsTime::FORMAT_MERIDIEM;
                    $pattern .= '(AM|PM)';
                    break;

                case BlibsTime::FORMAT_MERIDIEM_LC:
                    $found[]  = BlibsTime::FORMAT_MERIDIEM_LC;
                    $pattern .= '(am|pm)';
                    break;

                case BlibsTime::FORMAT_MINUTES:
                    $found[]  = BlibsTime::FORMAT_MINUTES;
                    $pattern .= '(\d{2})';
                    break;

                case BlibsTime::FORMAT_SECONDS:
                    $found[]  = BlibsTime::FORMAT_SECONDS;
                    $pattern .= '(\d{2})\.?\d{0,6}';
                    break;

                case BlibsTime::FORMAT_TIMEZONE:
                    $found[]  = BlibsTime::FORMAT_TIMEZONE;
                    $pattern .= '([+\-]\d{2}:?\d{0,2}|\s?Z|\s?GMT|\s?UTC|\s?z|\s?gmt|\s?utc)';
                    break;

                case BlibsTime::FORMAT_TIMEZONE_SMALL:
                    $found[]  = BlibsTime::FORMAT_TIMEZONE_SMALL;
                    $pattern .= '([+\-]\d{4}|\s?Z|\s?GMT|\s?UTC|\s?z|\s?gmt|\s?utc)';
                    break;

                case '.':
                case '/':
                case '(':
                case ')':
                case '-':
                    $pattern .= '\\'.$format{$position};
                    break;

                default:
                    $pattern .= $format{$position};
                    break;
            }
        }

        $year = $month = $day = $hour = $meridiem = $minutes = $seconds = $timezone = null;
        $language = 'de';

        if(preg_match("/".$pattern."/u", $dateTimeStr, $matches))
        {
            foreach($found as $index => $format)
            {
                switch($format)
                {
                    case BlibsDate::FORMAT_YEAR:
                        $year = $matches[$index + 1];
                        break;

                    case BlibsDate::FORMAT_YEAR_SMALL:
                        $year = $matches[$index + 1] < 50? 2000 + $matches[$index + 1] : 1900 + $matches[$index + 1];
                        break;

                    case BlibsDate::FORMAT_MONTH:
                    case BlibsDate::FORMAT_MONTH_INT:
                        $month = $matches[$index + 1];
                        break;

                    case BlibsDate::FORMAT_MONTH_FULLTEXT:
                        $monthStr = $matches[$index + 1];
                        $monthMap = BlibsDate::getMonthMapping();

                        $month = false;
                        foreach($monthMap as $lang => $mapping)
                            if($month = array_search($monthStr, $mapping) !== false)
                            {
                                $language = $lang;
                                break;
                            }

                        break;

                    case BlibsDate::FORMAT_MONTH_TEXT:
                        $monthStr = $matches[$index + 1];
                        $monthMap = BlibsDate::getMonthMapping();

                        $month = false;
                        foreach($monthMap as $lang => $mapping)
                        {
                            foreach($mapping as $no => $fullMonthStr)
                            {
                                if($monthStr == utf8_substr($fullMonthStr, 0, 3))
                                {
                                    $month = $no;
                                    $language = $lang;
                                    break;
                                }
                            }
                        }
                        break;

                    case BlibsDate::FORMAT_DAY:
                    case BlibsDate::FORMAT_DAY_INT:
                        $day = $matches[$index + 1];
                        break;

                    case BlibsDate::FORMAT_UNIXTIME:
                        $dt = getDate($matches[$index + 1]);

                        $day   = $dt['mday'];
                        $month = $dt['mon'];
                        $year  = $dt['year'];
                        break;

                    case BlibsTime::FORMAT_HOUR_24:
                    case BlibsTime::FORMAT_HOUR_24_INT:
                        $hour = $matches[$index + 1];
                        break;

                    case BlibsTime::FORMAT_HOUR_12:
                    case BlibsTime::FORMAT_HOUR_12_INT:
                        $hour = $matches[$index + 1];
                        $meridiem = 'AM';
                        break;

                    case BlibsTime::FORMAT_MINUTES:
                        $minutes = $matches[$index + 1];
                        break;

                    case BlibsTime::FORMAT_SECONDS:
                        $seconds = $matches[$index + 1];
                        break;

                    case BlibsTime::FORMAT_MERIDIEM:
                    case BlibsTime::FORMAT_MERIDIEM_LC:
                        $meridiem = $matches[$index + 1];
                        break;

                    case BlibsTime::FORMAT_TIMEZONE:
                    case BlibsTime::FORMAT_TIMEZONE_SMALL:
                        $timezone = $matches[$index + 1];
                        break;
                }
            }
        }

        /**
         * Normalize hour
         */
        if($meridiem &&
           utf8_strtolower($meridiem) == 'pm')
            $hour = 12 + $hour % 12;

        $year     = is_numeric($year)?    (int)$year    : null;
        $month    = is_numeric($month)?   (int)$month   : null;
        $day      = is_numeric($day)?     (int)$day     : null;
        $hour     = is_numeric($hour)?    (int)$hour    : null;
        $minutes  = is_numeric($minutes)? (int)$minutes : null;
        $seconds  = is_numeric($seconds)? (int)$seconds : null;
        $timezone = $timezone?            $timezone     : null;

        $Date = new BlibsDate();
        $Date->setLanguage($language);
        $Date->setDateParts($year, $month, $day);

        $Time = new BlibsTime();
        $Time->setTimeParts($hour, $minutes, $seconds, $timezone);

        return [$Date, $Time];
    }
    // End parseDateTimeString

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Parses a iso time string format
     *
     * @param  string $timestr
     * @return array
     */
    protected function parseISODateTimeString($dateTimeStr)
    {
        $Date = new BlibsDate($dateTimeStr, 'Y-m-d');
        $Time = new BlibsTime($dateTimeStr, 'H:i:sP');

        /**
         * If Time fails, try again without timezone format
         */
        if(!$Time->isValid())
            $Time->setTimeString('H:i:s');

        return [$Date, $Time];
    }
    // End parseISODateTimeString

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Checks if the date time array is valid. This is the case if at least the date parts
     * are valid
     *
     * @param  array  $dateTimeArray
     * @return boolean
     */
    protected function checkValidDateTimeArray($dateTimeArray)
    {
        $Date = new BlibsDate($dateTimeArray);

        return $Date->isValid();
    }
    // End checkValidDateTimeArray

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns time duration as list of segments like (1 year, 2 month, 2 weeks, 3 days, 4 hours, 1 minute, 12 seconds)
     * Watch out leapyears if you need exact values for month and year.
     * The month and year values in period array are the number of seconds in a solar month and year respectively.
     * In most cases you shouldn't care if an event "2 months, 3 days" in the future differs from the calendar
     * interpretation by a day or two.
     *
     * @param int $seconds     - number of seconds elapsed
     * @param string $segments - which time periods to display. (Default: 'yMwdhms', exp. 'd' display only days! Klingelt's?)
     * @param bool $zeros      - whether to show zero time periods
     *
     * @return boolean
     */
    public static function timeDuration($seconds, $use = null, $zeros = false)
    {
        // Define time periods
        $periods = [
            'years'     => 31556926,
            'Months'    => 2629743,
            'weeks'     => 604800,
            'days'      => 86400,
            'hours'     => 3600,
            'minutes'   => 60,
            'seconds'   => 1
            ];

        // Break into periods
        $seconds = (float) $seconds;
        $segments = [];
        foreach ($periods as $period => $value) {
            if ($use && strpos($use, $period[0]) === false) {
                continue;
            }
            $count = floor($seconds / $value);
            if ($count == 0 && !$zeros) {
                continue;
            }
            $segments[strtolower($period)] = $count;
            $seconds = $seconds % $value;
        }

        return $segments;
    }

    //////////////////////////////////////////////////////////////////////////////////////
}
// End class BlibsDateTime
