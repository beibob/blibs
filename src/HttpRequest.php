<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs;

use Beibob\Blibs\Interfaces\Response;

/**
 * HttpRequest accessor
 *
 * @package blibs
 * @author Tobias Lode <tobias@beibob.de>
 * @author Fabian Möller <fab@beibob.de>
 *
 */
class HttpRequest extends RequestBase
{
    /**
     * HTTP methods
     */
    const METHOD_GET  = 'GET';
    const METHOD_POST = 'POST';

    /**
     * Request method
     */
    private $method;

    /**
     * Request url
     */
    private $requestUrl;

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Constructs the instance
     *
     * @param -
     */
    public function __construct(array $request = null)
    {
        $this->method = mb_strtoupper($_SERVER['REQUEST_METHOD']);
        parent::__construct(!is_null($request)? $request : ($this->method == self::METHOD_POST? $_POST : $_GET));
    }
    // End __construct

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the http method
     *
     * @param  -
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }
    // End getMethod

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns true on GET method
     *
     * @param  -
     * @return string
     */
    public function isGet()
    {
        return $this->method == self::METHOD_GET;
    }
    // End isGet

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns true on POST method
     *
     * @param  -
     * @return string
     */
    public function isPost()
    {
        return $this->method == self::METHOD_POST;
    }
    // End isPost

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns true on GET method
     *
     * @param  -
     * @return string
     */
    public function isMethodGet()
    {
        return $this->method == self::METHOD_GET;
    }
    // End isMethodGet

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns true on POST method
     *
     * @param  -
     * @return string
     */
    public function isMethodPost()
    {
        return $this->method == self::METHOD_POST;
    }
    // End isMethodPost

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the HTTP_HOST
     *
     * @param  -
     * @return string
     */
    public function getHost()
    {
        return isset($_SERVER['HTTP_HOST'])? $_SERVER['HTTP_HOST'] : null;
    }
    // End getHost

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the HTTP_USER_AGENT
     *
     * @param  -
     * @return string
     */
    public function getUserAgent()
    {
        return isset($_SERVER['HTTP_USER_AGENT'])? $_SERVER['HTTP_USER_AGENT'] : null;
    }
    // End getUserAgent

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the HTTP_REFERER
     *
     * @param  -
     * @return string
     */
    public function getReferer()
    {
        return isset($_SERVER['HTTP_REFERER'])? $_SERVER['HTTP_REFERER'] : null;
    }
    // End getReferer

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns a http header
     *
     * @param  string $header
     * @return string
     */
    public function getHttpHeader($header)
    {
        $phpHeaderConst = 'HTTP_'.strtr(strtoupper($header), '-', '_');
        return isset($_SERVER[$phpHeaderConst])? $_SERVER[$phpHeaderConst] : null;
    }
    // End getHttpHeader

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the HTTP_ACCEPT
     *
     * @param  -
     * @return string
     */
    public function getAccept()
    {
        return isset($_SERVER['HTTP_ACCEPT'])? $_SERVER['HTTP_ACCEPT'] : null;
    }
    // End getAccept

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the HTTP_ACCEPT_LANGUAGE
     *
     * @param  -
     * @return string
     */
    public function getAcceptLanguage()
    {
        return Environment::getInstance()->getAcceptLanguage();
    }
    // End getAcceptLanguage

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the HTTP_ACCEPT_LANGUAGE as a parsed array
     *
     * @param  -
     * @return array
     */
    public function getAcceptLanguageInformation()
    {
        Environment::getInstance()->getAcceptLanguageInformation();
    }
    // End getAcceptLanguageInformation

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the HTTP_ACCEPT_ENCODING
     *
     * @param  -
     * @return string
     */
    public function getAcceptEncoding()
    {
        return isset($_SERVER['HTTP_ACCEPT_ENCODING'])? $_SERVER['HTTP_ACCEPT_ENCODING'] : null;
    }
    // End getAcceptEncoding

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the HTTP_ACCEPT_CHARSET
     *
     * @param  -
     * @return string
     */
    public function getAcceptCharset()
    {
        return isset($_SERVER['HTTP_ACCEPT_CHARSET'])? $_SERVER['HTTP_ACCEPT_CHARSET'] : null;
    }
    // End getAcceptCharset

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the HTTP_KEEP_ALIVE
     *
     * @param  -
     * @return string
     */
    public function getKeepAlive()
    {
        return isset($_SERVER['HTTP_KEEP_ALIVE'])? $_SERVER['HTTP_KEEP_ALIVE'] : null;
    }
    // End getKeepAlive

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the HTTP_CONNECTION
     *
     * @param  -
     * @return string
     */
    public function getConnection()
    {
        return isset($_SERVER['HTTP_CONNECTION'])? $_SERVER['HTTP_CONNECTION'] : null;
    }
    // End getConnection

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the HTTP_COOKIE
     *
     * @param  -
     * @return array
     */
    public function getCookie()
    {
        return $_COOKIE;
    }
    // End getCookie

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the HTTP_IF_MODIFIED_SINCE
     *
     * @param  -
     * @return integer
     */
    public function getIfModifiedSince()
    {
        return isset($_SERVER['HTTP_IF_MODIFIED_SINCE'])? strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE']) : null;
    }
    // End getIfModifiedSince

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the HTTP_IF_NONE_MATCH
     *
     * @param  -
     * @return string
     */
    public function getIfNoneMatch()
    {
        return isset($_SERVER['HTTP_IF_NONE_MATCH'])? $_SERVER['HTTP_IF_NONE_MATCH'] : null;
    }
    // End getIfNoneMatch

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the HTTP_CACHE_CONTROL
     *
     * @param  -
     * @return string
     */
    public function getCacheControl()
    {
        return isset($_SERVER['HTTP_CACHE_CONTROL'])? $_SERVER['HTTP_CACHE_CONTROL'] : null;
    }
    // End getCacheControl

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the SERVER_PROTOCOL
     *
     * @param   -
     * @return  string
     */
    public function getServerProtocol()
    {
        return isset($_SERVER['SERVER_PROTOCOL'])? $_SERVER['SERVER_PROTOCOL'] : null;
    }
    // End getServerProtocol

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the REMOTE_ADDR
     *
     * @param   -
     * @return  string
     */
    public function getRemoteAddress()
    {
        return isset($_SERVER['REMOTE_ADDR'])? $_SERVER['REMOTE_ADDR'] : null;
    }
    // End getRemoteAddress

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the URI
     *
     * @return  string
     */
    public function getUri()
    {
        return ($url = $this->getRequestUri())? $url : $this->getRedirectUrl();
    }
    // End getUri

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the REQUEST_URI
     *
     * @return  string
     */
    public function getRequestUri()
    {
        return isset($_SERVER['REQUEST_URI'])? $_SERVER['REQUEST_URI'] : null;
    }
    // End getRequestUri

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the REDIRECT_URL
     *
     * @param   -
     * @return  string
     */
    public function getRedirectUrl()
    {
        return isset($_SERVER['REDIRECT_URL'])? $_SERVER['REDIRECT_URL'] : null;
    }
    // End getRedirectURL

    //////////////////////////////////////////////////////////////////////////////////////
    // Cache validation methods
    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Validates the cache header Last-Modified
     *
     * @param  int      $modTimestamp
     * @param  Response $Response
     * @return boolean
     */
    public function validateCacheHeaderLastModified($modTimestamp, Response $Response)
    {
        if($this->getIfModifiedSince() == $modTimestamp)
        {
            /**
             * Return http status 304 and exit
             */
            $Response->unsetContent();
            $Response->setHeader('HTTP/1.1 304 Not Modified');
            $Response->send();
            exit();
        }

        /**
         * Add Last-Modified header
         */
        $Response->setHeader('Last-Modified: '. date('r', $modTimestamp));
    }
    // End validateCacheHeaders

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Validates the cache header ETag
     *
     * @param  string   $eTag
     * @param  Response $Response
     * @return boolean
     */
    public function validateCacheHeaderETag($eTag, Response $Response)
    {
        if($this->getIfNoneMatch() == $eTag)
        {
            /**
             * Return http status 304 and exit
             */
            $Response->unsetContent();
            $Response->setHeader('HTTP/1.1 304 Not Modified');
            $Response->send();
            exit();
        }

        /**
         * Add ETag header
         */
        $Response->setHeader('ETag: '. $eTag);
    }
    // End validateCacheHeaders

    //////////////////////////////////////////////////////////////////////////////////////
}
// End class HttpRequest
