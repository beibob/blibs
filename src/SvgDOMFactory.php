<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs;

use DOMDocument;

/**
 * A SvgDOMFactory class
 *
 * @package blibs
 * @author Tobias Lode <tobias@beibob.de>
 * @author Fabian Möller <fab@beibob.de>
 *
 */
class SvgDOMFactory
{
    /**
     * Current document
     */
    private $Document;



    /**
     * Constructor
     *
     * @param  DOMDocument $DOMDocument
     * @return SvgDOMFactory
     */
    public static function factory(DOMDocument $DOMDocument)
    {
        return new SvgDOMFactory($DOMDocument);
    }
    // End factory



    /**
     * Constructor
     *
     * @param  DOMDocument $DOMDocument
     * @return SvgDOMFactory
     */
    public function __construct(DOMDocument $DOMDocument)
    {
        $this->Document = $DOMDocument;
    }
    // End __construct



    /**
    * getDefs
    * create and return a defs tag
    *
    * @return DOMElement
    */
    public function getDefs()
    {
        return $this->Document->createElement('defs');
    }
    // End getDefs



    /**
     * getImage
     * create and return a image tag
     *
     * @param integer $x
     * @param integer $y
     * @param integer $width
     * @param integer $heigth
     * @param string  $href
     * @param array   $attributes
     * @return DOMElement
     */
    public function getImage($x, $y, $width ,$heigth, $href, array $attributes = [])
    {
        $attributes['xlink:href'] = $href;
        $attributes['x'] = $x;
        $attributes['y'] = $y;
        $attributes['width'] = $width;
        $attributes['height'] = $heigth;

        return $this->Document->createElement('image', null, $attributes);
    }
    // End getImage



    /**
     * getPattern
     * create and return a pattern tag
     *
     *
     * @param string $id
     * @param integer $x
     * @param integer $y
     * @param integer $width
     * @param integer $heigth
     * @param array $attributes
     * @return  DOMElement
     */
    public function getPattern($id, $x, $y, $width ,$heigth, array $attributes = [])
    {
        $attributes['id'] = $id;
        $attributes['x'] = $x;
        $attributes['y'] = $y;
        $attributes['width'] = $width;
        $attributes['height'] = $heigth;
        $attributes['patternUnits'] = 'userSpaceOnUse';

        return $this->Document->createElement('pattern', null, $attributes);
    }
    // End getPattern



    /**
     * getRect
     * create and return a rect tag
     *
     * @param integer $x
     * @param integer $y
     * @param integer $width
     * @param integer $heigth
     * @param array $attributes
     * @return DOMElement
     */
    public function getRect($x, $y, $width ,$heigth, array $attributes = [])
    {
        $attributes['x'] = $x;
        $attributes['y'] = $y;
        $attributes['width'] = $width;
        $attributes['height'] = $heigth;

        return $this->Document->createElement('rect', null, $attributes);
    }
    // End getRect



    /**
     * getSvg
     * create and return a svg tag
     *
     * @param array $attributes
     * @return svg tag
     */
    public function getSvg(array $attributes = [])
    {
        $attributes['xmlns'] = 'http://www.w3.org/2000/svg';
        $attributes['version'] = '1.1';
        $attributes['xmlns:xlink'] = 'http://www.w3.org/1999/xlink';

        return $this->Document->createElement('svg', null, $attributes);
    }
    // End getSvg



    /**
     * getLine
     * create and return a line tag
     *
     * @param $x1
     * @param $y1
     * @param $x2
     * @param $y2
     * @param array $attributes
     * @return line
     */
    public function getLine($x1, $y1, $x2, $y2, $attributes = [])
    {
        $attributes['x1'] = $x1;
        $attributes['y1'] = $y1;
        $attributes['x2'] = $x2;
        $attributes['y2'] = $y2;
        $attributes['stroke'] = '#000000';

        return $this->Document->createElement('line', null, $attributes);
    }
    // End getLine



    /**
     * getCircle
     * create and return a circle tag
     *
     * @param $cx
     * @param $cy
     * @param $r
     * @param array $attributes
     * @return DOMElement
     */
    public function getCircle($cx, $cy, $r, $attributes = [])
    {
        $attributes['cx'] = $cx;
        $attributes['cy'] = $cy;
        $attributes['r'] = $r;
        $attributes['stroke'] = '#000000';

        return $this->Document->createElement('circle', null, $attributes);
    }
    // End getCircle



    /**
     * getText
     * create and return a text tag
     *
     * @param $text
     * @param $x
     * @param $y
     * @param array $attributes
     * @return DOMElement
     */
    public function getText($text, $x, $y, $attributes = [])
    {
        $attributes['x'] = $x;
        $attributes['y'] = $y;
        $attributes['fill'] = '#000000';

        return $this->Document->createElement('text', $text, $attributes);
    }
    // End getText



    /**
     * getTSpan
     * create and return a tspan tag
     *
     * @param $text
     * @param $x
     * @param $y
     * @param array $attributes
     * @return DOMElement
     */
    public function getTSpan($text, $x, $y, $attributes = [])
    {
        $attributes['x'] = $x;
        $attributes['y'] = $y;

        return $this->Document->createElement('tspan', $text, $attributes);
    }
    // End getTSpan



    /**
     * getGroup
     * create and return a tspan tag
     *
     * @param array $attributes
     * @return DOMElement
     */
    public function getGroup($attributes = [])
    {
        return $this->Document->createElement('g', null, $attributes);
    }
    // End getGroup



    /**
     * Rotate
     *
     * @param $angle
     * @param $x
     * @param $y
     * @return string
     */
    public function rotate($angle, $x = 0, $y = null)
    {
        if(is_null($y))
            $y = $x;

        return 'rotate('. $angle.' '. $x .' '. $y .')';
    }
    // End rotate



    /**
     * Translate
     *
     * @param $x
     * @param $y
     * @return string
     */
    public function translate($x = 0, $y = null)
    {
        if(is_null($y))
            $y = $x;

        return 'translate('. $x .' '. $y .')';
    }
    // End translate



    /**
     * Scale
     *
     * @param $x
     * @param $y
     * @return string
     */
    public function scale($x = 0, $y = null)
    {
        if(is_null($y))
            $y = $x;

        return 'scale('. $x .' '. $y .')';
    }
    // End scale

}
// End SvgDOMFactory
