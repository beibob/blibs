<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs;

/**
 * Global Bootstrap class
 *
 * @package blibs
 * @author Fabian Möller  <fab@beibob.de>
 * @author Tobias Lode <tobias@beibob.de>
 *
 */

/**
 * Supported application type constants
 *
 * These constants may be used in the config.ini file!
 */
use Beibob\Blibs\Interfaces\Request;
use Beibob\Blibs\Interfaces\Response;
use PDO;
use PDOException;

define('APP_TYPE_WEB', 'web');
define('APP_TYPE_CLI', 'cli');

/**
 * Severities
 *
 * These constants may be used in the config.ini file!
 */
define('LOG_NONE',  0x00);
define('LOG_DBG',   0x01);
define('LOG_NOTE',  0x02);
define('LOG_WARN',  0x04);
define('LOG_ERROR', 0x08);
define('LOG_FATAL', 0x10);
define('LOG_ALL',   0xff);

/**
 * AutoLoader can't find itself
 */
//require_once('AutoLoader.cls.php');

class Bootstrap
{
    /**
     * Default appDir relative blibs path
     */
//    const LIB_CORE_DIR      = 'lib/blibs/';
//    const LIB_CORE_INTF_DIR = 'lib/blibs/interfaces/';

    /**
     * Config file path and name
     */
    const CONFIG_DIR  = 'etc/';
    const CONFIG_NAME = 'config.ini';
    const CONFIG_LOCAL_NAME = 'config.local.ini';

    /**
     * default time zone
     */
    const DEFAULT_TIME_ZONE = 'Europe/Berlin';

    /**
     * Cached include paths array
     */
    private static $includePaths = [];

    /**
     * Enviroment
     */
    private $Environment;

    /**
     * Application dir
     */
    private $appDir;

    /**
     * Project base dir
     */
    private $baseDir;

    /**
     * Config
     */
    private $Config;

    /**
     * Start time
     */
    private $startTime;

    /**
     * Output buffering state
     */
    private $outputBuffering = false;

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Constructor
     *
     * @param  string $baseDir
     * @return Bootstrap
     */
    public function __construct($baseDir)
    {
        /**
         * UTF-8
         */
        if (PHP_VERSION_ID < 50600) {
            mb_internal_encoding("UTF-8");
            mb_regex_encoding("UTF-8");
            iconv_set_encoding("input_encoding", "UTF-8");
            iconv_set_encoding("internal_encoding", "UTF-8");
            iconv_set_encoding("output_encoding", "UTF-8");
        }
        require_once('contrib/utf8/utf8.php');

        /**
         * Register static auto load method
         */
        //spl_autoload_register(['blibs\AutoLoader', 'resolve']);
        set_exception_handler(['Beibob\Blibs\Bootstrap', 'exceptionHandler']);

        /**
         * Init some pathes
         *
         * Bootstrap is located in basedir/vendorDir/beibob/blibs/src/Bootstrap.cls.php.
         */
        $this->baseDir = $baseDir;
        $this->appDir = $baseDir . DIRECTORY_SEPARATOR . 'app';

        /**
         * Init environment
         */
        $this->Environment = Environment::getInstance();

        /**
         * Try loading the config file. Searches in baseDir/etc first and
         * then looks in appDir/etc for a default config file
         */
        $this->searchAndLoadConfig();

        /**
         * Application type
         */
        $this->Environment->setApplicationType($this->getAppType());

        /**
         * Sets some php ini variables
         */
        if(isset($this->Config->php->time_limit))
            set_time_limit((int)$this->Config->php->time_limit);

        if(isset($this->Config->php->display_errors))
            ini_set('display_errors', $this->Config->php->display_errors? 'On' : 'Off');

        if(isset($this->Config->php->error_reporting))
            ini_set('error_reporting', $this->Config->php->error_reporting);

        if(isset($this->Config->php->default_time_zone))
            date_default_timezone_set($this->Config->php->default_time_zone);
        else
            date_default_timezone_set(self::DEFAULT_TIME_ZONE);


        /**
         * Init dependency injection
         */
        $this->initContainer();

        /**
         * Init logging
         */
        $this->initLogging();

        /**
         * Capture startTime
         */
        if(isset($this->Config->bootstrap->showRunTime) && $this->Config->bootstrap->showRunTime)
        {
            $this->startTime = self::getMicroTime();

            $startMsg = sprintf('%s%s %s',
                                Environment::isXmlHttpRequest()? 'xhr.' : '',
                                $_SERVER['REQUEST_METHOD'],
                                $_SERVER['REQUEST_URI']);

            Log::getDefault()->notice($startMsg);
        }

        /**
         * Init database if needed
         */
        $this->initDatabases();

        /**
         * Load required modules and libs
         */
        $this->initRequiredModules();

        /**
         * Start output buffer
         */
        $this->startOutputBuffer();

        /**
         * Init session if needed
         */
        $this->initSession();
    }
    // End __construct

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Destructor
     *
     * @param  -
     * @return -
     */
    public function __destruct()
    {
        $this->closeSession();

        /**
         * Show runTime
         */
        if(isset($this->Config->bootstrap->showRunTime) && $this->Config->bootstrap->showRunTime)
            $this->printRunTime();

        /**
         * Flush everything
         */
        $this->endFlushOutputBuffer();
    }
    // End __destruct

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Runs the application
     *
     * @param  -
     * @return -
     */
    public function run(Request $Request = null, Response $Response = null, $responseSendType = null)
    {
        /**
         * Choose Request and Response types from applicationType
         */
        switch($this->getAppType())
        {
            case APP_TYPE_CLI:
                $Request  = is_null($Request)?  new CliRequest()  : $Request;
                $Response = is_null($Response)? new CliResponse() : $Response;
                break;

            case APP_TYPE_WEB:
                $Request  = is_null($Request)?  new HttpRequest()  : $Request;
                $Response = is_null($Response)? new HttpResponse() : $Response;
                break;
        }

        if (!is_null($responseSendType))
            $Response->setSendType($responseSendType);

        /**
         * Hand-off to the FrontController
         */
        $FrontController = FrontController::getInstance($this->getEnvironment());

        /**
         * Register the filters
         */
        $this->initFilters($FrontController);

        /**
         * Handle request
         */
        return $FrontController->handleRequest($Request, $Response);
    }
    // End run

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Set include pathes for modules
     *
     * @param  string $ident
     * @return -
     */
    public function requireModule($ident)
    {
        $this->getEnvironment()->registerModule($ident);
    }
    // End requireModule

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the COnfig
     *
     * @param  -
     * @return Config
     */
    public function getConfig()
    {
        return $this->Config;
    }
    // End getConfig

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the Environment
     *
     * @param
     * @return Environment
     */
    public function getEnvironment()
    {
        return $this->Environment;
    }
    // End getEnviroment

    //////////////////////////////////////////////////////////////////////////////////////
    // protected
    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the application type
     *
     * @param  -
     * @return string
     */
    protected function getAppType()
    {
        $appType = $this->Config->bootstrap->applicationType;
        //        if (isset($_ENV['APP_TYPE']))
        //            $appType = $_ENV['APP_TYPE'];
        return $appType;
    }
    // End getAppType

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Search config file and load it
     *
     * @param  -
     * @return Config
     */
    protected function searchAndLoadConfig()
    {
        $configPaths = [];
        $configPaths[] = $this->baseDir . DIRECTORY_SEPARATOR . self::CONFIG_DIR . self::CONFIG_NAME;
        $configLocalPath = $this->baseDir . DIRECTORY_SEPARATOR . self::CONFIG_DIR . self::CONFIG_LOCAL_NAME;

        if (\file_exists($configLocalPath)) {
            $configPaths[] = $configLocalPath;
            $this->Config = new ConfigIni($configPaths, $this->Environment->getName());
        }

        $this->Config->configDir = $this->baseDir . DIRECTORY_SEPARATOR . self::CONFIG_DIR;
        $this->Config->appDir  = $this->appDir;
        $this->Config->baseDir = $this->baseDir;
        $this->Config->docRoot = dirname($_SERVER['SCRIPT_FILENAME']);

        $this->Environment->registerConfig($this->Config);
    }
    // End searchAndLoadConfig

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Inits the Session if needed
     *
     * @param  -
     * @return -
     */
    protected function initSession()
    {
        if(!is_object($SessionConfig = $this->Config->session))
            return;

        $Session = new Session($SessionConfig->name,
                               $SessionConfig->lifeTime,
                               $SessionConfig->saveHandler,
                               $SessionConfig->toDir('savePath'),
                               $this->getAppType() == APP_TYPE_WEB? $SessionConfig->clientSession : null,
                               $SessionConfig->clientSessionLifeTime
                               );

        $Session->start();

        /**
         * Register Session for the current environment
         */
        $this->Environment->registerSession($Session);
    }
    // End initSession

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Closes the Session
     *
     * @param  -
     * @return -
     */
    protected function closeSession()
    {
        if(!is_object($Session = $this->Environment->getSession()))
            return;

        $Session->save();
    }
    // End closeSession

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Inits the outputbuffering
     *
     * @param  -
     * @return -
     */
    protected function startOutputBuffer()
    {
        if(!isset($this->Config->outputBuffer) ||
           !$this->Config->outputBuffer->enable)
            return;

        if(isset($this->Config->outputBuffer->handler))
            $this->outputBuffering = ob_start($this->Config->outputBuffer->handler);

        else
            $this->outputBuffering = ob_start();
    }
    // End initOutputBuffer

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Flushes and end the output buffering
     *
     * @param  -
     * @return -
     */
    protected function endFlushOutputBuffer()
    {
        if($this->outputBuffering)
            ob_end_flush();
    }
    // End flushOutputBuffer

    //////////////////////////////////////////////////////////////////////////////////////

    private function initContainer()
    {
        if(!is_object($this->Config->di) || !isset($this->Config->di->enable) || !$this->Config->di->enable)
            return;

        if (!class_exists('\DI\ContainerBuilder')) {
            throw new Exception('\DI\ContainerBuilder not found');
        }

        $builder = new \DI\ContainerBuilder();

        if ($this->Config->di->autowiring)
            $builder->useAutowiring(true);
        else
            $builder->useAutowiring(false);

        if ($this->Config->di->definitionCache)
            $builder->setDefinitionCache($this->Config->di->definitionCache);

        if ($this->Config->di->writeProxiesToFile)
            $builder->writeProxiesToFile(true, 'tmp/proxies');

        $definitionFiles = [];

        /**
         * @deprecated
         */
        if ($this->Config->di->definitionsFile) {
            $filePath  = $this->Config->toDir('configDir') . $this->Config->di->definitionsFile;

            if (!file_exists($filePath))
                throw new Exception('[DI] Definition file `'. $filePath. '\' does not exist');

            $definitionFiles[] = $filePath;
        }

        if ($this->Config->di->definitionFiles) {
            $baseDir = $this->Config->toDir('baseDir');

            foreach ($this->Config->di->toList('definitionFiles') as $definitionFile) {
                $filePath  = $baseDir . $definitionFile;

                if (!file_exists($filePath))
                    throw new Exception('[DI] Definition file `'. $filePath. '\' is specified, but does not exist');

                $definitionFiles[] = $filePath;
            }
        }

        foreach ($definitionFiles as $definitionFile) {
            $builder->addDefinitions($definitionFile);
        }

        $container = $builder->build();

        $this->Environment->registerContainer($container);
    }
    // End getMicrotime

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Init logging
     *
     * @param  -
     * @return -
     */
    protected function initLogging()
    {
        if(!is_object($this->Config->log))
            return;

        $adapters = $this->Config->log->toList('adapters');

        foreach($adapters as $name)
        {
            if(!is_object($AdapterConfig = $this->Config->log->$name))
                continue;

            if(($adapter = $AdapterConfig->get('adapter', 'none')) === 'none')
                continue;

            if(!class_exists($adapter))
                continue;

            Log::registerAdapter($name, new $adapter($AdapterConfig));
        }
    }
    // End initLogging

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Inits database connections if needed
     *
     * @param  -
     * @return -
     */
    protected function initDatabases()
    {
        if(!is_object($DbConfig = $this->Config->db))
            return;

        $handles = $DbConfig->toList('handles');

        foreach($handles as $name)
        {
            /**
             * Skip incomplete configurations
             */
            if(!is_object($HandleConfig = $this->Config->db->$name) ||
               !$HandleConfig->dsn)
                continue;

            try
            {
                DbHandle::createInstance($name,
                                                   $HandleConfig->dsn,
                                                   $HandleConfig->user,
                                                   $HandleConfig->passwd,
                                                   [PDO::ATTR_PERSISTENT => $HandleConfig->toBoolean('persistent')],
                                                   (bool)$HandleConfig->isDefault
                                                   );
            }
            catch(PDOException $Exception)
            {
                /**
                 * Do not show user and password in back trace
                 */
            }
        }
    }
    // End initDatabases

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Inits all configured modules
     *
     * @param  -
     * @return -
     */
    protected function initRequiredModules()
    {
        if(!is_object($Config = $this->Config->bootstrap))
            return;

        if(!isset($Config->requiredModules))
            return;

        foreach($Config->toList('requiredModules') as $module)
            $this->requireModule($module);
    }
    // End initRequiredModules

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Inits all filters
     *
     * @param  -
     * @return -
     */
    private function initFilters(FrontController $FrontController)
    {
        if(!is_object($Config = $this->Config->mvc))
            return;

        if(isset($Config->preFilters))
            foreach($Config->toList('preFilters') as $preFilter)
                $FrontController->registerPreFilter(new $preFilter);

        if(isset($Config->postFilters))
            foreach($Config->toList('postFilters') as $postFilter)
                $FrontController->registerPostFilter(new $postFilter);
    }
    // End initFilters

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Exception handler callback
     *
     * @param  Exception $exception
     * @return -
     */
    public static function exceptionHandler($exception)
    {
        /**
         * Be always silent
         */
        if(!ini_get('display_errors'))
            return;

        $Request = new HttpRequest();
        $content = "<html><head></head><body><div style=\"border-style:solid; border-width:thin; border-color:#FF0000; padding:1cm\"><font color=#0000FF><b>Bootstrap exception handler</b></font color><br><br>";
        $content .= "<font color=#0000FF>Request:</font color><br>";
        $content .= "Protocol: ".$Request->getServerProtocol()."<br>";
        $content .= "Method: ".$Request->getMethod()."<br>";
        $content .= "URI: <a href=".$Request->getHost().$Request->getRequestURI().">".$Request->getHost().$Request->getRequestURI()."</a><br>";
        $content .= "Redirect URL: <a href=".$Request->getHost().$Request->getRedirectURL().">".$Request->getHost().$Request->getRedirectURL()."</a><br>";
        $content .= "Remote Address: ".$Request->getRemoteAddress()."<br>";
        $content .= "Referer: <a href=".$Request->getReferer().">".$Request->getReferer()."</a><br>";
        $content .= "<br>";

        $content .= "<font color=#0000FF>Exception Message:</font color><br>";
        $content .= '<pre>'. $exception->getMessage()."</pre>";
        $content .= "<pre>". $exception->getFile() .' '. $exception->getLine() ."</pre><br>";
        $content .= "<pre>";
        foreach ($exception->getTrace() as $t) {
            $content .= ((isset($t['class']) ? $t['class'] : "") . (isset($t['type']) ? $t['type'] : "") . $t['function'] . " (" . (isset($t['file']) ? $t['file'] : "location unknown") . (isset($t['line']) ? ":" . $t['line'] : "") . ")\n");
        }
        $content .= "</pre>";
        $content .= "<div></body></html>";

        header('Content-Type: text/html', true, 500);

        print $content;
    }
    // End exceptionHandler

    //////////////////////////////////////////////////////////////////////////////////////
    // private
    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the micro time
     */
    public function printRunTime()
    {
        Log::getInstance()->notice(self::getMicrotime() - $this->startTime, 'Runtime');
    }
    // End getMicrotime

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the micro time
     */
    private static function getMicrotime()
    {
        list($usec, $sec) = explode(" ",microtime());
        return ((float)$usec + (float)$sec);
    }

    //////////////////////////////////////////////////////////////////////////////////////


}
// End Bootstrap
