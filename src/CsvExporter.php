<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs;

/**
 * Exports an array of DataObjects into csv
 *
 * @package blibs
 * @author     Fabian Möller  <fab@beibob.de>
 * @author Tobias Lode <tobias@beibob.de>
 *
 */
class CsvExporter
{
    /**
     * Export type
     */
    const EXPORT_TYPE = 'CSV';

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * The list to export
     */
    protected $list;

    /**
     * Columns to export
     */
    protected $exportColumns;

    /**
     * Column Headers
     */
    protected $headers;

    /**
     * Boolean Expression
     * Default can be overwritten
     */
    protected $booleanExpression = [true  => 'Ja',
                                         false => 'Nein'];
    /**
     * Decimal Separator
     * Default can be overwritten
     */
    protected $decimalSeparator = ',';

    /**
     * NULL Expression
     * Default can be overwritten
     */
    protected $nullExpression = 'NULL';

    /**
     * Line Feed Expression
     * Default can be overwritten
     */
    protected $lineFeed = "\r\n";

    /**
     * Quote Expression
     * Default can be overwritten
     */
    protected $quotationMark = '"';

    /**
     * Quote every value?
     */
    protected $quotation = true;

    /**
     * Which delimiter should be used
     * Default can be overwritten
     */
    protected $delimiter = ',';

    /**
     * Enables strict single line mode.
     * This mode escapes LFs and CRs into \n and \r expression
     */
    protected $escapeCRLF = false;

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the export type
     *
     * @param  int $exportType
     * @return -
     */
    public static function getExportType()
    {
        return self::EXPORT_TYPE;
    }
    // End getExportType

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Constructor method
     *
     * @param DOList $List - [optional]
     */
    public function __construct(array $list = null, array $columns = null)
    {
        if(!is_null($list))
            $this->setDataObjectList($list, $columns);
    }
    // End __construct

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the DOList the data should taken from.
     * The columns array need to contain all the DO Propertynames
     * which should be exported
     *
     * @param  DOList $List
     * @param  array $columns
     * @return -
     */
    public function setDataObjectList(array $list, array $columns)
    {
        if(count($list) > 0 && count($columns) > 0)
        {
            $this->list          = $list;
            $this->exportColumns = $columns;
        } else {
            $this->list = [];
            $this->exportColumns = [];
        }
    }
    // End setDOList

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Set column headlines. this method expects an array with all column
     * headers as values. All headers has to be in the same order like
     * the data columns array given by setDOList
     *
     * @param  array $headers
     * @return -
     */
    public function setHeaders(array $headers = null)
    {
        $this->headers = $headers;
    }
    // End setHeaders

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the delimiter expression
     *
     * @param  string $delimiter
     * @return -
     */
    public function setDelimiter($delimiter = ",")
    {
        $this->delimiter = $delimiter;
    }
    // End setDelimiter

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * switch setting quotations
     *
     * @param  boolean $enable - or disable
     * @param  string  $quotationMark
     * @return -
     */
    public function quoteValues($enable = true, $quotationMark = '"')
    {
        $this->quotation     = $enable;
        $this->quotationMark = $quotationMark;
    }
    // End quoteValues

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the boolean expression
     *
     * @param  string $trueValue
     * @param  string $falseValue
     * @return -
     */
    public function setBooleanExpression($trueValue = "Ja", $falseValue = "Nein")
    {
        $this->booleanExpression[true]  = $trueValue;
        $this->booleanExpression[false] = $falseValue;
    }
    // End setBooleanExpression

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the float expression. Replace the decimal dot
     * with the specified character
     *
     * @param  string $separator
     * @return -
     */
    public function setDecimalSeparator($separator = ',')
    {
        $this->decimalSeparator = $separator;
    }
    // End setDecimalSeparator

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the expression for NULL values
     *
     * @param  string $nullString
     * @return -
     */
    public function setNullExpression($nullString = 'NULL')
    {
        $this->nullExpression = $nullString;
    }
    // End setNullExpression

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the linefeed character that should be used
     *
     * @param  string $linefeed;
     * @return -
     */
    public function setLineFeed($lineFeed = "\r\n")
    {
        $this->lineFeed = $lineFeed;
    }
    // End setLineFeed

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Enables the strict mode "each record one line" which escapes LFs and CRs
     * with \n and \r
     *
     * @param  string $delimiter
     * @return -
     */
    public function escapeCRLF($enable = true)
    {
        $this->escapeCRLF = $enable;
    }
    // End setOneLinePerRecord

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Exports the list into a string
     *
     * @param  -
     * @return string
     */
    public function getString()
    {
        return $this->processData();
    }
    // End getString

    //////////////////////////////////////////////////////////////////////////////////////
    // protected
    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Process the data and return it as string
     *
     * @param  -
     * @return string - data
     */
    protected function processData()
    {
        return $this->getCSVData();
    }
    // End processData

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns an CSV representation of current DOList
     *
     * @param  -
     * @return string - data
     */
    protected function getCSVData()
    {
        if(!is_array($this->list))
            return false;

        $data = $this->getHeadline();

        foreach($this->list as $DO)
        {
            if(!is_object($DO))
                continue;

            foreach($this->exportColumns as $propertyname)
            {
                $value     = isset($DO->$propertyname)? $DO->$propertyname : null;
                $value     = $this->adjust($value);
                $columns[] = $this->quote($value);
            }

            $data .= join($this->delimiter, $columns). $this->lineFeed;
            unset($columns);
        }

        return $data;
    }
    // End getCSVData

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the headline
     *
     * @param  -
     * @return string
     */
    protected function getHeadline()
    {
        if(!is_array($this->headers))
            return false;

        foreach($this->headers as $headername)
            $columns[] = $this->quote($headername);

        return join($this->delimiter, $columns). $this->lineFeed;
    }
    // End getHeadline

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * If quotation was set, this method returns the value quoted
     *
     * @param  mixed $value
     * @return string
     */
    protected function quote($value)
    {
        /**
         * Force quotation if the delimiter is used within the value
         */
        if(!$this->quotation && strpos($value, $this->delimiter) !== false)
            return $value;

        /**
         * Set default quotation mark if forced to use it
         */
        if(!$this->quotation)
            $this->quotationMark = '"';

        return $this->quotationMark . $value . $this->quotationMark;
    }
    // End quote

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Adjust some values to string representation
     *
     * @param  mixed $value
     * @return string
     */
    protected function adjust($value)
    {
        if(is_null($value))
            $value = $this->nullExpression;

        elseif(is_numeric($value))
            $value = $this->getDecimalExpression($value);

        elseif(is_bool($value))
            $value = $this->booleanExpression[$value];

        else
            $value = $this->escapeString($value);

        return $value;
    }
    // End adjust

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Casts an float value to its string expression.
     * the class property decimalSeparator defines how to
     * translate a decimal dot.
     *
     * @param  float $value
     * @return string
     */
    protected function getDecimalExpression($value)
    {
        return str_replace('.', $this->decimalSeparator, $value);
    }
    // getDecimalExpression

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Escapes a string by using escaping rules found
     * on http://www.csvreader.com/csv_format.php
     *
     * @param  string $value
     * @return string
     */
    protected function escapeString($value)
    {
        if($this->quotation == false)
            return $value;

        /**
         * Escape CRs and LFs withing the string
         */
        if($this->escapeCRLF)
            $value = strtr($value, ["\r" => '\r',
                                         "\n" => '\n']);

        /**
         * Escape quotation marks within the string
         */
        switch($this->quotationMark)
        {
            case '"':
                $value = str_replace('"', '""', $value);
                break;

            case "'":
                $value = str_replace("'", '\'\'', $value);
                break;

            case "`":
                $value = str_replace("`", '\`', $value);
                break;

            default:
                break;
        }

        return $value;
    }
    // End escapeString

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Writes the data into file
     *
     * @param  string $filepath
     * @return -
     */
    protected function writeIntoFile($filepath, $data)
    {
        if(is_dir($filepath))
            throw new Exception("File `".$filepath."' is a directory", Exception::FILE_NOT_FOUND);

        $filename = basename($filepath);

        if(!Media::validateFilename($filename))
            throw new Exception("Non-valid filename `".$filename."'", Exception::INVALID_FILENAME);

        $fh = fopen($filepath, 'w');
        fwrite($fh, $data);
        fclose($fh);
    }
    // End writeIntoFile
}
// End class Exporter
