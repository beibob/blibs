<?php
/**
 * This file is part of blibs - mvc development framework
 * 
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 * 
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Resolves an actionController to build the content
 *
 * @package blibs
 * @author Tobias Lode <tobias@beibob.de>
 * @author Fabian Möller <fab@beibob.de>
 * @deprecated see HttpRouter
 */
class InputController
{
    /**
     * @var FrontController $FrontController
     */
    protected $FrontController;

    /**
     * Controller name
     */
    protected $ctrlName;

    /**
     * Invoked action
     */
    protected $action;

    /**
     * Invoked module
     */
    protected $module;

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Contructor
     *
     * @param  -
     * @return -
     */
    public function __construct(FrontController $FrontController, $reqUrl = null)
    {
        $this->FrontController = $FrontController;
        $this->init($reqUrl);
    }
    // End __consturct

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the action Controller name
     *
     * @return string
     */
    public function getInvokedControllerName()
    {
        return $this->ctrlName;
    }
    // End getInvokedControllerName

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the invoked module
     *
     * @return string
     */
    public function getInvokedModule()
    {
        return $this->module;
    }
    // End getInvokedModule

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the invoked action
     *
     * @return string
     */
    public function getInvokedAction()
    {
        return $this->action;
    }
    // End getInvokedAction

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns a href string to a controller
     *
     * @param  mixed $ActionController
     * @param  string $action
     * @param  array $arguments
     * @param  string $module
     * @return string
     */
    public function getLinkTo($module = null, $actionControllerName = null, $action = null, array $arguments = null, $anchor = null)
    {
        $parts = array();

        if(!is_null($module))
            $parts[] = $module;

        if(!is_null($actionControllerName))
            $parts[] =  $this->buildRequestName($actionControllerName, $module);

        if(!is_null($action))
            $parts[] = $action;

        $scriptName = '/' . join('/', $parts);
        if ($scriptName != '/')
            $scriptName .= '/';

        $Config = Environment::getInstance()->getConfig();
        if ($Config->baseUrl && $Config->baseUrl != '/')
            $scriptName = $Config->baseUrl . $scriptName;

        return (string)Url::factory($scriptName, $arguments, $anchor);
    }
    // End getLinkTo

    //////////////////////////////////////////////////////////////////////////////////////
    // protected
    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Inititialization method
     *
     * @param  -
     * @return -
     */
    protected function init($reqUrl = null)
    {
        if(is_null($reqUrl))
        {
            $Request = $this->FrontController->getRequest();

            if(!$reqUrl = $Request->getURI())
                throw new Exception('Missing a request_URI');
        }

        $Config = Environment::getInstance()->getConfig();
        if ($Config->baseUrl && $Config->baseUrl != '/' && utf8_substr($reqUrl, 0, utf8_strlen($Config->baseUrl)) == $Config->baseUrl)
            $reqUrl = utf8_substr($reqUrl, utf8_strlen($Config->baseUrl));

        $Url  = Url::parse($reqUrl);
        $path = preg_split('/\//u', $Url->getScriptName(), -1, PREG_SPLIT_NO_EMPTY);

        switch(count($path))
        {
            case 3:
                $this->module   = $path[0];
                $this->ctrlName = $this->buildClassName($path[1]);
                $this->action   = $path[2];
            break;

            case 2:
                $registeredModules = Environment::getInstance()->getModules();

                if(isset($registeredModules[$path[0]]))
                {
                    $this->module   = $path[0];
                    $this->ctrlName = $this->buildClassName($path[1]);
                }
                else
                {
                    $this->module   = FrontController::getDefaultModule();
                    $this->ctrlName = $this->buildClassName($path[0]);
                    $this->action   = $path[1];
                }
                break;

            case 1:
                $registeredModules = Environment::getInstance()->getModules();

                if(isset($registeredModules[$path[0]]))
                {
                    $this->module   = $path[0];
                    $this->ctrlName = FrontController::getDefaultActionControllerName($this->module);
                }
                else
                {
                    $this->module   = FrontController::getDefaultModule();
                    $this->ctrlName = $this->buildClassName($path[0]);
                }
                break;
            case 0:
                $this->module   = FrontController::getDefaultModule();
                $this->ctrlName = FrontController::getDefaultActionControllerName($this->module);
            break;
        }
    }
    // End init

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Baut den korrekten Klassennamen, an Hand dessen was �ber die Url kommt
     */
    protected function buildClassName($requestName)
    {
        $Config = Environment::getInstance()->getConfig();

        $className = StringFactory::camelCase($requestName, '-');

        $classPrefix = '';
        $module = $this->module;
        if (isset($Config->$module->mvc->classPrefix))
            $classPrefix = $Config->$module->mvc->classPrefix;

        return $classPrefix . $className . 'Ctrl';
    }
    // End buildClassName

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Baut den Requestnamen an Hand der übergeben Klasse
     */
    protected function buildRequestName($className, $module = null)
    {
        // Skip if className ist the default action controller
        if($className == FrontController::getDefaultActionControllerName($module))
            return '';

        $Config = Environment::getInstance()->getConfig();

        $classPrefix = '';
        $module = is_null($module) ? $this->module : $module;
        if (isset($Config->$module->mvc->classPrefix))
            $classPrefix = $Config->$module->mvc->classPrefix;

        $className = utf8_substr($className, utf8_strlen($classPrefix));
        return StringFactory::unCamelCase(utf8_substr($className, 0, -4), '-');
    }
    // End buildRequestName

    //////////////////////////////////////////////////////////////////////////////////////
}
// End InputController
