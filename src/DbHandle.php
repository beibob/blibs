<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs;

use PDO;

/**
 * Database class extending PDO
 *
 * @package blibs
 * @author Tobias Lode <tobias@beibob.de>
 * @author     Fabian Möller <fab@beibob.de>
 *
 */
class DbHandle extends PDO
{
    /**
     * Instances
     */
    private static $instances = [];

    /**
     * Default instance name
     */
    private static $defaultInstance;

    /**
     * Transaction stack
     */
    private $transactionStack = [];

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Creates a new DbHandle instance
     *
     * @param  string $name
     * @return
     */
    public static function createInstance($name, $dsn, $user = null, $passwd = null, array $driverOptions = null, $isDefault = false)
    {
        self::$instances[$name] = new \StdClass();
        self::$instances[$name]->dsn = $dsn;
        self::$instances[$name]->user = $user;
        self::$instances[$name]->passwd = $passwd;
        self::$instances[$name]->driverOptions = $driverOptions;

        if($isDefault)
            self::$defaultInstance = $name;
    }
    // End createInstance

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns a named DbHandle instance. If name is omitted the default instance will
     * be returned or the first created instance. Null will be returned, if no instance exists
     *
     * @param  string $name
     * @return DbHandle
     */
    public static function getInstance($name = false)
    {
        if(!count(self::$instances))
            return null;

        $name = $name? $name : self::$defaultInstance;

        if(!$name)
        {
            if(self::$defaultInstance)
                $name = self::$defaultInstance;
            else
            {
                reset(self::$instances);
                $name = key(self::$instances);
            }
        }

        if(!isset(self::$instances[$name]->dbHandle))
            self::$instances[$name]->dbHandle = new DbHandle(self::$instances[$name]->dsn,
                                                             self::$instances[$name]->user,
                                                             self::$instances[$name]->passwd,
                                                             self::$instances[$name]->driverOptions);

        return self::$instances[$name]->dbHandle;
    }
    // End getInstance

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * SQL transaction begin
     *
     * @param  -
     * @return -
     */
    public function begin()
    {
        /**
         * Take care of nesting transaction blocks
         */
        if(count($this->transactionStack) == 0)
        {
            $this->transactionStack[0] = true;
            parent::beginTransaction();
        }
        else
        {
            $savePoint = 'savepoint_'. count($this->transactionStack);
            array_push($this->transactionStack, $savePoint);

            $this->setSavePoint($savePoint);
        }
    }
    // End begin

    /**
     * Just an alias for begin
     */
    public function beginTransaction() { $this->begin(); }

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * SQL transaction commit
     *
     * @param  -
     * @return -
     */
    public function commit()
    {
        /**
         * Take care of nesting transaction blocks
         */
        if(count($this->transactionStack) > 1)
        {
            $savePoint = array_pop($this->transactionStack);
            $this->releaseSavePoint($savePoint);
        }
        else
        {
            $this->transactionStack = [];
            parent::commit();
        }
    }
    // End commit

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets a SQL transaction savepoint
     *
     * @param  string $savePointName
     * @return -
     */
    public function setSavePoint($name)
    {
        if(!empty($name))
            $this->exec('SAVEPOINT '. $name);
    }
    // End setSavePoint

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * SQL transaction rollback
     *
     * @param  -
     * @return -
     */
    public function rollback()
    {
        /**
         * Take care of nesting transaction blocks
         */
        if(count($this->transactionStack) > 1)
        {
            $savePoint = array_pop($this->transactionStack);
            $this->rollbackToSavePoint($savePoint);
        }
        else
        {
            $this->transactionStack = [];
            parent::rollback();
        }
    }
    // End rollback

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * SQL transaction rollback
     *
     * @param  string $toSavePoint - optional savepoint name
     * @return -
     */
    public function rollbackToSavePoint($savePoint)
    {
        $sql = 'ROLLBACK TO SAVEPOINT '. $savePoint;
        $this->exec($sql);
    }
    // End rollbackToSavePoint

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Releases a SQL transaction savepoint
     *
     * @param  string $savePointName
     * @return -
     */
    public function releaseSavePoint($name)
    {
        if(!empty($name))
            $this->exec('RELEASE SAVEPOINT '. $name);
    }
    // releaseSavePoint

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * @param \Closure $transaction
     * @return mixed
     * @throws \Exception
     */
    public function atomic(\Closure $transaction)
    {
        try {
            $this->begin();

            $result = $transaction($this);

            $this->commit();
        }
        catch (\Exception $exception) {
            $this->rollback();

            throw $exception;
        }

        return $result;
    }


    /**
     * Returns the PDO type for an php variable
     *
     * @param  &mixed $var
     * @return int
     */
    public static function getPDOType(&$mixed)
    {
        if(is_null($mixed))
            return PDO::PARAM_NULL;

        if(is_bool($mixed))
            return PDO::PARAM_BOOL;

        if(is_int($mixed))
            return PDO::PARAM_INT;

        return PDO::PARAM_STR;
    }
    // End getPDOType

    //////////////////////////////////////////////////////////////////////////////////////
}
// End DbHandle
