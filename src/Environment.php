<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs;

/**
 * Environment
 *
 * @package blibs
 * @author Tobias Lode <tobias@beibob.de>
 * @author Fabian Möller <fab@beibob.de>
 *
 */
class Environment
{
    /**
     * Instance
     */
    private static $Instance;

    /**
     * Environment name
     */
    private $name;

    /**
     * Config
     */
    private $Config;

    /**
     * Session
     */
    private $Session;

    /**
     * Dependency injection container
     */
    private $container;

    /**
     * Application type
     */
    private $appType;

    /**
     * Active modules
     */
    private $modules = [];

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Create or return the environment instance
     *
     * @param  string $name [optional]
     * @return Environment
     */
    public static function getInstance($name = false)
    {
        if(!is_object(self::$Instance))
        {
            if(!$name)
                $name = Environment::getServerHostName();

            self::$Instance = new Environment($name);
        }

        return self::$Instance;
    }
    // End getInstance

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the name of the environment
     *
     * @param  -
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    // End getName

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the name application type
     *
     * @param  -
     * @return string
     */
    public function getApplicationType()
    {
        return $this->appType;
    }
    // End getApplicationType

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the Config object
     *
     * @param  -
     * @return Config
     */
    public function getConfig()
    {
        return $this->Config;
    }
    // End getConfig

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the Session object
     *
     * @param  -
     * @return Session
     */
    public function getSession()
    {
        return $this->Session;
    }
    // End getSession

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the list of registered modules
     *
     * @param  -
     * @return array
     */
    public function getModules()
    {
        return $this->modules;
    }
    // End getModules

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the dependency injection container
     *
     * @param  -
     * @return \DI\Container
     */
    public function getContainer()
    {
        return $this->container;
    }
    // End getContainer

    //////////////////////////////////////////////////////////////////////////////////////
    /**
     * Returns true if the given module name is registered
     *
     * @param  string
     * @return boolean
     */
    public function isRegisteredModule($module)
    {
        return isset($this->modules[$module]);
    }
    // End isRegisteredModule

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Register Config
     *
     * @param  Config $Config
     * @return -
     */
    public function registerConfig(Config $Config)
    {
        $this->Config = $Config;
    }
    // End registerConfig

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Register Session
     *
     * @param  Session $Session
     * @return -
     */
    public function registerSession(Session $Session)
    {
        $this->Session = $Session;
    }
    // End registerSession

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Register module
     *
     * @param  string $module
     * @return -
     */
    public function registerModule($module)
    {
        $this->modules[$module] = $module;
    }
    // End registerConfig

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Register container
     *
     * @param  \DI\Container $container
     * @return -
     */
    public function registerContainer(\Di\Container $container)
    {
        $this->container = $container;
    }
    // End registerContainer

    //////////////////////////////////////////////////////////////////////////////////////
    /**
     * Sets the appType
     *
     * @param  string $appType
     * @return -
     */
    public function setApplicationType($appType)
    {
        $this->appType = $appType;
    }
    // End setApplicationType

    //////////////////////////////////////////////////////////////////////////////////////
    // PHP environmental helpers
    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the current script name
     *
     * @param  -
     * @return string
     */
    public static function getScriptName()
    {
        return isset($_SERVER['SCRIPT_NAME'])? $_SERVER['SCRIPT_NAME'] : '/';
    }
    // End getScriptName

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the remote address
     *
     * @param  -
     * @return string
     */
    public static function getRemoteAddress()
    {
        return isset($_SERVER['HTTP_X_FORWARDED_FOR']) && !empty($_SERVER['HTTP_X_FORWARDED_FOR'])? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'];
    }
    // End getRemoteAddress

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the server name without the port
     *
     * @param  -
     * @return string
     */
    public static function getServerHostName()
    {
        $server_name = Environment::getServerHost();
        if (!$server_name) {
            return $server_name;
        } else {
            if (preg_match('/:/u', $server_name)) {
                list($host, $port) = mb_split(":", $server_name);
            } else {
                $host = $server_name;
            }
            return $host;
        }
    }
    // End getServerHost

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the server name
     *
     * @param  -
     * @return string
     */
    public static function getServerHost()
    {
        if(isset($_SERVER["HTTP_HOST"]) && !empty($_SERVER["HTTP_HOST"]))
            $serverHost = $_SERVER["HTTP_HOST"];

        elseif(isset($_SERVER["SERVER_NAME"]) && !empty($_SERVER["SERVER_NAME"]))
            $serverHost = $_SERVER["SERVER_NAME"];

        elseif(isset($_ENV["HTTP_HOST"]) && !empty($_ENV["HTTP_HOST"]))
            $serverHost = $_ENV["HTTP_HOST"];

        elseif(isset($_ENV["SERVER_NAME"]) && !empty($_ENV["SERVER_NAME"]))
            $serverHost = $_ENV["SERVER_NAME"];

        else
            $serverHost = trim(`hostname`);

        return $serverHost? utf8_strtolower($serverHost) : false;
    }
    // End getServerHost

    ///////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sagt, ob SSL am Start ist
     */
    public static function sslActive()
    {
        return isset($_SERVER['HTTPS']) || (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && strtolower($_SERVER['HTTP_X_FORWARDED_PROTO']) == 'https');
    }
    // End sslIsActive

    ///////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Checks if the request is an ajax request
     *
     * @param  -
     * @return bool
     */
    public static function isXmlHttpRequest()
    {
        return (isset($_SERVER["HTTP_X_REQUESTED_WITH"]) && $_SERVER["HTTP_X_REQUESTED_WITH"] == 'XMLHttpRequest');
    }
    // End isXMLHttpRequest

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the HTTP_ACCEPT_LANGUAGE
     *
     * @param  -
     * @return string
     */
    public function getAcceptLanguage()
    {
        return isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])? $_SERVER['HTTP_ACCEPT_LANGUAGE'] : null;
    }
    // End getAcceptLanguage

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the HTTP_ACCEPT_LANGUAGE as a parsed array
     *
     * @param  -
     * @return array
     */
    public function getAcceptLanguageInformation()
    {
        $accept = [];
        if ($this->getAcceptLanguage())
        {
            $langDefs = explode(",", $this->getAcceptLanguage());
            foreach ($langDefs as $count => $langDef)
            {
                $parts = explode(";", $langDef);
                $localePart = mb_split("-", $parts[0]);

                $accept[] = ['locale' => $localePart[0],
                             'country' => isset($localePart[1]) ? $localePart[1] : '',
                             'preference' => isset($parts[1]) ? str_replace("q=", "", $parts[1]) : 1];
            }
        }

        return $accept;
    }
    // End getAcceptLanguageInformation

    //////////////////////////////////////////////////////////////////////////////////////


    //////////////////////////////////////////////////////////////////////////////////////
    // private
    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Constructor
     *
     * @param  string $name
     * @return Environment
     */
    private function __construct($name)
    {
        $this->name = $name;
    }
    // End __construct

    //////////////////////////////////////////////////////////////////////////////////////
}
// End Environment
