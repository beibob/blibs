<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs;

use Beibob\Blibs\Interfaces\Viewable;

/**
 * A container view for multipart views
 *
 * @package blibs
 * @author Tobias Lode <tobias@beibob.de>
 * @author Fabian Möller <fab@beibob.de>
 *
 */
class MultiPartView extends View
{
    /**
     * Views to be encoded
     */
    private $views = [];

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Adds a view. The view needs already to be rendered
     *
     * @param  Viewable $View
     * @return Viewable
     */
    public function add(Viewable $View)
    {
        $this->views[] = $View;
    }
    // End add

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the content as string
     *
     * @param  -
     * @return string
     */
    public function __toString()
    {
        return join('', $this->views);
    }
    // End __toString

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Processes the view
     *
     * @param  array $args
     * @return -
     */
    public function process(array $args = [], $module = null)  {}

    //////////////////////////////////////////////////////////////////////////////////////
}
// End MultiPartView
