<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs;

/**
 * Handles a set of Page(Node)s
 *
 * @package blibs
 * @author Tobias Lode <tobias@beibob.de>
 * @author Fabian Möller <fab@beibob.de>
 *
 */
class PageSet extends DbObjectSet
{
    /**
     * Page view
     */
    const VIEW_NAME = 'public.pages_v';

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Finds all direct child nodes of the given parent node
     *
     * @param  -
     * @return PageSet
     */
    public static function findByParent(NestedNode $Node, array $initValues = [], $force = false)
    {
        if(!$Node->isValid())
            return new PageSet();

        $sql = sprintf("SELECT *
                          FROM %s
                         WHERE root_id = :rootId
                           AND lft BETWEEN :lft AND :rgt
                           AND level = :level + 1
                         ORDER BY lft"
                       , self::VIEW_NAME
                       );


        $initValues['rootId'] = $Node->getRootId();
        $initValues['lft']    = $Node->getLft();
        $initValues['rgt']    = $Node->getRgt();
        $initValues['level']  = $Node->getLevel();

        return self::_findBySql(get_class(), $sql, $initValues, $force);
    }
    // End findByParent

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Wie findByParent nur, dass man einen filter übergeben kann
     *
     * @param  -
     * @return PageSet
     */
    public static function filterByParent(NestedNode $Node, $filter = null, $force = false)
    {
        if(!$Node->isValid())
            return new PageSet();

        $sql = sprintf("SELECT *
                          FROM %s
                         WHERE root_id = :rootId
                           AND lft BETWEEN :lft AND :rgt
                           AND level = :level + 1
                           %s
                         ORDER BY lft"
                       , self::VIEW_NAME
                       , is_null($filter) ? '' : 'AND (' . $filter . ')'
                       );


        $initValues['rootId'] = $Node->getRootId();
        $initValues['lft']    = $Node->getLft();
        $initValues['rgt']    = $Node->getRgt();
        $initValues['level']  = $Node->getLevel();

        return self::_findBySql(get_class(), $sql, $initValues, $force);
    }
    // End findByParent

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Finds a list of all Root nodes.
     *
     * @param  -
     * @return -
     */
    public static function findRootNodes(array $initValues = null, $force = false)
    {
        $sql = sprintf("SELECT *
                          FROM %s
                          WHERE id = root_id"
                       , self::VIEW_NAME
                       );

        return self::_findBySql(get_class(), $sql, $initValues, $force);
    }
    // End findRootNodes

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Inits a list of all subordinate nodes of the given parent node
     *
     * @param  Page $Parent
     * @return PageSet
     */
    public static function findSubordinatesByParent(NestedNode $Node, $force = false)
    {
        if(!$Node->isValid())
            return new PageSet();

        $sql = sprintf("SELECT *
                          FROM %s
                         WHERE root_id = :rootId
                           AND lft BETWEEN :lft AND :rgt
                           AND level > :level
                         ORDER BY lft"
                       , self::VIEW_NAME);

        return self::_findBySql(get_class(), $sql, ['rootId' => $Node->getRootId(),
                                                        'lft'    => $Node->getLft(),
                                                        'rgt'    => $Node->getRgt(),
                                                        'level'  => $Node->getLevel()
                                                         ], $force);
    }
    // End initSubordinatesByParent

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Inits a list of parent nodes of a given subordinate
     *
     * @param  Page $Subordinate
     * @param  bool $excludeSubordinate  - exclude the start node itself
     * @return PageSet
     */
    public static function findParentsOf(NestedNode $Subordinate, $excludeSubordinate = false, $force = false)
    {
        if(!$Subordinate->isValid())
            return new PageSet();

        $initValues = ['rootId' => $Subordinate->getRootId(),
                            'lft'    => $Subordinate->getLft()
                            ];

        if($excludeSubordinate)
        {
            $excludeSql = "AND level < :level";
            $initValues['level'] = $Subordinate->getLevel();
        }

        $sql = sprintf("SELECT *
                          FROM %s
                         WHERE root_id = :rootId
                           AND :lft BETWEEN lft AND rgt
                            %s
                      ORDER BY lft"
                       , self::VIEW_NAME
                       , $excludeSubordinate? $excludeSql : ''
                       );


        return self::_findBySql(get_class(), $sql, $initValues, $force);
    }
    // End findParentsOf

    //////////////////////////////////////////////////////////////////////////////////////
}
// End class PageSet
