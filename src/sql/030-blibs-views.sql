--------------------------------------------------------------------------------
-- blibs views
--------------------------------------------------------------------------------

SET client_encoding = 'LATIN9';
SET search_path = public, pg_catalog;

BEGIN;
--------------------------------------------------------------------------------

DROP VIEW IF EXISTS public.pages_v;
DROP VIEW IF EXISTS public.roles_v;
DROP VIEW IF EXISTS public.users_v;

--------------------------------------------------------------------------------

CREATE VIEW public.pages_v AS
  SELECT n.*
       , p.*
    FROM public.nested_nodes n
    JOIN public.pages        p ON n.id = p.node_id;

--------------------------------------------------------------------------------
-- users
--------------------------------------------------------------------------------

CREATE VIEW public.users_v AS
    SELECT u.*
         , p.company
         , p.firstname
         , p.lastname
         , p.email
         , p.birthday
         , CASE WHEN p.firstname <> '' OR p.lastname <> '' THEN
                btrim(coalesce(p.firstname, '') ||' '|| coalesce(p.lastname, ''))
           ELSE
                u.auth_name
           END AS fullname
      FROM public.users u
 LEFT JOIN public.user_profiles p ON u.id = p.user_id;

--------------------------------------------------------------------------------
-- roles
--------------------------------------------------------------------------------

CREATE VIEW public.roles_v AS
    SELECT n.id
         , n.root_id
         , n.lft
         , n.rgt
         , n.level
         , n.ident
         , r.node_id
         , r.role_name
         , r.description
      FROM public.roles r
      JOIN public.nested_nodes n ON n.id = r.node_id;

--------------------------------------------------------------------------------
COMMIT;