--------------------------------------------------------------------------------
-- blibs procedures
--------------------------------------------------------------------------------
BEGIN;
CREATE OR REPLACE FUNCTION public.get_page_by_rubric_path(IN in_rubric_path   text,
                                                          IN in_page_root_id  int)  RETURNS public.pages_v
AS $$

DECLARE
   a_rubrics text[];
   t_rubric  text;

   p_page public.pages_v;

BEGIN
   SELECT *
     INTO p_page
     FROM public.pages_v
    WHERE id = in_page_root_id;

   IF NOT FOUND THEN
      RAISE NOTICE 'root page % is not found', in_page_root_id;
      RETURN NULL;
   END IF;

   a_rubrics := string_to_array(trim(both '/' from lower(in_rubric_path)), '/');

   IF a_rubrics IS NOT NULL THEN
       FOR i IN 1..array_upper(a_rubrics, 1) LOOP
           t_rubric := a_rubrics[i];

           SELECT *
             INTO p_page
             FROM public.pages_v
            WHERE root_id = in_page_root_id
              AND lft BETWEEN p_page.lft AND p_page.rgt
              AND level = p_page.level + 1
              AND lower(rubric) = t_rubric;

            IF NOT FOUND THEN
               RETURN NULL;
            END IF;
       END LOOP;
   END IF;

   RETURN p_page;
END;
$$ LANGUAGE plpgsql;

--------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION register_patch(IN patchname text, IN relmodule text)
     RETURNS boolean AS $$

BEGIN

   INSERT INTO public.patches (name, module) VALUES (patchname, relmodule);
   RAISE NOTICE 'Patch %.% registered', relmodule, patchname;
   RETURN true;

EXCEPTION
    WHEN unique_violation THEN
        RAISE EXCEPTION 'Patch %.% is already registered', relmodule, patchname;
        RETURN false;

END;
$$ LANGUAGE plpgsql;

--------------------------------------------------------------------------------------
COMMIT;