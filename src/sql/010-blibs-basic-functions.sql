--------------------------------------------------------------------------------
-- blibs procedures
--------------------------------------------------------------------------------

SET client_encoding = 'LATIN9';
SET search_path = public, pg_catalog;

BEGIN;
--------------------------------------------------------------------------------------
-- PgSQL Timestamp to unixtime
--------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION public.to_unixtime(timestamp without time zone, OUT integer) RETURNS integer
    AS $_$
  SELECT extract('epoch' from $1)::integer;
$_$
    LANGUAGE sql;

--------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION public.to_unixtime(timestamp with time zone, OUT integer) RETURNS integer
    AS $_$
  SELECT extract('epoch' from $1)::integer;
$_$
    LANGUAGE sql;

--------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION public.to_unixtime() RETURNS integer
    AS $_$
  SELECT public.to_unixtime(now());
$_$
   LANGUAGE sql;

--------------------------------------------------------------------------------------
-- Unixtime to PgSQL Timestamp
--------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION public.from_unixtime(integer, OUT timestamp without time zone) RETURNS timestamp without time zone
    AS $_$
  SELECT 'epoch'::timestamp + $1 * '1 second'::interval;
$_$
    LANGUAGE sql;

CREATE OR REPLACE FUNCTION public.from_unixtime(numeric, OUT timestamp without time zone) RETURNS timestamp without time zone
    AS $_$
  SELECT 'epoch'::timestamp + $1 * '1 second'::interval;
$_$
    LANGUAGE sql;

--------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION public.from_unixtimez(integer, OUT timestamp with time zone) RETURNS timestamp with time zone
    AS $_$
  SELECT 'epoch'::timestamp with time zone + $1 * '1 second'::interval;
$_$
    LANGUAGE sql;

CREATE OR REPLACE FUNCTION public.from_unixtimez(numeric, OUT timestamp with time zone) RETURNS timestamp with time zone
    AS $_$
  SELECT 'epoch'::timestamp with time zone + $1 * '1 second'::interval;
$_$
    LANGUAGE sql;

--------------------------------------------------------------------------------------
-- Generating sequelIds
--------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION get_sequel_id()
     RETURNS numeric(20,0) AS $$
--
-- Procedure:   get_sequel_id
-- Description: Calculates a sequelId like it is done by IDFactory::getSequelId
--
-- Author:      Tobias Lode <tobias.lode@beibob.de>
--
DECLARE
   blibs_epoch CONSTANT numeric(20,0) := 1104534000;
   currtime             timestamp with time zone;
   sequel_id            numeric(20,0);

BEGIN
   SELECT clock_timestamp()
     INTO currtime;

   SELECT (extract(epoch FROM currtime)::numeric(20) - blibs_epoch) * 1000000
        + extract(microseconds FROM currtime)::numeric(20) % 1000000
     INTO sequel_id;

   RETURN sequel_id;
END;
$$ LANGUAGE plpgsql VOLATILE;

--------------------------------------------------------------------------------------
-- AGGREGATOR
--------------------------------------------------------------------------------------

CREATE AGGREGATE array_accum (anyelement)
(
    sfunc = array_append,
    stype = anyarray,
    initcond = '{}'
);

--------------------------------------------------------------------------------------
COMMIT;