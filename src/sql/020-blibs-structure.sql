--------------------------------------------------------------------------------
-- blibs structure
--------------------------------------------------------------------------------

SET client_encoding = 'UTF8';
SET search_path = public, pg_catalog;

BEGIN;
--------------------------------------------------------------------------------
-- sessions
--------------------------------------------------------------------------------

CREATE TABLE public.sessions
(
    "id"          varchar(32)     NOT NULL                -- session id
  , "created"     timestamptz(0)     NOT NULL default now()  -- creation time
  , "modified"    timestamptz(0)              default now()  -- modification time
  , "expires"     integer                                 -- expire time
  , "data"        text                                    -- serialized session data
  , PRIMARY KEY ("id")
);

--------------------------------------------------------------------------------
-- nested nodes
--------------------------------------------------------------------------------

CREATE SEQUENCE public.nested_nodes_seq;

CREATE TABLE public.nested_nodes
(
    "id"                integer      NOT NULL default nextval('public.nested_nodes_seq'::regclass) -- nodeId
  , "root_id"           integer      NOT NULL                -- id of the root node
  , "lft"               integer      NOT NULL                -- left number
  , "rgt"               integer      NOT NULL                -- right number
  , "level"             integer      NOT NULL default 0      -- level
  , "ident"             varchar(100)                         -- ident
  , PRIMARY KEY ("id")
  , UNIQUE ("root_id", "ident")
  , FOREIGN KEY ("root_id") REFERENCES nested_nodes ("id") ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE INDEX IX_public_nested_nodes_root_id ON public.nested_nodes ("root_id");
CREATE INDEX IX_public_nested_nodes_lft     ON public.nested_nodes ("lft");
CREATE INDEX IX_public_nested_nodes_rgt     ON public.nested_nodes ("rgt");
CREATE INDEX IX_public_nested_nodes_ident   ON public.nested_nodes ("ident");

--------------------------------------------------------------------------------
-- user and groups
--------------------------------------------------------------------------------

CREATE TABLE public.groups
(
    "id"            serial       NOT NULL               -- groupId
  , "name"          varchar(200) NOT NULL               -- name
  , "is_usergroup"  boolean      NOT NULL default false -- isUserGroup
  , "created"       timestamptz(0)  NOT NULL default now() -- creation time
  , "modified"      timestamptz(0)           default now() -- modification time
  , PRIMARY KEY ("id")
);

--------------------------------------------------------------------------------

CREATE TABLE public.users
(
    "id"            serial       NOT NULL                -- userId
  , "auth_name"     varchar(100) NOT NULL                -- authName
  , "auth_key"      varchar(50)  NOT NULL default ''     -- authKey
  , "auth_method"   smallint     NOT NULL default 0      -- authMethod
  , "group_id"      integer      NOT NULL                -- userGroupId
  , "created"       timestamptz(0)  NOT NULL default now()  -- creation time
  , "modified"      timestamptz(0)           default now()  -- modification time
  , "login_time"    timestamptz(0)                          -- login time
  , PRIMARY KEY ("id")
  , FOREIGN KEY ("group_id") REFERENCES public.groups ("id") ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE RULE rule_auth_name_updates_user_group_name AS ON UPDATE TO public.users
     WHERE OLD.auth_name <> NEW.auth_name
   DO ALSO UPDATE public.groups
              SET "name" = NEW.auth_name
            WHERE id = NEW.group_id;

--------------------------------------------------------------------------------

CREATE TABLE public.user_profiles
(
    "user_id"       integer      NOT NULL             -- userId
  , "company"       varchar(100) NOT NULL default ''  -- company
  , "firstname"     varchar(100) NOT NULL default ''  -- firstname
  , "lastname"      varchar(100) NOT NULL default ''  -- lastname
  , "email"         varchar(100) NOT NULL default ''  -- email address
  , "birthday"      date                              -- birthday
  , PRIMARY KEY ("user_id")
  , FOREIGN KEY ("user_id") REFERENCES public.users ("id") ON UPDATE CASCADE ON DELETE CASCADE
);

--------------------------------------------------------------------------------

CREATE TABLE public.group_members
(
    "user_id"       integer      NOT NULL             -- userId
  , "group_id"      integer      NOT NULL             -- groupId
  , PRIMARY KEY ("group_id", "user_id")
  , FOREIGN KEY ("user_id")  REFERENCES public.users ("id")  ON UPDATE CASCADE ON DELETE CASCADE
  , FOREIGN KEY ("group_id") REFERENCES public.groups ("id") ON UPDATE CASCADE ON DELETE CASCADE
);

--------------------------------------------------------------------------------

CREATE TABLE public.roles
(
    "node_id"       integer         NOT NULL               -- nodeId
  , "role_name"     varchar(100)    NOT NULL               -- roleName
  , "description"   text            NOT NULL               -- description
  , PRIMARY KEY ("node_id")
  , FOREIGN KEY ("node_id") REFERENCES public.nested_nodes ("id") ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE INDEX IX_roles_role_name ON public.roles ("role_name");

--------------------------------------------------------------------------------

CREATE TABLE public.role_members
(
    "role_id"        integer         NOT NULL               -- roleId
  , "group_id"       integer         NOT NULL               -- groupId
  , "is_ceased"      boolean         NOT NULL default false -- membership is ceased
  , PRIMARY KEY ("role_id", "group_id")
  , FOREIGN KEY ("role_id")  REFERENCES public.roles  ("node_id") ON UPDATE CASCADE ON DELETE CASCADE
  , FOREIGN KEY ("group_id") REFERENCES public.groups ("id")      ON UPDATE CASCADE ON DELETE CASCADE
);

--------------------------------------------------------------------------------
-- page nodes
--------------------------------------------------------------------------------

CREATE TABLE public.pages
(
    "node_id"           int           NOT NULL                   -- nestedNodeId
  , "caption"           varchar(200)  NOT NULL                   -- caption
  , "rubric"            varchar(220)                             -- rubric path
  , "module"            varchar(220)                             -- module
  , "ctrl_name"         varchar(120)  NOT NULL                   -- controller
  , "ctrl_action"       varchar(120)                             -- controller action
  , "is_visible"        boolean       NOT NULL default true      -- visiblity
  , "is_secure"         boolean       NOT NULL default false     -- ssl flag
  , "role_id"           int                                      -- role required to access this page
  , "deny_page_id"      int                                      -- alternate page id on no access
  , "created"           timestamptz(0) NOT NULL default now()    -- creation time
  , "modified"          timestamptz(0)          default now()    -- modification time
  , PRIMARY KEY ("node_id")
  , FOREIGN KEY ("node_id")      REFERENCES public.nested_nodes ("id")    ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE INITIALLY IMMEDIATE
  , FOREIGN KEY ("deny_page_id") REFERENCES public.pages ("node_id") ON DELETE SET NULL ON UPDATE CASCADE DEFERRABLE INITIALLY IMMEDIATE
  , FOREIGN KEY ("role_id")      REFERENCES public.roles ("node_id")      ON DELETE SET NULL ON UPDATE CASCADE DEFERRABLE INITIALLY IMMEDIATE
);

--------------------------------------------------------------------------------

CREATE TABLE public.media
(
    "id"                serial          NOT NULL               -- mediaId
  , "name"              varchar(100)    NOT NULL               -- name
  , "rel_path"          varchar(150)    NOT NULL               -- base-relative path
  , "mime_type"         varchar(100)    NOT NULL               -- mimeType
  , "width"             integer                                -- image width
  , "height"            integer                                -- image width
  , "file_name"         varchar(150)                           -- fileName
  , "created"           timestamptz(0)     NOT NULL default now() -- creation time
  , PRIMARY KEY ("id")
);

--------------------------------------------------------------------------------
-- patches
--------------------------------------------------------------------------------

CREATE TABLE public.patches
(
    "name"              varchar(200)    NOT NULL                        -- patchName
  , "module"            varchar(50)     NOT NULL                        -- related module
  , "owner"             varchar(200)    NOT NULL default current_user   -- user applied the patch
  , "registered"        timestamptz(0)  NOT NULL default now()          -- time
  , UNIQUE  ("name", "module")
);

--------------------------------------------------------------------------------
COMMIT;