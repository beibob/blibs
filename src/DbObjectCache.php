<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs;

/**
 * Request based cache for DbObject .
 *
 * This cache handles three types of objects
 *  1. a simple mixed-value cache
 *  2. a DbObject cache
 *  3. a DbObjectSet cache
 *
 * The value cache is completly separated from the DbObject(Set) cache.
 * Basicly it is simple hash map.
 *
 * DbObject and DbObjectSet caches are handled in common. Each DbObject can be
 * cached by a unique signature. Basicly, there are two signature types if the
 * method DbObjectCache::getSignature is used. Of course other types are possible,
 * but it's not recommended.
 *
 * The first type is the object signature type and is build from the object name
 * and a md5 hash from the primary key values: OBJECTNAME_md5(PK) (PK may also be a tuple)
 *
 * The second type is the sql-and-initvalues signature which is build from
 * the object name, the sql statement and its initValues: OBJECTNAME_md5(sql+initvalues)
 *
 * DbObjects are stored in the cache only by object signature. sql signatures
 * maps to object signatures, which is stored in a second hash map (objectCacheMap).
 *
 * @package blibs
 * @author Tobias Lode <tobias@beibob.de>
 * @author     Fabian M�ller <fab@beibob.de>
 *
 */
class DbObjectCache
{
    /**
     * value cache
     */
    private static $valueCache = [];

    /**
     * object cache
     */
    private static $objectCache = [];

    /**
     * Object cache map
     */
    private static $objectCacheMap = [];

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns true on a cached value
     *
     * @param  string $signature
     * @return boolean
     */
    public static function has($signature)
    {
        return isset(self::$valueCache[$signature]);
    }
    // End has

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns true on a cached value
     *
     * @param  string $signature
     * @return boolean
     */
    public static function hasDbObject($signature)
    {
        return isset(self::$dbObjectCache[$signature]) || isset(self::$objectCacheMap[$signature]);
    }
    // End hasDbObject

    /////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns a cached value by its signature
     *
     * @param  string $signature
     * @return mixed
     */
    public static function get($signature)
    {
        if(!isset(self::$valueCache[$signature]))
            return null;

        return self::$valueCache[$signature];
    }
    // End get

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns a cached object by its signature
     *
     * @param  string $signature
     * @return DbObject
     */
    public static function getDbObject($signature)
    {
        /**
         * Check the object signature cache (OBJECTNAME_PK)
         */
        if(isset(self::$objectCache[$signature]))
            return self::$objectCache[$signature];

        /**
         * Check the sql signature cache (OBJECTNAME_SQL_INITVALUES)
         */
        if(!isset(self::$objectCacheMap[$signature]))
            return null;

        /**
         * Stale cacheMap entry? This should not happen!
         */
        if(!isset(self::$objectCache[self::$objectCacheMap[$signature]]))
            return null;

        /**
         * Cache hit
         */
        return self::$objectCache[self::$objectCacheMap[$signature]];
    }
    // End getDbObject

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns a cached object set by its signature
     *
     * @param  string $signature
     * @return DbObjectSet
     */
    public static function getObjectSet($signature)
    {
        if(!isset(self::$objectCache[$signature]) || !$DbObjectSet = self::$objectCache[$signature])
            return null;

        /**
         * Remove invalid objects from list
         */
        foreach($DbObjectSet as $key => $DbObject)
            if(!$DbObject->isInitialized())
                unset($DbObjectSet[$key]);

        return $DbObjectSet;
    }
    // End getObjectSet

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Caches a value
     *
     * @param  string $signature
     * @param  mixed  $value
     * @return mixed
     */
    public static function set($signature, $value)
    {
        return self::$valueCache[$signature] = $value;
    }
    // End set

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Caches an object
     *
     * @param  string $signature
     * @param  DbObject $Object
     * @return DbObject
     */
    public static function setDbObject($signature, DbObject $DbObject)
    {
        /**
         * Lookup cached dbobject by signature
         */
        if($CachedObject = self::getDbObject($signature))
            return $CachedObject;

        /**
         * On cache miss, get the object signature (Objectname_PK)
         */
        $objectSignature = self::getSignature(get_class($DbObject), $DbObject->getPrimaryKey());

        /**
         * And compare it with the given one.
         * If not identical, create a mapping entry
         * from this signature to the object signature
         */
        if($signature != $objectSignature)
            self::$objectCacheMap[$signature] = $objectSignature;

        /**
         * Save the objectSignature
         */
        return self::$objectCache[$objectSignature] = $DbObject;
    }
    // End setDbObject

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Caches an object set
     *
     * @param  string $signature
     * @param  DbObjectSet $ObjectSet
     * @return DbObjectSet
     */
    public static function setObjectSet($signature, DbObjectSet $DbObjectSet)
    {
        return self::$objectCache[$signature] = $DbObjectSet;
    }
    // End setObjectSet

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Removes a value from cache
     *
     * @param  string $signature
     * @return -
     */
    public static function free($signature)
    {
        if(!isset(self::$valueCache[$signature]))
            return;

        unset(self::$valueCache[$signature]);
    }
    // End freeDbObject

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Removes a DbObject from cache
     *
     * @param  string $signature
     * @return -
     */
    public static function freeDbObject($signature)
    {
        if(!isset(self::$objectCache[$signature]))
            return;

        unset(self::$objectCache[$signature]);

        foreach(array_keys(self::$objectCacheMap, $signature) as $key => $val)
            unset(self::$objectCacheMap[$key]);
    }
    // End freeDbObject

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Removes a DbObject from cache
     *
     * @param  DbObject $DbObject
     * @return -
     */
    public static function freeByObject(DbObject $DbObject)
    {
        $signature = self::getSignature(get_class($DbObject), $DbObject->getPrimaryKey());
        self::freeDbObject($signature);
    }
    // End freeByObject

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Removes a DbObjectSet from cache
     *
     * @param  DbObjectSet $DbObjectSet
     * @return -
     */
    public static function freeObjectSet(DbObjectSet $DbObjectSet)
    {
        if(!$signature = array_search($DbObjectSet, self::$objectCache, true))
            return;

        unset(self::$objectCache[$signature]);
    }
    // End freeObjectSet

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Clears the complete cache
     *
     * @param  -
     * @return -
     */
    public static function clear()
    {
        self::$valueCache = [];
        self::$objectCache = [];
        self::$objectCacheMap = [];
    }
    // End clear

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns a signature string from some initial values
     *
     * @param  array $initValues
     * @return string
     */
    public static function getSignature($dbObjectName, array $initValues = null, $sql = null)
    {
        if((is_null($initValues) && is_null($sql)) || empty($dbObjectName))
            return false;

        $signature = '';

        if(!is_null($sql))
            $signature .= $sql;

        if(!is_null($initValues))
            $signature .= join('|', $initValues);

        return $dbObjectName .'_'. md5($signature);
    }
    // End getSignature

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Shows the cache content
     */
    public static function show($tryFirePHP = true)
    {
        show(self::$valueCache, $tryFirePHP);
        show(self::$objectCache, $tryFirePHP);
        show(self::$objectCacheMap, $tryFirePHP);
    }
    // End show
}
// End DbObjectCache
