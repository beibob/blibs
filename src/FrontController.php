<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs;

use Beibob\Blibs\Interfaces\ApplicationController;
use Beibob\Blibs\Interfaces\Filter;
use Beibob\Blibs\Interfaces\Request;
use Beibob\Blibs\Interfaces\Response;
use Beibob\Blibs\Interfaces\Router;
use Beibob\Blibs\Interfaces\Viewable;
use Beibob\Blibs\Interfaces\ViewPostProcessingPlugin;


/**
 * Front Controller
 *
 * @package blibs
 * @author Tobias Lode <tobias@beibob.de>
 * @author Fabian Möller <fab@beibob.de>
 *
 */
class FrontController
{
	/**
	 * Default router
	 */
	const DEFAULT_ROUTER = 'Beibob\Blibs\HttpRouter';

    /**
     * Singleton instance
     */
    private static $Instance;

    /**
     * Environment
     */
    private $Environment;

    /**
     * Request
     */
    private $Request;

    /**
     * Respone
     */
    private $Response;

    /**
     * Invoked controller instance
     */
    private $ActionController;

    /**
     * Invoked action
     */
    private $action;

    /**
     * Invoked module
     */
    private $module;

    /**
     * Current Router
     */
    private $Router;

    /**
     * Postprocessing Plugins
     */
    private $viewPostProcessingPlugins = [];

    /**
     * Prefilter Chain
     */
    private $preFilterChain = [];

    /**
     * Postfilter Chain
     */
    private $postFilterChain = [];

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Constructs the FrontController singleton
     *
     * @deprecated
     * @param Environment $Environment
     * @return FrontController
     */
    public static function getInstance(Environment $Environment = null)
    {
        if(!self::$Instance)
            self::$Instance = new FrontController($Environment);

        return self::$Instance;
    }
    // End getInstance

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Constructor
     *
     * @param  Environment $Environment
     * @return FrontController
     */
    public function __construct(Environment $Environment = null)
    {
        /**
         * Set Environment
         */
        $this->Environment = $Environment ? $Environment : Environment::getInstance();
    }
    // End __construct


    /**
     * Handles a request
     *
     * @param  -
     * @return -
     */
    public function handleRequest(Request $Request, Response $Response)
    {
        /**
         * Set Request and Response objects
         */
        $this->Request  = $Request;
        $this->Response = $Response;

        try
		{
            $Router = $this->getRouter();
            list($ctrlName, $action, $query) = $Router->resolve($Request);

            /**
             *  preFilter handling
             */
            foreach($this->preFilterChain as $Filter)
                if($Filter instanceOf Filter)
                    $Filter->filter($Request, $Response, $Router);

            $this->forward($ctrlName, $action, $query);
        }
        catch(ForwardException $ForwardException)
        {
			/**
			 * @todo: adapt ForwardException
			 */
            $this->forward($ForwardException->getControllerName(),
                           $ForwardException->getAction(),
                           $ForwardException->getQuery()
			);
        }
        catch(RedirectException $RedirectException)
        {
            $Response->setHeaderLocation($RedirectException->getLocation());
        }
        catch(AutoLoaderException $Exception)
        {
            $Response->setStatus(404);
            $Response->addException($Exception);

            /**
             * @todo: build an error handler
             */
            // $this->forward('404', $Exception->getMessage());
        }
        catch(Exception $Exception)
        {
            $Response->addException($Exception);
        }

        /**
         * postFilter handling
         */
        foreach($this->postFilterChain as $Filter)
            if($Filter instanceOf Filter)
                $Filter->filter($Request, $Response);

        /**
         * Finaly send the output
         */
        return $Response->send();
    }
    // End handleRequest

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Forwards the processing to another controller
     *
     * @param  string $ctrlName
     * @param  string $action
     * @param  string $query
     * @return -
     */
    private function forward($ctrlName = null, $action = null, array $query = null)
    {
        $ActionController = $this->setActionController($ctrlName);
        $action           = $this->setAction($action);

        if(!$ActionController instanceOf ApplicationController)
            throw new Exception('No ApplicationController could be resolved');

		/**
		 * @todo: check if necessary
		 */
		$module = $this->setModule($ActionController->getModule());

        /**
		 * Hand off to action controller
		 */
		if($View = $ActionController->process($action, $query? $query : $this->Request->getAsArray()))
        {
            $this->doViewPostProcessing($View);
            $this->getResponse()->setContent($View);
        }
    }
    // End Forward

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns an url to an action controller
     *
     * @param  string $actionControllerName
     * @param  string $action
     * @param  array $query
     * @return string
     */
    public function getUrlTo($actionControllerName = null, $action = null, array $query = null)
    {
        if(!$actionControllerName)
            $actionControllerName = $this->getActionControllerName();

        return $this->getRouter()->getUrlTo($actionControllerName, $action, $query);
    }
    // End getUrlTo

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the current ActionController
     *
     * @param  -
     * @return ActionController
     */
    public function getActionController()
    {
        return $this->ActionController;
    }
    // End getActionController

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the current ActionController name
     *
     * @param  -
     * @return string
     */
    public function getActionControllerName()
    {
        return get_class($this->ActionController);
    }
    // End getActionController

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the current module
     *
     * @todo: should it be $this->ActionController->getModule()?
     *
     * @return string
     */
    public function getModule()
    {
        return $this->module;
    }
    // End getModule

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the current action
     *
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }
    // End getAction

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the Request object
     *
     * @param  -
     * @return Request
     */
    public function getRequest()
    {
        return $this->Request;
    }
    // End getRequest

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the Response object
     *
     * @param  -
     * @return Response
     */
    public function getResponse()
    {
        return $this->Response;
    }
    // End getResponse

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the Config
     *
     * @param  -
     * @return Config
     */
    public function getConfig()
    {
        return $this->getEnvironment()->getConfig();
    }
    // End getConfig

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the Session
     *
     * @param  -
     * @return Session
     */
    public function getSession()
    {
        return $this->getEnvironment()->getSession();
    }
    // End getSession

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the Environment
     *
     * @param  -
     * @return Environment
     */
    public function getEnvironment()
    {
        return $this->Environment;
    }
    // End getEnvironment

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the default module
     *
     * @param  -
     * @return string
     */
    public function getDefaultModule()
    {
        $Config = $this->Environment->getConfig();

        if(!isset($Config->mvc->defaultModule))
            return null;

        return $Config->mvc->defaultModule;
    }
    // End getDefaultModule

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the name of the default action controller
     *
     * @param  string $module
     * @return string
     */
    public function getDefaultActionControllerName($module = null)
    {
        $Config = $this->Environment->getConfig();

        if(!$module)
            $module = $this->getDefaultModule();

        if(isset($Config->$module) && isset($Config->$module->mvc->defaultActionController))
		{
            $defaultControllerName = $Config->$module->mvc->defaultActionController;
		}
        else
        {
            if(!isset($Config->mvc) || !isset($Config->mvc->defaultActionController))
                throw new Exception('No defaultActionController in config defined');

            $defaultControllerName = $Config->mvc->defaultActionController;
        }

        return $defaultControllerName;
    }
    // End getDefaultActionControllerName

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Registers a postProcessingPlugin
     *
     * @param ViewPostProcessingPlugin $Plugin - Das Plugin
     * @return -
     */
    public function registerViewPostProcessingPlugin(ViewPostProcessingPlugin $Plugin, $prepend = false)
    {
        if($prepend)
            array_unshift($this->viewPostProcessingPlugins, $Plugin);

        else
            $this->viewPostProcessingPlugins[] = $Plugin;
    }
    // End registerViewPostProcessingPlugin

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Registers a prefilter
     *
     * @param   Filter $Filter
     */
    public function registerPreFilter(Filter $Filter)
    {
        $this->preFilterChain[] = $Filter;
    }
    // End registerPreFilter

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Registers a postfilter
     *
     * @param   Filter $Filter
     */
    public function registerPostFilter(Filter $Filter)
    {
        $this->postFilterChain[] = $Filter;
    }
    // End registerPostFilter

    //////////////////////////////////////////////////////////////////////////////////////
    // protected
    //////////////////////////////////////////////////////////////////////////////////////


    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the invoked action controller and returns the controller instance
     *
     * @param  string $ctrlName
     * @return ActionController
     */
    protected function setActionController($ctrlName)
    {
        try
        {
            if(!$ctrlName)
                return $this->ActionController = $this->getDefaultActionController();

            /**
             * Autoloader gets triggerd by class_exists and throws an exception
             * if class not exists
             */
            if(class_exists($ctrlName))
                $this->ActionController = new $ctrlName;
        }
        catch (Exception $Exception)
        {
            /**
             * Was:
             *   Redirect to default page
             * Now:
             *   Simply throw an exception
             * We need to have a better mechanism here.
             */
           throw $Exception;
        }

        return $this->ActionController;
    }
    // End setActionController

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the invoked action
     *
     * @param  string $action
     * @return string
     */
    protected function setAction($action)
    {
        return $this->action = $action;
    }
    // End setAction

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the invoked module
     *
     * @param  string module
     * @return string
     */
    protected function setModule($module)
    {
        return $this->module = $module? $module : $this->getDefaultModule();
    }
    // End setModule

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the Router
     *
     * @param  -
     * @return Router
     */
    protected function getRouter()
    {
        if($this->Router instanceOf Router)
            return $this->Router;

        /**
         * Resolve router from config
         */
        $Config = $this->Environment->getConfig();
        $Request = $this->getRequest();

        $routerName = isset($Config->mvc->router)? $Config->mvc->router : self::DEFAULT_ROUTER;

		return $this->Router = new $routerName($this);
    }
    // End getRouter

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the default action controller
     *
     * @param  -
     * @return ActionController
     */
    protected function getDefaultActionController()
    {
        if(!$ctrlName = $this->getDefaultActionControllerName($this->getModule()))
            throw new Exception('No default ActionController defined');

        if(!class_exists($ctrlName))
            return null;

        return new $ctrlName();
    }
    // End getDefaultActionController

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Hands the given view off to the registered view post processing plugins
     *
     * @todo: bad placement, rethink!
     */
    protected function doViewPostProcessing(Viewable $View)
    {
        foreach($this->viewPostProcessingPlugins as $Plugin)
            $Plugin->doViewPostProcessing($View);
    }
    // End doViewPostProcessing

    //////////////////////////////////////////////////////////////////////////////////////
}
// End FrontController
