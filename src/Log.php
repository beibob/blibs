<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs;

use Beibob\Blibs\Interfaces\LogAdapter;
use Beibob\Blibs\Interfaces\Logger;

/**
 * Log front end
 *
 * @package blibs
 * @author Tobias Lode <tobias@beibob.de>
 *
 */
class Log implements Logger
{
    /**
     * Mapping of LOG::* constants to printable text
     */
    private static $severityMap = [LOG_DBG   => 'debug',
                                        LOG_NOTE  => 'notice',
                                        LOG_WARN  => 'warning',
                                        LOG_ERROR => 'error',
                                        LOG_FATAL => 'fatal'
                                        ];

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Named instances
     */
    private static $instances = [];

    /**
     * Default instance name
     */
    private static $defaultInstance;

    /**
     * Name
     */
    private $name;

    /**
     * Log adapter
     */
    private $adapter;

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Registers an loggin adapter
     *
     * @param  string $name
     * @param  Config $AdapterConfig
     * @return -
     */
    public static function registerAdapter($name, LogAdapter $Adapter)
    {
        if(!count(self::$instances))
            self::$defaultInstance = $name;

        self::$instances[$name] = new Log($name, $Adapter);
    }
    // End registerAdapter

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the default (the 1st) instance
     *
     * @param  string $name
     * @return Log
     */
    public static function getDefault()
    {
        if(!self::$defaultInstance || !isset(self::$instances[self::$defaultInstance]))
            return self::getInstance();

        return self::$instances[self::$defaultInstance];
    }
    // End getDefault

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns a Log instance for the named adapter. If name is omitted, the returned
     * instance will send log messages to all adapters!
     *
     * @param  string $name
     * @return Log
     */
    public static function getInstance($name = null)
    {
        if(!$name || !isset(self::$instances[$name]))
            self::$instances[$name] = new Log($name);

        return self::$instances[$name];
    }
    // End getInstance

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Constructor
     *
     * @param  string     $name
     * @param  LogAdapter $adapter
     * @return Log
     */
    private function __construct($name = null, LogAdapter $adapter = null)
    {
        $this->name    = $name;
        $this->adapter = $adapter;
    }
    // End __construct


    /**
     * Logs a debug message
     *
     * @param  string $message - message text
     * @param  string $label   - label
     * @return -
     */
    public function debug($message = '', $label = null)
    {
        $this->send(LOG_DBG, $message, $label);
    }
    // End debug

    ///////////////////////////////////////////////////////////////////////////

    /**
     * Logs a notice message
     *
     * @param  string $message   - message text
     * @param  string $label    - label name
     * @return -
     */
    public function notice($message = '', $label = null)
    {
        $this->send(LOG_NOTE, $message, $label);
    }
    // End notice

    ///////////////////////////////////////////////////////////////////////////

    /**
     * Logs a warning message
     *
     * @param  string $message   - message text
     * @param  string $label    - label name
     * @return -
     */
    public function warning($message = '', $label = null)
    {
        $this->send(LOG_WARN, $message, $label);
    }
    // End warning

    ///////////////////////////////////////////////////////////////////////////

    /**
     * Logs an error message
     *
     * @param  string $message   - message text
     * @param  string $label    - label name
     * @return -
     */
    public function error($message = '', $label = null)
    {
        $this->send(LOG_ERROR, $message, $label);
    }
    // End error

    ///////////////////////////////////////////////////////////////////////////

    /**
     * Logs a fatal message
     *
     * @param  string $message   - message text
     * @param  string $label    - label name
     * @return -
     */
    public function fatal($message = '', $label = null)
    {
        $this->send(LOG_FATAL, $message, $label);
    }
    // End fatal

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sends the message to the adapter
     *
     * @param  string $message   - message text
     * @param  string $label    - label name
     * @return -
     */
    public function send($severity, $message = '', $label = null)
    {
        if($this->name)
        {
            if($this->adapter instanceof LogAdapter)
                $this->adapter->log($severity, $message, $label);
        }
        else
        {
            foreach(self::$instances as $name => $Instance)
            {
                if($name)
                    $Instance->send($severity, $message, $label);
            }
        }
    }
    // End fatal

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns a printable severity string
     *
     * @param  int $severity
     * @return string
     */
    public static function getSeverityString($severity)
    {
        return self::$severityMap[$severity];
    }
    // End getSeverityString

    //////////////////////////////////////////////////////////////////////////////////////
    // private
    //////////////////////////////////////////////////////////////////////////////////////


    //////////////////////////////////////////////////////////////////////////////////////
}
// End Log
