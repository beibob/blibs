<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs;

/**
 * A class representing a date
 *
 * @package blibs
 * @author Fabian Möller  <fab@beibob.de>
 * @author Tobias Lode <tobias@beibob.de>
 *
 */
class BlibsDate
{
    /**
     * Year
     */
    protected $year = null;

    /**
     * Month
     */
    protected $month = null;

    /**
     * Day
     */
    protected $day = null;

    /**
     * Language for textual representation of day'n'month
     */
    protected $language = null;

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Days per month in non leap years
     */
    protected static $daysPerMonth = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

    /**
     * Weekday mappings
     */
    protected static $weekdayMap = ['de' => [1 => 'Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag', 'Sonntag'],
                                         'en' => [1 => 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']
                                         ];

    /**
     * Month mappings
     */
    protected static  $monthMap = ['de' => [1 => 'Januar' ,'Februar', 'M�rz', 'April', 'Mai', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember'],
                                        'en' => [1 => 'January' ,'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
                                        ];

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * full numeric representation of a year, 4 digit
     */
    const FORMAT_YEAR = 'Y';

    /**
     * a 2 digit representation of a year
     */
    const FORMAT_YEAR_SMALL = 'y';

    /**
     * Numeric representation of a month, with leading zeros, 01 - 12
     */
    const FORMAT_MONTH = 'm';

    /**
     * Numeric representation of a month, 2 digits without leading zeros, 1 - 12
     */
    const FORMAT_MONTH_INT = 'n';

    /**
     * month, textual 3 letters
     */
    const FORMAT_MONTH_TEXT = 'M';

    /**
     * month, full textual representation
     */
    const FORMAT_MONTH_FULLTEXT = 'F';

    /**
     * day of the month, 2 digits with leading zeros 01 - 31
     */
    const FORMAT_DAY = 'd';

    /**
     * ISO-8601 numeric representation of the day of the week (added in PHP 5.1.0)
     * 1 (for Monday) through 7 (for Sunday)
     */
    const FORMAT_WEEKDAY_ISO8601 = 'N';

    /**
     * numeric representation of the day of the week
     * 0 (for Sunday) through 6 (for Saturday)
     */
    const FORMAT_WEEKDAY = 'w';

    /**
     * day of the month, 2 digits without leading zeros 1 - 31
     */
    const FORMAT_DAY_INT = 'j';

    /**
     * day of the month, full textual representation
     */
    const FORMAT_DAY_FULLTEXT = 'l';

    /**
     * day of the month, 3 letters short
     */
    const FORMAT_DAY_TEXT = 'D';

    /**
     * ISO date string format
     */
    const FORMAT_ISO_DATE = 'Y-m-d';

    /**
     * Unix timestamp - seconds since the Unix Epoch
     */
    const FORMAT_UNIXTIME = 'U';

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Date Factory
     *
     * @param  mixed   $date   - may either a Date object, a date string or a unix timestamp
     * @param  string  $format - [only if $date is a date string]
     * @param  boolean $strict - dates must not be incomplete (without day or day and month specified)
     * @return BlibsDate
     */
    public static function factory($date = false, $format = false, $strict = false)
    {
        return new BlibsDate($date, $format, $strict);
    }
    // End factory

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Constructs the object
     *
     * @param  mixed   $date   - may either a Date object, a date string or a unix timestamp
     * @param  string  $format - [only if $date is a date string]
     * @param  boolean $strict - dates must not be incomplete (without day or day and month specified)
     * @return object
     */
    public function __construct($date = false, $format = false, $strict = false)
    {
        if(is_string($date))
            $this->setDateString($date, $format, $strict);

        elseif(is_array($date))
            $this->setDateArray($date, $strict);

        elseif(is_object($date))
            $this->setDate($date, $strict);

        elseif(is_numeric($date))
            $this->setTimestamp($date);

        elseif($date === false)
            $this->setTimestamp();

        else
            $this->setInvalid();
    }
    // End __construct

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the current iso (or format specific) date
     *
     * @param  string $format
     * @return string
     */
    public static function today($format = false)
    {
        $Today = new BlibsDate(false, $format);
        return $Today->getDateString($format);
    }
    // End today

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Inits the object by todays date
     *
     * @param  -
     * @return boolean
     */
    public function setToday()
    {
        return $this->setTimestamp();
    }
    // End setToday

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the language for textual representation of day'n'month
     *
     * @param  string $language
     * @return -
     */
    public function setLanguage($language)
    {
        $this->language = $language;
    }
    // End setLanguage

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Inits the object by another Date object
     *
     * @param  BlibsDate $Date
     * @param  boolean $strict - dates must not be incomplete (without day or day and month specified)
     * @return boolean
     */
    public function setDate(BlibsDate $Date, $strict = false)
    {
        if(!$Date->isValid())
            return false;

        return $this->setDateParts($Date->getYear(), $Date->getMonth(), $Date->getDay(), $strict);
    }
    // End initByDate

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Inits the object by a date string. the format string expects
     * the format conventions (see FORMAT_* constants above)
     *
     * Order of placeholders does not matter
     *
     * @param  string $dateStr
     * @param  string $format
     * @param  boolean $strict - dates must not be incomplete (without day or day and month specified)
     * @return boolean
     */
    public function setDateString($dateStr, $format = false, $strict = false)
    {
        if(!$format)
            return $this->setISODateString($dateStr, $strict);

        if(empty($dateStr))
            return false;

        list($year, $month, $day) = $this->parseDateString($dateStr, $format);

        return $this->setDateParts($year, $month, $day, $strict);
    }
    // End initByDateString

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets date by an array. The array must contain the keys year, month and/or day
     *
     * @param  array $dataArray
     * @param  boolean $strict - dates must not be incomplete (without day or day and month specified)
     * @return boolean
     */
    public function setDateArray(array $dateArray, $strict = false)
    {
        /**
         * DateArray must contain at least a year or year and month or year, month and day!
         * or the ones that are specified in $format
         */
        if(!$this->checkValidDateArray($dateArray, $strict))
            return false;

        return $this->setDateParts($dateArray['year'], $dateArray['month'], $dateArray['day'], $strict);
    }
    // End setDateArray

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Inits the object by a iso date string format YYYY-MM-DD
     *
     * @param  string $dateStr
     * @param  boolean $strict - dates must not be incomplete (without day or day and month specified)
     * @return boolean
     */
    public function setISODateString($dateStr, $strict = false)
    {
        if(empty($dateStr))
            return false;

        list($year, $month, $day) = $this->parseISODateString($dateStr);

        return $this->setDateParts($year, $month, $day, $strict = false);
    }
    // End initByISODateString

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Inits the object by a unix timestamp
     *
     * @param  integer $timestamp - optional, if false time() is used
     * @return boolean
     */
    public function setTimestamp($timestamp = false)
    {
        if($timestamp === false)
            $dateArr = getdate();
        else
            $dateArr = getdate($timestamp);

        return $this->setDateParts($dateArr['year'], $dateArr['mon'], $dateArr['mday']);
    }
    // End initByTimestamp

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the date parts of a date
     * Standard behaviour is to check the date parts. if any of them is invalid, the method
     * returns false without setting the date.
     *
     * @param  mixed $year
     * @param  mixed $month
     * @param  mixed $day
     * @param  boolean $strict - dates must not be incomplete (without day or day and month specified)
     * @return boolean
     */
    public function setDateParts($year = false, $month = false, $day = false, $strict = false)
    {
        if(!$this->checkDateParts($year, $month, $day, $strict))
        {
            $this->setInvalid();
            return false;
        }

        $this->year  = $year > 0 || is_numeric($year)? (int)$year : (int)date('Y');
        $this->month = $month > 0 && $month < 13? (int)$month : 1;
        $this->day   = $day > 0 && $day <= $this->getLastDayOfMonth()? (int)$day : 1;

        return true;
    }
    // End setDateParts

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Unsets the date object. This makes it invalid
     *
     * @param  -
     * @return -
     */
    public function setInvalid()
    {
        $this->year  = null;
        $this->month = null;
        $this->day   = null;
    }
    // End setInvalid

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Overwrites the php-method __toString for converting an object to string
     *
     * @param  --
     * @return string
     */
    public function __toString()
    {
        return (string)$this->getISODateString();
    }
    // End __toString

    //////////////////////////////////////////////////////////////////////////////////////

   /**
     * Returns a formated date string
     *
     * @param  string $format
     * @return string
     */
    public function getDateString($format = false, $language = null)
    {
        if(!$this->isValid())
            return '';

        if(!$format)
            return $this->getISODateString();

        if($language)
            $this->setLanguage($language);

        $substitute = [self::FORMAT_YEAR            => $this->getYear(self::FORMAT_YEAR),
                            self::FORMAT_YEAR_SMALL      => $this->getYear(self::FORMAT_YEAR_SMALL),
                            self::FORMAT_MONTH           => $this->getMonth(self::FORMAT_MONTH),
                            self::FORMAT_MONTH_INT       => $this->getMonth(self::FORMAT_MONTH_INT),
                            self::FORMAT_MONTH_TEXT      => $this->getMonth(self::FORMAT_MONTH_TEXT),
                            self::FORMAT_MONTH_FULLTEXT  => $this->getMonth(self::FORMAT_MONTH_FULLTEXT),
                            self::FORMAT_DAY             => $this->getDay(self::FORMAT_DAY),
                            self::FORMAT_DAY_INT         => $this->getDay(self::FORMAT_DAY_INT),
                            self::FORMAT_DAY_TEXT        => $this->getDay(self::FORMAT_DAY_TEXT),
                            self::FORMAT_DAY_FULLTEXT    => $this->getDay(self::FORMAT_DAY_FULLTEXT),
                            self::FORMAT_WEEKDAY_ISO8601 => $this->getDay(self::FORMAT_WEEKDAY_ISO8601),
                            self::FORMAT_WEEKDAY         => $this->getDay(self::FORMAT_WEEKDAY)
                           ];

        return strtr($format, $substitute);
    }
    // End getDateString

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns a iso formated date string
     *
     * @param  -
     * @return string
     */
    public function getISODateString()
    {
        if(!$this->isValid())
            return false;

        return $this->getYear()."-".$this->getMonth()."-".$this->getDay();
    }
    // End getISODateString

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns a unix timestamp if possible
     *
     * @param  -
     * @return int/false
     */
    public function getUnixtime()
    {
        if(!$this->isValid())
            return false;

        if (!is_numeric($this->getMonth()) || !is_numeric($this->getDay()) || !is_numeric($this->getYear()))
            return false;

        return mktime(0, 0, 0, $this->getMonth(), $this->getDay(), $this->getYear());
    }
    // End getUnixtime

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns a date array
     *
     * @param  -
     * @return array
     */
    public function getDateArray()
    {
        if(!$this->isValid())
            return false;

        return ['year'  => $this->getYear(),
                     'month' => $this->getMonth(),
                     'day'   => $this->getDay()];
    }
    // End getDateArray

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the year part of the date, if either format is empty or
     * contains a year format specifier
     *
     * @param  string $format
     * @return string
     */
    public function getYear($format = false)
    {
        if(!$this->isValid())
            return false;

        if($format === false || strpos($format, 'Y') !== false)
        {
            $year = $this->year;
            $digits = 4;
        }
        elseif(strpos($format, 'y') !== false)
        {
            $year = $this->year % 100;
            $digits = 2;
        }
        else
        {
            return false;
        }

        return str_pad($year, $digits, '0', STR_PAD_LEFT);
    }
    // End getYear

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the month part of the date, if either format is empty or contains a month format specifier
     *
     * @param  string $format - output format
     * @return mixed
     */
    public function getMonth($format = false)
    {
        if(!$this->isValid())
            return false;

        if($format === false || strpos($format, self::FORMAT_MONTH) !== false)
        {
            $month = str_pad($this->month, 2, '0', STR_PAD_LEFT);
        }
        elseif(strpos($format, self::FORMAT_MONTH_INT) !== false)
        {
            $month = $this->month;
        }
        elseif(strpos($format, self::FORMAT_MONTH_FULLTEXT) !== false)
        {
            $language = $this->language? $this->language : 'de';
            $month = $this->getMonthString($language);
        }
        elseif(strpos($format, self::FORMAT_MONTH_TEXT) !== false)
        {
            $language = $this->language? $this->language : 'de';
            $month = utf8_substr($this->getMonthString($language), 0, 3);
        }
        else
        {
            $month = false;
        }

        return $month;
    }
    // End getMonth

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the day part of the date, if either format is empty or
     * contains a day format specifier
     *
     * @param  string $format
     * @return mixed
     */
    public function getDay($format = false)
    {
        if(!$this->isValid())
            return false;

        if($format === false || strpos($format, self::FORMAT_DAY) !== false)
        {
            $day = str_pad($this->day, 2, '0', STR_PAD_LEFT);
        }
        elseif(strpos($format, self::FORMAT_DAY_INT) !== false)
        {
            $day = $this->day;
        }
        elseif(strpos($format, self::FORMAT_DAY_FULLTEXT) !== false)
        {
            $language = $this->language? $this->language : 'de';
            $weekdayMapping = self::getWeekdayMapping($language);
            $day = $weekdayMapping[date('N', $this->getUnixtime())];
        }
        elseif(strpos($format, self::FORMAT_DAY_TEXT) !== false)
        {
            $language = $this->language? $this->language : 'de';
            $weekdayMapping = self::getWeekdayMapping($language);
            $day = utf8_substr($weekdayMapping[date(self::FORMAT_WEEKDAY_ISO8601, $this->getUnixtime())], 0, 3);
        }
        elseif(strpos($format, self::FORMAT_WEEKDAY_ISO8601) !== false)
            $day = date(self::FORMAT_WEEKDAY_ISO8601, $this->getUnixtime());
        elseif(strpos($format, self::FORMAT_WEEKDAY) !== false)
            $day = date(self::FORMAT_WEEKDAY, $this->getUnixtime());
        else
        {
            $day = false;
        }

        return $day;
    }
    // End getDay

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the last day of the date's month
     *
     * @param  -
     * @return integer
     */
    public function getLastDayOfMonth($month = false, $year = false)
    {
        $month = $month? $month : $this->month;

        $days = self::$daysPerMonth[$month - 1];

        /**
         * check for february and leap year
         */
        if($month == 2 && $this->isLeapYear($year))
            $days++;

        return $days;
    }
    // End getLastDayOfMonth

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns true if the date is today
     *
     * @param  -
     * @return boolean
     */
    public function isToday()
    {
        $DateToday = new BlibsDate();
        return $DateToday->getDateArray() == $this->getDateArray();
    }
    // End isToday

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns true if the date's year is a leap year
     *
     * @param  -
     * @return boolean
     */
    public function isLeapYear($year = false)
    {
        $year = $year? $year : $this->year;
        return $year % 4 == 0;
    }
    // End isLeapYear

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Checks if the Date object represents a valid date
     *
     * @param  -
     * @return boolean
     */
    public function isValid()
    {
        return !(is_null($this->day) && is_null($this->month) && is_null($this->year));
    }
    // End isValid

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Static method to verify date parts
     *
     * @param  int $year
     * @param  int $month
     * @param  int $day
     * @param  boolean $strict - dates must not be incomplete (without day or day and month specified)
     * @return boolean
     */
    public function checkDateParts($year = false, $month = false, $day = false, $strict = false)
    {
        if(!$year && !$month && !$day)
            return false;

        if(($strict || $year) && utf8_strlen($year) > 4)
            return false;

        if(($strict || $month) && ($month < 1 || $month > 12))
            return false;

        if(($strict || $day) && ($day < 1 || $day > 31))
            return false;

        if(($strict || ($day && $month)) && ($day > $this->getLastDayOfMonth($month, $year)))
            return false;

        return true;
    }
    // End checkDate

    ///////////////////////////////////////////////////////////////////////////

    /**
     * returns the month as string
     */
    public function getMonthString($language = "de")
    {
        $monthNumber = (int) $this->getMonth('n');
        $monthMapping = self::getMonthMapping($language);

        return $monthMapping[$monthNumber];
    }
    // End getMonthString

    ///////////////////////////////////////////////////////////////////////////

    /**
     * returns the weekNumber of the first week in the month
     */
    public function getFirstWeekOfMonth()
    {
        return date('W', mktime(0,0,0,$this->getMonth(),1,$this->getYear()));
    }
    // End getFirstWeekOfMonth

    ///////////////////////////////////////////////////////////////////////////

    /**
     * returns the weekNumber of the last week in the month
     */
    public function getLastWeekOfMonth()
    {
        return date('W', mktime(0,0,0,$this->getMonth(),$this->getLastDayOfMonth(),$this->getYear()));
    }
    // End getLastWeekOfMonth

    ///////////////////////////////////////////////////////////////////////////

    /**
     * returns the ISO-8601 weekday-mapping for a given language
     */
    public static function getWeekdayMapping($language = false)
    {
        if(!$language)
            return self::$weekdayMap;

        return isset(self::$weekdayMap[$language])? self::$weekdayMap[$language] : false;
    }
    // End getWeekdayMapping

    ///////////////////////////////////////////////////////////////////////////

    /**
     * returns the month-mapping for a given language
     */
    public static function getMonthMapping($language = false)
    {
        if(!$language)
            return self::$monthMap;

        return isset(self::$monthMap[$language])? self::$monthMap[$language] : false;
    }
    // End getMonthMapping

    //////////////////////////////////////////////////////////////////////////////////////
    // static
    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * returns a Date-Object created by a given week (optional with day and year)
     * day from 1 - 7 according to ISO08601
     *
     * @param  int $weekNumber
     * @param  int $day
     * @param  int $year
     * @return BlibsDateTime
     */
    public static function getDateByWeekNumber($weekNumber, $day = 1, $year = false)
    {
        if($year === false)
            $year = date('Y');

        // count from '0104' because January 4th is always in week 1 (according to ISO 8601).
        $time = strtotime($year . '0104 +' . ($weekNumber - 1) . ' weeks');

        // get the time of the first day of the week
        $mondayTime = strtotime('-' . (date('N', $time) - 1) . ' days', $time);

        // get the time of the given day
        $day = strtotime('+' . $day - 1 . 'days', $mondayTime);

        return new BlibsDate($day);
    }
    // End getDateByWeekNumber

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * checks if the given weekNumber is within the given month'n'year
     *
     * @param  int $weekNumber
     * @param  int $month
     * @param  int $year
     * @return bool
     */
    public static function weekIsInMonth($weekNumber, $month, $monthYear)
    {
        // check if year of month equals year of week
        $weekYear = self::getWeekYear($weekNumber, $month, $monthYear);

        // we need to check the first and the last day of the week,
        // as a week could belong to two months
        $DateFirstDayOfWeek = self::getDateByWeekNumber($weekNumber, 1, $weekYear);
        $DateLastDayOfWeek = self::getDateByWeekNumber($weekNumber, 7, $weekYear);

        return $DateFirstDayOfWeek->getMonth() === $month || $DateLastDayOfWeek->getMonth() === $month;
    }
    // End weekIsInMonth

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * returns the year of the week is within
     * this could differ from the year in wich the month is in (when the year changes)
     *
     * @param  int $weekNumber
     * @param  int $month
     * @param  int $monthYear
     * @return int
     */
    public static function getWeekYear($weekNumber, $month, $monthYear)
    {
        $Date = new BlibsDate(['year' => $monthYear, 'month' => $month, 'day' => 1]);
        $firstWeekOfMonth = $Date->getFirstWeekOfMonth();

        return ($weekNumber < $firstWeekOfMonth)? $monthYear + 1 : $monthYear;
    }
    // End getWeekYear

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * return the next weekNumber to the given one
     *
     * @param  int $weekNumber
     * @param  int $month
     * @param  int $year
     * @return int
     */
    public static function getNextWeek($weekNumber, $month, $monthYear)
    {
        // get the weekYear (sometimes year of month doesn't equal year of week)
        $weekYear = self::getWeekYear($weekNumber, $month, $monthYear);

        $Date = self::getDateByWeekNumber($weekNumber, 1, $weekYear);

        return date('W', strtotime('next Week', $Date->getUnixtime()));
    }
    // End getNextWeek

    //////////////////////////////////////////////////////////////////////////////////////
    // protected
    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Parses a date string by a given format
     *
     * not really perfomant
     *
     * @param  string $datestr
     * @param  string $format
     * @return array
     */
    protected function parseDateString($dateStr, $format)
    {
        $found   = [];
        $pattern = '';

        for($position = 0; $position < utf8_strlen($format); $position++)
        {
            switch($format{$position})
            {
                case self::FORMAT_YEAR:
                    $found[]  = self::FORMAT_YEAR;
                    $pattern .= '(\d{4})';
                    break;

                case self::FORMAT_YEAR_SMALL:
                    $found[]  = self::FORMAT_YEAR_SMALL;
                    $pattern .= '(\d{2})';
                    break;

                case self::FORMAT_MONTH:
                    $found[]  = self::FORMAT_MONTH;
                    $pattern .= '(\d{2})';
                    break;

                case self::FORMAT_MONTH_INT:
                    $found[]  = self::FORMAT_MONTH_INT;
                    $pattern .= '(\d{1,2})';
                    break;

                case self::FORMAT_MONTH_FULLTEXT:
                    $found[]  = self::FORMAT_MONTH_FULLTEXT;
                    $pattern .= '(\w+?)';
                    break;

                case self::FORMAT_MONTH_TEXT:
                    $found[]  = self::FORMAT_MONTH_TEXT;
                    $pattern .= '(\w{3})';
                    break;

                case self::FORMAT_DAY:
                    $found[]  = self::FORMAT_DAY;
                    $pattern .= '(\d{2})';
                    break;

                case self::FORMAT_DAY_INT:
                    $found[]  = self::FORMAT_DAY_INT;
                    $pattern .= '(\d{1,2})';
                    break;

                case self::FORMAT_DAY_TEXT:
                    $pattern .= '\w{3}';
                    break;

                case self::FORMAT_DAY_FULLTEXT:
                    $pattern .= '\w+?';
                    break;

                case self::FORMAT_UNIXTIME:
                    $found[]  = self::FORMAT_UNIXTIME;
                    $pattern .= '(\d+)';
                    break;

                case '.':
                case '/':
                case '(':
                case ')':
                    $pattern .= '\\'.$format{$position};
                    break;

                default:
                    $pattern .= $format{$position};
                    break;
            }
        }

        if(preg_match("/".$pattern."/u", $dateStr, $matches))
        {
            foreach($found as $index => $format)
            {
                switch($format)
                {
                    case self::FORMAT_YEAR:
                        $year = $matches[$index + 1];
                        break;

                    case self::FORMAT_YEAR_SMALL:
                        $year = $matches[$index + 1] < 50? 2000 + $matches[$index + 1] : 1900 + $matches[$index + 1];
                        break;

                    case self::FORMAT_MONTH:
                    case self::FORMAT_MONTH_INT:
                        $month = $matches[$index + 1];
                        break;

                    case self::FORMAT_MONTH_FULLTEXT:
                        $monthStr = $matches[$index + 1];
                        $monthMap = self::getMonthMapping();

                        $month = false;
                        foreach($monthMap as $lang => $mapping)
                            if($month = array_search($monthStr, $mapping) !== false)
                            {
                                $this->setLanguage($lang);
                                break;
                            }

                        break;

                    case self::FORMAT_MONTH_TEXT:
                        $monthStr = $matches[$index + 1];
                        $monthMap = self::getMonthMapping();

                        $month = false;
                        foreach($monthMap as $lang => $mapping)
                        {
                            foreach($mapping as $no => $fullMonthStr)
                            {
                                if($monthStr == utf8_substr($fullMonthStr, 0, 3))
                                {
                                    $month = $no;
                                    $this->setLanguage($lang);
                                    break;
                                }
                            }
                        }
                        break;

                    case self::FORMAT_DAY:
                    case self::FORMAT_DAY_INT:
                        $day = $matches[$index + 1];
                        break;

                    case self::FORMAT_UNIXTIME:
                        $dt = getDate($matches[$index + 1]);

                        $day   = $dt['mday'];
                        $month = $dt['mon'];
                        $year  = $dt['year'];
                }
            }
        }

        $dateArr[] = is_numeric($year)?  (int)$year  : null;
        $dateArr[] = is_numeric($month)? (int)$month : null;
        $dateArr[] = is_numeric($day)?   (int)$day   : null;

        return $dateArr;
    }
    // End parseDateString

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Parses a iso date string format
     *
     * @param  string $datestr
     * @return array
     */
    protected function parseISODateString($dateStr)
    {
        $year = $month = $day = null;

        if(preg_match('/^(\d{4})-(\d{2})-(\d{2})/u', $dateStr, $dateArr))
        {
            $year  = $dateArr[1];
            $month = $dateArr[2];
            $day   = $dateArr[3];
        }

        return [$year, $month, $day];
    }
    // End parseISODateString

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Checks if the date array is valid. If strict is set to true, all date keys has to be set.
     * Otherwise at least a year must be specified
     *
     * @param  array  $dateArray
     * @param  boolean $strict - if strict is set to true, all date keys has to be
     * @return boolean
     */
    protected function checkValidDateArray($dateArray, $strict = false)
    {
        if($strict && !($dateArray['year'] && $dateArray['month'] && $dateArray['day']))
            return false;

        if(empty($dateArray['year']) ||
           (!empty($dateArray['year']) && empty($dateArray['month']) && !empty($dateArray['day'])))
            return false;

        return true;
    }
    // End checkValidDateArray
}
// End class BlibsDate
