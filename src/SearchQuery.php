<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs;

/**
 * SearchQuery
 *
 * @package blibs
 * @author Fabian Möller  <fab@beibob.de>
 * @author Tobias Lode <tobias@beibob.de>
 *
 */
class SearchQuery implements Iterator
{
    /**
     * Search modes
     */
    const SEARCH_MODE_ALL_WORDS    = 0;
    const SEARCH_MODE_ANY_WORDS    = 1;
    const SEARCH_MODE_EXACT_PHRASE = 2;

    /**
     * Search literals
     */
    const LIT_WILDCARD_MULTI  = '*';
    const LIT_WILDCARD_SINGLE = '?';
    const LIT_MUST         = '+';
    const LIT_MUST_NOT     = '-';
    const LIT_CAN          = ' ';

    /**
     * Internal used constants
     */
    const LIT_END_OF_QUERY = '$';

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * SearchMode
     */
    private $searchMode;

    /**
     * Permit wildcards in query
     */
    private $permitWildcards;

    /**
     * Original query
     */
    private $origQuery;

    /**
     * query with search mode applied
     */
    private $query;

    /**
     * Tokens
     */
    private $tokens = [];

    /**
     * Registered stemmers
     */
    private static $stemmers = [];

    /**
     * mapping between literals and operators
     */
    private static $litOpMap = [self::LIT_MUST     => SearchToken::OP_MUST,
                                     self::LIT_MUST_NOT => SearchToken::OP_MUST_NOT,
                                     self::LIT_CAN      => SearchToken::OP_CAN
                                     ];

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Constructs a search query
     *
     * @param  string  $query
     * @param  integer $searchMode
     * @return -
     */
    public function __construct($query = false, $searchMode = self::SEARCH_MODE_ALL_WORDS, $permitWildcards = false)
    {
        $this->set($query, $searchMode, $permitWildcards);
    }
    // End __construct

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets a new query string
     *
     * @param  string $query
     * @param  integer $searchMode
     * @return -
     */
    public function set($query, $searchMode = self::SEARCH_MODE_ALL_WORDS, $permitWildcards = false)
    {
        $this->origQuery       = $query;
        $this->searchMode      = $searchMode;
        $this->permitWildcards = $permitWildcards;
        $this->query           = self::applySearchMode($searchMode, $query);
        $this->tokens          = $this->parseQuery($this->query);
    }
    // End set

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns true if the query is valid
     *
     * @param  -
     * @return boolean
     */
    public function isValid()
    {
        return count($this->tokens) > 0;
    }
    // End isValid

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns a list of SearchToken objects
     *
     * @param  -
     * @return array
     */
    public function getSearchTokens()
    {
        return $this->tokens;
    }
    // End getTokens

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the query (with applied search mode)
     *
     * @param  -
     * @return string
     */
    public function getQuery()
    {
        return $this->query;
    }
    // End getQuery

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the original query
     *
     * @param  -
     * @return string
     */
    public function getOrigQuery()
    {
        return $this->origQuery;
    }
    // End getOrigQuery

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Registers a stemmer
     *
     * @param  Stemmer $Stemmer
     * @return -
     */
    public static function registerStemmer(Stemmer $Stemmer)
    {
        self::$stemmers[] = $Stemmer;
    }
    // End register Stemmer

    //////////////////////////////////////////////////////////////////////////////////////
    // iterator interface
    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Resets the token list to the beginning
     *
     * @param  -
     * @return -
     */
    public function rewind()
    {
        if(!is_array($this->tokens))
            return;

        reset($this->tokens);
    }
    // End rewind

    ///////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the current token
     *
     * @param  -
     * @return SearchToken
     */
    public function current()
    {
        if(!is_array($this->tokens))
            return false;

        return current($this->tokens);
    }
    // End current

    ///////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the key value of the current token
     *
     * @param  -
     * @return mixed
     */
    public function key()
    {
        if(!is_array($this->tokens))
            return false;

        return key($this->tokens);
    }
    // End key

    ///////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the next token
     *
     * @param  -
     * @return SearchToken
     */
    public function next()
    {
        if(!is_array($this->tokens))
            return false;

        return next($this->tokens);
    }
    // End next

    ///////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns true if the current token is valid. False otherwise
     *
     * @param  -
     * @return boolean
     */
    public function valid()
    {
        return $this->current() !== false;
    }
    // End valid

    //////////////////////////////////////////////////////////////////////////////////////
    // private
    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Apply the given searchMode
     *
     * @param  string $searchMode
     * @param  string $query
     * @return string
     */
    private static function applySearchMode($searchMode, $query)
    {
        switch($searchMode)
        {
            case self::SEARCH_MODE_EXACT_PHRASE:
                /**
                 * Remove all quotes and quote complete string
                 */
                $query = '"'. str_replace('"', '', $query) .'"';
                break;

            case self::SEARCH_MODE_ANY_WORDS:
            case self::SEARCH_MODE_ALL_WORDS:
            default:
                break;
        }

        return $query;
    }
    // End applySearchMode

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Splits the search string into tokens
     *
     * @param  string  $search
     * @return array
     */
    private function parseQuery($search)
    {
        if(!($search = utf8_strtolower(trim($search))))
            return [];

        $splitCharSet = 'a-zäüöß0-9-+"\'';

        if($this->permitWildcards)
            $splitCharSet .= self::LIT_WILDCARD_MULTI . self::LIT_WILDCARD_SINGLE;

        $search  = join(' ', preg_split("/[^".preg_quote($splitCharSet)."]+/u", $search, -1, PREG_SPLIT_NO_EMPTY));
        $search  = preg_replace('/(\w)[\-\+](\w)/um', '$1 $2', $search);
        $search .= self::LIT_END_OF_QUERY;

        $len = utf8_strlen($search);

        $tokens  = $stack = [];
        $current = $qtLit = $wc = $op = false;

        for($i = 0; $i < $len; $i++)
        {
            $c = $search{$i};

            switch($c)
            {
                case '"':
                case "'":
                    $qtLit    = ($qtLit == $c? false : $c);
                    $current .= ' ';
                    break;

                case self::LIT_MUST:
                case self::LIT_MUST_NOT:
                    if(!$qtLit)
                        $op = $c;
                    break;

                case self::LIT_END_OF_QUERY:
                case self::LIT_CAN:
                    if(!($current = trim($current)))
                        break;

                    /**
                     * Save all wildcard positions and discard them
                     */
                    $mPositions = $sPositions = [];

                    if(count(self::$stemmers))
                    {
                        if($this->permitWildcards)
                        {
                            $current = self::saveWildcardPositions($current, self::LIT_WILDCARD_MULTI,  $mPositions);
                            $current = self::saveWildcardPositions($current, self::LIT_WILDCARD_SINGLE, $sPositions);
                        }

                        foreach(self::$stemmers as $Stemmer)
                        {
                            if(($current = $Stemmer->stem($current)) === false)
                                break;
                        }

                        /**
                         * Restore all wildcard positions
                         */
                        if($this->permitWildcards)
                        {
                            $current = self::restoreWildcardPositions($current, self::LIT_WILDCARD_SINGLE, $sPositions);
                            $current = self::restoreWildcardPositions($current, self::LIT_WILDCARD_MULTI,  $mPositions);
                        }
                    }

                    if($current)
                    {
                        $stack[] = $current;

                        if(!$qtLit)
                        {
                            $op = ($op? $op : ($this->searchMode == self::SEARCH_MODE_ALL_WORDS? self::LIT_MUST : self::LIT_CAN));

                            $tokenString = join(' ', $stack);
                            $Token = new SearchToken($tokenString,
                                                     self::$litOpMap[$op],
                                                     count($stack) > 1,
                                                     $wc
                                                     );

                            $tokens[$Token->content] = $Token;
                        }
                    }

                    if($qtLit)
                    {
                        $current = '';
                    }
                    else
                    {
                        $current = $op = $wc = false;
                        $stack = [];
                    }
                    break;

                default:
                    if($this->permitWildcards && !$qtLit && ($c == self::LIT_WILDCARD_MULTI ||
                                                             $c == self::LIT_WILDCARD_SINGLE))
                        $wc = true;

                    $current .= $c;
                    break;
            }
        }

        /**
         * Unset all only-wildcard tokens
         */
        if($this->permitWildcards)
        {
            unset($tokens[self::LIT_WILDCARD_SINGLE]);
            unset($tokens[self::LIT_WILDCARD_MULTI]);
        }

        $valid = false;
        foreach($tokens as $Token)
        {
            if($Token->operator != SearchToken::OP_MUST_NOT)
            {
                $valid = true;
                break;
            }
        }

        if(!$valid)
            return [];

        return $tokens;
    }
    // End parseSearchString

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Save wildcard positions and returns a cleaned string
     *
     * @param  string $context
     * @param  string $wildCard
     * @param  &array $position
     * @return string
     */
    private static function saveWildcardPositions($context, $wildcard, &$positions)
    {
        if(!$context)
            return false;

        $pos = -1;

        while(($pos = strpos($context, $wildcard, $pos + 1)) !== false)
            $positions[] = $pos;

        return str_replace($wildcard, '', $context);
    }
    // End saveWildcardPositions

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Restored previously stored wildcard positions and returns the original string
     *
     * @param  string $context
     * @param  string $wildCard
     * @param  &array $position
     * @return string
     */
    private static function restoreWildcardPositions($context, $wildcard, &$positions)
    {
        if(!$context)
            return false;

        /**
         * Restore all ? positions
         */
        foreach($positions as $pos)
            $context = utf8_substr($context, 0, $pos) .$wildcard. utf8_substr($context, $pos);

        return $context;
    }
    // End restoreWildcardPositions

    //////////////////////////////////////////////////////////////////////////////////////
}
// End SearchQuery
