<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\Blibs;

use Beibob\Blibs\Interfaces\Response;

/**
 * Response implementation
 *
 * @package blibs
 * @author Tobias Lode <tobias@beibob.de>
 * @author Fabian Möller <fab@beibob.de>
 *
 */
class ResponseBase implements Response
{
    /**
     * content buffer
     */
    protected $contentBuffer;

    /**
     * Status
     */
    protected $status;

    /**
     * Catched exceptions during the processing
     */
    protected $exceptions = [];

    const SENDTYPE_ECHO = 0;
    const SENDTYPE_ARRAY = 1;

    protected $sendType = self::SENDTYPE_ECHO;

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns sendType
     *
     * @param  -
     * @return string
     */
    public function getSendType()
    {
        return $this->sendType;
    }
    // End getSendType

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets sendType
     *
     * @param  integer $sendType
     * @return -
     */
    public function setSendType($sendType = self::SENDTYPE_ECHO)
    {
        $this->sendType = $sendType;
    }
    // End setSendType

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns content
     *
     * @param  -
     * @return string
     */
    public function getContent()
    {
        return $this->contentBuffer;
    }
    // End getContent

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets content at once
     *
     * @param  string $content
     * @return -
     */
    public function setContent($content)
    {
        $this->contentBuffer = $content;
    }
    // End setContent

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Clears the content
     *
     * @param
     * @return -
     */
    public function unsetContent()
    {
        $this->contentBuffer = null;
    }
    // End unsetContent

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Appends content
     *
     * @param  string $content
     * @return -
     */
    public function appendContent($content)
    {
        $this->contentBuffer .= $content;
    }
    // End appendContent

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the status of the response
     *
     * @param  mixed $status
     * @return -
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }
    // End setStatus

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the status of the response
     *
     * @return  mixed
     */
    public function getStatus()
    {
        return $this->status;
    }
    // End getStatus

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Adds an exception to the response
     *
     * @param  Exception $Exception
     * @return -
     */
    public function addException(\Exception $Exception)
    {
        $this->exceptions[] = $Exception;
    }
    // End addException

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns true if an exception was catched
     *
     * @param  -
     * @return boolean
     */
    public function hasExceptions()
    {
        return (bool)count($this->exceptions);
    }
    // End hasExceptions

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns true if an exception of the given type (with error code) was catched
     *
     * @param  string $className
     * @param  int    $errCode
     * @return boolean
     */
    public function hasException($className, $errCode = null)
    {
        foreach($this->exceptions as $Exception)
        {
            if($Exception instanceOf $className)
            {
                if(is_null($errCode))
                    return true;

                return $Exception->getCode() === $errCode;
            }
        }

        return false;
    }
    // End hasExceptions

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the exception array
     *
     * @return Exception[]|array
     */
    public function getExceptions()
    {
        return $this->exceptions;
    }
    // End getExceptions

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sends the data to the client
     *
     * @param  -
     * @return mixed
     */
    public function send()
    {
        if ($this->getSendType() == self::SENDTYPE_ECHO)
        {
            /**
             * Output response
             */
            echo $this->contentBuffer;

            /**
             * Return status
             */
            return $this->status;
        }
        else
        {
            return ['contentBuffer' => $this->contentBuffer, 'status' => $this->status];
        }
    }
    // End send
}
// End ResponseBase
